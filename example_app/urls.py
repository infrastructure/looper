from django.urls import path

from .views import settings_home
from looper.views import checkout_stripe, settings

urlpatterns = [
    # The settings/ path prefix is shared between this module and looper.urls.
    path('settings/', settings_home, name='settings_home'),
    path('settings/receipts/', settings.settings_receipts, name='settings_receipts'),
    path('settings/receipts/<int:order_id>', settings.ReceiptView.as_view(),
         name='settings_receipt'),
    path('settings/receipts/example-app-<int:order_id>.pdf',
         settings.ReceiptPDFView.as_view(),
         name='settings_receipt_pdf'),
    path(
        'settings/billing/payment-methods/change/<int:subscription_id>',
        settings.PaymentMethodChangeView.as_view(
            success_url='payment_method_change_done',
            cancel_url='settings_home',
        ),
        name='payment_method_change',
    ),
    path(
        'settings/billing/payment-methods/change-done/<int:subscription_id>/<stripe_session_id>',
        # TODO? settings_home redirects to settings_membership_edit if there is just one membership
        # so this is good for 99%
        # as an improvement we could make success_url parameter a callable that does a proper
        # pk lookup and explicitly redirects to settings_membership_edit
        settings.PaymentMethodChangeDoneView.as_view(success_url='settings_home'),
        name='payment_method_change_done',
    ),
    path(
        'stripe-success-create-subscription/<stripe_session_id>',
        checkout_stripe.StripeSuccessCreateSubscriptionView.as_view(),
        name='stripe_success_create_subscription',
    ),
]
