from django.apps import AppConfig


class ExampleAppConfig(AppConfig):
    name = 'example_app'

    def ready(self) -> None:
        import example_app.signals  # noqa: F401
