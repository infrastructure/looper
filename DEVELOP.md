# General design principles

## Keep things simple

Since Looper deals with money the impact of bugs are larger than in most other
projects. Therefore, we place simplicity above all else, in order to limit the
number of bugs. 

## Defer complicated matters

Looper needs to be very flexible in order to accommodate all its use cases.
This is in direct conflict with priority number one: simplicity. We circumvent
this problem by not *directly* incorporating these use cases into Looper.
Instead, we provide simple tools for downstream projects to build their use
case. So whenever things are becoming complicated, ask yourself: "Can I defer
this to downstream instead?".


# Development environment

To set up your development environment make sure you have valid version of
Python (check `pyproject.toml`) and a `virtualenv`.

Use your favourite way of setting up a `virtualenv`, e.g.:
```
mkvirtualenv -a `pwd` looper -p /usr/bin/python3.10
```

Or simply use `venv` module directly:
```
python3.10 -m venv .venv ; source .venv/bin/activate
```

After `virtualenv` has been created and activated, continue with the following:

```
pip install -e .[dev]  # Install package and its development dependencies.
cp looper_example_project/settings.example.py looper_example_project/settings.py  # Adjust settings.py as needed.
# Make sure you have your database setup according to your settings.py
./manage.py migrate  # Create database tables and run migrations.
./manage.py test  # Make sure the tests run.
```

# Testing

Make sure your development environment is set up correctly and then run
`./manage.py test` in the root of the repository.

# Type Checking

We use [Mypy](http://mypy-lang.org/) extensively and in a very strict way. Types
are checked as part of `./test.sh`.

# Code Formatting

We use [Black](https://black.readthedocs.io/en/stable/) to format code. Proper
formatting is checked as part of the tests in `./test.sh`. To reformat the whole
codebase run `black .`
