from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path

import example_app.urls
import looper.braintree_urls
import looper.urls

# We need these URL patterns because we are developing a Django app. However,
# to be able to test our app we need an actual Django project. Here we define
# the URLs we use in our test project.
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(looper.urls)),
    path('', include(example_app.urls)),
    path('braintree/', include((looper.braintree_urls.urlpatterns, 'looper-braintree'))),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
