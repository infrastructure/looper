import pathlib

from dateutil.relativedelta import relativedelta

BASE_DIR = pathlib.Path(__file__).absolute().parent.parent

DEBUG = True
USE_TZ = True

SECRET_KEY = 'test'

ROOT_URLCONF = 'looper_example_project.urls'

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.messages',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.staticfiles',
    'pipeline',
    'looper',
    'example_app',
    'nested_admin',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'looper.middleware.PreferredCurrencyMiddleware',
]

SITE_ID = 1

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [BASE_DIR / 'looper' / 'templates'],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.10/howto/static-files/
STATIC_URL = '/static/'
STATIC_ROOT = BASE_DIR / 'static'
STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'pipeline.finders.PipelineFinder',
]

PIPELINE = {
    'JS_COMPRESSOR': 'pipeline.compressors.jsmin.JSMinCompressor',
    'CSS_COMPRESSOR': 'pipeline.compressors.NoopCompressor',
    'JAVASCRIPT': {
        'looper': {
            'source_filenames': [
                'looper/scripts/*.js',
            ],
            'output_filename': 'js/looper.js',
            'extra_context': {'async': False, 'defer': False},
        },
        'looper_currency': {
            # Separate script for currency selector for pages that don't need a bundle
            'source_filenames': [
                'looper/scripts/currency.js',
            ],
            'output_filename': 'js/looper_currency.js',
            'extra_context': {'async': False, 'defer': False},
        },
    },
    'STYLESHEETS': {
    },
    'COMPILERS': ('libsasscompiler.LibSassCompiler',),
    'DISABLE_WRAPPER': True,
}

# Cache busting for static files.
# See https://docs.djangoproject.com/en/2.1/ref/contrib/staticfiles/#manifeststaticfilesstorage
STATICFILES_STORAGE = 'pipeline.storage.PipelineManifestStorage'

# Collection of automatically renewing subscriptions will be attempted this
# many times before giving up and setting the subscription status to 'on-hold'.
#
# This value is only used when automatic renewal fails, so setting it < 1 will
# be treated the same as 1 (one attempt is made, and failure is immediate, no
# retries).
LOOPER_CLOCK_MAX_AUTO_ATTEMPTS = 3

# Only retry collection of automatic renewals this long after the last failure.
# This separates the frequency of retrials from the frequency of the clock.
LOOPER_ORDER_RETRY_AFTER = relativedelta(days=2)

# The system user from looper/fixtures/systemuser.json. This user is required
# for logging things in the admin history (those log entries always need to
# have a non-NULL user ID).
LOOPER_SYSTEM_USER_ID = 1

# Conversion rates from the given rate to euros.
# This allows us to express the foreign currency in €.
LOOPER_CONVERTION_RATES_FROM_EURO = {
    'EUR': 1.0,
    'USD': 1.15,
}

LOOPER_MONEY_LOCALE = 'en_US.UTF-8'

LOOPER_SUBSCRIPTION_CREATION_WARNING_THRESHOLD = relativedelta(days=1)

LOOPER_ORDER_RECEIPT_PDF_URL = 'settings_receipt_pdf'

# Additional user fields to be added as search fields in the admin.
# E.g. if your customer User model has a "full_name" field and you want it included into search:
# LOOPER_USER_SEARCH_FIELDS = (
#     'user__full_name',
# )

SUPPORTED_CURRENCIES = {'EUR', 'USD'}

# This product includes GeoLite2 data created by MaxMind, available from
# https://dev.maxmind.com/geoip/geoip2/geolite2/
GEOIP2_DB = BASE_DIR / 'GeoLite2-Country_20181002' / 'GeoLite2-Country.mmdb'

DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'

# A list of payment method types used with stripe for setting a recurring payment:
# https://stripe.com/docs/api/checkout/sessions/create#create_checkout_session-payment_method_types
STRIPE_OFF_SESSION_PAYMENT_METHOD_TYPES = [
    'card',
    'link',
    'paypal',
]
