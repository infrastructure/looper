import braintree
import logging
import sys

from .settings_common import *

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": "db.sqlite3",
    }
}

GATEWAYS = {
    # These settings need to be correctly filled for the tests to pass.
    # Get the credentials at https://sandbox.braintreegateway.com/
    'braintree': {
        'environment': braintree.Environment.Sandbox,
        'merchant_id': 'SECRET',
        'public_key': 'SECRET',
        'private_key': 'SECRET',
        # Merchant Account IDs for different currencies.
        # Configured in Braintree: Settings Icon → Business.
        'merchant_account_ids': {
            'EUR': 'merchant-account-id-for-eur',
            'USD': 'merchant-account-id-for-usd',
        },
        # DevFund allows only automatic collection with Braintree:
        'supported_collection_methods': {'automatic'},
        # Blender Studio allows both automatic and manual collection with Braintree:
        # 'supported_collection_methods': {'automatic', 'manual'},
    },
    'stripe': {
        'api_secret_key': 'SECRET',
        'endpoint_secret': 'SECRET',
        'supported_collection_methods': {'manual', 'automatic'},
    },
    'bank': {
        'supported_collection_methods': {'manual'},
    },
}

# Email address for customer support (displayed in form error messages)
EMAIL_SUPPORT = ""

if len(sys.argv) > 1 and sys.argv[1] == 'test':
    logging.disable(logging.CRITICAL)
