from decimal import Decimal
from typing import Optional, Union
import logging
import re

from django.core.exceptions import ValidationError
from django.core.validators import validate_ipv46_address
from django.http import HttpRequest

logger = logging.getLogger(__name__)


IPV4_WITH_PORT = re.compile(r"([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+):[0-9]+")
"""Regexp matching an IPv4 address with a port number."""

IPV6_WITH_PORT = re.compile(r"\[([0-9:]+)\]:[0-9]+")
"""Regexp matching an IPv6 address with a port number."""


def clean_ip_address(request: HttpRequest) -> str:
    """Retrieve a valid IP address from the given request.

    Raises a django.code.exceptions.ValidationError
    if no valid IP address could be determined.
    """
    ip_address = get_client_ip(request)
    validate_ipv46_address(ip_address)
    return ip_address


def get_client_ip(request: HttpRequest) -> str:
    """Returns the IP of the request, accounting for the possibility of being
    behind a proxy.
    """
    x_forwarded_for: Optional[str] = request.META.get('HTTP_X_FORWARDED_FOR', '')
    # Most of the times IPs are separated with "," and not  ", ".
    # We want to simplify the split, so removing all spaces first:
    x_forwarded_for = x_forwarded_for.replace(' ', '')
    if x_forwarded_for:
        # X_FORWARDED_FOR returns client1,proxy1,proxy2,...
        remote_addr = x_forwarded_for.split(',', 1)[0].strip()
        ip_address = _remove_port_nr(remote_addr)
        try:
            # X_FORWARDED_FOR can contain bogus, only use it if it's valid
            validate_ipv46_address(ip_address)
            return ip_address
        except ValidationError:
            logger.warning('Unable to parse X-Forwarded-For %s', x_forwarded_for)

    remote_addr = request.META.get('REMOTE_ADDR', '')
    if not remote_addr:
        return ''

    # REMOTE_ADDR can also be 'ip1,ip2' if people mess around with HTTP
    # headers (we've seen this happen). Don't trust anything in that case.
    if ',' in remote_addr:
        return ''

    return _remove_port_nr(remote_addr)


def _remove_port_nr(remote_addr: str) -> str:
    # Occasionally the port number is included in REMOTE_ADDR.
    # This needs to be filtered out so that downstream requests
    # can just interpret the value as actual IP address.

    if len(remote_addr) > 128:
        # Prevent DoS attacks by not allowing obvious nonsense.
        return ''

    if ':' not in remote_addr:
        return remote_addr

    ipv4_with_port_match = IPV4_WITH_PORT.match(remote_addr)
    if ipv4_with_port_match:
        return ipv4_with_port_match.group(1)

    ipv6_with_port_match = IPV6_WITH_PORT.match(remote_addr)
    if ipv6_with_port_match:
        return ipv6_with_port_match.group(1)

    return remote_addr


def make_absolute_url(url: str) -> str:
    """Turn a host-absolute URL into a fully absolute URL.

    For simplicity, assumes we're using HTTPS and not running on some URL prefix.
    """
    from django.contrib.sites.models import Site
    from urllib.parse import urljoin

    site: Site = Site.objects.get_current()
    return urljoin(f'https://{site.domain}/', url)


def remove_exponent(decimal_value: Decimal, quantize_to: str) -> Decimal:
    """Remove exponent from a given decimal if it represents a whole number, otherwise quantize."""
    if decimal_value == decimal_value.to_integral():
        return decimal_value.quantize(Decimal(1))
    return decimal_value.quantize(Decimal(quantize_to)).normalize()


def ensure_decimal(value: Union[Decimal, int, float, str], quantize_to='1.0') -> Decimal:
    """Convert a given value to a decimal and quantize it to a given number of decimal places.

    The default number of decimal places is 1, because looper doesn't need high precision:
    decimal values are currently used to calculate percentages from money values in cents.

    This function also normalises the decimal to ensure it isn't displayed with trailing zeroes.
    Whenever this decimal is formatted to be shown, 'f' (fixed-point notation) flag should be used
    to avoid scientific notation being used for '0e0', e.g.:

        format(decimal_value, 'f')
        f'{decimal_value:f}'
    """
    decimal_value = value if isinstance(value, Decimal) else Decimal(value)
    return remove_exponent(decimal_value, quantize_to=quantize_to)


def format_percentage(value: Union[Decimal, int, float, str]) -> str:
    """Format a percentage value to a string."""
    return f'{ensure_decimal(value):f}%'
