from django.urls import path

from .views import checkout, checkout_braintree, settings_braintree

# Keep Braintree views and URLs but prefix them: the main checkout flow is based on Stripe.
urlpatterns = [
    path(
        'checkout/<int:plan_id>',
        checkout.ChooseDefaultPlanVariationView.as_view(pattern_name='looper-braintree:checkout'),
        name='checkout_new',
    ),
    path(
        'checkout/<int:plan_id>/variation/<int:plan_variation_id>',
        checkout_braintree.CheckoutView.as_view(),
        name='checkout',
    ),
    path(
        'checkout/pay/<int:order_id>',
        checkout_braintree.CheckoutExistingOrderView.as_view(),
        name='checkout_existing_order',
    ),
    path(
        'settings/billing/payment-methods/change/<int:subscription_id>',
        settings_braintree.PaymentMethodChangeView.as_view(),
        name='payment_method_change',
    ),
]
