import looper.models as models


def interval(subscription: models.Subscription) -> str:
    """Combines interval_unit and interval_length into one string.

    This helps making the admin's list_display a little bit tidier.
    """
    if subscription.interval_length == 1:
        return subscription.interval_unit
    return f'{subscription.interval_length} {subscription.interval_unit}'
