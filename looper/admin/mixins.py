from typing import Any, Callable, Dict, Optional, Sequence, Set, Tuple, Union
import copy
import csv
import inspect
import logging
import re

from django.contrib.admin.utils import label_for_field
from django.core.exceptions import PermissionDenied
from django.http import HttpRequest, HttpResponse
from django.urls import path, resolve
from django.utils.safestring import SafeString
import django.contrib.admin.options
import django.db.models

import bleach

import looper.money

logger = logging.getLogger(__name__)


FieldsetType = Sequence[Tuple[Optional[str], Dict[str, Any]]]


class EditableWhenNewMixin:
    # These should be set on the subclass using this mix-in.
    readonly_fields: Sequence[Union[str, Callable[..., object]]]
    fieldsets: FieldsetType

    # Those callable fields will become editable when adding a new instance.
    # Assumes that these callables have a 'admin_order_field' property that
    # contains the field they map to.
    editable_when_new: Set[Callable[..., object]] = set()

    def get_readonly_fields(
        self, request: HttpRequest, obj: Optional[django.db.models.Model] = None
    ) -> Any:
        if obj:  # Editing an existing object
            return self.readonly_fields

        # When adding a new subscription, remove the the 'xxx_link' callables
        # from the readonly_fields list. This optimises for the more common
        # case of subscriptions being created by the checkout process, rather
        # than manually via the admin.
        return [field for field in self.readonly_fields if not hasattr(field, '__call__')]

    def get_fieldsets(
        self, request: HttpRequest, obj: Optional[django.db.models.Model] = None
    ) -> FieldsetType:
        if obj:  # Editing an existing object.
            return self.fieldsets

        fieldsets = copy.deepcopy(self.fieldsets)

        for fieldset in fieldsets:
            label, config = fieldset
            if 'fields' not in config:
                continue

            fields = []
            # Replace callables with the field name they map to (using
            # the_callable.admin_order_field), or remove callables if
            # they are not in self.editable_when_new.
            for field in config['fields']:
                if not hasattr(field, '__call__'):
                    fields.append(field)
                    continue
                if field not in self.editable_when_new:
                    continue
                assert hasattr(
                    field, 'admin_order_field'
                ), f"callable {field} should have an attribute 'admin_order_field'"
                fields.append(field.admin_order_field)
            config['fields'] = fields
        return fieldsets


class NoChangeMixin:
    """Disallow changing objects via the admin."""

    def has_change_permission(self, *args, **kwargs):
        """Disallow adding new objects via the admin."""
        return False


class NoAddDeleteMixin:
    """Disallow adding and deleting objects via the admin."""

    def has_add_permission(self, *args, **kwargs):
        """Disallow adding new objects via the admin."""
        return False

    def has_delete_permission(self, *args, **kwargs):
        """Disallow removing objects via the admin."""
        return False


class NoAddMixin:
    """Disallow adding objects via the admin."""

    def has_add_permission(self, *args, **kwargs):
        """Disallow adding new objects via the admin."""
        return False


class ExportCSVMixin:
    """Export current page of results or all filtered results as a CSV.

    Based on http://djangosnippets.org/snippets/1697/ and /2020/
    """

    def generate_csv_filename(self, request, changelist, export_all):
        """Generate a file name using the model and current filters."""
        opts = self.model._meta
        model_name = (
            opts.verbose_name_plural.replace('.', '_').replace(' ', '_').replace('looper_', '')
        )
        site = re.sub('\s+', '_', self.admin_site.site_header)

        # E.g.: ?currency__exact=USD&is_legacy__exact=1&paid_at__quarter=1&paid_at__year=2021
        def _format_filter(param, value):
            if param.endswith('__quarter'):
                return f'Q{value}'
            elif param == 'is_legacy__exact':
                if value == '1':
                    return 'legacy'
                return ''
            # Strip hours from datetimes
            return re.sub(r'\s00:00:00+.*$', '', value)

        filters = '_'.join(
            [_format_filter(*_)
             for _ in sorted(request.GET.items(), key=lambda x: x[0], reverse=True)]
        )
        if export_all:
            filters = filters + '_all'
        else:
            filters = filters + f'_page_{(changelist.page_num + 1):03}'
        # Prefix filters by another "_"
        filters = '_' + filters if filters else filters
        filename = f'{site}_{model_name}{filters}.csv'.replace(self.date_hierarchy or '', '')
        return re.sub(r'[\]\[]+', '', re.sub(r'_+', '_', filename))

    def export_as_csv(self, request, changelist, export_all=False):
        """CSV export current results."""
        if not request.user.is_staff:
            raise PermissionDenied

        results_to_export = changelist.queryset if export_all else changelist.result_list

        field_names = self.list_display
        if 'action_checkbox' in field_names:
            field_names.remove('action_checkbox')

        filename = self.generate_csv_filename(request, changelist, export_all=export_all)
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = f'attachment; filename={filename}'

        writer = csv.writer(response)
        header = True
        if header:
            headers = []
            for field_name in list(field_names):
                label = label_for_field(field_name, self.model, self)
                if str.islower(label):
                    label = str.title(label)
                headers.append(label)
            writer.writerow(headers)

        for row in results_to_export:
            values = []
            for field in field_names:
                if callable(field):
                    value = field(row)
                else:
                    if hasattr(row, field):
                        value = getattr(row, field)
                    else:
                        value = getattr(self, field)
                    if callable(value):
                        try:
                            # Unsure if it makes sense to pass row as an argument always,
                            # so checking if this callable requires any arguments at all
                            if len(inspect.signature(value).parameters) > 0:
                                value = value(row)
                            else:
                                value = value() or ''
                        except Exception:
                            logger.exception(
                                'Error retrieving value of %s for %s CSV export', field, self
                            )
                            value = 'Error retrieving value'
                    if value is None:
                        value = ''

                if isinstance(value, SafeString):
                    value = bleach.clean(value, tags=[], attributes={}, styles=[], strip=True)

                # String representation of `Money` is prefixed with a currency code.
                # We want CSVs to have amounts without currency codes, however.
                if isinstance(value, looper.money.Money):
                    value = value.decimals_string

                values.append(str(value))
            writer.writerow(values)
        return response

    def get_urls(self):
        """Return URLs of additional admin views, such as CSV export view."""
        urls = super().get_urls()
        model_name = f'{self.model._meta.app_label}_{self.model._meta.model_name}'
        report_urls = [
            path('export-csv/', self.export_as_csv_view, name=f'{model_name}_export_as_csv'),
            path(
                'export-all-csv/',
                self.export_all_as_csv_view,
                name=f'{model_name}_export_all_as_csv',
            ),
        ]
        return report_urls + urls

    def export_as_csv_view(self, request):
        """Export a changelist (not its original queryset!) as a CSV."""
        cl = self.get_changelist_instance(request)
        return self.export_as_csv(request, cl, export_all=False)

    def export_all_as_csv_view(self, request):
        """Export the filtered queryset as a CSV."""
        cl = self.get_changelist_instance(request)
        return self.export_as_csv(request, cl, export_all=True)


class HistoryDescMixin:
    """Changes ordering of "History" from default ascending by date to descending."""

    def get_paginator(self, request, queryset, *args, **kwargs):
        """Replace order_by in action_list, queryset.object queryset."""
        current_url = resolve(request.path_info)
        if current_url.func.__name__ == 'history_view':
            return self.paginator(
                queryset.order_by('-action_time'), *args, **kwargs
            )
        return super().get_paginator(request, queryset, *args, **kwargs)
