from typing import Any, Callable, Dict, List, Optional, Tuple, Type, Union

from django.conf import settings
from django.contrib import admin
from django.contrib.admin import helpers
from django.contrib.auth import get_user_model
from django.db.models.query import QuerySet
from django.http import HttpRequest
from django.template.response import TemplateResponse
from django.urls import reverse
from django.utils.html import format_html
from django.utils.safestring import SafeText
import django.db.models

from .filters import EmptyFieldListFilter, ChoicesFieldListWithEmptyFilter
from .mixins import EditableWhenNewMixin, ExportCSVMixin, HistoryDescMixin, NoAddMixin
from .utils import interval
from looper.utils import make_absolute_url
import looper.clock
import looper.decorators as decorators
import looper.forms as forms
import looper.models as models

import nested_admin

User = get_user_model()

CUSTOMER_SEARCH_FIELDS = (
    'token__token',
    'address__email',
    'address__vat_number',
    'address__full_name',
    'address__company',
)
REL_CUSTOMER_SEARCH_FIELDS = (
    'customer__user__email',
    *(f'customer__{_}' for _ in CUSTOMER_SEARCH_FIELDS),
    *(f'customer__user__{_}' for _ in getattr(settings, 'LOOPER_USER_SEARCH_FIELDS', [])),
)


def _get_admin_url_name(model: Type['django.db.models.Model'], action: str = 'change') -> str:
    return 'admin:{}_{}_{}'.format(model._meta.app_label, model._meta.model_name, action)


def get_admin_change_url(obj: django.db.models.Model) -> str:
    """Return an admin URL to admin change view for the given object."""
    return reverse(_get_admin_url_name(obj.__class__), args=[obj.pk])


@decorators.short_description('ID')
def id_column(instance: django.db.models.Model) -> str:
    pk = instance.pk
    if pk is None:
        return '-'
    if isinstance(pk, int):
        return f'#{pk}'
    return str(pk)


@decorators.short_description('')
def create_subscription_button_link(planvar: models.PlanVariation) -> str:
    assert isinstance(planvar, models.PlanVariation), f'Expected PlanVariation, not {type(planvar)}'
    if planvar.pk is None:
        return ''

    from urllib.parse import urlencode

    button_url = reverse('admin:looper_subscription_add')
    query = urlencode(
        {
            'plan': planvar.plan_id,
            'collection_method': 'manual',
            'currency': planvar.currency,
            'price': planvar.price.decimals_string,
            'interval_unit': planvar.interval_unit,
            'interval_length': planvar.interval_length,
        }
    )

    return format_html(
        '<span class="object-tools"><a class="historylink" href="{}">Create Subscription</a></span>',  # noqa: E501
        f'{button_url}?{query}',
    )


class PlanVariationInline(admin.TabularInline):
    model = models.PlanVariation
    min_num = 1
    extra = 0
    fields = [
        id_column,
        'currency',
        'price',
        'interval_unit',
        'interval_length',
        'is_default_for_currency',
        'collection_method',
        'is_active',
        create_subscription_button_link,
    ]
    readonly_fields = [id_column, create_subscription_button_link]


@admin.register(models.Gateway)
class GatewayAdmin(admin.ModelAdmin):
    list_display = ['name', 'is_default', 'frontend_name']


@admin.register(models.Product)
class ProductAdmin(admin.ModelAdmin):
    search_fields = ('name', 'description')


LinkFunc = Callable[[django.db.models.Model], str]


def create_admin_fk_link(
    field_name: str, short_description: str, view_name: str, title_func=str
) -> LinkFunc:
    """Construct a function that constructs a link to the admin:xxxx_change form.

    :param field_name: The object is taken from model_instance.{field_name}
    :param short_description: The label for this link as shown in the admin.
    :param view_name: The admin view to link to. Must take one parameter `object_id`.
    :param title_func: A callable that takes model_instance and returns a string used a link title.

    For example, to create a link to a customer (instead of a drop-down to edit it),
    use:

    `plan_link = create_admin_fk_link('plan', 'plan', 'admin:looper_plan_change')`
    """

    def create_link(model_instance: django.db.models.Model) -> str:
        # Support looking up a relation or relation
        if '__' in field_name:
            field_names = field_name.split('__')
            referenced: Optional[object] = getattr(
                getattr(model_instance, field_names[0]),
                field_names[1],
            )
        else:
            referenced: Optional[object] = getattr(model_instance, field_name)
        if referenced is None:
            return '-'
        assert isinstance(
            referenced, django.db.models.Model
        ), f'Expected Model, not {type(referenced)}'
        admin_link = reverse(view_name, kwargs={'object_id': referenced.pk})
        link_title = title_func(referenced)
        # Use primary key as link title if got a blank string:
        if not link_title:
            link_title = str(referenced.pk)
        return format_html('<a href="{}">{}</a>', admin_link, link_title)

    # Callable[[Model], str] doesn't have those Django-specific attributes,
    # and I don't feel like speccing that all out. ~~Sybren
    create_link.admin_order_field = field_name  # type: ignore
    create_link.short_description = short_description  # type: ignore
    return create_link


def legacy_link(instance: Union[models.Subscription, models.Order]) -> str:
    """Return a formatted link to Blender Store, if applicable."""
    if not instance.is_legacy or not instance.pk:
        return ''
    url = f'https://store.blender.org/wp-admin/post.php?post={instance.pk}&action=edit'
    return format_html(
        '<a href="{}" target="_blank">{}</a>', url, f'#{instance.pk} at Blender Store'
    )


subscription_link = create_admin_fk_link(
    'subscription', 'subscription', 'admin:looper_subscription_change', lambda s: s.pk
)
order_link = create_admin_fk_link('order', 'order', 'admin:looper_order_change')
plan_link = create_admin_fk_link('plan', 'plan', 'admin:looper_plan_change')
product_link = create_admin_fk_link('product', 'product', 'admin:looper_product_change')
user_link = create_admin_fk_link('user', 'user', _get_admin_url_name(User))
customer_link = create_admin_fk_link('customer', 'customer', _get_admin_url_name(models.Customer))
customer_user_link = create_admin_fk_link(
    'customer__user', 'user', _get_admin_url_name(models.User)
)


@admin.register(models.Plan)
class PlanAdmin(admin.ModelAdmin):
    list_display = [id_column, 'name', 'is_active', product_link]
    inlines = [
        PlanVariationInline,
    ]


def copy_to_clipboard_link(
    view_name: str, kwargs: Dict[str, Any], link_text: str, title: str
) -> SafeText:
    """Construct a link that copies a link to the clipboard when clicked.

    This is for things like payment links, e.g. links that should be sent to
    a customer (instead of followed by the admin).
    """
    link = reverse(view_name, kwargs=kwargs)
    link = make_absolute_url(link)

    return format_html(
        '<a href="{}" title="{}" data-clipboard-text="{}">{}</a><br>'
        '<span class="help">{}</span>',
        link,
        title,
        link,
        link_text,
        'Click link to copy',
    )


@decorators.short_description('Payment Link')
def payment_link(model_instance: Optional[models.Order]) -> str:
    """Show the payment link, can be sent to users."""
    if not model_instance or not model_instance.pk:
        return ''

    url_name = getattr(settings, 'LOOPER_PAY_EXISTING_ORDER_URL', 'looper:checkout_existing_order')
    return copy_to_clipboard_link(
        url_name,
        kwargs={'order_id': model_instance.pk},
        link_text='Payment Link',
        title='Send this to customers',
    )


class OrderInline(admin.TabularInline):
    model = models.Order
    min_num = 0
    extra = 0
    show_change_link = True
    fields = [
        'status',
        'created_at',
        'updated_at',
        'price',
        'tax_type',
        'tax_rate',
        'tax_country',
        'tax',
        'payment_method',
        'collection_method',
        'collection_attempts',
        payment_link,
    ]
    readonly_fields = fields
    can_delete = False


def require_confirmation(func):
    def wrapper(modeladmin, request, queryset):
        action_name = func.__name__
        opts = modeladmin.model._meta
        app_label = opts.app_label
        if request.POST.get('post') == 'yes':
            return func(modeladmin, request, queryset)

        request.current_app = modeladmin.admin_site.name
        context = {
            'action': request.POST['action'],
            'media': modeladmin.media,
            'opts': opts,
            'queryset': queryset,
            'action_checkbox_name': helpers.ACTION_CHECKBOX_NAME,
            'title': func.short_description,
        }
        return TemplateResponse(
            request,
            [
                'admin/{}/{}/{}_selected_confirmation.html'.format(
                    app_label, opts.model_name, action_name
                ),
                'admin/{}/{}_selected_confirmation.html'.format(app_label, action_name),
                'admin/{}_selected_confirmation.html'.format(action_name),
            ],
            context,
        )

    wrapper.__name__ = func.__name__
    return wrapper


@require_confirmation
@decorators.short_description('Force renewal of selected subscriptions')
def force_renewal(
    modeladmin: 'SubscriptionAdmin', request: HttpRequest, queryset: 'QuerySet[models.Subscription]'
) -> None:
    """Force what clock_tick does: create an order, attempt to charge it if automatic."""
    clock = looper.clock.Clock()
    for subscription in queryset:
        clock.renew_subscription(subscription)
    modeladmin.message_user(
        request,
        f'Attempted to renew and collect payments for {queryset.count()} subscriptions.',
    )


ACCOUNT_INFORMATION_FIELDSET = [
    'Account information',
    {
        'fields': [
            (customer_user_link, 'customer'),
        ],
        'description':
            'When <code>User</code> is empty, it means that payment was made '
            'or attempted without signing in with an account,<br>'
            "and this customer's billing, subscription and payment records "
            'are yet to be claimed by a user account.<br>'
            'Someone should sign in and use a customer token link to do so.',
    },
]


@admin.register(models.Subscription)
class SubscriptionAdmin(ExportCSVMixin, EditableWhenNewMixin, HistoryDescMixin, admin.ModelAdmin):
    ordering = ['-created_at']
    save_on_top = True
    list_per_page = 50
    list_max_show_all = 60
    list_display = [
        'id',
        'collection_method',
        'status',
        'started_at',
        'cancelled_at',
        interval,
        'intervals_elapsed',
        customer_user_link,
        'payment_method',
    ]
    list_filter = [
        'plan',
        'collection_method',
        'status',
        'created_at',
        'started_at',
        'next_payment',
        'pending_cancellation_since',
        'cancelled_at',
        ('payment_method__method_type', ChoicesFieldListWithEmptyFilter),
        'currency',
        'is_legacy',
        'tax_type',
        ('customer__user', EmptyFieldListFilter),
    ]
    list_select_related = ('plan', 'payment_method', 'customer', 'customer__user')
    search_fields = ['id', 'order__number', *REL_CUSTOMER_SEARCH_FIELDS]
    date_hierarchy = 'created_at'

    form = forms.SubscriptionAdminForm
    autocomplete_fields = ['customer']

    def get_readonly_fields(self, request, obj=None):
        return [
            customer_user_link,
            customer_link,
            plan_link,
            'created_at',
            'updated_at',
            'pending_cancellation_since',
            'intervals_elapsed',
            'last_notification',
            'is_legacy',
            legacy_link,
        ]

    editable_when_new = {plan_link}

    def get_fieldsets(self, request, obj=None) -> List[Tuple[Any, Dict]]:
        if obj is None:  # staff manually creating a new subscription
            return [
                (
                    None,
                    {
                        'fields': [
                            'plan',
                            'customer',
                            'status',
                            'interval_unit',
                            'interval_length',
                            'intervals_elapsed',
                        ],
                    },
                ),
                (
                    'Money',
                    {
                        'fields': [
                            'payment_method',
                            'collection_method',
                            'currency',
                            'price',
                            'tax',
                            'tax_type',
                            'tax_rate',
                            'tax_country',
                        ],
                    },
                ),
                (
                    'Dates',
                    {
                        'fields': [
                            'started_at',
                            'next_payment',
                        ],
                    },
                ),
            ]
        return [
            (
                None,
                {
                    'fields': [
                        plan_link,
                        'status',
                        'interval_unit',
                        'interval_length',
                        'intervals_elapsed',
                        'is_legacy',
                        legacy_link,
                    ],
                },
            ),
            ACCOUNT_INFORMATION_FIELDSET,
            (
                'Money',
                {
                    'fields': [
                        'payment_method',
                        'collection_method',
                        'currency',
                        'price',
                        'tax',
                        'tax_type',
                        'tax_rate',
                        'tax_country',
                    ],
                },
            ),
            (
                'Dates',
                {
                    'fields': [
                        'created_at',
                        'updated_at',
                        'started_at',
                        ('cancelled_at', 'pending_cancellation_since'),
                        # FIXME(anna): appears to be unused
                        # 'current_interval_started_at',
                        'next_payment',
                        'last_notification',
                    ],
                },
            ),
        ]

    inlines = [
        OrderInline,
    ]
    actions = [force_renewal]


def refund_button_link(trans: models.Transaction) -> str:
    assert isinstance(trans, models.Transaction), f'Expected Model, not {type(trans)}'
    if trans.pk is None:
        return ''
    refund_link = reverse('admin:looper_transaction_change', kwargs={'object_id': trans.pk})
    return format_html(
        '<a class="changelink" href="{}">View to refund</a>',
        refund_link,
    )


class TransactionsInline(NoAddMixin, admin.TabularInline):
    model = models.Transaction
    min_num = 0
    extra = 0
    show_change_link = True
    fields = [
        'status',
        'created_at',
        'failure_message',
        'failure_code',
        'amount',
        'amount_refunded',
        'refunded_at',
        refund_button_link,
        'source',
    ]
    readonly_fields = fields
    can_delete = False
    ordering = ('-created_at',)


def mark_as_paid(
    modeladmin: 'OrderAdmin', request: HttpRequest, queryset: 'QuerySet[models.Order]'
) -> None:
    """Mark all selected orders as 'paid'."""
    for order in queryset:
        order.status = 'paid'
        order.save()


@decorators.short_description('Receipt')
def order_receipt_link(order: models.Order) -> str:
    if order.status != 'paid':
        return format_html('<span title="{}">-</span>', 'Only available for paid orders')

    pdf_url = reverse(settings.LOOPER_ORDER_RECEIPT_PDF_URL, kwargs={'order_id': order.id})
    return format_html('<a href="{}" target="_blank">PDF</a>', pdf_url)


@admin.register(models.Order)
class OrderAdmin(ExportCSVMixin, HistoryDescMixin, admin.ModelAdmin):
    ordering = ['-created_at']
    save_on_top = True
    date_hierarchy = 'created_at'
    list_per_page = 50
    list_max_show_all = 60
    list_display = [
        'display_number',
        'status',
        'created_at',
        'paid_at',
        'price',
        subscription_link,
        'collection_method',
        customer_user_link,
        'payment_method',
        order_receipt_link,
        # 'tax_type',
        'tax_rate',
        'tax_country',
        'tax',
        # 'vat_number',
    ]
    list_filter = [
        'status',
        'created_at',
        'collection_method',
        'collection_attempts',
        'payment_method__gateway',
        'payment_method__method_type',
        'payment_method__is_deleted',
        'tax_type',
        'refunded_at',
        'currency',
        'is_legacy',
        ('customer__user', EmptyFieldListFilter),
        ('subscription', EmptyFieldListFilter),
    ]
    list_select_related = (
        'subscription',
        'subscription__customer',
        'subscription__customer__user',
        'payment_method',
        'payment_method__gateway',
        'customer',
        'customer__user',
        'product',
    )
    search_fields = [
        'id', 'email', 'number', 'gateway_order_ids__gateway_order_id', *REL_CUSTOMER_SEARCH_FIELDS
    ]

    actions = [mark_as_paid]

    form = forms.OrderAdminForm
    autocomplete_fields = ['customer', 'subscription', 'product']
    readonly_fields = [
        customer_user_link,
        payment_link,
        'created_at',
        'updated_at',
        'collection_attempts',
        'tax_type',
        'tax_rate',
        'tax_country',
        'tax',
        'vat_number',
        'refunded',
        'number',
        'is_legacy',
        legacy_link,
        'tax_refunded',
        'refunded_at',
        'gateway_order_ids',
    ]
    fieldsets = [
        (
            None,
            {
                'fields': [
                    'product',
                    'subscription',
                    payment_link,
                    'status',
                    'name',
                    ('number', 'external_reference'),
                    'is_legacy',
                    legacy_link,
                    'gateway_order_ids',
                ],
            },
        ),
        ACCOUNT_INFORMATION_FIELDSET,
        (
            'Money',
            {
                'fields': [
                    'payment_method',
                    ('collection_method', 'collection_attempts'),
                    'currency',
                    ('price', 'refunded'),
                    ('tax_type', 'tax_rate', 'tax_country'),
                    ('tax', 'tax_refunded'),
                    'vat_number',
                ],
            },
        ),
        (
            'Dates',
            {
                'fields': ['created_at', 'updated_at', 'paid_at', 'refunded_at', 'retry_after'],
                'classes': ('collapse',),
            },
        ),
        (
            'Addresses',
            {
                'fields': ['email', 'billing_address'],
                'classes': ('collapse',),
            },
        ),
    ]
    inlines = [
        TransactionsInline,
    ]

    def gateway_order_ids(self, obj):
        return ' '.join([_.gateway_order_id for _ in obj.gateway_order_ids.all()])

    class Media:
        css = {"all": ("css/looper_admin.css",)}

    def get_queryset(self, request):
        return super().get_queryset(
            request
        ).prefetch_related(
            'gateway_order_ids',
        )


@admin.register(models.Transaction)
class TransactionAdmin(ExportCSVMixin, NoAddMixin, HistoryDescMixin, admin.ModelAdmin):
    ordering = ['-created_at']
    list_display = (
        'id',
        'status',
        'created_at',
        'updated_at',
        'amount',
        'amount_refunded',
        'refunded_at',
        'source',
    )
    list_filter = (
        'status',
        'created_at',
        'payment_method__gateway',
        'payment_method__method_type',
        'source',
        ('authentication', EmptyFieldListFilter),
        'order__is_legacy',
        ('customer__user', EmptyFieldListFilter),
        'failure_code',
        'failure_message',
    )
    search_fields = ('id', 'transaction_id', 'order__number', *REL_CUSTOMER_SEARCH_FIELDS)
    date_hierarchy = 'created_at'

    form = forms.AdminTransactionRefundForm
    fieldsets = [
        (
            None,
            {
                'fields': [
                    order_link,
                    'status',
                    'paid',
                    'transaction_id',
                    'ip_address',
                    'source',
                    'authentication',
                    ('failure_message', 'failure_code'),
                ],
            },
        ),
        ACCOUNT_INFORMATION_FIELDSET,
        (
            'Dates',
            {
                'fields': ['created_at', 'updated_at'],
            },
        ),
        (
            'Money',
            {
                'fields': [
                    'payment_method',
                    'currency',
                    'amount',
                    'refunded_at',
                    'amount_refunded',
                    'refund_amount',
                ],
            },
        ),
    ]

    # Make everything read-only except the amount to refund.
    readonly_fields = (
        customer_user_link,
        customer_link,
        'customer',
        order_link,
        'status',
        'failure_message',
        'failure_code',
        'authentication',
        'paid',
        'transaction_id',
        'created_at',
        'updated_at',
        'payment_method',
        'currency',
        'amount',
        'refunded_at',
        'amount_refunded',
        'ip_address',
        'source',
    )

    class Media:
        css = {"all": ("css/looper_admin.css",)}


class GatewayCustomerIdInline(nested_admin.NestedTabularInline):
    model = models.GatewayCustomerId
    can_delete = True
    extra = 0
    readonly_fields = ('gateway', 'gateway_customer_id')
    classes = {'collapse'}


class AddressInline(nested_admin.NestedStackedInline):
    model = models.Address
    can_delete = True
    extra = 0
    max_num = 1
    # Right now users only have one address, which is always a billing address.
    verbose_name = 'Billing Address'
    verbose_name_plural = 'Billing Address'
    exclude = ['category']


class PaymentMethodInline(nested_admin.NestedTabularInline):
    model = models.PaymentMethod
    can_delete = False
    extra = 0
    max_num = 0

    readonly_fields = ('pk', 'created_at', 'updated_at', 'method_type', 'recognisable_name')
    fields = readonly_fields + ('is_deleted', )

    def get_queryset(self, request):
        return super().get_queryset(request).exclude(is_deleted=True)


class CustomerTokenInline(nested_admin.NestedTabularInline):
    model = models.LinkCustomerToken
    raw_id_fields = ['linked_to_user']
    extra = 0
    max_num = 0
    can_delete = False

    def has_change_permission(self, request, obj=None):
        """Don't allow modification of existing tokens.

        **N.B.**: this is not the same as adding `readonly_fields = ['token']`,
        because that would make it impossible to create
        a token via "Add another" button.
        """
        return False


class CustomerInline(nested_admin.NestedStackedInline):
    model = models.Customer
    can_delete = False
    # It's a one-on-one relationship, so users have only one 'customer'.
    verbose_name_plural = 'customer'
    inlines = [
        AddressInline,
        GatewayCustomerIdInline,
        PaymentMethodInline,
        CustomerTokenInline,
    ]


class LinkCustomerTokenInline(nested_admin.NestedTabularInline):
    model = models.LinkCustomerToken
    max_num = 0
    extra = 0
    fields = ('updated_at', 'token')
    readonly_fields = fields
    classes = {'collapse'}
    verbose_name_plural = 'Used customer link tokens'
    can_delete = False


class PaymentMethodAuthenticationInlineAdmin(admin.TabularInline):
    model = models.PaymentMethodAuthentication
    can_delete = False
    extra = 0
    max_num = 0

    fields = (
        'created_at',
        'authentication_id',
        'authentication_type',
        'consumed_at',
        'status',
        'version',
    )
    readonly_fields = fields


@admin.register(models.Customer)
class CustomerAdmin(nested_admin.NestedModelAdmin):
    list_display = ['id', 'user__username', 'user__email', 'billing_email', 'billing_address']
    raw_id_fields = ['user']
    search_fields = [
        *CUSTOMER_SEARCH_FIELDS,
        *(f'user__{_}' for _ in getattr(settings, 'LOOPER_USER_SEARCH_FIELDS', [])),
    ]
    inlines = CustomerInline.inlines
    list_filter = (
        ('user', EmptyFieldListFilter),
    )
    list_select_related = ('user', 'token')

    def get_queryset(self, request):
        """Prefetch related objects."""
        return super().get_queryset(request).prefetch_related('address_set')

    def user__username(self, customer):
        return customer.user.username if customer.user_id else '-'

    def user__email(self, customer):
        return customer.user.email if customer.user_id else '-'

    def billing_email(self, customer):
        return customer.billing_address.email if customer.user_id else '-'

    def billing_address(self, customer):
        if not customer.billing_address.pk:
            return '-'
        address = customer.billing_address.as_text()
        return SafeText(address.replace('\n', '<br>'))


@admin.register(models.PaymentMethod)
class PaymentMethodAdmin(admin.ModelAdmin):
    inlines = [PaymentMethodAuthenticationInlineAdmin]
    list_display = (
        'created_at',
        'updated_at',
        'gateway',
        'method_type',
        'recognisable_name',
        customer_user_link,
        'is_deleted',
    )
    list_filter = (
        'created_at',
        'gateway',
        'is_deleted',
        ('paymentmethodauthentication__authentication_id', EmptyFieldListFilter),
        ('customer__user', EmptyFieldListFilter),
    )
    list_select_related = (
        'gateway',
        'customer',
        'customer__user',
    )
    search_fields = ('id', *REL_CUSTOMER_SEARCH_FIELDS, 'recognisable_name')
    date_hierarchy = 'created_at'
    autocomplete_fields = ['customer']

    def get_fieldsets(self, request, obj=None) -> List[Tuple[Any, Dict]]:
        if obj is None:  # staff manually creating a payment method
            return [
                (
                    None,
                    {
                        'fields': [
                            'gateway',
                            'method_type',
                            'customer',
                        ],
                    },
                ),
            ]
        return [
            (
                'Gateway-specific data',
                {
                    'fields': [
                        'gateway',
                        'method_type',
                        'token',
                    ],
                },
            ),
            ACCOUNT_INFORMATION_FIELDSET,
            (
                'Dates',
                {
                    'fields': ['created_at', 'updated_at'],
                },
            ),
        ]

    # Make everything read-only, unless adding
    def get_readonly_fields(self, request, obj=None):
        if obj is None:
            return []
        return (
            'created_at',
            'gateway',
            'method_type',
            'recognisable_name',
            'token',
            'updated_at',
            'customer',
            customer_user_link,
            customer_link,
        )


@admin.register(models.PaymentMethodAuthentication)
class PaymentMethodAuthenticationAdmin(admin.ModelAdmin):
    list_display = (
        'created_at',
        'authentication_id',
        'authentication_type',
        'consumed_at',
        'status',
        'version',
    )
    fields = (
        'created_at',
        'gateway',
        'payment_method',
        'authentication_id',
        'authentication_type',
        'consumed_at',
        'status',
        'version',
        'details',
    )
    readonly_fields = fields
    date_hierarchy = 'created_at'
    search_fields = ('authentication_id', 'payment_method__token')
    list_filter = ('gateway', 'authentication_type', 'consumed_at')


@admin.register(models.TaxRule)
class TaxRuleAdmin(admin.ModelAdmin):
    ordering = ('country', 'updated_at')
    list_display = (
        'updated_at',
        'country',
        'country_code',
        'formatted_rate',
        'note',
    )
    fieldsets = (
        (
            None,
            {
                'fields': (('updated_at', 'created_at'),),
            },
        ),
        (
            None,
            {
                'fields': (('country', 'product_type'), 'rate', 'note'),
            },
        ),
    )
    readonly_fields = ('created_at', 'updated_at')
    search_fields = ('country', 'rate')

    def get_readonly_fields(self, request, obj=None):
        """Make country and product type read-only for existing records."""
        readonly_fields = super().get_readonly_fields(request, obj)
        if obj:  # Editing an existing object
            return readonly_fields + ('country', 'product_type')
        return readonly_fields


@admin.register(models.TaxNumberVerification)
class TaxNumberVerificationAdmin(admin.ModelAdmin):
    ordering = ('number', '-updated_at')
    list_display = (
        'updated_at',
        'number',
        'is_valid',
        'response',
    )
    readonly_fields = ('created_at', 'updated_at', 'response')
    search_fields = ('number', 'response')


# Include reports, which are custom admin views
import looper.admin.reports  # noqa: F401,E402
