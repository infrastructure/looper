"""Defines proxy models used by custom admin views, such as reports."""
from django.urls import reverse
from django.utils.html import format_html

from looper import models
from looper.money import Money
from looper.taxes import Taxable, TaxType


class _DaysMixin:
    @property
    def days(self):
        return self._days

    @days.setter
    def days(self, value):
        self._days = value


class _PerPlanMixin:
    @property
    def plan(self):
        return self._plan

    @plan.setter
    def plan(self, value):
        self._plan = value


class _TitleMixin:
    @property
    def title(self):
        return self._title

    @title.setter
    def title(self, value):
        self._title = value


class _PerPlanVariationMixin:
    @property
    def plan_variation(self):
        return self._plan_variation

    @plan_variation.setter
    def plan_variation(self, value):
        self._plan_variation = value


class EUVatPerCountryReport(models.Order):
    class Meta:
        verbose_name_plural = '[Report] EU VAT per country'
        verbose_name = 'EU VAT'
        app_label = 'looper'
        proxy = True
        managed = False

    @property
    def country(self):
        return self.tax_country.name

    @property
    def country_code(self):
        return self.tax_country.code

    # Because this is a proxy model, using get-set properties
    # is the only way to add field-like attributes to it
    @property
    def revenue(self):
        return self._revenue

    @revenue.setter
    def revenue(self, value):
        self._revenue = value

    @property
    def refunds(self):
        return self._refunds

    @refunds.setter
    def refunds(self, value):
        self._refunds = value

    @property
    def vat_charged(self):
        if self.tax_rate is None:
            return self._vat_charged
        return Taxable(self.revenue, tax_rate=self.tax_rate, tax_type=TaxType.VAT_CHARGE).tax

    @vat_charged.setter
    def vat_charged(self, value):
        self._vat_charged = value

    @property
    def vat_refunded(self):
        if self.tax_rate is None:
            return self._vat_refunded
        return Taxable(self.refunds, tax_rate=self.tax_rate, tax_type=TaxType.VAT_CHARGE).tax

    @vat_refunded.setter
    def vat_refunded(self, value):
        self._vat_refunded = value

    @property
    def total_charged(self):
        return self.revenue - self.refunds

    @property
    def vat_total(self):
        return self.vat_charged - self.vat_refunded


class RevenuePerCountry(models.Order):
    class Meta:
        verbose_name_plural = '[Report] Revenue per country'
        verbose_name = 'Revenue'
        app_label = 'looper'
        proxy = True
        managed = False

    @property
    def country(self):
        return self.tax_country.name

    @property
    def country_code(self):
        return self.tax_country.code

    # Because this is a proxy model, using get-set properties
    # is the only way to add field-like attributes to it
    @property
    def revenue(self):
        return self._revenue

    @revenue.setter
    def revenue(self, value):
        self._revenue = value

    @property
    def refunds(self):
        return self._refunds

    @refunds.setter
    def refunds(self, value):
        self._refunds = value

    @property
    def total_charged(self):
        return self.revenue - self.refunds


class OrdersPerVATNumber(models.Order):
    class Meta:
        verbose_name_plural = '[Report] Orders per VAT number'
        verbose_name = 'VAT number'
        app_label = 'looper'
        proxy = True
        managed = False

    @property
    def country(self):
        return self.tax_country.code

    @property
    def orders(self):
        return self._orders

    @orders.setter
    def orders(self, value):
        self._orders = value

    @property
    def order_id(self):
        return format_html(
            ','.join(
                '<a href="{}">{}</a>'.format(
                    reverse('admin:looper_order_change', kwargs={'object_id': order.pk}), order.pk
                )
                for order in self.orders
            )
        )

    @property
    def revenue(self):
        return self._revenue

    @revenue.setter
    def revenue(self, value):
        self._revenue = value


class Revenue(_DaysMixin, _TitleMixin, models.Order):
    class Meta:
        verbose_name_plural = '[Report] Revenue by date'
        verbose_name = 'Revenue'
        app_label = 'looper'
        proxy = True
        managed = False

    @property
    def gross_revenue(self):
        return self._gross_revenue

    @gross_revenue.setter
    def gross_revenue(self, value):
        self._gross_revenue = value

    @property
    def taxes(self):
        return self._taxes

    @taxes.setter
    def taxes(self, value):
        self._taxes = value

    @property
    def avg_daily_gross_revenue(self):
        currency = self.gross_revenue.currency
        if not self.days:
            return Money(currency, 0)
        return Money(currency, round(self.gross_revenue._cents / self.days))

    @property
    def net_revenue(self):
        return self.gross_revenue - self.refunds - self.taxes

    @property
    def avg_daily_net_revenue(self):
        currency = self.net_revenue.currency
        if not self.days:
            return Money(currency, 0)
        return Money(currency, round(self.net_revenue._cents / self.days))

    @property
    def orders_paid(self):
        return self._orders_paid

    @orders_paid.setter
    def orders_paid(self, value):
        self._orders_paid = value

    @property
    def orders_refunded(self):
        return self._orders_refunded

    @orders_refunded.setter
    def orders_refunded(self, value):
        self._orders_refunded = value

    @property
    def refunds(self):
        return self._refunds

    @refunds.setter
    def refunds(self, value):
        self._refunds = value


class RevenuePerPlanAndProduct(_PerPlanMixin, Revenue):
    class Meta:
        verbose_name_plural = '[Report] Revenue per plan and product'
        verbose_name = 'Revenue'
        app_label = 'looper'
        proxy = True
        managed = False


class RevenuePerPlanVariation(_PerPlanVariationMixin, Revenue):
    class Meta:
        verbose_name_plural = '[Report] Revenue per plan variation'
        verbose_name = 'Revenue'
        app_label = 'looper'
        proxy = True
        managed = False


class SubscriptionsCount(_PerPlanVariationMixin, _DaysMixin, models.Subscription):
    class Meta:
        verbose_name_plural = '[Report] Subscriptions created by date'
        verbose_name = 'Subscriptions'
        app_label = 'looper'
        proxy = True
        managed = False

    @property
    def count(self):
        return self._count

    @count.setter
    def count(self, value):
        self._count = value


class SubscriptionsCancelledCount(SubscriptionsCount):
    class Meta:
        verbose_name_plural = '[Report] Subscriptions cancelled by date'
        verbose_name = 'Subscriptions'
        app_label = 'looper'
        proxy = True
        managed = False
