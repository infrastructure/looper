from collections import namedtuple
from operator import attrgetter
from typing import Optional
import json
import logging

from colorhash import ColorHash
from django.contrib import admin
from django.contrib.admin.filters import AllValuesFieldListFilter
from django.contrib.auth import get_permission_codename
from django.contrib.auth import get_user_model
from django.core.serializers.json import DjangoJSONEncoder
from django.db.models import Sum, Count, Case, When, Q
from django.db.models.functions import TruncDay
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.utils import timezone
from django.utils.html import format_html
import django.db.models.lookups

from .filters import (
    EmptyFieldListFilter,
    ChoicesFieldNoAllListFilter,
    ChoicesFieldListWithEmptyFilter,
)
from .mixins import NoChangeMixin, NoAddDeleteMixin, ExportCSVMixin
from .proxy_models import (
    EUVatPerCountryReport,
    OrdersPerVATNumber,
    Revenue,
    RevenuePerCountry,
    RevenuePerPlanAndProduct,
    RevenuePerPlanVariation,
    SubscriptionsCount,
    SubscriptionsCancelledCount,
)
from looper.money import Money
from looper.templatetags.looper import pricing, renewal_period
import looper.decorators
import looper.models
import looper.taxes

User = get_user_model()
logger = logging.getLogger(__name__)

# A stub type for displaying readable plan variation renewals without PlanVariation present
_variation_renewal_period = namedtuple('PlanRenewalPeriod', ['interval_unit', 'interval_length'])


def _hex2rgb(value: str) -> tuple:
    value = value[1:].lower()
    return (int(value[:2], 16), int(value[2:4], 16), int(value[4:], 16))


class Report(NoChangeMixin, NoAddDeleteMixin, ExportCSVMixin, admin.ModelAdmin):
    class Media:
        css = {"all": ("css/looper_admin.css",)}

    list_display_links = None
    change_list_template = 'admin/looper/reports/report.html'
    date_hierarchy_default_period = 'year'

    def has_view_permission(self, request, obj=None):
        """Inherit permission from the parent Order model.

        Proxy models require new permissions to be created, they don't
        inherit parent model's permissions.
        See https://code.djangoproject.com/ticket/11154 for more details.
        """
        opts = looper.models.Order._meta
        codename = get_permission_codename('view', opts)
        return request.user.has_perm("%s.%s" % (opts.app_label, codename))

    def get_currency(self, queryset) -> Optional[str]:
        """Try to figure out the currency from the request."""
        for lookup in queryset.query.where.children:
            if not isinstance(lookup, django.db.models.lookups.Exact):
                continue
            if not hasattr(lookup.lhs, 'target'):
                continue
            if not lookup.lhs.target.name == 'currency':
                continue
            return lookup.rhs
        return None

    def changelist_view(self, request, extra_context=None):
        """We can only report per currency, so make sure currency filter is on."""
        referrer = request.META.get('HTTP_REFERER', '')
        no_filters = (
            len(request.GET) == 0 and '?' not in referrer or (
                len(request.GET) == 1 and 'e' in request.GET
            )
        )
        if no_filters:
            now = timezone.now()
            year = now.year
            get_params = f'currency__exact=EUR&{self.date_hierarchy}__year={year}'
            if self.date_hierarchy_default_period == 'quarter':
                quarter = (now.month - 1) // 3 + 1
                get_params += f'&{self.date_hierarchy}__quarter={quarter}'
            return redirect(f'{request.path}?{get_params}')
        return super().changelist_view(request, extra_context=extra_context)

    def get_report_lines(self, queryset):
        """Return a list of proxy models that will be displayed instead of the changelist."""
        raise NotImplementedError('Report must implement its own get_report_lines method')

    def get_changelist_instance(self, request):
        """Monkey-patch changelist replacing the queryset with the report lines."""
        try:
            cl = super().get_changelist_instance(request)
            report = self.get_report_lines(cl.queryset)
            cl.result_list = report
            cl.result_count = len(report)
            cl.can_show_all = True
            cl.multi_page = False
            cl.title = cl.opts.verbose_name_plural
            cl.date_hierarchy_with_quarters = getattr(self, 'date_hierarchy_with_quarters', False)
            # cl.all_filters = self.get_all_filters(cl.queryset)

            # Make chart data accessible via ChangeList.
            if getattr(self, 'chart', None):
                cl.chart = self.chart
            return cl
        except Exception as e:
            logger.exception(e)
            raise


@admin.register(EUVatPerCountryReport)
class EUVatPerCountryReportAdmin(Report):
    list_display = (
        'country',
        'country_code',
        'tax_rate',
        'revenue',
        'refunds',
        'vat_charged',
        'vat_refunded',
        'total_charged',
        'vat_total',
    )
    list_filter = ('is_legacy', ('currency', ChoicesFieldNoAllListFilter))
    ordering = ('tax_country',)
    date_hierarchy = 'paid_at'
    date_hierarchy_with_quarters = True

    def get_queryset(self, request):
        """Apply additional filters to the report's queryset."""
        queryset = super().get_queryset(request)
        return queryset.filter(
            paid_at__isnull=False
        ).filter(
            Q(tax_type=looper.taxes.TaxType.VAT_CHARGE.value) |
            Q(tax__gt=0)
        )

    def get_report_lines(self, queryset):
        """Return a list of proxy models that will be displayed instead of the changelist."""
        currency = self.get_currency(queryset)
        per_country = {}
        for order in queryset.all():
            key = (order.tax_country, order.tax_rate, order.currency)
            if key not in per_country:
                per_country[key] = []
            row = per_country[key]
            row.append(order)
        report_lines = [
            EUVatPerCountryReport(
                tax_country=key[0],
                tax_rate=key[1],
                revenue=sum((_.price for _ in orders), Money(currency, 0)),
                refunds=sum((_.refunded for _ in orders), Money(currency, 0)),
            )
            for key, orders in per_country.items()
        ]
        report_lines.append(
            EUVatPerCountryReport(
                tax_country=format_html('<b>Total</b>'),
                tax_rate=None,
                revenue=sum((_.revenue for _ in report_lines), Money(currency, 0)),
                refunds=sum((_.refunds for _ in report_lines), Money(currency, 0)),
                vat_charged=sum((_.vat_charged for _ in report_lines), Money(currency, 0)),
                vat_refunded=sum((_.vat_refunded for _ in report_lines), Money(currency, 0)),
            )
        )
        return report_lines


@admin.register(RevenuePerCountry)
class RevenuePerCountryAdmin(Report):
    list_display = (
        'country',
        'country_code',
        'revenue',
        'refunds',
        'total_charged',
    )
    list_filter = ('is_legacy', ('currency', ChoicesFieldNoAllListFilter))
    ordering = ('tax_country',)
    date_hierarchy = 'paid_at'
    date_hierarchy_with_quarters = True

    def get_queryset(self, request):
        """Apply additional filters to the report's queryset."""
        queryset = super().get_queryset(request)
        return queryset.filter(paid_at__isnull=False)

    def get_report_lines(self, queryset):
        """Return a list of proxy models that will be displayed instead of the changelist."""
        currency = self.get_currency(queryset)
        per_country = {}
        for order in queryset.all():
            key = (order.tax_country, order.currency)
            if key not in per_country:
                per_country[key] = []
            row = per_country[key]
            row.append(order)
        report_lines = [
            RevenuePerCountry(
                tax_country=key[0],
                revenue=sum((_.price for _ in orders), Money(currency, 0)),
                refunds=sum((_.refunded for _ in orders), Money(currency, 0)),
            )
            for key, orders in per_country.items()
        ]
        report_lines.append(
            RevenuePerCountry(
                tax_country=format_html('<b>Total</b>'),
                revenue=sum((_.revenue for _ in report_lines), Money(currency, 0)),
                refunds=sum((_.refunds for _ in report_lines), Money(currency, 0)),
            )
        )
        return report_lines


@admin.register(OrdersPerVATNumber)
class OrdersPerVATNumberAdmin(Report):
    list_display = ('order_id', 'country', 'vat_number', 'revenue')
    list_filter = (
        'is_legacy',
        ('currency', ChoicesFieldNoAllListFilter),
        ('tax_country', AllValuesFieldListFilter),
    )
    ordering = ('tax_country', 'vat_number')
    date_hierarchy = 'paid_at'
    date_hierarchy_with_quarters = True
    date_hierarchy_default_period = 'quarter'

    def get_queryset(self, request):
        """Apply additional filters to the report's queryset."""
        queryset = super().get_queryset(request)
        return (
            queryset.filter(
                paid_at__isnull=False,
                vat_number__isnull=False,
            )
            .exclude(vat_number='')
            .order_by('vat_number', 'pk')
        )

    def get_report_lines(self, queryset):
        """Return a list of proxy models that will be displayed instead of the changelist."""
        currency = self.get_currency(queryset)
        per_vat_number = {}
        for order in queryset.all():
            key = (order.tax_country, order.vat_number)
            if key not in per_vat_number:
                per_vat_number[key] = []
            row = per_vat_number[key]
            row.append(order)
        report_lines = [
            OrdersPerVATNumber(
                tax_country=key[0],
                vat_number=key[1],
                revenue=sum((_.price for _ in orders), Money(currency, 0)),
                orders=orders,
            )
            for key, orders in per_vat_number.items()
        ]
        report_lines.append(
            OrdersPerVATNumber(
                tax_country=format_html('<b>Total</b>'),
                vat_number='',
                revenue=sum((_.revenue for _ in report_lines), Money(currency, 0)),
                orders=[],
            )
        )
        return report_lines


@admin.register(Revenue)
class RevenueAdmin(Report):
    payment_method_colors = {
        ('bank', 'ba'): '#e5e5e5',
        ('braintree', 'cc'): '#FFD140',
        ('braintree', 'pa'): '#08287f',
        ('stripe', 'bancontact'): '#00B8D5',
        ('stripe', 'cc'): '#ff8c00',
        ('stripe', 'eps'): '#7aedff',
        ('stripe', 'giropay'): '#005401',
        ('stripe', 'ideal'): '#CC0066',
        ('stripe', 'link'): '#635BFF',
        ('stripe', 'pa'): '#0048ff',
        ('stripe', 'sepa_debit'): '#009600',
        ('stripe', 'sofort'): '#00f400',
    }
    payment_method_names = {
        'pa': 'PayPal',
        'cc': 'Credit card',
    }
    list_display = (
        'title',
        'gross_revenue',
        'refunds',
        'net_revenue',
        'orders_paid',
        'orders_refunded',
        'avg_daily_gross_revenue',
        'avg_daily_net_revenue',
        'taxes',
    )
    list_filter = (
        'is_legacy',
        'currency',
        'paid_at',
        'collection_method',
        ('payment_method__method_type', ChoicesFieldListWithEmptyFilter),
        ('subscription', EmptyFieldListFilter),
        ('customer__user', EmptyFieldListFilter),
        ('tax_country', AllValuesFieldListFilter),
    )
    date_hierarchy = 'paid_at'
    date_hierarchy_with_quarters = True
    change_list_template = 'admin/looper/reports/report_with_chart.html'

    def _format_title(self, value) -> str:
        if value[1] == 'ba':
            return 'Bank transfer'
        name = self.payment_method_names.get(value[1], value[1].upper())
        return f'{value[0].capitalize()}: {name}'

    def _row_color(self, method):
        if method in self.payment_method_colors:
            hex_color = self.payment_method_colors[method]
            return _hex2rgb(hex_color)
        return row_color(method)

    def aggregate_revenue(self, queryset):
        """Aggregate and annotate a given queryset with values needed for displaying the revenue."""
        return queryset.annotate(
            date=TruncDay(self.date_hierarchy)
        ).values('date').annotate(
            y=Sum('price') / 100,
            gross_revenue=Sum('price'),
            refunds=Sum('refunded'),
            taxes=Sum('tax'),
            orders_paid=Count('id'),
            orders_refunded=Count(Case(When(refunded__gt=0, then=1))),
        ).order_by('-date')

    def get_form(self, request, obj=None, **kwargs):
        """Add help texts to the report's fields. Currently unused."""
        help_texts = {
            'gross_revenue': 'This is the sum of the order totals'
            ' after any refunds and including shipping and taxes.',
            'net_revenue': 'This is the sum of the order totals'
            ' after any refunds and excluding shipping and taxes.',
        }
        kwargs.update({'help_texts': help_texts})
        return super().get_form(request, obj, **kwargs)

    def get_queryset(self, request):
        """Apply additional filters to the report's queryset."""
        queryset = super().get_queryset(request)
        return queryset.filter(paid_at__isnull=False)

    def _construct_report_line(self, currency, per_date, title) -> Revenue:
        return Revenue(
            title=title,
            gross_revenue=Money(
                currency,
                sum(_['gross_revenue'] for _ in per_date),
            ),
            refunds=Money(
                currency,
                sum(_['refunds'] for _ in per_date),
            ),
            taxes=Money(
                currency,
                sum(_['taxes'] for _ in per_date),
            ),
            orders_paid=sum(_['orders_paid'] for _ in per_date),
            orders_refunded=sum(_['orders_refunded'] for _ in per_date),
            days=len(per_date),
        )

    def get_report_lines(self, queryset):
        """Return a list of proxy models that will be displayed instead of the changelist."""
        currency = self.get_currency(queryset)
        is_currency_ignored = currency is None
        gateways_by_id = {_.pk: _ for _ in looper.models.Gateway.objects.all()}
        payment_method_types = looper.models.PaymentMethod.objects.values_list(
            'gateway', 'method_type'
        ).distinct()
        rows = {
            (gateways_by_id[gw_id].name, m_type): list(self.aggregate_revenue(
                queryset.filter(
                    payment_method__gateway=gw_id,
                    payment_method__method_type=m_type,
                )
            ))
            for gw_id, m_type in payment_method_types
        }
        other_q = queryset
        for gw_id, m_type in payment_method_types:
            other_q = other_q.exclude(
                payment_method__gateway=gw_id, payment_method__method_type=m_type
            )
        other_per_date = list(self.aggregate_revenue(other_q))
        totals_per_date = list(self.aggregate_revenue(queryset))

        # When currency is not selected, EUR is used as a fallback
        currency = currency or 'EUR'

        report_lines = sorted([
            self._construct_report_line(currency, per_date, self._format_title((gw, m_type)))
            for (gw, m_type), per_date in rows.items()
            # Don't show lines with no orders
            if sum(_['orders_paid'] for _ in per_date) > 0
        ], key=attrgetter('title'))
        other = self._construct_report_line(currency, other_per_date, 'Other')
        total = self._construct_report_line(currency, totals_per_date, 'Total')

        color_rows = {method: self._row_color(method) for method in rows}
        chart_data = [
            {
                'data': totals_per_date,
                'label': self._format_title(p),
                'backgroundColor': f'rgba{color_rows[p]}',
            } for p, totals_per_date in rows.items()
        ]
        self.chart = {
            'datasets': json.dumps(chart_data, cls=DjangoJSONEncoder),
            'aggregate_by': self.date_hierarchy,
            'aggregate_by_prefix': Money(currency, 0).currency_symbol,
            'is_currency_ignored': is_currency_ignored,
        }
        # "Other" group might have orders in case order's payment method
        # no longer matches any known types, e.g. it was deleted.
        if other.orders_paid > 0:
            # Don't show lines with no orders
            return report_lines + [other, total]
        return report_lines + [total]


def plan_variation_title(pv) -> str:
    if isinstance(pv, str):
        return pv

    if isinstance(pv, tuple):
        interval_unit, interval_length, plan_name = pv
        _plan_interval = _variation_renewal_period(interval_unit, interval_length)
        return f'{plan_name}/{renewal_period(_plan_interval)}'
    return f'{pv.plan} ({pricing(pv)})'


def row_color(p):
    return ColorHash(str(p)).rgb


def plan_variation_color(pv):
    return ColorHash(plan_variation_title(pv)).rgb


@looper.decorators.short_description('Plan variation')
def plan_variation_legend(r: RevenuePerPlanVariation):
    pv = r._plan_variation
    return format_html(
        f'<span style="display: inline-block;width: 1em; height: 1em; margin-right: 0.5em;'
        f' background-color: rgb{plan_variation_color(pv)}"></span>'
        f'{plan_variation_title(pv)}'
    )


@admin.register(RevenuePerPlanAndProduct)
class RevenuePerPlanAndProductAdmin(RevenueAdmin):
    list_display = ('row_legend', ) + RevenueAdmin.list_display

    def row_title(self, r) -> str:
        if isinstance(r, str):
            return r
        return f'{r}'

    @looper.decorators.short_description('Plan')
    def row_legend(self, r: RevenuePerPlanAndProduct):
        p = r._plan
        return format_html(
            f'<span style="display: inline-block;width: 1em; height: 1em; margin-right: 0.5em;'
            f' background-color: rgb{row_color(p)}"></span>'
            f'{self.row_title(p)}'
        )

    def get_queryset(self, *args, **kwargs):
        """Prefetch subscription."""
        queryset = super().get_queryset(*args, **kwargs)
        return queryset.select_related('subscription', 'subscription__plan')

    def get_report_lines(self, queryset):
        """Return a list of proxy models that will be displayed instead of the changelist."""
        currency = self.get_currency(queryset)
        is_currency_ignored = currency is None
        plans = looper.models.Plan.objects.all()
        rows = {
            p: list(self.aggregate_revenue(
                queryset.filter(subscription__plan_id=p.id)
            ))
            for p in plans
        }
        products = looper.models.Product.objects.filter(plan__isnull=True)
        rows.update({
            p: list(self.aggregate_revenue(
                queryset.filter(subscription__isnull=True, product=p.id)
            ))
            for p in products
        })
        # Subscription orders that don't appear to belong to any existing plans:
        other_queryset = queryset
        for p in plans:
            other_queryset = other_queryset.exclude(subscription__plan_id=p.id)
        for p in products:
            other_queryset = other_queryset.exclude(subscription__isnull=True, product=p.id)
        if other_queryset.exists():
            rows['Other'] = list(self.aggregate_revenue(other_queryset))

        color_rows = {p: row_color(p) for p in rows}
        color_rows['Other'] = row_color('Other')

        chart_data = [
            {
                'data': totals_per_date,
                'label': self.row_title(pv),
                'backgroundColor': f'rgba{color_rows[pv]}',
            } for pv, totals_per_date in rows.items()
        ]
        # When currency is not selected, EUR is used as a fallback
        currency = currency or 'EUR'
        self.chart = {
            'datasets': json.dumps(chart_data, cls=DjangoJSONEncoder),
            'no_legend': True,
            'aggregate_by': self.date_hierarchy,
            'aggregate_by_prefix': Money(currency, 0).currency_symbol,
            'is_currency_ignored': is_currency_ignored,
        }

        report_lines = sorted([
            RevenuePerPlanAndProduct(
                title=str(p),
                currency=currency,
                plan=p,
                gross_revenue=Money(
                    currency,
                    sum(_['gross_revenue'] for _ in totals_per_date),
                ),
                refunds=Money(
                    currency,
                    sum(_['refunds'] for _ in totals_per_date),
                ),
                taxes=Money(
                    currency,
                    sum(_['taxes'] for _ in totals_per_date),
                ),
                orders_paid=sum(_['orders_paid'] for _ in totals_per_date),
                orders_refunded=sum(_['orders_refunded'] for _ in totals_per_date),
                days=len(totals_per_date),
            ) for p, totals_per_date in rows.items()
        ], key=attrgetter('gross_revenue'), reverse=True)

        # Append the total:
        report_lines.append(
            RevenuePerPlanAndProduct(
                title='Total',
                currency=currency,
                plan=format_html('<b>Total</b>'),
                gross_revenue=Money(
                    currency,
                    sum(_.gross_revenue._cents for _ in report_lines),
                ),
                refunds=Money(
                    currency,
                    sum(_.refunds._cents for _ in report_lines),
                ),
                taxes=Money(
                    currency,
                    sum(_.taxes._cents for _ in report_lines),
                ),
                orders_paid=sum(_.orders_paid for _ in report_lines),
                orders_refunded=sum(_.orders_refunded for _ in report_lines),
                days=sum(_.days for _ in report_lines),
            )
        )
        return report_lines


@admin.register(RevenuePerPlanVariation)
class RevenuePerPlanVariationAdmin(RevenueAdmin):
    list_display = (plan_variation_legend, ) + RevenueAdmin.list_display

    def get_queryset(self, *args, **kwargs):
        """Prefetch subscription."""
        queryset = super().get_queryset(*args, **kwargs)
        return queryset.select_related('subscription', 'subscription__plan')

    def get_report_lines(self, queryset):
        """Return a list of proxy models that will be displayed instead of the changelist."""
        currency = self.get_currency(queryset)
        is_currency_ignored = currency is None
        plan_variations = looper.models.PlanVariation.objects.order_by(
            'collection_method', 'interval_unit', 'interval_length', 'plan__name'
        ).select_related('plan')
        if currency:
            plan_variations = plan_variations.filter(currency=currency)
        plan_variations_keys = set()
        # Ignore currency when grouping by plan variations
        for pv in plan_variations:
            plan_variations_keys.add((
                pv.interval_unit,
                pv.interval_length,
                pv.plan.name,
            ))
        # Subscriptions don't link directly to plan variation, we can only "guess"
        # plan variation based on plan + interval_unit + interval_length
        # (optionally, +  currency)
        # collection_method is ignored because it can be changed by customer,
        # generating too many orders under category Other.
        per_plan_variation = {
            (interval_unit, interval_length, plan_name): list(self.aggregate_revenue(
                queryset.filter(
                    subscription__interval_unit=interval_unit,
                    subscription__interval_length=interval_length,
                    subscription__plan__name=plan_name,
                )
            ))
            for interval_unit, interval_length, plan_name in plan_variations_keys
        }
        # Subscription orders that don't appear to belong to any existing plan variations:
        other_queryset = queryset
        for interval_unit, interval_length, plan_name in plan_variations_keys:
            other_queryset = other_queryset.exclude(
                subscription__interval_unit=interval_unit,
                subscription__interval_length=interval_length,
                subscription__plan__name=plan_name,
            )
        if other_queryset.exists():
            per_plan_variation['Other'] = list(self.aggregate_revenue(other_queryset))

        color_per_plan_variation = {pv: plan_variation_color(pv) for pv in plan_variations_keys}
        color_per_plan_variation['Other'] = row_color('Other')

        chart_data = [
            {
                'data': totals_per_date,
                'label': plan_variation_title(pv),
                'backgroundColor': f'rgba{color_per_plan_variation[pv]}',
            } for pv, totals_per_date in per_plan_variation.items()
        ]
        # When currency is not selected, EUR is used as a fallback
        currency = currency or 'EUR'
        self.chart = {
            'datasets': json.dumps(chart_data, cls=DjangoJSONEncoder),
            'no_legend': True,
            'aggregate_by': self.date_hierarchy,
            'aggregate_by_prefix': Money(currency, 0).currency_symbol,
            'is_currency_ignored': is_currency_ignored,
        }
        report_lines = sorted([
            RevenuePerPlanVariation(
                title=str(pv),
                currency=currency,
                plan_variation=pv,
                gross_revenue=Money(
                    currency,
                    sum(_['gross_revenue'] for _ in totals_per_date),
                ),
                refunds=Money(
                    currency,
                    sum(_['refunds'] for _ in totals_per_date),
                ),
                taxes=Money(
                    currency,
                    sum(_['taxes'] for _ in totals_per_date),
                ),
                orders_paid=sum(_['orders_paid'] for _ in totals_per_date),
                orders_refunded=sum(_['orders_refunded'] for _ in totals_per_date),
                days=len(totals_per_date),
            ) for pv, totals_per_date in per_plan_variation.items()
        ], key=attrgetter('gross_revenue'), reverse=True)

        # Append the total:
        report_lines.append(
            RevenuePerPlanVariation(
                title='Total',
                currency=currency,
                plan_variation=format_html('<b>Total</b>'),
                gross_revenue=Money(
                    currency,
                    sum(_.gross_revenue._cents for _ in report_lines),
                ),
                refunds=Money(
                    currency,
                    sum(_.refunds._cents for _ in report_lines),
                ),
                taxes=Money(
                    currency,
                    sum(_.taxes._cents for _ in report_lines),
                ),
                orders_paid=sum(_.orders_paid for _ in report_lines),
                orders_refunded=sum(_.orders_refunded for _ in report_lines),
                days=sum(_.days for _ in report_lines),
            )
        )
        return report_lines


@admin.register(SubscriptionsCount)
class SubscriptionsCountReportAdmin(Report):
    list_display = (plan_variation_legend, 'count')
    list_filter = (
        'is_legacy',
        'currency',
        'collection_method',
        'status',
        ('tax_country', AllValuesFieldListFilter),
    )
    change_list_template = 'admin/looper/reports/report_with_chart.html'
    date_hierarchy = 'created_at'
    date_hierarchy_with_quarters = True
    changelist_url = reverse_lazy('admin:looper_subscription_changelist')

    def changelist_view(self, request, extra_context=None):
        """We can report for all currencies, because report is not summing any money.

        By default report is limited to current year, to avoid loading too much data.
        """
        referrer = request.META.get('HTTP_REFERER', '')
        year = timezone.now().year
        if len(request.GET) == 0 and '?' not in referrer or (
            len(request.GET) == 1 and 'e' in request.GET
        ):
            get_params = f'{self.date_hierarchy}__year={year}'
            return redirect(f'{request.path}?{get_params}')
        return super(Report, self).changelist_view(request, extra_context=extra_context)

    def count_subscriptions(self, queryset):
        """Annotate a given queryset with subscriptions counts per day."""
        return queryset.annotate(
            date=TruncDay(self.date_hierarchy)
        ).values('date').annotate(
            y=Count('id'),
        ).order_by('-date')

    def get_report_lines(self, queryset):
        """Return a list of proxy models that will be displayed instead of the changelist."""
        plan_variations = looper.models.PlanVariation.objects.select_related('plan').order_by(
            'currency', 'collection_method', 'interval_unit', 'interval_length', 'price'
        )

        def _make_filters(pv):
            return dict(
                currency=pv.currency,
                collection_method=pv.collection_method,
                interval_unit=pv.interval_unit,
                interval_length=pv.interval_length,
                price=pv.price,
            )
        # Subscriptions don't link directly to plan variation, we can only "guess"
        # plan variation based on collection_method+interval_unit+interval_length+currency
        per_plan_variation = {
            pv: list(self.count_subscriptions(queryset.filter(**_make_filters(pv))))
            for pv in plan_variations
        }

        # Subscription orders that don't appear to belong to any existing plan variations:
        other_queryset = queryset
        for pv in plan_variations:
            other_queryset = other_queryset.exclude(**_make_filters(pv))
        if other_queryset.exists():
            per_plan_variation['Other'] = list(self.count_subscriptions(other_queryset))

        color_per_plan_variation = {pv: plan_variation_color(pv) for pv in per_plan_variation}
        color_per_plan_variation['Other'] = row_color('Other')

        chart_data = [
            {
                'data': totals_per_date,
                'label': plan_variation_title(pv),
                'backgroundColor': f'rgba{color_per_plan_variation[pv]}',
            } for pv, totals_per_date in per_plan_variation.items()
        ]
        self.chart = {
            'datasets': json.dumps(chart_data, cls=DjangoJSONEncoder),
            'no_legend': True,
            'aggregate_by': self.date_hierarchy,
            'changelist_url': self.changelist_url,
        }
        report_lines = sorted([
            SubscriptionsCount(
                plan_variation=pv,
                count=sum(_['y'] for _ in totals_per_date),
                days=len(totals_per_date),
            ) for pv, totals_per_date in per_plan_variation.items()
        ], key=lambda _: _.count, reverse=True)
        return report_lines


@admin.register(SubscriptionsCancelledCount)
class SubscriptionsCancelledCountReportAdmin(SubscriptionsCountReportAdmin):
    date_hierarchy = 'pending_cancellation_since'

    def get_queryset(self, request):
        """Apply additional filters to the report's queryset."""
        queryset = super().get_queryset(request)
        return queryset.filter(pending_cancellation_since__isnull=False)
