from django import forms

from . import models, form_fields, form_mixins


class AddressForm(form_mixins.BlenderClassesFormMixin, forms.ModelForm):
    class Meta:
        model = models.Address
        exclude = ['category', 'customer', 'tax_exempt']


class BasePaymentForm(forms.Form):
    payment_method_nonce = forms.CharField(initial='set-in-javascript', widget=forms.HiddenInput())
    gateway = form_fields.GatewayChoiceField()
    device_data = forms.CharField(
        initial='set-in-javascript', widget=forms.HiddenInput(), required=False
    )

    # The following fields are used in the client-side 3D Secure flow
    # Price value is a decimal number in major units of selected currency.
    price = forms.CharField(widget=forms.HiddenInput(), required=False)
    email = forms.EmailField(required=True)


class BaseCheckoutForm(BasePaymentForm):
    # The CheckoutView will test whether the user has recently already created a subscription.
    # If that is the case, the user will get a warning about this, and will have to confirm
    # the creation of a new subscription.
    approve_new_subscription = forms.BooleanField(
        label='Another Membership',
        required=False,
        initial=False,
        widget=forms.HiddenInput(),
        help_text='',
    )

    # These are used when a payment fails, so that the next attempt to pay can reuse
    # the already-created subscription and order.
    subscription_pk = forms.CharField(widget=forms.HiddenInput(), required=False)
    order_pk = forms.CharField(widget=forms.HiddenInput(), required=False)

    def show_approve_new_subscription(self, help_text: str):
        """Show the 'approve_new_subscription' checkbox."""

        field = self.fields['approve_new_subscription']
        field.widget = form_fields.BootstrapCheckbox()
        field.help_text = help_text
        field.required = True


class CheckoutForm(BaseCheckoutForm, AddressForm):
    class Meta(AddressForm.Meta):
        pass

    gateway = form_fields.GatewayChoiceField(
        queryset=models.Gateway.objects.filter(name__in={'braintree', 'bank', 'mock'})
    )


class ChangePaymentMethodForm(CheckoutForm):
    # FIXME(anna): address and other fields used by front-end 3DSecure
    # could be in a separate form that isn't sent to the back-end
    class Meta(AddressForm.Meta):
        pass

    next_url_after_done = forms.CharField(widget=forms.HiddenInput())
