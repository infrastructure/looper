import abc
import enum
import json
import logging
from collections import namedtuple
from typing import (
    Any,
    Dict,
    Iterable,
    Optional,
    Set,
    Union,
    cast,
)

import attr
import braintree
import braintree.resource
from braintree.exceptions.not_found_error import NotFoundError
from django.conf import settings
import django.http.request
import stripe
import stripe.error

from looper.money import Money
import looper.exceptions

log = logging.getLogger(__name__)

MaybeConfiguredGateway = Union['AbstractPaymentGateway', 'GatewayNotConfigured']


class GatewayNotConfigured:
    """Dummy class that indicates a payment provider gateway isn't configured."""

    supports_transactions = False


class BraintreeError(looper.exceptions.GatewayError):
    import braintree.attribute_getter
    import braintree.error_result

    def __init__(self, braintree_error: braintree.error_result.ErrorResult) -> None:
        """Convert Braintree-specific errors into strings.

        Braintree returns errors as strings (i.e. ending in a period) which
        makes composition more difficult, so we strip trailing periods.
        """

        message = braintree_error.message.rstrip('.')
        code = ''
        if getattr(braintree_error, 'transaction', None):
            code = braintree_error.transaction.processor_response_code
        super().__init__(
            message,
            (
                self._nice_message(err, idx, message)
                for idx, err in enumerate(braintree_error.errors.deep_errors)
            ),
            code=code,
        )

    @staticmethod
    def _nice_message(
        braintree_error: braintree.attribute_getter.AttributeGetter,
        error_index: int,
        base_message: str,
    ) -> str:
        parts = [getattr(braintree_error, 'message', 'unknown error').rstrip('.')]
        if hasattr(braintree_error, 'code'):
            code = braintree_error.code

            if error_index == 0 and parts[0] == base_message:
                # This is common with Braintree, f.e. base_message='Payment method token is invalid'
                # and parts[0]='Payment method token is invalid'. In that case we just include
                # the error code and avoid repeating the same message.
                return f'code {code}'

            parts.append(f'(code {code})')
        return ' '.join(parts)


@attr.s(auto_attribs=True)
class AuthenticationDetailsThreeDSecure:
    acs_transaction_id: Optional[str]
    liability_shift_possible: Optional[bool]
    liability_shifted: Optional[bool]
    pares_status: Optional[str]


@attr.s(auto_attribs=True)
class AuthenticationInfo:
    class Type(enum.Enum):
        UNSUPPORTED = 'unsupported'
        THREE_D_S = 'three_d_s'

    authentication_id: str
    authentication_type: Type = Type.UNSUPPORTED
    status: Optional[str] = None
    version: Optional[str] = None
    # Currently there's only one known type of authentication, could be more in the future.
    details: Optional[Union[AuthenticationDetailsThreeDSecure]] = None

    @classmethod
    def from_braintree(
        cls, payment_method_nonce: braintree.resource.Resource
    ) -> Optional['AuthenticationInfo']:
        tds_info = payment_method_nonce.three_d_secure_info
        if not tds_info or not tds_info.three_d_secure_authentication_id:
            return None
        return AuthenticationInfo(
            authentication_id=tds_info.three_d_secure_authentication_id,
            authentication_type=cls.Type.THREE_D_S,
            status=tds_info.status,
            version=tds_info.three_d_secure_version,
            details=AuthenticationDetailsThreeDSecure(**{
                field: getattr(tds_info, field) for field in (
                    'acs_transaction_id',
                    'liability_shift_possible',
                    'liability_shifted',
                    'pares_status',
                ) if hasattr(tds_info, field)
            })
        )

    def type_for_database(self) -> str:
        """Type indication compatible with models.PaymentMethodAuthentication.TYPES

        :return: the three-letter code, or an empty string if unknown.
        """
        try:
            return {
                self.Type.THREE_D_S: 'tds',
            }[self.authentication_type]
        except KeyError:
            return ''

    def details_for_database(self) -> str:
        if not self.details:
            return ''
        try:
            return json.dumps(attr.asdict(self.details))
        except Exception:
            log.exception('Failed to serialise AuthenticationInfo details')
            return ''


@attr.s(auto_attribs=True)
class PaymentMethodInfo:
    class Type(enum.Enum):
        UNKNOWN = 'unknown'
        CREDIT_CARD = 'cc'
        PAYPAL_ACCOUNT = 'pa'
        BANK_ACCOUNT = 'ba'
        IDEAL = 'ideal'
        EPS = 'eps'
        GIROPAY = 'giropay'
        SOFORT = 'sofort'
        BANCONTACT = 'bancontact'
        SEPA_DEBIT = 'sepa_debit'
        ALIPAY = 'alipay'
        KLARNA = 'klarna'
        LINK = 'link'

    token: str
    method_type: Type = Type.UNKNOWN
    authentication: Optional[AuthenticationInfo] = None

    # Only for known payment types ('credit_card' and 'paypal_account')
    image_url: str = ''

    # Only when method_type in ('credit_card', 'ideal', 'bancontact', 'sofort', 'giropay')
    last_4: str = ''

    # Only when method_type = 'credit_card'
    expiration_date: str = ''  # probably 'MM/YYYY'
    card_type: str = ''

    # Only when method_type = 'paypal_account'
    email: str = ''

    # Only when method_type in ('ideal', sofort', 'giropay', 'bancontact')
    bic: str = ''

    _log = log.getChild('PaymentMethodInfo')

    @classmethod
    def from_braintree(cls, payment_method: braintree.resource.Resource) -> 'PaymentMethodInfo':
        info = PaymentMethodInfo(payment_method.token)
        if isinstance(payment_method, braintree.CreditCard):
            info.method_type = cls.Type.CREDIT_CARD
            info.last_4 = payment_method.last_4
            info.expiration_date = payment_method.expiration_date
            info.image_url = payment_method.image_url
            info.card_type = payment_method.card_type
        elif isinstance(payment_method, braintree.PayPalAccount):
            info.method_type = cls.Type.PAYPAL_ACCOUNT
            info.email = payment_method.email
            info.image_url = payment_method.image_url
        else:
            cls._log.warning(
                'Payment method %r is of unsupported class %r',
                payment_method,
                payment_method.__class__,
            )
        return info

    # TODO cleanup the token argument if possible, it should be contained in the Dict
    @classmethod
    def from_stripe(
        cls, token: str, payment_method: Dict[str, Any]
    ) -> 'PaymentMethodInfo':
        # Stripe appears to return meaningful details about the payment method only
        # in Charge.payment_method_details, not PaymentMethod (for most types of payments),
        # that's why payment token is passed separately not just read from payment_method.id
        # https://stripe.com/docs/api/charges/object#charge_object-payment_method_details
        info = PaymentMethodInfo(token)
        if payment_method['type'] == 'card':
            info.method_type = cls.Type.CREDIT_CARD
            card = payment_method['card']
            info.last_4 = card['last4']
            info.expiration_date = '{exp_month}/{exp_year}'.format(**card)
            info.card_type = card['brand']
        elif payment_method['type'] == 'paypal':
            info.method_type = cls.Type.PAYPAL_ACCOUNT
            info.email = payment_method['paypal']['payer_email']
        elif hasattr(cls.Type, payment_method['type'].upper()):
            info.method_type = cls.Type[payment_method['type'].upper()]
            details = payment_method.get(info.method_type.value)
            if details:
                for source_field, target_field in {
                    ('bic', 'bic'), ('iban_last4', 'last_4'), ('last4', 'last_4')
                }:
                    if details.get(source_field):
                        setattr(info, target_field, details[source_field])
        else:
            fallback_type = namedtuple('Type', ['value', 'name'])
            _method_type = payment_method.get('type', info.method_type)
            info.method_type = fallback_type(_method_type, _method_type.capitalize())
            cls._log.warning('Payment method %r is of unknown type', payment_method)
        return info

    def recognisable_name(self) -> str:
        """Construct a name so that customers can recognise the payment method."""
        if self.method_type == self.Type.CREDIT_CARD:
            return f'{self.card_type} credit card ending in {self.last_4}'
        if self.method_type == self.Type.PAYPAL_ACCOUNT:
            return f'PayPal account {self.email}'
        if self.method_type == self.Type.BANK_ACCOUNT:
            return 'Bank Transfer'
        if self.method_type in {
            self.Type.IDEAL,
            self.Type.GIROPAY,
            self.Type.SOFORT,
            self.Type.BANCONTACT,
            self.Type.SEPA_DEBIT,
        }:
            _title = f'{self.bic}'
            if self.last_4:
                _title += f'{"*" * 10}{self.last_4}'
            if self.method_type == self.Type.IDEAL:
                _title += ' via iDEAL'
            elif self.method_type == self.Type.SEPA_DEBIT:
                _title += ' via SEPA Direct Debit'
            else:
                _title += f' via {self.method_type.name}'
            return _title
        return ''

    def type_for_database(self) -> str:
        """Type indication compatible with models.PaymentMethod.METHOD_TYPES

        :return: non-empty string or an empty string if unknown.
        """
        return self.method_type.value


class Registry:
    """Contains an instance for each AbstractPaymentGateway instance.

    This is the primary means of obtaining an interface with a payment provider.
    """

    _log = log.getChild('Registry')
    _instances: Dict[str, MaybeConfiguredGateway] = {}  # initialized lazily
    """Mapping from payment gateway name to instance for that gateway."""

    @classmethod
    def gateway_names(cls) -> Iterable[str]:
        """Return names of the registered gateways."""
        if not cls._instances:
            cls._instantiate_gateways()
        return cls._instances.keys()

    @classmethod
    def gateway_names_supports_transactions(cls) -> Iterable[str]:
        """Return names of the registered gateways that support transactions."""
        if not cls._instances:
            cls._instantiate_gateways()
        return {name for name, gw in cls._instances.items() if gw.supports_transactions}

    @classmethod
    def instance_for(cls, gateway_name: str) -> 'AbstractPaymentGateway':
        if not cls._instances:
            cls._instantiate_gateways()

        try:
            gw_instance = cls._instances[gateway_name]
        except KeyError:
            raise looper.exceptions.GatewayNotImplemented(
                f'No such Gateway provider {gateway_name!r}'
            )

        if gw_instance is GatewayNotConfigured:
            raise looper.exceptions.GatewayConfigurationMissing(
                f'Gateway provider {gateway_name!r} not configured'
            )

        assert isinstance(gw_instance, AbstractPaymentGateway)
        return gw_instance

    @classmethod
    def _instantiate_gateways(cls) -> None:
        """Create one instance per AbstractPaymentGateway subclass."""
        cls._instances.clear()

        for gw_class in AbstractPaymentGateway.__subclasses__():
            assert issubclass(gw_class, AbstractPaymentGateway)
            assert gw_class is not AbstractPaymentGateway
            try:
                # Ignore the type here, because mypy thinks gw_class is an AbstractPaymentGateway
                # and complains we can't instantiate an abstract class here.
                gw_instance = gw_class()  # type: ignore
            except looper.exceptions.GatewayConfigurationMissing:
                cls._log.error('Gateway provider %r not configured', gw_class.gateway_name)
                # Ignore the type here because we cannot declare the type at the assignment
                # in the try-block.
                gw_instance = GatewayNotConfigured  # type: ignore
            cls._instances[gw_class.gateway_name] = gw_instance


class AbstractPaymentGateway(metaclass=abc.ABCMeta):
    gateway_name: str = ''  # set in each subclass.
    supports_refunds = True
    supports_transactions = True

    _log = log.getChild('AbstractPaymentGateway')

    def __init__(self) -> None:
        gateway_settings: Optional[object] = settings.GATEWAYS.get(self.gateway_name)
        if gateway_settings is None:
            raise looper.exceptions.GatewayConfigurationMissing(
                f'Missing settings for Gateway {self.gateway_name!r}'
            )

        if not isinstance(gateway_settings, dict):
            raise looper.exceptions.GatewayConfigurationError(
                f'Expected a dict for Gateway {self.gateway_name}, found: {gateway_settings}'
            )
        if 'supported_collection_methods' not in gateway_settings:
            raise looper.exceptions.GatewayConfigurationError(
                f'Gateway settings {self.gateway_name!r} must'
                ' provide "supported_collection_methods"'
            )

        self.settings: Dict[str, object] = cast(Dict[str, object], gateway_settings)

    def __init_subclass__(cls) -> None:
        assert cls.gateway_name, 'subclasses must set gateway_name'
        assert cls.supported_collection_methods, 'subclasses must set supported_collection_methods'
        super().__init_subclass__()
        cls._log = log.getChild(cls.__name__)

    @property
    def supported_collection_methods(self) -> Set[str]:
        """Retrieve supported collection methods from gateway settings."""
        return set(self.settings['supported_collection_methods'])

    @abc.abstractmethod
    def customer_create(self, customer_data: Optional[Dict[str, str]] = None) -> str:
        """Create a customer record at the payment provider.

        :return: The customer ID at the payment provider.
        :raise: GatewayError if the user cannot be created.
        """
        pass

    @abc.abstractmethod
    def payment_method_create(
        self,
        payment_method_nonce: str,
        gateway_customer_id: str,
        verification_data: Optional[Any] = None,
    ) -> PaymentMethodInfo:
        """Store a payment method for the specified customer.

        :return: The PaymentMethodInfo, which contains the payment method token.
        :raise: GatewayError if the payment method cannot be created.
        """
        pass

    @abc.abstractmethod
    def payment_method_delete(self, payment_method_token: str) -> None:
        """Delete a payment method for the specified customer."""
        pass

    @abc.abstractmethod
    def find_customer_payment_method(self, payment_method_token: str) -> PaymentMethodInfo:
        """Lookup a payment method and return it as a dictionary."""
        pass

    @abc.abstractmethod
    def transact_sale(
        self,
        payment_method_token: str,
        amount: Money,
        authentication_id: str = '',
        source: str = '',
        payment_intent_id: str = '',
        payment_intent_metadata: Optional[Dict[str, str]] = None,
    ) -> Dict:
        """Perform a sale transaction.

        :return: The transaction ID of the payment gateway.
        :raise looper.exceptions.CurrencyNotSupported: when the amount has a
            currency for which there is no configured Merchant Account ID.
        :raise looper.exceptions.GatewayError: when there was an error
            performing the transaction at the payment gateway.
        """
        pass

    @abc.abstractmethod
    def find_transaction(self, transaction_id: str) -> Dict[str, object]:
        """Lookup a transaction and return it as a dictionary."""
        pass

    @abc.abstractmethod
    def refund(self, gateway_transaction_id: str, amount: Money) -> None:
        """Refund a transaction.

        :param: gateway_transaction_id: the transaction ID on the gateway.
        :param amount: The amount to refund, must not be more than the original
            transaction amount.

        Note that the amount to refund is non-optional, contrary to, for
        example, the Braintree API. This is to make the amount explicit.
        In the unlikely case that Braintree has a different amount stored
        for the transaction than we have, we should be refunding the amount
        we expect to be refunding. If this is not possible, errors should be
        explicit.
        """
        pass


class MockableGateway(AbstractPaymentGateway):
    """Payment gateway that expects to be mocked.

    Every function will raise an exception. This allows functions to be mocked,
    and unexpected calls to non-mocked functions to be detected.
    """

    gateway_name: str = 'mock'
    _log = log.getChild('MockableGateway')

    # noinspection PyMissingConstructor
    def __init__(self) -> None:
        # Dont' call the superclass, it'll expect configuration which we don't need nor want.
        self.settings: Dict[str, object] = {'supported_collection_methods': {'automatic'}}

    def generate_client_token(
        self, for_currency: str, gateway_customer_id: Optional[str] = None
    ) -> str:
        raise NotImplementedError(
            f'Unexpected call to MockableGateway.generate_client_token({gateway_customer_id!r})'
        )

    def customer_create(self, customer_data: Optional[Dict[str, str]] = None) -> str:
        raise NotImplementedError('Unexpected call to MockableGateway.customer_create()')

    def payment_method_create(
        self,
        payment_method_nonce: str,
        gateway_customer_id: str,
        verification_data: Optional[Any] = None,
    ) -> PaymentMethodInfo:
        raise NotImplementedError(
            f'Unexpected call to MockableGateway.payment_method_create'
            f'({payment_method_nonce!r}, {gateway_customer_id!r})'
        )

    def payment_method_delete(self, payment_method_token: str) -> None:
        raise NotImplementedError(
            f'Unexpected call to MockableGateway.payment_method_delete({payment_method_token!r})'
        )

    def find_customer_payment_method(self, payment_method_token: str) -> PaymentMethodInfo:
        raise NotImplementedError(
            f'Unexpected call to MockableGateway.find_customer_payment_method'
            f'({payment_method_token!r})'
        )

    def transact_sale(
        self,
        payment_method_token: str,
        amount: Money,
        authentication_id: str = '',
        source: str = '',
        payment_intent_id: str = '',
        payment_intent_metadata=None,
    ) -> Dict:
        raise NotImplementedError(
            f'Unexpected call to MockableGateway.transact_sale'
            f'({payment_method_token!r}, {amount!r}, {authentication_id!r}, {source!r})'
        )

    def find_transaction(self, transaction_id: str) -> Dict[str, object]:
        raise NotImplementedError(
            f'Unexpected call to MockableGateway.find_transaction({transaction_id!r})'
        )

    def refund(self, gateway_transaction_id: str, amount: Money) -> None:
        raise NotImplementedError(
            f'Unexpected call to MockableGateway.refund({gateway_transaction_id!r}, {amount})'
        )


class BraintreeGateway(AbstractPaymentGateway):
    gateway_name = 'braintree'

    TRANSACTION_SUCCESS_STATUSES = [
        braintree.Transaction.Status.Authorized,
        braintree.Transaction.Status.Authorizing,
        braintree.Transaction.Status.Settled,
        braintree.Transaction.Status.SettlementConfirmed,
        braintree.Transaction.Status.SettlementPending,
        braintree.Transaction.Status.Settling,
        braintree.Transaction.Status.SubmittedForSettlement,
    ]

    def __init__(self) -> None:
        super().__init__()
        self.braintree = braintree.BraintreeGateway(
            braintree.Configuration(
                environment=self.settings['environment'],
                merchant_id=self.settings['merchant_id'],
                public_key=self.settings['public_key'],
                private_key=self.settings['private_key'],
            )
        )

    def _merchant_account_id(self, currency: str) -> str:
        """Return the Merchant Account ID for the given currency.

        Configured in Braintree at Account → Merchant Account Info.
        """
        merchant_account_ids = self.settings['merchant_account_ids']
        if not isinstance(merchant_account_ids, dict):
            raise looper.exceptions.GatewayConfigurationError(
                f'The field merchant_account_ids for Gateway {self.gateway_name}'
                f' should be a dict but found: {merchant_account_ids}'
            )

        merchant_account_ids = cast(Dict[str, str], merchant_account_ids)
        try:
            return merchant_account_ids[currency]
        except KeyError:
            self._log.error('Unsupported currency %r requested', currency)
            supported = set(merchant_account_ids.keys())
            raise looper.exceptions.CurrencyNotSupported(
                f'Currency {currency!r} not supported, only {supported}'
            )

    def generate_client_token(
        self, for_currency: str, gateway_customer_id: Optional[str] = None
    ) -> str:
        """Generate Braintree client token.

        Returns a string which contains all authorization and configuration
        information our client needs to initialize the Braintree Client SDK to
        communicate with Braintree.

        See https://developers.braintreepayments.com/reference/request/client-token/generate/python
        """

        params = {}
        if gateway_customer_id:
            params['customer_id'] = gateway_customer_id
        if for_currency:
            merchant_account_id = self._merchant_account_id(for_currency)
            params['merchant_account_id'] = merchant_account_id

        client_token = self.braintree.client_token.generate(params)
        assert isinstance(client_token, str)
        return client_token

    def customer_create(self, customer_data: Optional[Dict[str, str]] = None) -> str:
        """Create a customer at BrainTree.

        See https://developers.braintreepayments.com/reference/request/customer/create/python
        """
        self._log.debug('Creating new customer at Braintree')
        # Braintree insists on first name and last name, so, as a compromise,
        # we "improvise" them out of full names that we have,
        # in the same way it is done in gateways.js
        if customer_data.get('full_name'):
            full_name = customer_data.pop('full_name')
            if full_name:
                customer_data['first_name'] = ' '.join(full_name.split(' ')[:-1])
                customer_data['last_name'] = full_name.split(' ')[-1]
        result = self.braintree.customer.create(customer_data or {})
        if not result.is_success:
            self._log.debug(
                'Error creating new customer: message=%s  errors=%s', result.message, result.errors
            )
            raise BraintreeError(result)
        customer_id = result.customer.id
        assert isinstance(customer_id, str)
        return customer_id

    def payment_method_create(
        self,
        payment_method_nonce: str,
        gateway_customer_id: str,
        verification_data: Optional[Any] = None,
    ) -> PaymentMethodInfo:
        """Create a payment method at BrainTree.

        :param verification_data: `device_data` to be passed to Payment Method: Create.

        See https://developers.braintreepayments.com/reference/request/payment-method/create/python
        """
        # Retrieve authentication ID, if available, to be used in the transaction later
        try:
            nonce_info = self.braintree.payment_method_nonce.find(payment_method_nonce)
        except NotFoundError as e:
            self._log.exception('Error retrieving payment method nonce')
            raise looper.exceptions.GatewayError(str(e))

        authentication_info = AuthenticationInfo.from_braintree(nonce_info)

        # **N.B.**: 3DS stored with card verifications produced by "payment_method.create()" or
        # "verifyCard()" calls are reserved for metered billing,
        # which is not the same use case as our custom recurring billing.
        # Passing "verify_card: false" prevents the SCA acquired in the previous step
        # from being consumed during payment_method.create().
        # The SCA instead **must** be passed in the transaction.sale() call later.
        verify_card = authentication_info is None
        result = self.braintree.payment_method.create(
            {
                'customer_id': gateway_customer_id,
                'payment_method_nonce': payment_method_nonce,
                'options': {
                    'make_default': True,
                    'verify_card': verify_card,
                },
                'device_data': verification_data,
            }
        )
        if not result.is_success:
            self._log.debug(
                'Error creating payment method: message=%s  errors=%s',
                result.message,
                result.errors,
            )
            raise BraintreeError(result)

        pm_info = PaymentMethodInfo.from_braintree(result.payment_method)
        pm_info.authentication = authentication_info
        return pm_info

    def payment_method_delete(self, payment_method_token: str) -> None:
        """Delete a payment method at BrainTree.

        See https://developers.braintreepayments.com/reference/request/payment-method/delete/python
        """
        from braintree.exceptions import not_found_error, braintree_error

        try:
            result = self.braintree.payment_method.delete(payment_method_token)
        except not_found_error.NotFoundError:
            # Deleting something that isn't there is fine.
            self._log.debug('Error deleting payment method, token was unknown at Braintree.')
            return
        except braintree_error.BraintreeError as ex:
            self._log.info('Error deleting payment token: %s', ex)
            raise looper.exceptions.GatewayError(message=str(ex))

        if not result.is_success:
            self._log.debug(
                'Error deleting payment method: message=%s  errors=%s',
                result.message,
                result.errors,
            )
            raise BraintreeError(result)

    def find_customer_payment_method(self, payment_method_token: str) -> PaymentMethodInfo:
        from braintree.exceptions.not_found_error import NotFoundError

        try:
            payment_method = self.braintree.payment_method.find(payment_method_token)
        except NotFoundError:
            raise looper.exceptions.NotFoundError('Payment gateway not found')

        return PaymentMethodInfo.from_braintree(payment_method)

    def transact_sale(
        self,
        payment_method_token: str,
        amount: Money,
        authentication_id: str = '',
        source: str = '',
        payment_intent_id: str = '',
        payment_intent_metadata=None,
    ) -> Dict:
        """Perform a sale by requesting a monetary amount to be paid.

        See https://developers.braintreepayments.com/reference/request/transaction/sale/python

        :return: The transaction ID of the payment gateway.
        :raise looper.exceptions.CurrencyNotSupported: when the amount has a
            currency for which there is no configured Merchant Account ID.
        :raise looper.exceptions.GatewayError: when there was an error
            performing the transaction at Braintree.
        """
        self._log.debug('Performing transaction of %s', amount)
        ma_id = self._merchant_account_id(amount.currency)

        params = {
            'payment_method_token': payment_method_token,
            'amount': amount.decimals_string,
            'merchant_account_id': ma_id,
            'options': {
                'submit_for_settlement': True,
            },
        }
        if source:
            params['transaction_source'] = source
        if authentication_id:
            params['three_d_secure_authentication_id'] = authentication_id
        result = self.braintree.transaction.sale(params)
        if not result.is_success:
            raise BraintreeError(result)

        self._log.info(
            'Transaction %r (recurring: %r, %s) for amount %s was successful with status=%r',
            result.transaction.id,
            result.transaction.recurring,
            result.transaction.sca_exemption_requested or 'no SCA exemption',
            amount,
            result.transaction.status,
        )
        sca_info = result.transaction.three_d_secure_info
        if sca_info:
            self._log.info(
                'Transaction %r 3DS v%s, status: %s, liability shift possible: %r, shifted: %r',
                result.transaction.id,
                sca_info.three_d_secure_version,
                sca_info.status,
                sca_info.liability_shift_possible,
                sca_info.liability_shifted,
            )
        transaction_id = result.transaction.id
        assert isinstance(transaction_id, str)
        return {
            'transaction_id': transaction_id,
        }

    def find_transaction(self, transaction_id: str) -> Dict[str, object]:
        transaction = self.braintree.transaction.find(transaction_id)
        return {'transaction_id': transaction.id}

    def refund(self, gateway_transaction_id: str, amount: Money) -> None:
        """Refund a transaction.

        :param: gateway_transaction_id: the transaction ID on the gateway.
        :param amount: The amount to refund, may not be more than the original
            transaction amount.

        See https://developers.braintreepayments.com/reference/request/transaction/refund/python
        """
        from braintree.exceptions import not_found_error, braintree_error

        self._log.debug('Refunding transaction %r for %s', gateway_transaction_id, amount)
        try:
            result = self.braintree.transaction.refund(
                gateway_transaction_id, amount.decimals_string
            )
        except not_found_error.NotFoundError as ex:
            self._log.debug(
                'Error refunding, transaction %r was unknown at Braintree: %s',
                gateway_transaction_id,
                ex,
            )
            raise looper.exceptions.GatewayError(
                message=f'transaction {gateway_transaction_id} was unknown at Braintree'
            )
        except braintree_error.BraintreeError as ex:
            self._log.info('Error refunding transaction %r: %s', gateway_transaction_id, ex)
            raise looper.exceptions.GatewayError(message=str(ex))

        if not result.is_success:
            raise BraintreeError(result)

        self._log.info(
            'Transaction %r refund for amount %s was successful', gateway_transaction_id, amount
        )


class StripeGateway(AbstractPaymentGateway):
    gateway_name = 'stripe'

    def __init__(self) -> None:
        super().__init__()
        stripe.api_key = self.settings['api_secret_key']
        self.endpoint_secret = self.settings['endpoint_secret']

    def construct_event_from_request(
        self, request: 'django.http.request.HttpRequest'
    ) -> 'stripe.api_resources.Event':
        """Verify webhook payload and return an Event object constructed from request body."""
        payload = request.body
        sig_header = request.META['HTTP_STRIPE_SIGNATURE']
        return stripe.Webhook.construct_event(payload, sig_header, self.endpoint_secret)

    def construct_event(self, payload) -> 'stripe.api_resources.Event':
        """
        Return Event object constructed from given payload.

        When handling webhook events must only be used after request signature was verified.
        """
        return stripe.Event.construct_from(payload, self.endpoint_secret)

    def customer_create(self, customer_data: Optional[Dict[str, str]] = None) -> str:
        """Create a customer record at the payment provider.

        :return: The customer ID at the payment provider.
        """
        customer = stripe.Customer.create(
            email=customer_data['email'],
            name=customer_data['full_name'],
        )
        return customer.id

    def payment_method_create(
        self,
        payment_method_nonce: str,
        gateway_customer_id: str,
        verification_data: Optional[Any] = None,
    ) -> PaymentMethodInfo:
        """Not supposed to be called directly for Stripe, a proper payment method setup happens via
        a checkout session.
        """
        raise NotImplementedError('Unexpected call: use PaymentIntent or SetupIntent')

    def payment_method_delete(self, payment_method_token: str) -> None:
        """Disable a payment method for the specified customer.
        See https://stripe.com/docs/api/payment_methods/detach
        Stripe doesn't have an actual delete method (which makes sense, since you may have
        historical payments referencing the payment method.
        Once detached, a payment method is unusable, that's exactly what we want.
        """
        try:
            stripe.PaymentMethod.detach(payment_method_token)
        except stripe.error.InvalidRequestError as e:
            if 'is not attached' in str(e):
                log.warning('Unable to detach PaymentMethod: it is not attached to a customer')
                return
            raise

    def find_customer_payment_method(self, payment_method_token: str) -> PaymentMethodInfo:
        """Lookup a payment method and return it as a dictionary."""
        return PaymentMethodInfo.from_stripe(
            payment_method_token,
            stripe.PaymentMethod.retrieve(payment_method_token),
        )

    def transact_sale(
        self,
        payment_method_token: str,
        amount: Money,
        authentication_id: str = '',
        source: str = '',
        payment_intent_id: str = '',
        payment_intent_metadata=None,
    ) -> Dict:
        """Perform a sale transaction.

        :return: The transaction ID of the payment gateway.
        :raise looper.exceptions.GatewayError: when there was an error
            performing the transaction at the payment gateway.
        """

        # dirty hack to supply customer
        # we don't record a relation between gateway_customer_id and payment_method, but should
        payment_method = stripe.PaymentMethod.retrieve(payment_method_token)
        customer = payment_method.customer

        # if this is not the first attempt to pay for this order
        # we assume that the order hasn't changed, and the existing payment_intent's amount still
        # matches
        try:
            if payment_intent_id:
                payment_intent = stripe.PaymentIntent.confirm(
                    payment_intent_id,
                    customer=customer,
                    metadata=payment_intent_metadata,
                    off_session=True,
                    payment_method=payment_method_token,
                )
            else:
                payment_intent = stripe.PaymentIntent.create(
                    amount=amount.cents,
                    confirm=True,
                    currency=amount.currency.lower(),
                    customer=customer,
                    metadata=payment_intent_metadata,
                    off_session=True,
                    payment_method=payment_method_token,
                )
        except Exception as ex:
            raise looper.exceptions.GatewayError(message=str(ex))

        return {
            'payment_intent_id': payment_intent.id,
            'transaction_id': payment_intent.latest_charge,
        }

    def find_transaction(self, transaction_id: str) -> Dict[str, object]:
        """Lookup a transaction and return it as a dictionary."""
        charge = stripe.Charge.retrieve(transaction_id)
        return {'transaction_id': charge.id}

    def refund(self, gateway_transaction_id: str, amount: Money) -> None:
        """Refund a transaction.

        :param: gateway_transaction_id: the charge id in Stripe
        :param amount: The amount to refund, must not be more than the original
            transaction amount.

        Note that the amount to refund is non-optional, contrary to, for
        example, the Braintree API. This is to make the amount explicit.
        In the unlikely case that Braintree has a different amount stored
        for the transaction than we have, we should be refunding the amount
        we expect to be refunding. If this is not possible, errors should be
        explicit.
        """
        try:
            stripe.Refund.create(charge=gateway_transaction_id, amount=amount.cents)
        except stripe.error.InvalidRequestError as e:
            if getattr(e, 'code', '') == 'charge_already_refunded':
                log.warning('Transaction ID %s has already been refunded', gateway_transaction_id)
                return
            raise

    def retrieve_payment_intent(self, object_id: str) -> 'stripe.api_resources.PaymentIntent':
        return stripe.PaymentIntent.retrieve(
            object_id,
            expand=[
                'customer',
                'latest_charge',
                'latest_charge.payment_method_details',
                'payment_method',
            ],
        )

    def retrieve_checkout_session_expanded(
        self, object_id: str
    ) -> 'stripe.api_resources.checkout.session.Session':
        return stripe.checkout.Session.retrieve(
            object_id,
            expand=[
                'payment_intent',
                'payment_intent.customer',
                'payment_intent.latest_charge',
                'payment_intent.latest_charge.payment_method_details',
                'payment_intent.payment_method',
            ],
        )


class BankGateway(AbstractPaymentGateway):
    """Payment gateway to support bank payments.

    Requires manual intervention in the admin to mark orders as paid.
    """

    gateway_name: str = 'bank'
    supports_refunds = False
    supports_transactions = False
    _log = log.getChild('BankGateway')

    def generate_client_token(
        self, for_currency: str, gateway_customer_id: Optional[str] = None
    ) -> str:
        return ''

    def customer_create(self, customer_data: Optional[Dict[str, str]] = None) -> str:
        """Always return the same meaningless customer ID."""
        return 'bank'

    def payment_method_create(
        self,
        payment_method_nonce: str,
        gateway_customer_id: str,
        verification_data: Optional[Any] = None,
    ) -> PaymentMethodInfo:
        """Always return the same PaymentMethodInfo"""
        return PaymentMethodInfo(None, PaymentMethodInfo.Type.BANK_ACCOUNT)

    def payment_method_delete(self, payment_method_token: str) -> None:
        """Does nothing, as the gateway itself (that is, the bank) does nothing."""

    def find_customer_payment_method(self, payment_method_token: str) -> PaymentMethodInfo:
        """Always return the same PaymentMethodInfo"""
        return PaymentMethodInfo('bank', PaymentMethodInfo.Type.BANK_ACCOUNT)

    def transact_sale(
        self,
        payment_method_token: str,
        amount: Money,
        authentication_id: str = '',
        source: str = '',
        payment_intent_metadata=None,
    ) -> Dict:
        raise NotImplementedError(
            f'Unexpected call to BankGateway.transact_sale'
            f'({payment_method_token!r}, {amount!r}, {authentication_id!r}, {source!r})'
        )

    def find_transaction(self, transaction_id: str) -> Dict[str, object]:
        raise NotImplementedError(
            f'Unexpected call to BankGateway.find_transaction({transaction_id!r})'
        )

    def refund(self, gateway_transaction_id: str, amount: Money) -> None:
        raise NotImplementedError(
            f'Unexpected call to BankGateway.refund({gateway_transaction_id!r}, {amount})'
        )
