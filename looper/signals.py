from django.dispatch import Signal

# Sender is the Subscription.
subscription_activated = Signal()  # Providing args: ['old_status']
subscription_deactivated = Signal()  # Providing args: ['old_status']
subscription_expired = Signal()  # Providing args: ['old_status']
# Sent when next_payment < now and last_notification < next_payment:
managed_subscription_notification = Signal()

# Sender is the Order that's being processed.
automatic_payment_succesful = Signal()  # Providing args: ['transaction']
automatic_payment_soft_failed = Signal()  # Providing args: ['transaction']
automatic_payment_failed = Signal()  # Providing args: ['transaction']
automatic_payment_soft_failed_no_payment_method = Signal()
automatic_payment_failed_no_payment_method = Signal()
