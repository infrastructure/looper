from typing import Optional


class BlenderClassesFormMixin:
    """Adds the CSS classes we need to Django forms."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = f'field-{field_name}'

    def order_fields(self, field_order):
        fields = super().order_fields(['email'])
        return fields

    def __getitem__(self, field_name: str):
        """Ugly hack to get custom CSS classes in the row container element.

        This will break when Django changes its internal structure.
        """
        from django.forms import BoundField

        # BaseForm._html_output() uses `self[field_name]` to obtain the bound field.
        # To hook into that. we override __getitem__().
        bound_field = super().__getitem__(field_name)  # type: ignore
        if not isinstance(bound_field, BoundField):
            return bound_field

        # `bound_field.css_classes()` is used by BaseForm._html_output() to obtain the
        # CSS classes that are used by the <li> element (when using form.as_ul()).
        # Fortunately it accepts extra CSS classes, but unfortunately we cannot pass
        # anything to the call. Instead, we replace that function with our own, and
        # inject the extra classes there.
        orig_css_classes = bound_field.css_classes

        def css_classes(extra_class: Optional[str] = None) -> str:
            widget_classes: str = bound_field.field.widget.attrs.get('class', '')
            if extra_class:
                widget_classes = f'{widget_classes} {extra_class}'
            return orig_css_classes(widget_classes)

        bound_field.css_classes = css_classes
        return bound_field
