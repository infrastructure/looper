from django.urls import path

from .views import checkout, checkout_stripe, settings, preferred_currency

app_name = 'looper'
urlpatterns = [
    path(
        'checkout/<int:plan_id>',
        checkout.ChooseDefaultPlanVariationView.as_view(pattern_name='looper:checkout'),
        name='checkout_new',
    ),
    path(
        'checkout/<int:plan_id>/choose/<int:current_plan_variation_id>',
        checkout.ChoosePlanVariationView.as_view(),
        name='checkout_choose_plan_variation',
    ),
    path(
        'checkout/<int:plan_id>/variation/<int:plan_variation_id>',
        checkout_stripe.CheckoutStripeView.as_view(),
        name='checkout',
    ),
    path(
        'stripe_success/<int:pk>/<stripe_session_id>',
        checkout_stripe.StripeSuccessView.as_view(),
        name='stripe_success',
    ),
    path(
        'checkout/pay/<int:order_id>',
        checkout_stripe.CheckoutExistingOrderView.as_view(),
        name='checkout_existing_order',
    ),
    path('checkout/done/<int:transaction_id>', checkout.DoneView.as_view(), name='checkout_done'),
    path(
        'checkout/<gateway_name>/<int:pk>',
        checkout.TransactionlessCheckoutDoneView.as_view(),
        name='transactionless_checkout_done',
    ),
    path('settings/billing/', settings.BillingAddressView.as_view(), name='settings_billing_info'),
    path(
        'settings/billing/payment-methods/',
        settings.PaymentMethodsView.as_view(),
        name='payment_methods',
    ),
    path(
        'settings/billing/payment-methods/delete/<int:pk>',
        settings.PaymentMethodDeleteView.as_view(),
        name='payment_method_delete',
    ),
    # Note: this URL is used hardcodedly in scripts/tutti/02_currency.js
    path(
        'preferred-currency',
        preferred_currency.CurrencySelectorView.as_view(),
        name='preferred_currency',
    ),
]
