from decimal import Decimal
from typing import (
    Any,
    Dict,
    Iterable,
    List,
    Mapping,
    Optional,
    Set,
    Tuple,
    Type,
    Union,
    cast,
)
import datetime
import json
import logging
import re
import secrets

from django.contrib.auth import get_user_model
from django.core.serializers.json import DjangoJSONEncoder
from django.db import models
from django.db.models import Model, CheckConstraint, Q
from django.urls import reverse
from django.utils.html import format_html
from django_countries.fields import CountryField
import dateutil.relativedelta
import django.utils.timezone

from looper import admin_log, form_fields, gateways, model_mixins
from looper.decorators import requires_status
from looper.money import Money, CurrencyMismatch
from looper.taxes import TaxType, ProductType, Taxable, vies_validate_number
from looper.utils import format_percentage
import looper.exceptions
import looper.signals

User = get_user_model()
DEFAULT_CURRENCY = 'EUR'
CURRENCIES = (
    ('USD', 'USD'),
    ('EUR', 'EUR'),
    ('CNY', 'CNY'),
)
# SCA can be stored to be provided in a future transaction for up to 90 days.
AUTHORIZATION_EXPIRY = datetime.timedelta(days=90)

log = logging.getLogger(__name__)
UpdateFieldsType = Optional[Set[str]]
"""The type used by Django's save(update_fields=...) parameter."""


class CurrencyField(models.CharField):
    def __init__(self, **kwargs: Any) -> None:
        kwargs.setdefault('max_length', 3)
        kwargs.setdefault('choices', CURRENCIES)
        kwargs.setdefault('default', DEFAULT_CURRENCY)
        super().__init__(**kwargs)


class MoneyFieldDescriptor:
    """Ensures that a MoneyField produces/accepts a Money instance.

    A MoneyField accepts either an int (number of cents) or a Money instance.
    In the former case the currency is taken from the 'currency_field'
    attribute of the model instance. In the latter case that same attribute
    is used to ensure consistent currencies.
    """

    def __init__(self, field: 'MoneyField') -> None:
        self.field: 'MoneyField' = field

    def __get__(self, instance: object, cls: Optional[Type[object]] = None) -> Optional[Money]:
        if instance is None:
            return None
        try:
            return cast(Optional[Money], instance.__dict__[self.field.name])
        except KeyError as ex:
            raise AttributeError(f'No such attribute: {ex!s}')

    def __set__(self, instance: object, value: Union[None, int, Money]) -> None:
        try:
            instance_currency = instance.__dict__[self.field.currency_field]
        except KeyError:
            raise AttributeError(f'{instance!r} has no property {self.field.currency_field}')

        if value is None:
            value_to_set = None
        elif isinstance(value, int):
            value_to_set = Money(instance_currency, value)
        elif isinstance(value, Money):
            if value.currency != instance_currency:
                # This message has to be carefully constructed, as __set__()
                # can be called during object construction. As such, properties
                # accessed in __str__() may not exist or can cause unintended
                # database lookups.
                raise CurrencyMismatch(
                    f'Currency is {instance_currency!r} but trying to set '
                    f'.{self.field.name} = {value!r}'
                )
            value_to_set = value
        else:
            raise TypeError(f'Unsupported type {type(value)} for {self.field.name}')
        instance.__dict__[self.field.name] = value_to_set


class MoneyField(models.IntegerField):
    """Monetary Amount database field.

    The amount is stored in cents (that is, 1/100th of currency units).

    To use this field, the field referenced by the 'currency_field'
    parameter must exist already. That is, it must be declared on the
    model class before the MoneyField() instance that references it.

    To keep mypy happy, add a type declaration 'fieldname: Money' when
    using this model field.
    """

    def __init__(self, *args: Any, currency_field: str = 'currency', **kwargs: Any) -> None:
        self.currency_field: str = currency_field
        super().__init__(*args, **kwargs)

    def deconstruct(self) -> Any:
        """Return parameters for __init__ to recreate this field."""
        name, path, args, kwargs = super().deconstruct()
        kwargs['currency_field'] = self.currency_field
        return name, path, args, kwargs

    def get_prep_value(self, value: Union[None, int, Money]) -> Optional[int]:
        """Return field's value prepared for saving into a database."""
        if value is None:
            return None
        if isinstance(value, int):
            # The default value for an amount can be given as an integer.
            return value
        assert isinstance(value, Money)
        return value.cents

    def contribute_to_class(self, cls: Type['Model'], name: str, **kwargs: Any):
        """Ensure our descriptor is called when getting/setting the attribute."""
        super().contribute_to_class(cls, name, **kwargs)
        setattr(cls, self.name, MoneyFieldDescriptor(self))

    def formfield(self, **kwargs: Any) -> Any:
        """Default to MoneyFormField if form_class is not specified."""
        return super().formfield(
            **{
                'form_class': form_fields.MoneyFormField,
                **kwargs,
                'widget': form_fields.MoneyInput,
            }
        )

    def to_python(self, value: Union[None, int, str, Money]) -> Optional[int]:
        """The result of this function feeds the MoneyFieldDescriptor.

        I think.

        Note that this function is also used when deserialising values from
        JSON, in which case the Money value was serialised to the string
        '{currency}\u00A0{amount}'.
        """
        if isinstance(value, str):
            return Money.from_str(value).cents
        if isinstance(value, Money):
            return value.cents
        return cast(Optional[int], super().to_python(value))

    def get_attname_column(self) -> Tuple[str, str]:
        """Add '_in_cents' to the database column name."""
        attname, column = super().get_attname_column()  # type: ignore
        return attname, f'{column}_in_cents'


class Customer(models.Model):
    """An entity representing a customer making a payment, may be linked to a known user.

    It is referenced by:
    - gateway-specific customer IDs
    - addresses
    - payment_methods
    - orders
    - transactions
    - subscriptions

    It could also reference:
    - user token for each Gateway it uses
    """

    _log = log.getChild('Customer')

    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        related_name='customer',
        null=True,
        blank=True,
        help_text=(
            'When this is empty, it means that payment was made '
            'or attempted without signing in with an account,<br>'
            "and this customer's billing, subscription and payment records "
            'are yet to be claimed by a user account.<br>'
            'Someone should sign in and use a customer token link to do so.'
        ),
    )

    @property
    def billing_address(self) -> 'Address':
        new_billing_address = Address(category='billing', customer=self)
        if self.pk is None:
            return new_billing_address
        # `.all()` won't generate an extra query if `address_set` was previously `select_related`
        return next(
            (addr for addr in self.address_set.all() if addr.category == 'billing'),
            new_billing_address,
        )
        # TODO Ensure one 'billing' address per user

    @property
    def payment_method_default(self) -> Optional['PaymentMethod']:
        return self.paymentmethod_set.filter(is_deleted=False).first()

    def gateway_customer_id(self, gateway: 'Gateway') -> str:
        """Get the customer ID on the given gateway.

        :return: the ID string, or '' when the customer has no customer ID on
            this gateway.
        """
        if self.pk is None:
            return ''
        try:
            gateway_customer = self.gatewaycustomerid_set.get(gateway=gateway)
        except looper.models.GatewayCustomerId.MultipleObjectsReturned:
            gateway_customer = self.gatewaycustomerid_set.filter(
                gateway=gateway).order_by('gateway_customer_id').first()
            log.warning(f'Expected only one customer ID per gateway, picking {gateway_customer}')
        except GatewayCustomerId.DoesNotExist:
            return ''
        return gateway_customer.gateway_customer_id

    def gateway_customer_id_get_or_create(self, gateway: 'Gateway') -> str:
        """Get or create a customer on the gateway.

        :param gateway:
        :return: The customer's ID at that particular gateway.
        :raise: looper.exceptions.GatewayError if the customer could not be created.
        """
        try:
            gateway_customer = self.gatewaycustomerid_set.get(gateway=gateway)
        except looper.models.GatewayCustomerId.MultipleObjectsReturned:
            gateway_customer = self.gatewaycustomerid_set.filter(
                gateway=gateway).order_by('gateway_customer_id').first()
            log.exception(f'Expected only one customer ID per gateway, picking {gateway_customer}')
        except GatewayCustomerId.DoesNotExist:
            # Collect additional customer data to be sent to the gateway
            billing_address = self.billing_address
            customer_data = {
                'email': billing_address.email,
                'full_name': billing_address.full_name,
            }
            gateway_customer_id = gateway.provider.customer_create(customer_data)
            gateway_customer = self.gatewaycustomerid_set.create(
                gateway_customer_id=gateway_customer_id, gateway=gateway
            )
        return gateway_customer.gateway_customer_id

    def payment_method_add(
        self, payment_method_nonce: str, gateway: 'Gateway', **payment_options: Any
    ) -> 'PaymentMethod':
        """Add a payment method for the customer on the gateway.

        :raise: looper.exceptions.GatewayError if the payment method could not be added.
        """
        # Check if the customer already exists on the provided gateway.
        gateway_customer_id = self.gateway_customer_id_get_or_create(gateway)
        assert gateway_customer_id is not None, 'missing gateway_customer_id'

        pm_info = gateway.provider.payment_method_create(
            payment_method_nonce, gateway_customer_id, **payment_options
        )

        # I'd rather just query and handle duplicates in the django.db.IntegrityError exception
        # handler, but Django doesn't allow me to do queries there (raises a
        # TransactionManagementError).
        paymeth: Optional[PaymentMethod] = self.paymentmethod_set.filter(
            gateway=gateway, token=pm_info.token
        ).first()
        if not paymeth:
            self._log.debug('Storing new payment method for customer pk=%d', self.pk)
            method_type = pm_info.type_for_database()
            assert isinstance(method_type, str), f'expected str, not {method_type!r}'
            recog_name = pm_info.recognisable_name()
            paymeth = self.paymentmethod_set.create(
                gateway=gateway,
                token=pm_info.token,
                method_type=method_type,
                recognisable_name=recog_name,
            )
        else:
            if paymeth.is_deleted:
                # We're re-adding a previously-deleted payment method, so just restore it.
                paymeth.undelete()

            self._log.debug('Updating existing payment method for customer pk=%d', self.pk)
            paymeth.recognisable_name = pm_info.recognisable_name()
            paymeth.method_type = pm_info.type_for_database()
            paymeth.save(update_fields={'recognisable_name', 'method_type'})

        auth = pm_info.authentication
        if auth:
            self._log.info(
                'Storing authentication for payment method pk=%s for customer pk=%d',
                paymeth.pk,
                self.pk,
            )
            paymeth.paymentmethodauthentication_set.create(
                gateway=gateway,
                authentication_type=auth.type_for_database(),
                authentication_id=auth.authentication_id,
                version=auth.version,
                status=auth.status,
                details=auth.details_for_database(),
            )
        return paymeth

    def __str__(self) -> str:
        billing_address = self.billing_address
        customer_repr = f'{billing_address.full_name or billing_address.email or self.pk}'
        if not self.user_id:
            customer_repr = f'{customer_repr} (no account)'
        return customer_repr

    def get_tax(self, product_type: str) -> Tuple[TaxType, Decimal]:
        """Calculate tax type and rate for this customer and the given product type."""
        billing_address: Address = self.billing_address

        is_business = bool(billing_address.vat_number)
        if billing_address.vat_number:
            latest_verification = TaxNumberVerification.get_latest(billing_address.vat_number)
            # Unless the VAT number was recently verified and found invalid, consider it valid.
            if latest_verification and not latest_verification.is_valid:
                is_business = False
            # In the future, if verification records are created during validation of billing info,
            # this logic can change to only consider customer a business
            # if a record proving number's validity exists.

        tax_type, tax_rate = ProductType(product_type).get_tax(
            buyer_country_code=billing_address.country,
            is_business=is_business,
        )
        return tax_type, tax_rate


class Gateway(models.Model):
    """Payment gateway data.

    Links Customers to a specific payment gateway.
    """

    GATEWAY_CHOICES = [(name, name.title()) for name in gateways.Registry.gateway_names()]
    name = models.CharField(max_length=32, unique=True, choices=GATEWAY_CHOICES)
    customers = models.ManyToManyField(Customer, through='GatewayCustomerId')
    is_default = models.BooleanField(default=False)
    frontend_name = models.CharField(
        max_length=64, help_text='User-facing name for the gateway in payment forms.'
    )
    form_description = models.TextField(
        blank=True,
        help_text='Shown in payment forms when this gateway is selected. This is interpreted as '
        'HTML, so be careful and HTML-escape what needs to be HTML-escaped.',
    )
    log = log.getChild('Gateway')

    @classmethod
    def default(cls) -> 'Gateway':
        return cls.objects.get(is_default=True)

    @property
    def provider(self) -> gateways.AbstractPaymentGateway:
        return gateways.Registry.instance_for(self.name)

    def save(self, *args: Any, **kwargs: Any) -> None:
        """Ensure that only one Gateway is the default one."""
        if self.is_default:
            for old_default in Gateway.objects.filter(is_default=True):
                self.log.info('Switching default gateway from %r to %r', old_default, self)
                old_default.is_default = False
                old_default.save(update_fields={'is_default'})
        super().save(*args, **kwargs)

    def __str__(self) -> str:
        return self.name


class GatewayCustomerId(models.Model):
    """Identity of the Customer on the payment Gatweay.

    Stores a reference to a customer, to fetch payment methods on the Gateway.
    We could do without this table, specifying to the Gateway our Customer id,
    but that feels too implicit and there is no guarantee that all gateways support this.
    """

    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    gateway = models.ForeignKey(Gateway, on_delete=models.CASCADE)
    gateway_customer_id = models.CharField(max_length=255)

    def __str__(self) -> str:
        return (
            f'{self.gateway.name} GatewayCustomerId '
            f'{self.gateway_customer_id} for {self.customer_id}'
        )


class PaymentMethod(model_mixins.CreatedUpdatedMixin, models.Model):
    """Called Source in Stripe.

    Optionally create CreditCard, BankAccount, etc.
    """

    class Meta:
        unique_together = ('gateway', 'token', 'customer')

    DEFAULT_METHOD_TYPE = 'cc'
    METHOD_TYPES = (
        ('cc', 'Credit Card'),
        ('pa', 'PayPal'),
        ('ba', 'Bank'),
        ('ideal', 'iDEAL'),
        ('eps', 'EPS'),
        ('giropay', 'giropay'),
        ('sofort', 'SOFORT'),
        ('bancontact', 'Bancontact'),
        ('sepa_debit', 'SEPA Direct Debit'),
        ('alipay', 'Alipay'),
        ('klarna', 'Klarna'),
        ('link', 'Link'),
    )
    gateway = models.ForeignKey(Gateway, on_delete=models.CASCADE)
    method_type = models.CharField(choices=METHOD_TYPES, blank=True, max_length=255)
    # Payment token issued by the gateway, or null if this is a Bank payment
    token = models.CharField(max_length=255, null=True)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    is_deleted = models.BooleanField(default=False)
    recognisable_name = models.CharField(max_length=255, default='', blank=True)

    def gateway_properties(self) -> Optional[looper.gateways.PaymentMethodInfo]:
        """Retrieve from the gateway further information.

        Depending on the method type, we might get:
        - the last 4 digits of a credit card
        - an image url of the card type/PayPal

        Returns None if the token does not correspond to a Payment Method
        on the payment gateway.
        """
        if self.pk:
            log.debug('Fetching gateway properties of payment method pk=%d', self.pk)
        else:
            log.debug('Fetching gateway properties during payment method creation')
        try:
            pm_info = self.gateway.provider.find_customer_payment_method(self.token)
        except looper.exceptions.NotFoundError:
            log.info('Tried to find payment method that does not exist')
            return None

        return pm_info

    def delete(self, *args: Any, using: Any = None, **kwargs: Any) -> Tuple[int, Dict[str, int]]:
        """Soft-delete instead of really deleting.

        This allows us to keep references to this payment method.
        Note that deleting does really delete the payment method at the
        payment gateway; the record we keep is for our own history only.
        """

        # TODO check if the payment method is connected to a pending subscription or an order
        # TODO(Sybren): handle exceptions communicating with the gateway.
        self.gateway.provider.payment_method_delete(self.token)

        log.debug(
            'Soft-deleting payment method pk=%d and deleting it at the payment provider',
            self.pk,
        )
        self.is_deleted = True
        self.save(using=using, update_fields={'is_deleted'})

        message = 'Soft-deleting payment method and deleting it at the payment provider'
        admin_log.attach_log_entry(self, message)

        return None  # type: ignore

    def undelete(self) -> None:
        """Un-soft-deletes the instance.

        Note that this assumes the user has re-added the same payment method
        at the payment gateway, and that this resulted in the same payment
        token. Otherwise it should just be a new PaymentMethod instance.
        """
        log.debug('Un-deleting payment method pk=%d', self.pk)
        self.is_deleted = False
        self.save(update_fields={'is_deleted'})

        message = 'Undeleting payment method'
        admin_log.attach_log_entry(self, message)

    def __str__(self) -> str:
        as_str = f'{self.recognisable_name or self.method_type}'
        if self.is_deleted:
            return f'{as_str} (deleted)'
        return as_str

    def get_unused_authentication(self):
        """Find a payment authentication that hadn't been used yet."""
        return self.paymentmethodauthentication_set.filter(
            consumed_at__isnull=True,
            created_at__gt=django.utils.timezone.now() - AUTHORIZATION_EXPIRY,
        ).exclude(
            # Even if an authentication wasn't marked as consumed but is already
            # attached to a transaction, we can't be sure it can still be used.
            transaction__id__isnull=False,
        ).first()


class PaymentMethodAuthentication(model_mixins.CreatedUpdatedMixin, models.Model):
    """Store Strong Customer Authentication info for payment methods.

    If SCA was required when a payment method was created,
    authentication ID allows to reference it later in a transaction.
    Used to store 3D Secure authentication info returned by Braintree.
    """
    class Meta:
        unique_together = ('gateway', 'authentication_id')

    TYPES = (
        ('tds', '3D Secure'),
    )

    gateway = models.ForeignKey(Gateway, on_delete=models.CASCADE)
    authentication_type = models.CharField(choices=TYPES, max_length=20)
    authentication_id = models.CharField(max_length=255, help_text='External authentication ID.')
    payment_method = models.ForeignKey(PaymentMethod, null=False, on_delete=models.CASCADE)
    consumed_at = models.DateTimeField(
        null=True,
        blank=True,
        help_text='Date this authentication was used in a transaction.'
    )

    # Additional information, useful for debugging
    version = models.CharField(
        max_length=20,
        null=True,
        help_text='Version of the authentication method returned by the gateway (for debugging)',
    )
    status = models.CharField(
        max_length=255,
        null=True,
        help_text='Status of the authentication returned by the gateway (for debugging)',
    )
    details = models.CharField(
        max_length=255,
        null=True,
        help_text='Other info about the authentication returned by the gateway (for debugging)',
    )

    def __str__(self) -> str:
        return f'{self.gateway} Authentication {self.authentication_id}'

    def mark_as_consumed(self) -> None:
        """Mark this payment method authentication as used."""
        if self.consumed_at:
            log.error('Payment method authentication pk=%s is already marked as used', self.pk)
            return
        self.consumed_at = django.utils.timezone.now()
        self.save(update_fields={'consumed_at'})


class Address(models.Model):
    # These are the fields used to create forms, and publicly displayed
    PUBLIC_FIELDS = [
        'full_name',
        'company',
        'street_address',
        'extended_address',
        'locality',
        'postal_code',
        'region',
        'country',
    ]
    DEFAULT_ADDRESS_CATEGORY = 'billing'
    ADDRESS_CATEGORIES = (
        ('billing', 'Billing'),
        ('shipping', 'Shipping'),
    )

    category = models.CharField(
        choices=ADDRESS_CATEGORIES, default=DEFAULT_ADDRESS_CATEGORY, max_length=20
    )
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    email = models.EmailField(max_length=255, help_text='For billing notifications.')
    full_name = models.CharField(max_length=255)
    company = models.CharField(max_length=255, default='', blank=True)
    street_address = models.CharField(max_length=255, default='', blank=True)
    extended_address = models.CharField(max_length=255, default='', blank=True)
    locality = models.CharField(max_length=255, default='', blank=True, verbose_name='City')
    postal_code = models.CharField(
        max_length=255, verbose_name='ZIP/Postal Code', blank=True, default=''
    )
    region = models.CharField(
        max_length=255, verbose_name='State/Province/Region', blank=True, default=''
    )
    country = CountryField(blank=True)

    vat_number = models.CharField(
        max_length=255, blank=True, default='', verbose_name='VAT identification number'
    )
    tax_exempt = models.BooleanField(default=False)

    @property
    def public_values(self) -> Iterable[str]:
        return (getattr(self, f) for f in self.PUBLIC_FIELDS)

    @property
    def as_dict(self) -> Dict[str, object]:
        return {f: getattr(self, f) for f in self.PUBLIC_FIELDS}

    def __str__(self) -> str:
        return f'{self.category} address of {self.customer}'.capitalize()

    def as_text(self) -> str:
        from django_countries.fields import Country

        def to_str(element: object) -> str:
            if isinstance(element, Country):
                return cast(str, element.name)
            return str(element)

        return '\n'.join(to_str(elem) for elem in self.public_values if elem)


class TaxRule(model_mixins.RecordModificationMixin, model_mixins.CreatedUpdatedMixin, models.Model):
    """Stores tax percentages per product and countries."""

    record_modification_fields = {'rate', 'note'}

    # Only generic electronic service is currently supported
    PRODUCT_TYPE_CHOICES = ((ProductType.ELECTRONIC_SERVICE.value, 'Generic electronic service'),)

    country = CountryField(blank=False, null=False)
    product_type = models.CharField(
        choices=PRODUCT_TYPE_CHOICES,
        blank=False,
        null=False,
        default=ProductType.ELECTRONIC_SERVICE.value,
        max_length=20,
        help_text='The nature and type of goods or services supplied (for tax administration)',
    )
    rate = models.DecimalField(
        max_digits=5,
        decimal_places=2,
        null=False,
        blank=False,
        help_text='Tax rate percentage, %. ',
    )
    note = models.TextField(
        blank=True, default='', help_text='Optional note'
    )

    @property
    def formatted_rate(self) -> str:
        """Return formatted percentage."""
        return format_percentage(self.rate)

    formatted_rate.fget.short_description = 'Rate'

    @property
    def country_code(self) -> str:
        """Return alpha2 code of the country this rule applies to."""
        return self.country.code

    class Meta:
        unique_together = [('country', 'product_type')]

    def __str__(self):
        return (
            f'Tax rule {self.country.name}: '
            f'{self.get_product_type_display()}, {self.formatted_rate}'
        )

    def save(self, *args: Any, **kwargs: Any) -> None:
        """Act on state changes."""
        was_changed, old_state = self.pre_save_record(*args, **kwargs)

        super().save(*args, **kwargs)

        if 'rate' in old_state and old_state['rate'] != self.rate:
            formatted_old_rate = format_percentage(old_state["rate"])
            message = f'Rate changed from {formatted_old_rate} to {self.formatted_rate}'
            admin_log.attach_log_entry(self, message)

        if 'note' in old_state and old_state['note'] != self.note:
            message = f'Note changed from "{old_state["note"]}" to "{self.note}"'
            admin_log.attach_log_entry(self, message)


class TaxNumberVerification(model_mixins.CreatedUpdatedMixin, models.Model):
    VERIFICATION_SOURCE = (
        ('vies', 'VIES'),
    )
    VERIFICATION_SOURCE_DEFAULT = VERIFICATION_SOURCE[0][0]

    class Meta:
        ordering = ('-updated_at', )
        indexes = [
            models.Index(fields=['source', 'number', 'updated_at']),
            models.Index(fields=['source', 'number']),
            models.Index(fields=['number']),
            models.Index(fields=['is_valid']),
        ]

    number = models.CharField(null=False, blank=False, max_length=25)
    is_valid = models.BooleanField(null=False, blank=False, default=False)
    source = models.CharField(
        null=False,
        blank=False,
        choices=VERIFICATION_SOURCE,
        default=VERIFICATION_SOURCE_DEFAULT,
        max_length=20,
    )
    response = models.TextField(null=False, blank=True, default='')

    def __str__(self):
        formatted = 'valid' if self.is_valid else 'invalid'
        return f'<TaxNumberVerification {self.source} {self.updated_at}: {self.number} {formatted}>'

    @classmethod
    def get_latest(
        cls, number: str, source: str = VERIFICATION_SOURCE_DEFAULT
    ) -> Optional['TaxNumberVerification']:
        """Get latest verification record for a given tax number."""
        return cls.objects.filter(source=source, number=number).order_by('-updated_at').first()

    @classmethod
    def verify(
        cls, number: str, source: str = VERIFICATION_SOURCE_DEFAULT, save: bool = True
    ) -> Optional['TaxNumberVerification']:
        """Verify a given tax number against a given source.

        Currently, only validating VAT identification numbers against VIES is supported.
        """
        if source == 'vies':
            number = re.sub(r'\s*', '', number)
            is_valid, vies_response = vies_validate_number(number)
            if is_valid is None:
                log.warning('Unable to verify the tax number %s against %s', number, source)
                return None
            attributes = dict(
                source=source,
                is_valid=is_valid,
                number=number,
                response=json.dumps(vies_response, cls=DjangoJSONEncoder),
            )
            if save:
                verification, is_new = cls.objects.get_or_create(**attributes)
                verb = 'Creating' if is_new else 'Updating'
                log.info('%s a verification record for %s', verb, number)
                verification.save()
            else:
                verification = cls(**attributes)
            return verification
        else:
            raise NotImplementedError('Unknown tax number verification source "%s"', source)


TaxNumberVerification._meta.get_field('updated_at').verbose_name = 'updated at'


class Product(model_mixins.CreatedUpdatedMixin, models.Model):
    """Can be a good or a service, like 'Development Fund'."""

    name = models.CharField(max_length=200)
    type = models.CharField(
        choices=ProductType.as_choices(),
        default=ProductType.DONATION.value,
        max_length=20,
        help_text='The nature and type of goods or services supplied (for tax administration)',
    )
    description = models.TextField(
        null=True,
        blank=True,
        help_text=(
            'Tax office friendly description of the nature and type of goods or services supplied,'
            ' shown in invoices/receipts.'
        )
    )

    def __str__(self) -> str:
        return self.name


class Plan(models.Model):
    """Acts as a variation of the product, like 'Gold' for 'Development Fund'."""

    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    name = models.CharField(max_length=255)  # User-facing name
    description = models.CharField(max_length=255)
    is_active = models.BooleanField(default=True)

    def __str__(self) -> str:
        return self.name

    def variation_for_currency(self, currency: str) -> Optional['PlanVariation']:
        """Determine the default variation for the given currency.

        Returns None when there is no plan variation for the currency at all.
        """

        default_variation = (
            self.variations.active()
            .filter(currency=currency)
            .order_by('-is_default_for_currency', '-price')
            .first()
        )
        return default_variation

    def product_plan_name(self) -> str:
        return f'{self.product.name} / {self.name}'


class PlanVariationManager(models.Manager):
    def active(self) -> 'models.QuerySet[PlanVariation]':
        """Return QuerySet of only the active plan variations."""
        return self.filter(is_active=True)

    def active_ordered_by_renewal(self) -> List['PlanVariation']:
        """Return a list of active plan variations ordered by their renewal intervals ascending."""
        _now = django.utils.timezone.now()

        def _interval_in_days(_):
            # relativedelta doesn't give the delta in days, but we can get them from timedelta
            return (_now - (_now - _.interval)).days
        return sorted(self.active().all(), key=lambda _: _interval_in_days(_))


class PlanVariation(models.Model):
    DEFAULT_INTERVAL_UNIT = 'month'
    INTERVAL_UNITS = (
        ('day', 'Day'),
        ('week', 'Week'),
        ('month', 'Month'),
        ('year', 'Year'),
    )
    DEFAULT_COLLECTION_METHOD = 'automatic'
    COLLECTION_METHODS = (
        ('automatic', 'Automatic'),
        ('manual', 'Manual'),
    )
    plan = models.ForeignKey(Plan, on_delete=models.CASCADE, related_name='variations')
    currency: str = CurrencyField()
    price: Money = MoneyField(help_text='Including tax.')
    interval_unit = models.CharField(
        choices=INTERVAL_UNITS, default=DEFAULT_INTERVAL_UNIT, max_length=50
    )
    interval_length = models.PositiveIntegerField(default=1)
    collection_method = models.CharField(
        choices=COLLECTION_METHODS, default=DEFAULT_COLLECTION_METHOD, max_length=20
    )
    is_active = models.BooleanField(default=True)
    is_default_for_currency = models.BooleanField(default=False)

    objects: PlanVariationManager = PlanVariationManager()

    class Meta:
        unique_together = [
            ('plan', 'currency', 'interval_unit', 'interval_length', 'collection_method')
        ]
        ordering = (
            '-is_active', 'currency', '-is_default_for_currency', '-price', 'collection_method'
        )

    @property
    def price_per_month(self) -> Money:
        """Returns the rounded price per month for this plan variation.

        Note that this is a lossy operation, as it rounds down to entire cents.
        """
        months = {
            'day': 12 / 365,  # Approximate average nr of months in a day.
            'week': 12 / 52,  # Approximate average nr of months in a week.
            'month': 1,
            'year': 12,
        }
        months_per_interval = months[self.interval_unit] * self.interval_length
        return self.price // months_per_interval

    @property
    def interval(self) -> dateutil.relativedelta.relativedelta:
        """Return the renewal interval as relativedelta.

        The relativedelta can be added to regular `datetime.datetime` objects.
        """
        kwargs = {f"{self.interval_unit}s": self.interval_length}
        return dateutil.relativedelta.relativedelta(**kwargs)  # type: ignore

    @property
    def first_renewal(self) -> datetime.datetime:
        """Return the date of first renewal, considering now as the activation date."""
        now = django.utils.timezone.now()
        return now + self.interval

    def in_other_currency(self, currency: str) -> 'PlanVariation':
        """Get a similar plan variation but in given currency."""
        if self.currency == currency:
            return self
        return self.__class__.objects.active().get(
            currency=currency,
            plan_id=self.plan_id,
            interval_unit=self.interval_unit,
            interval_length=self.interval_length,
        )

    def __str__(self) -> str:
        return f"{self.plan.name} - {self.currency} - {self.interval_length} {self.interval_unit}"

    def __repr__(self) -> str:
        return (
            f'PlanVariation(pk={self.pk}, plan.name={self.plan.name!r}, price={self.price!r}, '
            f'interval_unit={self.interval_unit!r}, interval_length={self.interval_length}, '
            f'collection_method={self.collection_method})'
        )

    def save(self, *args: Any, **kwargs: Any) -> None:
        if self.is_default_for_currency:
            # Make sure there is only one default.
            self.__class__.objects.filter(plan=self.plan, currency=self.currency).exclude(
                pk=self.pk
            ).update(is_default_for_currency=False)

        super().save(*args, **kwargs)


class SharedFields(models.Model):
    """Fields shared between Subscription and Order."""

    class Meta:  # type: ignore
        abstract = True

    DEFAULT_COLLECTION_METHOD = 'automatic'

    # If you change COLLECTION_METHODS, also update looper.clock.AbstractClock.renew_subscription.
    COLLECTION_METHODS = (
        ('automatic', 'Automatic'),
        ('manual', 'Manual'),
        ('managed', 'Managed (by staff)'),
    )

    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    payment_method = models.ForeignKey(
        PaymentMethod, null=True, blank=True, on_delete=models.CASCADE
    )

    currency: str = CurrencyField()
    price: Money = MoneyField(default=0, help_text='Including tax.')
    collection_method = models.CharField(
        choices=COLLECTION_METHODS, default=DEFAULT_COLLECTION_METHOD, max_length=20
    )

    TAX_FIELDS = {'tax', 'tax_type', 'tax_country', 'tax_rate'}
    tax: Money = MoneyField(
        blank=True,
        default=0,
        help_text='Tax amount charged at the moment of sale.',
    )
    tax_type = models.CharField(
        choices=TaxType.as_choices(), max_length=20, default=TaxType.NO_CHARGE.value, blank=True
    )
    tax_country = CountryField(blank=True, default='')
    tax_rate = models.DecimalField(
        max_digits=5,
        decimal_places=2,
        null=False,
        default=Decimal(0),
        blank=True,
        help_text='Tax rate percentage applicable at the moment of sale. '
                  'Stored regardless of whether the tax was charged or not '
                  '(e.g. due to exemption or reverse-charge).',
    )

    @classmethod
    def field_names(cls) -> Set[str]:
        return {f.name for f in cls._meta.fields}

    @property
    def taxable(self) -> Taxable:
        """Return tax data for this object."""
        return Taxable(self.price, tax_type=TaxType(self.tax_type), tax_rate=self.tax_rate)


class SubscriptionManager(models.Manager):
    def payable(self) -> 'models.QuerySet[Subscription]':
        """Return subscriptions that can still be paid for."""
        return self.filter(status__in={'active', 'on-hold'})

    def active(self) -> 'models.QuerySet[Subscription]':
        return self.filter(status__in=Subscription._ACTIVE_STATUSES)

    def cancelled(self) -> 'models.QuerySet[Subscription]':
        return self.filter(status__in=Subscription._CANCELLED_STATUSES)


# Ignoring type because of the inner Meta class of the two mix-ins.
class Subscription(  # type: ignore
    model_mixins.RecordModificationMixin,
    model_mixins.CreatedUpdatedMixin,
    model_mixins.StateMachineMixin,
    SharedFields,
    models.Model,
):
    """Entity the defines orders and billing."""

    class Meta:
        indexes = [
            models.Index(fields=['status']),
        ]

    record_modification_fields = {'status', 'is_active', 'customer_id', *SharedFields.TAX_FIELDS}
    log = log.getChild('Subscription')

    DEFAULT_INTERVAL_UNIT = 'month'
    INTERVAL_UNITS = (
        ('day', 'Day'),
        ('week', 'Week'),
        ('month', 'Month'),
        ('year', 'Year'),
    )

    # How many of these fit in a month.
    _UNITS_PER_MONTH = {
        'day': 30,
        'week': 30 / 7,
        'month': 1,
        'year': 1 / 12,
    }

    DEFAULT_STATUS = 'on-hold'
    STATUSES = (
        ('active', 'Active'),
        ('on-hold', 'On Hold'),
        ('pending-cancellation', 'Pending Cancellation'),
        ('cancelled', 'Cancelled'),
        ('expired', 'Expired'),
    )
    _ACTIVE_STATUSES = {'active', 'pending-cancellation'}
    """Use subscription.is_active to test for the active status."""
    _CANCELLED_STATUSES = {'cancelled', 'expired'}
    """Use subscription.is_cancelled to test for the cancelled status."""

    # Map 'current status': {set of allowed new statuses}
    VALID_STATUS_TRANSITIONS: Mapping[str, Set[str]] = {
        'active': {'on-hold', 'pending-cancellation', 'cancelled'},
        'on-hold': {'active', 'cancelled', 'expired'},
        'pending-cancellation': {'active', 'cancelled'},
        'cancelled': set(),
        'expired': set(),
    }

    plan = models.ForeignKey(Plan, on_delete=models.CASCADE)
    status = models.CharField(
        choices=STATUSES,
        default=DEFAULT_STATUS,
        max_length=20,
        help_text="Note that setting the status to 'Active' will also set the "
        "next payment date one renewal interval from now.",
    )

    interval_unit = models.CharField(
        choices=INTERVAL_UNITS,
        default=DEFAULT_INTERVAL_UNIT,
        max_length=50,
        help_text='The units in which "interval length" is expressed',
    )
    interval_length = models.IntegerField(
        default=1, help_text='How many "interval units" is each billing cycle.'
    )
    intervals_elapsed = models.IntegerField(
        default=0, null=False, blank=True, help_text='How many billing cycles have happened.'
    )

    started_at = models.DateTimeField(null=True, blank=True, help_text='Date of first activation.')
    pending_cancellation_since = models.DateTimeField(
        null=True,
        blank=True,
        help_text=(
            'Date when status changed to pending cancellation, '
            'e.g. if cancelled by subscriber while still active.'
        ),
    )
    cancelled_at = models.DateTimeField(null=True, blank=True)

    current_interval_started_at = models.DateTimeField(null=True, blank=True)
    next_payment = models.DateTimeField(
        null=True, blank=True, help_text='When the next payment is due.'
    )

    last_notification = models.DateTimeField(
        null=True,
        blank=True,
        help_text='When Ton was last sent a notification that this subscription passed its next '
        'payment date. Only used for managed subscriptions.',
    )

    is_legacy = models.BooleanField(
        blank=False,
        default=False,
        null=False,
        help_text='Whether or not this subscription was migrated from Blender Store',
    )

    objects: SubscriptionManager = SubscriptionManager()

    @property
    def monthly_rounded_price(self) -> Money:
        """Compute the price per month, rounded to entire cents.

        Note that using montly_rounded_price() * 12 to get the
        yearly price is wrong, due to rounding errors. If you want
        to know that, just implement yearly_price() or something.
        """
        units_per_month = self._UNITS_PER_MONTH[self.interval_unit]
        price_in_cents = self.price.cents * units_per_month / self.interval_length
        return Money(currency=self.currency, cents=int(round(price_in_cents)))

    @property
    def interval(self) -> dateutil.relativedelta.relativedelta:
        """Return the renewal interval as relativedelta.

        The relativedelta can be added to regular `datetime.datetime` objects.
        """
        kwargs = {f"{self.interval_unit}s": self.interval_length}
        return dateutil.relativedelta.relativedelta(**kwargs)  # type: ignore

    @property
    def can_be_activated(self) -> bool:
        """Whether a status transition to 'active' is allowed."""
        return self.may_transition_to('active')

    @property
    def is_active(self) -> bool:
        """Whether the subscription is actively usable.

        This is of course true in status 'active', but also in status
        'pending-cancellation'.
        """
        return self.status in self._ACTIVE_STATUSES

    @property
    def is_cancelled(self) -> bool:
        """Whether the subscription is cancelled.

        This is of course true in status 'cancelled', but also in status 'expired'.
        """
        return self.status in self._CANCELLED_STATUSES

    @property
    def next_payment_in_future(self) -> bool:
        """True iff the next_payment is known and in the future."""
        if self.next_payment is None:
            return False
        now = django.utils.timezone.now()
        return self.next_payment > now

    @property
    def next_payment_in_past(self) -> bool:
        """True iff the next_payment is known and in the past."""
        if self.next_payment is None:
            return False
        now = django.utils.timezone.now()
        return self.next_payment < now

    @property
    def notification_required(self) -> bool:
        """True iff next_payment is in the past and the last notification was from even earlier."""

        if self.next_payment_in_future:
            return False

        return self.last_notification is None or self.last_notification < self.next_payment

    @requires_status('on-hold', 'active')
    def cancel_subscription(self) -> None:
        """Either directly cancels the subscription or moves to pending-cancellation."""

        old_status = self.status
        if self.status == 'on-hold':
            self.status = 'cancelled'
        elif self.status == 'active':
            self.status = 'pending-cancellation'
        else:
            self.log.warning(
                'Not cancelling subscription %d because it is in uncancellable status %r',
                self.pk,
                self.status,
            )
            return
        self.log.info(
            'Cancelling subscription %d from status %r to %r', self.pk, old_status, self.status
        )
        self.save(update_fields={'status'})

    @requires_status('on-hold')
    def expire_subscription(self) -> None:
        """Mark the subscription as expired."""

        old_status = self.status
        if self.status == 'on-hold':
            self.status = 'expired'
        else:
            self.log.warning(
                'Not expiring subscription %d because it is in status %r',
                self.pk,
                self.status,
            )
            return
        self.log.info(
            'Expiring subscription %d from status %r to %r', self.pk, old_status, self.status
        )
        self.save(update_fields={'status'})

    def latest_order(self) -> Optional['Order']:
        """Returns the last order for this Subscription.

        Since an Order is usually automatically created when a subscription
        is created, this mostly return an Order. The only exception is those
        subscriptions for which collection_method='managed'; those never have
        any orders attached.
        """
        if self.pk is None:
            return None
        try:
            return self.order_set.latest()
        except Order.DoesNotExist:
            return None

    def save(self, *args: Any, **kwargs: Any) -> None:
        """Act on state changes."""
        is_new = not self.pk
        was_changed, old_state = self.pre_save_record(*args, **kwargs)
        update_fields = kwargs.get('update_fields')

        self._handle_tax_change_pre_save(old_state, update_fields)

        if was_changed:
            self._handle_status_change_pre_save(old_state, update_fields)

        super().save(*args, **kwargs)

        if old_state.get('customer_id') != self.customer_id:
            self._handle_customer_changed(old_state.get('customer_id'))

        if was_changed:
            self._handle_status_change_post_save(old_state, is_new=is_new)
            self._handle_tax_change_post_save(old_state, is_new=is_new)

    def _get_taxable_from_old_state(self, old_state: Mapping[str, Any]) -> Optional[Taxable]:
        old_price = old_state.get('price')
        if not old_price:
            return None
        old_tax_type = old_state.get('tax_type', TaxType.NO_CHARGE.value)
        old_tax_rate = old_state.get('tax_rate', Decimal(0))
        return Taxable(old_price, tax_type=TaxType(old_tax_type), tax_rate=old_tax_rate)

    def _handle_status_change_pre_save(
        self, old_state: Mapping[str, Any], update_fields: UpdateFieldsType
    ) -> None:
        """Handle status changes that should be reflected in the to-be-saved instance.

        :param old_state: The current state in the database (which is about to be overwritten).
        :param update_fields: The `update_fields` param passed to the save() method. If you
            change fields in `self` that should be persisted, be sure to update this in-place.
        """
        old_status = old_state.get('status', '')
        if old_status == self.status:
            return

        was_active = old_state.get('is_active', False)
        if self.is_active and not was_active:
            self._on_activation_pre_save(old_status, update_fields)
        elif not self.is_active and was_active:
            self._on_deactivation_pre_save(old_status, update_fields)

        if self.status == 'cancelled':
            self._on_cancelled(update_fields)

        if self.status == 'expired':
            self._on_cancelled(update_fields)

        if self.status == 'pending-cancellation':
            self._on_pending_cancellation(update_fields)

    def _handle_tax_change_pre_save(
        self, old_state: Mapping[str, Any], update_fields: UpdateFieldsType
    ) -> None:
        """Handle tax changes that should be reflected in the to-be-saved instance.

        :param old_state: The current state in the database (which is about to be overwritten).
        :param update_fields: The `update_fields` param passed to the save() method. If you
            change fields in `self` that should be persisted, be sure to update this in-place.
        """
        old_taxable = self._get_taxable_from_old_state(old_state)
        new_taxable = self.taxable
        if old_taxable == new_taxable:
            return

        # Make sure the tax amount is updated too
        if self.tax != new_taxable.charged_tax:
            self.log.warning(
                'Subscription #%s: updating tax from %s to %s (%s)',
                self.pk,
                self.tax,
                new_taxable.charged_tax,
                new_taxable,
            )
            self.tax = new_taxable.charged_tax
            if update_fields is not None:
                update_fields.add('tax')

    def _handle_tax_change_post_save(self, old_state: Mapping[str, Any], is_new: bool) -> None:
        """Record all tax-related changes."""
        # Only add admin log entries if this is an existing subscription
        if is_new:
            return

        old_tax_country = old_state.get('tax_country')
        if old_tax_country and old_tax_country != self.tax_country:
            msg = f'Tax country changed from {old_tax_country} to {self.tax_country}'
            self.log.warning(f'Subscription #{self.pk}: {msg}')
            self.attach_log_entry(msg)
        old_tax_type = old_state.get('tax_type')
        if old_tax_type is not None and old_tax_type != self.tax_type:
            msg = f'Tax type changed from {TaxType(old_tax_type)} to {TaxType(self.tax_type)}'
            self.log.warning(f'Subscription #{self.pk}: {msg}')
            self.attach_log_entry(msg)
        old_tax_rate = old_state.get('tax_rate')
        if old_tax_rate is not None and old_tax_rate != self.tax_rate:
            msg = f'Tax rate changed from {old_tax_rate}% to {self.tax_rate}%'
            self.log.warning(f'Subscription #{self.pk}: {msg}')
            self.attach_log_entry(msg)
        old_tax = old_state.get('tax')
        if old_tax is not None and old_tax != self.tax:
            msg = (
                f'Tax changed from {old_tax} to {self.tax} '
                f'({self.tax_type} {self.tax_rate}%, {self.tax_country})'
            )
            self.log.warning(f'Subscription #{self.pk}: {msg}')
            self.attach_log_entry(msg)

    def _handle_customer_changed(self, old_customer_id: Optional[int]) -> None:
        """Reset the payment method if payment method doesn't belong to new customer."""
        if old_customer_id is None:
            return

        new_customer_id = self.customer_id
        self.log.info(
            'Subscription %d changed customer from %d to %s',
            self.pk,
            old_customer_id,
            new_customer_id,
        )

        for order in self.order_set.all():
            self.log.debug(
                'transferring order %d from user %d to %d',
                order.id,
                order.customer_id,
                new_customer_id,
            )
            # Setting the payment method to None and transferring the transactions
            # to the new user too could be done in the Order.save() method, but I
            # don't want to copy the same logic there too. Only subscriptions should
            # change ownership directly; the rest should just follow.
            order.customer_id = new_customer_id
            update_fields = {'customer_id'}
            if (
                order.payment_method is not None
                and order.payment_method.customer_id != new_customer_id
            ):
                order.payment_method_id = None
                update_fields.add('payment_method')
            order.save(update_fields=update_fields)
            for transaction in order.transaction_set.all():
                transaction.customer_id = new_customer_id
                transaction.save(update_fields={'customer_id'})

        if self.payment_method is not None and self.payment_method.customer_id != new_customer_id:
            self.payment_method = None
            self.save(update_fields={'payment_method'})

    def _on_pending_cancellation(self, update_fields: UpdateFieldsType) -> None:
        """Called when the subscription transitions to the 'pending-cancellation' status."""
        self.log.info(
            'Subscription %d is pending cancellation, setting pending_cancellation_since date.',
            self.pk,
        )
        self.pending_cancellation_since = django.utils.timezone.now()

        if update_fields is not None:
            update_fields.add('pending_cancellation_since')

    def _on_cancelled(self, update_fields: UpdateFieldsType) -> None:
        """Called when the subscription transitions to the 'cancelled' status."""
        self.log.info('Subscription %d was cancelled, setting cancelled_at field too.', self.pk)
        self.cancelled_at = django.utils.timezone.now()

        if update_fields is not None:
            update_fields.add('cancelled_at')

        order = self.latest_order()
        if order and order.status in {'created', 'soft-failed', 'failed'}:
            order.status = 'cancelled'
            order.save(update_fields={'status'})

    def _on_activation_pre_save(self, old_status: str, update_fields: UpdateFieldsType) -> None:
        """Called whenever the subscription is about to become active."""
        if self.started_at is None:
            self.started_at = django.utils.timezone.now()
            if update_fields is not None:
                update_fields.add('started_at')
        self._set_next_payment_after_activation(update_fields)

    def _on_deactivation_pre_save(self, old_status: str, update_fields: UpdateFieldsType) -> None:
        """Called whenever the subscription is about to become inactive."""

    def _handle_status_change_post_save(self, old_state: Mapping[str, Any], is_new: bool) -> None:
        """Most status change handling should be done here."""
        # Use old_state.get() because there might not be an old state.
        old_status = old_state.get('status', '')
        if old_status == self.status:
            return

        was_active = old_status in self._ACTIVE_STATUSES
        if self.is_active and not was_active:
            self._on_activation_post_save(old_status)
        elif not self.is_active and was_active:
            self._on_deactivation_post_save(old_status)
        elif self.status == 'expired':
            self._on_expiration_post_save(old_status)

        # Record change of the status if this is an existing subscription
        if is_new:
            return
        self.attach_log_entry(f'Status changed from {old_status} to {self.status}')

    def _on_activation_post_save(self, old_status: str) -> None:
        """Called whenever the subscription becomes active."""
        self.log.debug('Subscription pk=%d was activated', self.pk)
        looper.signals.subscription_activated.send(sender=self, old_status=old_status)

    def _on_deactivation_post_save(self, old_status: str) -> None:
        """Called whenever the subscription becomes inactive."""
        self.log.debug('Subscription pk=%d was deactivated', self.pk)
        looper.signals.subscription_deactivated.send(sender=self, old_status=old_status)

    def _on_expiration_post_save(self, old_status: str) -> None:
        """Called whenever the subscription expires."""
        self.log.debug('Subscription pk=%d has expired', self.pk)
        looper.signals.subscription_expired.send(sender=self, old_status=old_status)

    def generate_order(self, *, save: bool = True) -> 'Order':
        """Generate an order for the current subscription.

        :param save: save the generated order to the database.
        """
        # Dynamically get the fields to copy.
        field_copies = {name: getattr(self, name) for name in SharedFields.field_names()}

        # Final amount presented to customer and charged is affected by the tax
        # and can differ from the Subscription.price
        field_copies['price'] = self.taxable.price

        customer: Customer = self.customer
        billing_address = customer.billing_address
        order = Order(
            subscription=self,
            email=billing_address.email,
            billing_address=billing_address.as_text(),
            vat_number=billing_address.vat_number,
            status='created',
            name=self.plan.product_plan_name(),
            **field_copies,
        )
        if save:
            order.save()
        return order

    def set_next_payment_after_activation(self) -> None:
        """Conditionally set `next_payment` to a suitable value and save to database."""
        update_fields: Set[str] = set()
        self._set_next_payment_after_activation(update_fields)
        if update_fields:
            self.save(update_fields=update_fields)

    def _set_next_payment_after_activation(self, update_fields: Optional[Set[str]]) -> None:
        """Conditionally set `next_payment` to a suitable value."""
        now = django.utils.timezone.now()
        if self.next_payment is not None and self.next_payment > now:
            self.log.debug(
                'Subscription %r has future next_payment, not bumping after activation.', self.pk
            )
            return
        self.bump_next_payment()
        if update_fields is not None:
            update_fields.add('next_payment')

    def bump_next_payment(self) -> None:
        """Bump the 'next_payment' field to one interval from now.

        Does NOT save this subscription; that's up to the caller.
        """
        renewal = django.utils.timezone.now()
        self.next_payment = renewal + self.interval
        self.log.info('Bumped subscription %r next_payment to %s', self.pk, self.next_payment)

    def __str__(self) -> str:
        try:
            plan = self.plan.name
        except Plan.DoesNotExist:
            plan = '(no plan)'
        try:
            customer = self.customer.billing_address.full_name
        except Customer.DoesNotExist:
            customer = '(no customer)'
        return f'{self.id} - {customer} - {plan}'

    def attach_log_entry(self, message: str) -> None:
        """Attach an admin history log entry."""
        admin_log.attach_log_entry(self, message)

    def extend_subscription(self, *, from_timestamp: datetime.datetime, months: float) -> None:
        """Extend a subscription by shifting the next_payment date.

        :param from_timestamp: the timestamp to add the number of months to.
        :param months: (possibly fractional) number of months to add.
        """

        # By splitting up fractional months into months + days, paying for one
        # month worth of subscription will just increment the month by one.
        # This means that there is a fixed price per month (so january and
        # february cost the same, even though they have different length in
        # days). Any extra days are place on top of that.
        #
        # Personally I (Sybren) think this gives more intuitive results than
        # converting the entire 'months' into 'months * 30.4' days. With the
        # current implementation paying €50 for a €5-per-month subscription
        # will simply give you 10 months (instead of 304 days).
        entire_months = int(months // 1)
        fractional_month = months % 1
        days = fractional_month * 30.4  # Average 30.4 days per month

        # MyPy doesn't know relativedelta() can handle float days.
        delta = dateutil.relativedelta.relativedelta(
            months=entire_months, days=days  # type: ignore
        )
        new_next_payment = from_timestamp + delta

        self.log.info(
            'Extending subscription %r next_payment by %.3f months (%s) from %s to %s',
            self.id,
            months,
            delta,
            self.next_payment,
            new_next_payment,
        )
        self.next_payment = new_next_payment
        self.save(update_fields={'next_payment'})

    def switch_payment_method(self, payment_method: PaymentMethod) -> None:
        """Also switches collection method if needed.

        Does not influence any orders; that's the caller's responsibility.
        """

        self.log.info(
            'Switching subscription pk=%r to payment method pk=%r', self.pk, payment_method.pk
        )
        self.attach_log_entry(
            'Switching payment method from'
            f' {getattr(self.payment_method, "recognisable_name", "none")}'
            f' to {payment_method.recognisable_name}'
        )

        self.payment_method = payment_method
        supported = payment_method.gateway.provider.supported_collection_methods
        if self.collection_method in supported:
            self.save(update_fields={'payment_method'})
            return

        new_method = supported.copy().pop()
        self.attach_log_entry(
            f'Switching collection method from {self.collection_method} to {new_method}'
            f' because the new payment gateway only supports {supported!r}'
        )
        self.log.info(
            'Switching collection method of subscription pk=%r from %r to %r '
            ' because the new payment gateway only supports %r',
            self.pk,
            self.collection_method,
            new_method,
            supported,
        )
        self.collection_method = new_method
        self.save(update_fields={'payment_method', 'collection_method'})

    def update_tax(self, save=True):
        """Update tax type and rate, based on the plan and customer's billing details.

        It's up to the Django project to call this method when it's appropriate to update
        a Subscription's tax fields.
        It could be triggered by a signal, called by a view or a cronjob
        checking if a Customer's billing details are still valid.
        """
        customer: Customer = self.customer
        billing_address: Address = customer.billing_address
        self.tax_country = billing_address.country
        product_type = self.plan.product.type
        tax_type, tax_rate = customer.get_tax(product_type=product_type)
        self.tax_type = tax_type.value
        self.tax_rate = tax_rate

        if save:
            self.save(update_fields=self.TAX_FIELDS)

    def price_per_month(self):
        # TODO this is a copy-paste of PlanVariation method,
        # but it seems like we could deprecate that method, verify that and cleanup or refactor
        months = {
            'day': 12 / 365,  # Approximate average nr of months in a day.
            'week': 12 / 52,  # Approximate average nr of months in a week.
            'month': 1,
            'year': 12,
        }
        months_per_interval = months[self.interval_unit] * self.interval_length
        return self.price // months_per_interval


class OrderManager(models.Manager):
    def paid(self) -> 'models.QuerySet[Order]':
        """Return only paid orders, so status in 'paid' or 'fulfilled'."""
        return self.filter(status__in={'paid', 'fulfilled'})

    def payable(self) -> 'models.QuerySet[Order]':
        """Return orders that can still be paid."""
        return self.filter(status__in={'created', 'soft-failed'})


# Ignoring type because of the inner Meta class of the two mix-ins.
class Order(
    model_mixins.RecordModificationMixin,
    model_mixins.CreatedUpdatedMixin,
    model_mixins.StateMachineMixin,
    SharedFields,
    models.Model,
):
    class Meta:
        # We just want to order by creation time, which is reflected in the
        # strictly increasing primary key. Furthermore, that key is already
        # indexed in the database.
        get_latest_by = 'pk'
        ordering = ('-pk',)
        constraints = [
            CheckConstraint(
                check=Q(product__isnull=False) | Q(subscription__isnull=False),
                name='check_looper_order_subscription_or_product_not_null',
            ),
        ]
        indexes = [
            models.Index(fields=['created_at']),
            models.Index(fields=['paid_at']),
        ]

    record_modification_fields = {'status'}
    log = log.getChild('Order')

    DEFAULT_STATUS = 'created'
    STATUSES = (
        ('created', 'Created'),
        ('soft-failed', 'Soft-Failed; queued for retrying'),
        ('failed', 'Renewal Failed; manual payment needed'),
        ('paid', 'Paid'),
        ('cancelled', 'Cancelled'),
        ('fulfilled', 'Fulfilled'),
        ('refunded', 'Refunded'),
    )

    # Map 'current status': {set of allowed new statuses}
    VALID_STATUS_TRANSITIONS: Dict[str, Set[str]] = {
        'created': {'soft-faled', 'failed', 'paid', 'cancelled'},
        'soft-failed': {'failed', 'paid', 'cancelled'},
        'failed': {'cancelled', 'paid'},
        'paid': {'fulfilled'},
        'cancelled': set(),
        'fulfilled': set(),
    }

    # Each Order must refer to either a Subscription or a Product
    subscription = models.ForeignKey(Subscription, on_delete=models.CASCADE, blank=True, null=True)
    product = models.ForeignKey(
        Product,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        help_text="Only set when order doesn't have a subscription",
    )

    name = models.CharField(
        max_length=255, blank=True, help_text='What the order is for; shown to the customer'
    )
    email = models.EmailField(max_length=255)  # Snapshot of the current email
    billing_address = models.TextField()  # Snapshot of the current address
    vat_number = models.CharField(  # Snapshot of the current VATIN, if supplied
        max_length=255, blank=True, default='', verbose_name='VAT identification number'
    )
    external_reference = models.CharField(
        max_length=255,
        blank=True,
        default='',
        help_text=(
            'External reference provided by the customer, e.g. purchase order number. '
            'Shown in the receipt/invoice generated for this order.'
        ),
    )

    status = models.CharField(choices=STATUSES, default=DEFAULT_STATUS, max_length=20)
    collection_attempts = models.IntegerField(
        default=0,
        blank=True,
        editable=False,
        help_text='How often an automatic collection attempt was made.',
    )
    paid_at = models.DateTimeField(
        null=True, blank=True, help_text='When the order was paid, if at all.'
    )
    refunded_at = models.DateTimeField(null=True, help_text='Date of latest refund, if any.')
    refunded: Money = MoneyField(blank=True, default=0, help_text='Refunded total, if any.')
    tax_refunded: Money = MoneyField(blank=True, default=0, help_text='Refunded tax, if any.')

    retry_after = models.DateTimeField(
        null=True, blank=True, help_text='When automatic collection should be retried.'
    )

    number = models.CharField(
        null=True,
        unique=True,
        max_length=255,
        help_text='Optional customer-facing order number'
    )
    is_legacy = models.BooleanField(
        blank=False,
        default=False,
        null=False,
        help_text='Whether or not this order was migrated from Blender Store',
    )

    objects: OrderManager = OrderManager()

    @property
    def display_number(self) -> str:
        """Customer-facing number for this order for use in emails, receipts and so on."""
        return f'{self.number or self.pk}'

    def latest_transaction(self) -> Optional['Transaction']:
        """Returns the latest transaction for this Order, if there is one"""
        try:
            return self.transaction_set.latest()
        except Transaction.DoesNotExist:
            return None

    def save(self, *args: Any, **kwargs: Any) -> None:
        """Act on state changes."""
        was_changed, old_state = self.pre_save_record(*args, **kwargs)

        old_status = old_state.get('status', '')
        if (
            was_changed
            and old_status != self.status
            and self.status == 'paid'
            and self.paid_at is None
        ):
            self.paid_at = django.utils.timezone.now()
            update_fields: UpdateFieldsType = kwargs.get('update_fields')
            if update_fields is not None:
                update_fields.add('paid_at')

        super().save(*args, **kwargs)

        if was_changed:
            self._handle_status_change(old_status=old_status)

    def _handle_status_change(self, old_status: str) -> None:
        if old_status == self.status:
            return

        if self.status == 'paid':
            self._on_status_paid(old_status)
        else:
            self.log.debug('Order pk=%d changed status %r to %r', self.pk, old_status, self.status)

    def _on_status_paid(self, old_status: str) -> None:
        """Activate the subscription if possible."""
        if self.subscription is None:
            return

        if self.subscription.status == 'active':
            # Always check/bump the next payment date after activation:
            self.subscription.set_next_payment_after_activation()
            return

        if not self.subscription.can_be_activated:
            self.log.error(
                'Order pk=%d changed from %r to %r, but subscription %d cannot '
                'be activated from subscription status %r',
                self.pk,
                old_status,
                self.status,
                self.subscription.pk,
                self.subscription.status,
            )
            return

        self.log.info(
            'Order pk=%d changed from %r to %r, activating subscription %d',
            self.pk,
            old_status,
            self.status,
            self.subscription.pk,
        )
        self.subscription.status = 'active'
        self.subscription.save(update_fields={'status'})

    def has_recurring_transactions(self) -> bool:
        """Return true if a successful recurring transaction exists for this order."""
        return self.payment_method.transaction_set.filter(
            status='succeeded', amount=self.price
        ).exists()

    def get_transaction_authentication(
        self,
        is_recurring: bool,
    ) -> Tuple[Optional[PaymentMethodAuthentication], Optional[str]]:
        """Return appropriate payment method authentication and transaction source."""
        authentication = self.payment_method.get_unused_authentication()
        source = None
        if is_recurring:
            # If this is supposed to be a recurring transaction, but an unused authentication exists
            # or no other successful transactions for this method were found,
            # this transaction must be marked as recurring first:
            if authentication or not self.has_recurring_transactions():
                source = 'recurring_first'
            else:
                source = 'recurring'
        return authentication, source

    def generate_transaction(
        self,
        *,
        ip_address: Optional[str] = None,
        is_recurring: bool = False,
        save: bool = True,
    ) -> 'Transaction':
        """Generate a transaction that allows the customer to pay for the order.

        :param save: The transaction is saved to the database when save=True.
            Otherwise it's up to the caller to do this. This allows simulation
            of the subscription renewal flow.
        """
        authentication, source = self.get_transaction_authentication(is_recurring)
        transaction = Transaction(
            amount=self.price,
            currency=self.currency,
            customer=self.customer,
            order=self,
            paid=False,
            payment_method=self.payment_method,
            authentication=authentication,
            source=source,
            ip_address=ip_address,
        )
        if save:
            transaction.save()
        return transaction

    def __str__(self) -> str:
        s = f'Order {self.display_number}'
        if self.subscription_id:
            s += f' for Subscription {self.subscription_id}'
        return s

    def get_total_refunded(self) -> Money:
        """Compute the total refunded amount for this order.

        Since an order can have multiple transactions that all have a refunded
        amount, we have to sum them all.

        Note that this function assumes that all transactions have the same
        currency as the order.
        """
        result = self.transaction_set.all().aggregate(models.Sum('amount_refunded'))
        # Prevent None, which happens when an order has no transactions.
        refunded_sum = result['amount_refunded__sum'] or 0
        return Money(self.currency, refunded_sum)

    def get_tax_refunded(self) -> Money:
        """Compute the amount of tax refunded for this order."""
        if self.tax._cents == 0:
            return Money(self.currency, 0)
        total_refunded = self.get_total_refunded()
        if total_refunded._cents == 0:
            return Money(self.currency, 0)

        refund_taxable = Taxable(
            total_refunded, tax_type=TaxType(self.tax_type), tax_rate=self.tax_rate
        )
        return refund_taxable.tax

    def attach_log_entry(self, message: str) -> None:
        """Attach an admin history log entry to this Order and its Subscription."""
        admin_log.attach_log_entry(self, message)

        admin_url = reverse('admin:looper_order_change', kwargs={'object_id': self.id})
        subs: Subscription = self.subscription  # make PyCharm happy
        if subs is not None:
            subs.attach_log_entry(
                format_html('{} for <a href="{}">order #{}</a>', message, admin_url, self.id)
            )

    def switch_payment_method(self, payment_method: PaymentMethod) -> None:
        """Also switches collection method if needed.

        Does not influence the subscription; that's the caller's responsibility.
        """

        self.log.info('Switching order pk=%r to payment method pk=%r', self.pk, payment_method.pk)
        self.attach_log_entry(
            'Switching payment method from'
            f' {getattr(self.payment_method, "recognisable_name", "none")}'
            f' to {payment_method.recognisable_name}'
        )

        self.payment_method = payment_method
        supported = payment_method.gateway.provider.supported_collection_methods
        if self.collection_method in supported:
            self.save(update_fields={'payment_method'})
            return

        new_method = supported.copy().pop()
        self.attach_log_entry(
            f'Switching collection method from {self.collection_method} to {new_method}'
            f' because the new payment gateway only supports {supported!r}'
        )
        self.log.info(
            'Switching collection method of order pk=%r from %r to %r '
            ' because the new payment gateway only supports %r',
            self.pk,
            self.collection_method,
            new_method,
            supported,
        )
        self.collection_method = new_method
        self.save(update_fields={'payment_method', 'collection_method'})

    @requires_status('created', 'soft-failed', 'failed')
    def update(self):
        """Re-generate order copying all subscription values and updating the price.

        This method must be called explicitly when necessary, e.g.:
        when billing details are updated, affecting a yet unpaid order.
        """
        if self.subscription is None:
            # TODO: no subscription
            return
        order_tmp = self.subscription.generate_order(save=False)
        # Setting Money values will fail in case currency is changing unless it's updated first
        self.currency = order_tmp.currency
        for field in self._meta.fields:
            # Don't change dates or the primary key to avoid creating duplicate orders
            if field.primary_key or isinstance(field, models.DateTimeField):
                continue
            # Don't change order numbers either
            if field.name == 'number':
                continue
            setattr(self, field.name, getattr(order_tmp, field.name))
        self.save()

    @property
    def pi_id(self) -> Optional[str]:
        """Return Stripe PaymentIntent ID of this order, if exists."""
        gw_order_id = self.gateway_order_ids.filter(gateway__name='stripe').first()
        return gw_order_id.gateway_order_id if gw_order_id else None


class Transaction(model_mixins.CreatedUpdatedMixin, models.Model):
    """Transaction that can be charged or refunded."""

    log = log.getChild('Transaction')

    class Meta:
        # We just want to order by creation time, which is reflected in the
        # strictly increasing primary key. Furthermore, that key is already
        # indexed in the database.
        get_latest_by = 'pk'

    STATUS_DEFAULT = 'pending'
    STATUSES = (
        ('succeeded', 'Succeeded'),
        ('pending', 'Pending'),
        ('failed', 'Failed'),
        ('refunded', 'Refunded'),
    )
    SOURCES = (
        ('recurring', 'Recurring'),
        ('recurring_first', 'Recurring (first)'),
        (None, 'Unscheduled'),
    )

    order = models.ForeignKey(Order, on_delete=models.CASCADE, null=True)
    payment_method = models.ForeignKey(PaymentMethod, on_delete=models.CASCADE)
    # Can only be used once
    authentication = models.OneToOneField(
        PaymentMethodAuthentication,
        on_delete=models.CASCADE,
        null=True,
    )
    # How a transaction was created, possible values:
    # * "recurring" if initiated by a clock tick;
    # * "recurring_first" if initiated by a clock tick using previously acquired SCA,
    #                     or by a customer, creating a new subscription;
    # * null if neither of the above apply (e.g. this is a manually paid unscheduled transaction).
    source = models.CharField(choices=SOURCES, null=True, max_length=20)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)

    status = models.CharField(choices=STATUSES, default=STATUS_DEFAULT, max_length=20)
    currency: str = CurrencyField()
    amount: Money = MoneyField(default=0)
    ip_address = models.GenericIPAddressField(
        null=True, help_text='IP address of the user at the moment of paying.'
    )

    # TODO(Sybren): investigate DB constraint that requires both NULL or neither NULL.
    refunded_at = models.DateTimeField(null=True)
    amount_refunded: Money = MoneyField(blank=True, default=0, help_text='Refunded amount, if any.')

    failure_message = models.TextField(blank=True, default='')
    failure_code = models.CharField(blank=True, max_length=50, default='', db_index=True)
    paid = models.BooleanField(default=False)

    transaction_id = models.CharField(
        max_length=128, blank=True, default='', help_text='ID of the transaction on the gateway.'
    )

    @property
    def refundable(self) -> Money:
        return self.amount - self.amount_refunded

    def __str__(self) -> str:
        return f'Transaction {self.transaction_id or self.pk} for order {self.order.display_number}'

    def save(self, *args: Any, **kwargs: Any) -> None:
        """Act on state changes."""
        is_new = not self.pk
        super().save(*args, **kwargs)

        if is_new:
            self.log.debug('Transaction pk=%d was just created.', self.pk)

    def mark_as_failed(self, failure_message: str, failure_code: Optional[str] = ''):
        """Mark as failed and persist failure status and message."""

        self.status = 'failed'
        self.failure_message = failure_message
        self.failure_code = failure_code
        self.save(update_fields={'status', 'failure_message', 'failure_code'})

    @requires_status('pending')
    def charge(self) -> bool:
        """Actually charge the customer.

        :return: True when succesful, False otherwise, in which case `self.status`
            will be set to 'failed' and `self.failure_message` will contain the
            error description.
        """
        assert self.pk, 'Transaction needs to be saved before charging'

        authentication_id = self.authentication.authentication_id if self.authentication else None
        try:
            transaction_result = self.payment_method.gateway.provider.transact_sale(
                self.payment_method.token,
                self.order.price,
                source=self.source,
                # braintree-specific
                authentication_id=authentication_id,
                # stripe-specific
                payment_intent_id=self.order.pi_id,
                payment_intent_metadata={'order_id': self.order.pk},
            )
            payment_intent_id = transaction_result.get('payment_intent_id', None)
            transaction_id = transaction_result['transaction_id']
        except looper.exceptions.GatewayError as ex:
            self.log.warning(
                'Transaction pk=%d transact_sale() call failed (%r), failing transaction',
                self.pk,
                ex.message,
            )
            self.mark_as_failed(ex.with_errors(), ex.code)
            self.attach_log_entry(f'Charge of {self.amount} failed: {ex!s}')
            return False
        except IOError as ex:
            # All Requests connection/timeout errors subclass IOError.
            self.log.warning(
                'Transaction pk=%d transact_sale() call failed (%r), failing transaction',
                self.pk,
                ex,
            )
            self.mark_as_failed(str(ex))
            self.attach_log_entry(f'Charge of {self.amount} failed: {ex!s}')
            return False

        self.mark_as_paid(transaction_id)
        if payment_intent_id:
            GatewayOrderId(
                order=self.order,
                gateway=self.payment_method.gateway,
                gateway_order_id=payment_intent_id,
            ).save()

        # Mark the payment method authentication as used
        if self.authentication and not self.authentication.consumed_at:
            self.authentication.mark_as_consumed()
        return True

    @requires_status('pending')
    def mark_as_paid(self, transaction_id: str):
        assert isinstance(
            transaction_id, str
        ), f'transaction_id should be string, not {transaction_id!r}'

        self.log.info(
            'Transaction pk=%d was succesful, storing transaction ID %r amount=%s source=%s',
            self.pk,
            transaction_id,
            self.amount,
            self.source,
        )

        self.status = 'succeeded'
        self.paid = True
        self.transaction_id = transaction_id
        self.save(update_fields={'status', 'transaction_id', 'paid'})
        self.attach_log_entry(f'Charge of {self.amount} was successful')

        # TODO(Sybren): shouldn't this be done in Transaction.save(), in response
        # to the status changing to 'succeeded'?
        self.order.status = 'paid'
        self.order.save(update_fields={'status'})

    @requires_status('succeeded')
    def refund(self, amount: Money) -> None:
        """Refund the customer.

        :param amount: Amount to refund. May not be more than the total
            transaction amount minus the amount refunded so far.

        :raises looper.exceptions.GatewayError: when the refund could not
            be performed at the payment gateway.
        """
        assert self.pk, 'Transaction needs to be saved before refunding'
        assert isinstance(amount, Money), f'amount must be Money, not {amount!r}'
        if amount.currency != self.currency:
            raise ValueError(
                f'Refund currency {amount.currency!r} does not match '
                'transaction currency {self.currency!r}'
            )

        try:
            self.payment_method.gateway.provider.refund(self.transaction_id, amount)
        except looper.exceptions.GatewayError:
            raise

        self.log.info(
            'Refund of transaction pk=%d, ID %r for amount=%r was succesful',
            self.pk,
            self.transaction_id,
            amount,
        )

        self.refunded_at = django.utils.timezone.now()
        self.amount_refunded = self.amount_refunded + amount
        self.save(update_fields={'refunded_at', 'amount_refunded'})
        self.attach_log_entry(f'Refund of {amount} was successful')

        # Update order's refunded amount as well
        order = self.order
        order.refunded = order.get_total_refunded()
        order.refunded_at = self.refunded_at
        order.tax_refunded = order.get_tax_refunded()
        if order.refunded == order.price:
            order.status = 'refunded'
        order.save(update_fields={'refunded', 'tax_refunded', 'refunded_at', 'status'})
        order.attach_log_entry(
            f'Updated refunded amount to {order.refunded} (refunded tax {order.tax_refunded})'
        )

    def attach_log_entry(self, message: str) -> None:
        """Attach an admin history log entry to this Transaction and its Order + Subscription."""
        admin_log.attach_log_entry(self, message)

        admin_url = reverse('admin:looper_transaction_change', kwargs={'object_id': self.id})
        order: Order = self.order  # make PyCharm happy
        order.attach_log_entry(
            format_html('{} for <a href="{}">transaction #{}</a>', message, admin_url, self.id)
        )


class LinkCustomerToken(model_mixins.CreatedUpdatedMixin, models.Model):
    """Token that can be used to link an accountless customer to a user."""

    def _generate_token():
        return secrets.token_urlsafe(32)

    customer = models.OneToOneField(
        Customer, on_delete=models.CASCADE, related_name='token', null=True
    )
    linked_to_user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        related_name='linked_tokens',
    )
    token = models.CharField(
        null=False,
        unique=True,
        max_length=255,
        help_text='A unique token used to link customer data to a user.',
        default=_generate_token,
    )

    class Meta:
        indexes = [models.Index(fields=['token'])]


class GatewayOrderId(models.Model):
    """Gateway order ID if such a concept exists at a particular payment provider.

    GatewayOrderId exists to link Order with a particular object at a payment gateway API.
    Without these it would be impossible to identify which Order must be modified by events,
    passively received from the gateway, e.g. via webhooks.

    In case of Braintree no references to order exist.
    In case of Stripe `gateway_order_id` is ID of a PaymentIntent.
    """

    class Meta:
        indexes = [
            models.Index(fields=['gateway', 'gateway_order_id']),
        ]
        unique_together = ('gateway', 'gateway_order_id')

    order = models.ForeignKey(Order, on_delete=models.CASCADE, related_name='gateway_order_ids')
    gateway = models.ForeignKey(Gateway, on_delete=models.CASCADE)
    gateway_order_id = models.CharField(max_length=255)

    def __str__(self) -> str:
        return f'GatewayOrderId {self.gateway_order_id}'


# class Coupon(models.Model):
#     """Discounts! Implement later."""
#     pass
#
#
