"""Publicly usable exceptions.

Some of those exceptions may have more specific subclasses (such
as BraintreeError), but those shouldn't be used outside the looper
Django app.
"""
from typing import Iterable, List, Optional, Set


class LooperError(Exception):
    """Superclass of all Looper-specific exceptions."""

    def __init__(self, message: str) -> None:
        super().__init__(message)
        # The superclass stores the message in self.args[0], which isn't as nice.
        self.message = message


class GatewayConfigurationError(LooperError):
    """Raised when an Gateway provider is misconfigured."""


class GatewayConfigurationMissing(LooperError):
    """Raised when an Gateway provider is used but not configured."""


class GatewayNotImplemented(LooperError):
    """Raised when a provider is requested that does not exist."""


class GatewayError(LooperError):
    """Generic gateway exception. Supports an additional list of errors."""

    def __init__(self, message: str, errors: Optional[Iterable[str]] = None, code: Optional[str] = '') -> None:
        super().__init__(message)
        self.errors: List[str] = list(errors) if errors else []
        self.code = code
        assert all(isinstance(err, str) for err in self.errors)

    def with_errors(self) -> str:
        if self.errors:
            return f'{self.message}: {"; ".join(self.errors)}'
            if self.code:
                return f'{self.message}: {"; ".join(self.errors)}, code: {self.code}'
        return self.message

    def __str__(self) -> str:
        return self.with_errors()


class NotFoundError(GatewayError):
    """Raised when something cannot be found.

    Raised by, for example, AbstractPaymentGateway.find_customer_payment_method().
    """


class CurrencyNotSupported(LooperError):
    """A currency is requested that is not supported by the payment gateway."""


class IncorrectStatusError(LooperError):
    """The requested action is not allowed in the current state.

    Raised by functions on database models requiring the model's 'status'
    property to have a certain value, when this requirement isn't met.
    """

    def __init__(self, message: str, actual_status: str, required_status: Set[str]) -> None:
        super().__init__(message)
        self.actual_status = actual_status
        self.required_status = required_status
