from typing import Optional

from django.core.exceptions import ValidationError
from django.http import HttpResponse, HttpResponseBadRequest
import django.forms

import looper.utils as utils


class ExpectReadableIPAddressMixin:
    def _check_customer_ip_address(self, form: django.forms.Form = None) -> Optional[HttpResponse]:
        """Check request's IP address and block the user if it is not valid.

        :return: an HttpResponse if there is an error to show, or None if everything was fine.
        """
        try:
            utils.clean_ip_address(self.request)
        except ValidationError:
            message = 'Unable to proceed with the payment'
            if form:
                form.add_error('', message)
                response = self.form_invalid(form)
            else:
                response = HttpResponseBadRequest()
            response.status_code = 400
            response.reason_phrase = message
            return response
        return None
