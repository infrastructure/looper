from typing import Optional, Tuple
import datetime
import logging
import urllib

import dateutil.parser
from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth import get_user_model
from django.core.mail import mail_admins
from django.core.exceptions import ValidationError
from django.http import HttpResponse
from django.http.request import validate_host
from django.shortcuts import redirect, get_object_or_404, render
from django.urls import reverse
from django.utils import timezone
from django.views.generic import FormView
import requests.adapters

from looper.forms_braintree import CheckoutForm
from .. import exceptions, middleware, models, utils

recaptcha_session = requests.Session()
recaptcha_session.mount(
    'https://',
    requests.adapters.HTTPAdapter(
        max_retries=requests.adapters.Retry(total=10, backoff_factor=0.2),
    ),
)

log = logging.getLogger(__name__)
User = get_user_model()


class AbstractPaymentView(FormView):
    """Superclass for Braintree payment screens."""

    log = log.getChild('AbstractPaymentView')

    gateway: models.Gateway
    customer: models.Customer
    session_key_prefix = 'PAYMENT_GATEWAY_CLIENT_TOKEN_'

    def dispatch(self, request, *args, **kwargs):
        if not getattr(self, 'gateway', None):
            # Only set self.gateway if it hasn't been set already. This allows
            # subclasses to override the default gateway in their dispatch()
            # method before calling super().dispatch().
            self.gateway = models.Gateway.objects.get(name__in={'braintree', 'mock'})
        self.user = self.request.user
        if self.user.is_authenticated:
            self.customer = self.user.customer
        else:
            self.customer = models.Customer()
        return super().dispatch(request, *args, **kwargs)

    def get_initial(self) -> dict:
        return {
            'gateway': self.gateway.name,
        }

    def gateway_from_form(self, form) -> models.Gateway:
        self.gateway = form.cleaned_data['gateway']
        return self.gateway

    def get_context_data(self, **kwargs) -> dict:
        currency = self.get_currency()
        ctx = {
            **super().get_context_data(**kwargs),
            'client_token': self.get_client_token(currency),
        }
        return ctx

    def get_currency(self) -> str:
        """Returns the currency for the current page.

        Defaults to the user's preferred currency, but can be overridden
        in a subclass.
        """
        return self.request.session.get(
            middleware.PREFERRED_CURRENCY_SESSION_KEY, models.DEFAULT_CURRENCY
        )

    def client_token_session_key(self, for_currency: str) -> str:
        return f'{self.session_key_prefix}{self.gateway.pk}_{for_currency}'

    def get_client_token(self, for_currency: str) -> str:
        """Generate a Braintree client token, caching it in the session.

        This is hard-coded to Braintree instead of using the current payment
        gateway, because the user can switch between gateways in the web
        interface and thus we have to be able to show the Braintree drop-in
        UI at any moment.
        """

        sesskey = self.client_token_session_key(for_currency)
        sesskey_expiry = f'{sesskey}_expire'

        try:
            client_token = self.request.session[sesskey]
            token_expires = self.request.session[sesskey_expiry]
        except KeyError:
            pass
        else:
            expiry_timestamp = dateutil.parser.parse(token_expires)
            if expiry_timestamp > datetime.datetime.now():
                # Not yet expired, use the client token.
                return client_token

        # TODO(Sybren): handle Gateway errors.
        braintree_gw = models.Gateway.objects.get(name__in={'braintree', 'mock'})
        gateway_customer_id = self.customer.gateway_customer_id(braintree_gw)
        assert gateway_customer_id is not None, 'gateway_customer_id is missing'
        client_token = braintree_gw.provider.generate_client_token(
            for_currency, gateway_customer_id
        )

        expiry_timestamp = datetime.datetime.now() + datetime.timedelta(minutes=10)
        self.request.session[sesskey_expiry] = expiry_timestamp.isoformat()
        self.request.session[sesskey] = client_token

        return client_token

    def erase_client_token(self) -> None:
        """Erase the client token from the session."""

        to_erase = [
            key for key in self.request.session.keys() if key.startswith(self.session_key_prefix)
        ]
        for sesskey in to_erase:
            del self.request.session[sesskey]

    def _check_customer_ip_address(self, form: CheckoutForm) -> Optional[HttpResponse]:
        """Check request's IP address and block the user if it is not valid.

        :return: an HttpResponse if there is an error to show, or None if everything was fine.
        """
        try:
            utils.clean_ip_address(self.request)
        except ValidationError:
            message = 'Unable to proceed with the payment'
            form.add_error('', message)
            response = self.form_invalid(form)
            response.status_code = 400
            response.reason_phrase = message
            return response
        return None


def check_recaptcha(request) -> Tuple[Optional[bool], str]:
    """Handles recaptcha verification and sets request.recaptcha_is_valid.

    Be sure to set settings.GOOGLE_RECAPTCHA_SECRET_KEY to a non-empty string,
    otherwise this function does nothing.

    :returns: None when no check was performed, or True/False indicating a
        successful resp. unsuccessful check.
    """

    my_log = log.getChild('check_recaptcha')
    recaptcha_response = request.POST.get('g-recaptcha-response') or ''
    if not recaptcha_response:
        my_log.warning('reCaptcha response not included in request')
        return (
            False,
            'ReCaptcha failed to check your request. Please disable script/ad '
            'blockers and try again.',
        )

    data = {'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY, 'response': recaptcha_response}
    try:
        r = recaptcha_session.post('https://www.google.com/recaptcha/api/siteverify', data=data)
    except IOError as ex:
        my_log.exception('Error communicating with Google reCAPTCHA service')

        full_url = request.build_absolute_uri()
        mail_admins(
            'reCaptcha communication error',
            f'A request on {full_url} needed a reCaptcha check. The HTTPS request'
            f' failed with exception {ex}',
        )

        return None, 'There was a communication error checking reCAPTCHA. Please try again.'

    if r.status_code != 200:
        my_log.error("Error code %d verifying reCaptcha: %s", r.status_code, r.text)

        full_url = request.build_absolute_uri()
        mail_admins(
            'reCaptcha communication error',
            f'A request on {full_url} needed a reCaptcha check. The HTTPS request'
            f' failed with error {r.status_code} and this message:\n{r.text}',
        )

        return None, 'There was a communication error checking reCAPTCHA. Please try again.'

    result = r.json()

    if not result.get('success', False):
        my_log.warning(
            "reCaptcha failed to verify for URL %s: %s", request.build_absolute_uri(), r.text
        )
        return False, 'ReCaptcha failed to verify that you are human being. Please try again.'

    # Check that nobody is trying to fake the response via some other website.
    hostname = result.get('hostname')
    if not validate_host(hostname, settings.ALLOWED_HOSTS):
        my_log.error(
            "reCaptcha verified but for an unexpected hostname %r for URL %s",
            hostname,
            request.build_absolute_uri(),
        )
        return (
            False,
            'reCaptcha verified, but for an unexpected hostname. Please try again. If '
            'this keeps happening, send an email to production@blender.org.',
        )

    my_log.debug('reCaptcha verfied')
    return True, ''


class CheckoutView(AbstractPaymentView):
    """Perform the checkout procedure for a subscription."""

    template_name = 'checkout/checkout.html'
    form_class = CheckoutForm

    log = log.getChild('CheckoutView')

    plan: models.Plan
    plan_variation: models.PlanVariation

    def dispatch(self, request, *args, **kwargs):
        # Get the prerequisites for handling any request in this view.
        self.plan = get_object_or_404(models.Plan, pk=kwargs['plan_id'])
        self.plan_variation = get_object_or_404(
            self.plan.variations.active(), pk=kwargs['plan_variation_id']
        )
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs) -> dict:
        ctx = {
            **super().get_context_data(**kwargs),
            'plan': self.plan,
            'plan_variation': self.plan_variation,
        }
        return ctx

    def get_form_kwargs(self) -> dict:
        return {
            **super().get_form_kwargs(),
            'instance': self.customer.billing_address,
        }

    def get_form(self, form_class=None):
        form = super().get_form(form_class)

        if self.request.method == 'GET':
            # Do NOT call this here on a POST. At this point in the code,
            # the form hasn't been updated with the POSTed data yet,
            # and thus will nag about recent subscriptions even when the user has confirmed.
            recent_subs = self._check_recently_created_subscription(form)
            if recent_subs is not None:
                self._update_form_for_recent_subscription(form, recent_subs)

        return form

    def get_initial(self) -> dict:
        return {
            **super().get_initial(),
            'price': self.plan_variation.price.decimals_string,
            'email': self.customer.billing_address.email,
            'full_name': self.customer.billing_address.full_name,
        }

    def form_valid(self, form: CheckoutForm) -> HttpResponse:
        assert self.request.method == 'POST'

        response = self._check_recaptcha(form)
        if response:
            return response

        response = self._check_customer_ip_address(form)
        if response:
            return response

        gateway = self.gateway_from_form(form)

        # Update billing address if it changed.
        if form.has_changed():
            form.instance.customer.billing_email = form.cleaned_data['email']
            form.instance.customer.save()
            form.save()

        payment_method = self._check_payment_method_nonce(form, gateway)
        if payment_method is None:
            return self.form_invalid(form)

        recent_subs = self._check_recently_created_subscription(form)
        if recent_subs is not None:
            self._update_form_for_recent_subscription(form, recent_subs)
            return self.form_invalid(form)

        subscription = self._get_existing_subscription(form)
        if not subscription:
            subscription = self._create_subscription(gateway, payment_method)

        order = self._fetch_or_create_order(form, subscription)
        response = self._charge_if_supported(form, gateway, order)
        return response

    def _check_recaptcha(self, form: CheckoutForm) -> Optional[HttpResponse]:
        """Check the CAPTCHA and block the user if it is not valid.

        :return: an HttpResponse if there is an error to show, or None if everything was fine.
        """

        if not settings.GOOGLE_RECAPTCHA_SECRET_KEY:
            return None

        recaptcha_valid, message = check_recaptcha(self.request)
        if recaptcha_valid:
            return None

        form.add_error('', message)
        response = self.form_invalid(form)
        response.status_code = 400
        response.reason_phrase = 'CAPTCHA not valid'
        return response

    def _check_payment_method_nonce(
        self, form: CheckoutForm, gateway: models.Gateway
    ) -> Optional[models.PaymentMethod]:
        """Check the Braintree nonce, block the user if it is not valid.

        :return: the Payment Method, or None if there was an issue.
        """
        nonce = form.cleaned_data['payment_method_nonce']
        device_data = form.cleaned_data.get('device_data')
        try:
            payment_method = self.customer.payment_method_add(
                nonce, gateway, verification_data=device_data
            )
        except exceptions.GatewayError as e:
            self.log.exception('Error adding payment method: %s', e.errors)
            form.add_error(
                '',
                f'Error from the payment gateway: {e.with_errors()}. '
                'Please refresh the page and try again.',
            )
            # Erase the token in case the error is caused by a mismatching currency
            self.erase_client_token()
            return None
        return payment_method

    def _get_existing_subscription(self, form: CheckoutForm) -> Optional[models.Subscription]:
        subs_pk = form.cleaned_data.get('subscription_pk')
        if not subs_pk:
            return None

        # The following subscription lookup is only possible for a logged-in user
        if self.user.is_anonymous:
            return None

        subscription: models.Subscription = get_object_or_404(
            self.user.customer.subscription_set, pk=subs_pk
        )
        self.log.debug('Reusing subscription pk=%r for checkout', subscription.pk)
        return subscription

    def _create_subscription(
        self, gateway: models.Gateway, payment_method: models.PaymentMethod
    ) -> models.Subscription:
        collection_method = self.plan_variation.collection_method
        if collection_method not in gateway.provider.supported_collection_methods:
            # TODO(Sybren): when automatic is not supported, we need to switch to manual.
            # However, we may want to notify the user about this beforehand.
            # We also may want to choose a collection method more explicitly than just
            # picking a supported one randomly. In practice this is not so random, though,
            # as we only have two supported collection methods (automatic and manual),
            # so if one is not supported this always picks the other one.
            supported = gateway.provider.supported_collection_methods
            collection_method = supported.pop()

        if self.request.user.is_anonymous:
            token = models.LinkCustomerToken(customer=self.customer)
            token.save()
            self.log.info(
                'Created new link token pk=%r for account-less customer pk=%r',
                token.pk,
                self.customer.pk
            )
        subscription = models.Subscription.objects.create(
            plan=self.plan,
            customer=self.customer,
            payment_method=payment_method,
            price=self.plan_variation.price,
            currency=self.plan_variation.currency,
            interval_unit=self.plan_variation.interval_unit,
            interval_length=self.plan_variation.interval_length,
            collection_method=collection_method,
        )
        self.log.debug('Created new subscription pk=%r for checkout', subscription.pk)
        return subscription

    def _fetch_or_create_order(
        self,
        form: CheckoutForm,
        subscription: models.Subscription,
    ) -> models.Order:
        """Get an existing order of the subscription, or create a new one."""

        # Either get the existing order, or create a new one. Once the order is
        # created we perform a Charge on a new transaction.
        order_pk = form.cleaned_data.get('order_pk')
        if order_pk:
            order: models.Order = get_object_or_404(subscription.order_set, pk=order_pk)
            self.log.debug('Reusing order pk=%r for checkout', order.pk)
            return order

        latest_order = subscription.latest_order()
        if latest_order and latest_order.status == 'created':
            # There already was some order created on this subscription, so try
            # and pay for that.
            return latest_order

        # This is the expected situation: a new subscription won't have any order
        # on it yet, so we have to create it.
        order = subscription.generate_order()
        self.log.debug('Using auto-created order pk=%r for checkout', order.pk)
        return order

    def _charge_if_supported(
        self, form: CheckoutForm, gateway: models.Gateway, order: models.Order
    ) -> HttpResponse:
        """Create a transaction on the order, and charge it.

        If transactions are not supported by the payment gateway, just redirects to a 'done' page.

        :return: If everything went fine, a redirect to a 'done' page, either
            'looper:checkout_done' or 'looper:transactionless_checkout_done`.
            If something went wrong, an error page response is returned.
        """

        # Only follow through with a charge if it's actually supported.
        if not gateway.provider.supports_transactions:
            self.log.info(
                'Not creating transaction for order pk=%r because gateway %r does '
                'not support it',
                order.pk,
                gateway.name,
            )
            self.erase_client_token()
            url = reverse(
                'looper:transactionless_checkout_done',
                kwargs={'pk': order.pk, 'gateway_name': gateway.name},
            )
            if self.user.is_anonymous:
                url += '?' + urllib.parse.urlencode({'token': order.customer.token.token})
            return redirect(url)

        customer_ip_address = utils.get_client_ip(self.request)
        trans = order.generate_transaction(ip_address=customer_ip_address, is_recurring=True)

        if not trans.charge():
            form.add_error('', trans.failure_message)
            form.data = {
                **form.data.dict(),  # form.data is a QueryDict, which is immutable.
                'subscription_pk': order.subscription.pk,
                'order_pk': order.pk,
            }
            self.erase_client_token()
            return self.form_invalid(form)

        # The transaction, being marked as 'paid', will automatically
        # activate the subscription.
        self.erase_client_token()
        url = reverse('looper:checkout_done', kwargs={'transaction_id': trans.pk})
        if self.user.is_anonymous:
            url += '?' + urllib.parse.urlencode({'token': trans.customer.token.token})
        return redirect(url)

    def _check_recently_created_subscription(
        self, form: CheckoutForm
    ) -> Optional[models.Subscription]:
        """Check recently created subscriptions.

        If a subscription was created recently, AND the user did not indicate
        that creating a new subscription is okay, return that subscription.
        """

        # The 'cleaned_data' attribute is only available after the form has been posted, but this
        # function is called also on creation of the form.
        if hasattr(form, 'cleaned_data'):
            if form.cleaned_data.get('subscription_pk'):
                # Checking out an existing subscription, so a new subscription won't be  created.
                return None

            if form.cleaned_data.get('approve_new_subscription'):
                # User approved the creation of a new subscription.
                return None

        return self._recently_created_subscription(form)

    def _update_form_for_recent_subscription(
        self, form: CheckoutForm, recent_subs: models.Subscription
    ) -> None:
        """Updates the form with a confirmation checkbox if the user recently obtained a subscription.

        :return: The recent subscription (if there is one).
        """

        help_text = (
            f"You recently obtained a {recent_subs.plan.name} Membership already. "
            f"Please check this checkbox if it is your intention to obtain another Membership."
        )
        form.show_approve_new_subscription(help_text)

    def _recently_created_subscription(
        self, form: CheckoutForm
    ) -> Optional[models.Subscription]:
        """Check for recently created subscriptions, preventing users from accidentally creating multiple.

        :return: None if all is OK and creation can continue, or an existing subscription otherwise.
        """

        now = timezone.now()
        nag_threshold = now - settings.LOOPER_SUBSCRIPTION_CREATION_WARNING_THRESHOLD

        if self.customer.pk is None:
            return None

        try:
            subscription = self.customer.subscription_set.filter(
                created_at__gte=nag_threshold
            ).latest('created_at')
        except models.Subscription.DoesNotExist:
            return None

        if subscription.status != 'active' and not subscription.can_be_activated:
            # The subscription cannot be resurrected, so no need to ask for confirmation.
            return None

        return subscription


class CheckoutExistingOrderView(LoginRequiredMixin, AbstractPaymentView):
    """Perform the checkout procedure for an existing order."""

    template_name = 'checkout/checkout_existing_order.html'
    form_class = CheckoutForm

    log = log.getChild('CheckoutExistingOrderView')

    order: models.Order
    plan: models.Plan

    def dispatch(self, request, *args, **kwargs):
        # Get the prerequisites for handling any request in this view.
        self.order = get_object_or_404(models.Order, pk=kwargs['order_id'])
        if self.order.customer.user_id != request.user.id:
            return render(request, 'checkout/not_your_order.html', status=403)
        self.plan = self.order.subscription.plan
        return super().dispatch(request, *args, **kwargs)

    def get_currency(self) -> str:
        return self.order.currency

    def get_context_data(self, **kwargs) -> dict:
        ctx = {
            **super().get_context_data(**kwargs),
            'order': self.order,
            'subscription': self.order.subscription,
            'plan': self.plan,
        }
        return ctx

    def get_form_kwargs(self) -> dict:
        return {
            **super().get_form_kwargs(),
            'instance': self.customer.billing_address,
        }

    def get_initial(self) -> dict:
        return {
            **super().get_initial(),
            'price': self.order.price.decimals_string,
            'email': self.customer.billing_address.email,
            'full_name': self.customer.billing_address.full_name,
        }

    def form_valid(self, form: CheckoutForm) -> HttpResponse:
        response = self._check_customer_ip_address(form)
        if response:
            return response

        gateway = self.gateway_from_form(form)

        # Update billing address if it changed.
        if form.has_changed():
            form.save()

        nonce = form.cleaned_data['payment_method_nonce']
        try:
            payment_method = self.customer.payment_method_add(nonce, gateway)
        except exceptions.GatewayError as e:
            self.log.info('Error adding payment method: %s', e.errors)
            form.add_error(
                '',
                'Cannot use a payment nonce more than once. '
                'Please refresh the page and try again',
            )
            return self.form_invalid(form)

        # Switch to this new payment method.
        subs = self.order.subscription
        if subs.payment_method is None or subs.payment_method.pk != payment_method.pk:
            self.log.info(
                'Switching subscription pk=%d to payment method pk=%d', subs.pk, payment_method.pk
            )
            subs.switch_payment_method(payment_method)

        order = self.order
        if order.payment_method is None or order.payment_method.pk != payment_method.pk:
            order.switch_payment_method(payment_method)

        # If the user tries to pay by bank transfer, just show the Foundation's bank info.
        gateway = self.gateway_from_form(form)
        if not gateway.provider.supports_transactions:
            self.log.info(
                'Not creating transaction for order pk=%r because gateway %r does '
                'not support it',
                order.pk,
                gateway.name,
            )
            self.erase_client_token()
            return redirect(
                'looper:transactionless_checkout_done', pk=order.pk, gateway_name=gateway.name
            )

        # Charge a new transaction.
        customer_ip_address = utils.get_client_ip(self.request)
        trans = order.generate_transaction(ip_address=customer_ip_address, is_recurring=True)
        if not trans.charge():
            form.add_error('', trans.failure_message)
            return self.form_invalid(form)

        # The transaction, being marked as 'paid', will automatically
        # activate the subscription.
        self.erase_client_token()
        return redirect('looper:checkout_done', transaction_id=trans.pk)
