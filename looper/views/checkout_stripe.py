from typing import Optional
import logging
import urllib.parse

from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse, HttpResponseBadRequest
from django.shortcuts import redirect, get_object_or_404
from django.urls import reverse
from django.utils import timezone
from django.views import View
from django.views.generic import FormView, DetailView
import django.db.utils

from .. import forms, middleware, models, stripe_utils, utils
from looper.views.mixins import ExpectReadableIPAddressMixin

log = logging.getLogger(__name__)


class StripeSuccessView(View):
    def get(self, request, *args, **kwargs):
        order = get_object_or_404(models.Order, pk=self.kwargs['pk'])
        log.info('Processing stripe success url order pk=%s with status %s', order.pk, order.status)
        if order.status in {'paid', 'fulfilled'}:
            log.info('Order is already paid, success_url is visited twice or webhook was first')
            trans = order.latest_transaction()
        elif not order.may_transition_to('paid'):
            # should never happen here normally, TODO a proper template
            log.error(
                'Can\'t transition order pk=%s to paid: has status=%s', order.pk, order.status
            )
            return HttpResponseBadRequest()
        else:
            gateway = models.Gateway.objects.get(name='stripe')
            stripe_session_id = self.kwargs['stripe_session_id']
            stripe_session = gateway.provider.retrieve_checkout_session_expanded(stripe_session_id)
            payment_intent = stripe_session.payment_intent
            log.info('Processing payment_intent id=%s', payment_intent.id)
            if payment_intent.status != 'succeeded':
                log.error('payment_intent has status %s, won\'t process', payment_intent.status)
                return HttpResponseBadRequest()
            if int(payment_intent.metadata['order_id']) != order.pk:
                log.error(
                    'payment_intent metadata order_id=%s doesn\'t match the order.pk=%s',
                    payment_intent.metadata['order_id'], order.pk,
                )
                return HttpResponseBadRequest()

            log.info(
                'Creating a transaction based on a succeeded payment_intent pk=%s',
                payment_intent.id,
            )
            customer_ip_address = utils.get_client_ip(self.request)
            trans = stripe_utils.process_payment_intent_for_subscription_order(
                payment_intent, order, customer_ip_address
            )

        done_url = reverse('looper:checkout_done', kwargs={'transaction_id': trans.id})
        token = self.request.GET.get('token')
        if token:
            done_url += '?' + urllib.parse.urlencode({'token': token})
        return redirect(done_url)


class StripeSuccessCreateSubscriptionView(View):
    def get(self, request, *args, **kwargs):
        stripe_session_id = self.kwargs['stripe_session_id']
        gateway = models.Gateway.objects.get(name='stripe')
        stripe_session = gateway.provider.retrieve_checkout_session_expanded(stripe_session_id)
        payment_intent = stripe_session.payment_intent
        log.info('Processing payment_intent id=%s', payment_intent.id)
        if payment_intent.status != 'succeeded':
            log.error('payment_intent has status %s, won\'t process', payment_intent.status)
            return HttpResponseBadRequest()
        plan_variation_id = payment_intent.metadata.get('plan_variation_id', None)
        if not plan_variation_id:
            log.error('payment_intent.metadata is missing plan_variation_id, won\'t process')
            return HttpResponseBadRequest()

        gateway_order_id_q = models.GatewayOrderId.objects.filter(
            gateway=gateway,
            gateway_order_id=payment_intent.id,
        )
        gateway_order_id = gateway_order_id_q.first()
        if gateway_order_id:
            order = gateway_order_id.order
            if order.status in {'paid', 'fulfilled'}:
                log.info(
                    'Order pk=%s with payment_intent id=%s is already paid,'
                    ' success_url is visited twice or webhook was first',
                    order.pk, payment_intent.id,
                )
                trans = order.latest_transaction()
            else:
                log.error(
                    'Order pk=%s with payment_intent id=%s is not paid,'
                    ' won\'t process',
                    order.pk, payment_intent.id,
                )
                return HttpResponseBadRequest()
        else:
            plan_variation = get_object_or_404(
                models.PlanVariation, pk=plan_variation_id
            )
            customer_ip_address = utils.get_client_ip(self.request)
            try:
                trans = stripe_utils.process_payment_intent_for_plan_variation(
                    payment_intent,
                    plan_variation,
                    customer_ip_address,
                )
            except django.db.utils.IntegrityError as e:
                log.info(
                    'Retrying Order lookup by payment_intent id=%s,'
                    ' expected to have been written by the webhook: %s',
                    payment_intent.id, e
                )
                # if a record is missing we do want to get an exception, so using get(), not first()
                gateway_order_id = gateway_order_id_q.get()
                # intentional copy-paste of above code, for readability
                order = gateway_order_id.order
                if order.status in {'paid', 'fulfilled'}:
                    log.info(
                        'Order pk=%s with payment_intent id=%s is already paid,'
                        ' success_url is visited twice or webhook was first',
                        order.pk, payment_intent.id,
                    )
                    trans = order.latest_transaction()
                else:
                    log.error(
                        'Order pk=%s with payment_intent id=%s is not paid,'
                        ' won\'t process',
                        order.pk, payment_intent.id,
                    )
                    return HttpResponseBadRequest()

        done_url = reverse('looper:checkout_done', kwargs={'transaction_id': trans.id})
        if hasattr(trans.customer, 'token'):
            done_url += '?' + urllib.parse.urlencode({'token': trans.customer.token.token})
        return redirect(done_url)


class CheckoutExistingOrderView(LoginRequiredMixin, ExpectReadableIPAddressMixin, DetailView):
    """Perform the checkout procedure for an existing order."""

    # should be passed from the app, this url name is defined in example_app
    cancel_url = 'settings_home'

    def get_cancel_url(self):
        return reverse(self.cancel_url)

    def get_object(self, queryset=None) -> models.Order:
        order_id: int = self.kwargs['order_id']
        order_q = models.Order.objects.filter(customer=self.request.user.customer)
        return get_object_or_404(order_q, pk=order_id)

    def get(self, *args, **kwargs):
        response = self._check_customer_ip_address()
        if response:
            return response

        order = self.get_object()

        success_url = self.request.build_absolute_uri(
            reverse(
                'looper:stripe_success',
                kwargs={
                    'pk': order.id,
                    'stripe_session_id': 'CHECKOUT_SESSION_ID',
                }
            )
        )
        # we have to do it to avoid uri-encoding of curly braces,
        # otherwise stripe doesn't do the template substitution
        success_url = success_url.replace('CHECKOUT_SESSION_ID', '{CHECKOUT_SESSION_ID}', 1)
        cancel_url = self.request.build_absolute_uri(self.get_cancel_url())
        session = stripe_utils.create_stripe_checkout_session_for_order(
            order,
            success_url,
            cancel_url,
            payment_intent_metadata={'order_id': order.pk},
        )
        return redirect(session.url)


class CheckoutStripeView(ExpectReadableIPAddressMixin, FormView):
    """Perform the checkout procedure for a subscription using Stripe."""

    template_name = 'checkout/checkout_stripe.html'
    form_class = forms.CheckoutStripeForm

    customer: models.Customer
    plan: models.Plan
    plan_variation: models.PlanVariation

    def get_currency(self) -> str:
        """Returns the currency for the current page.

        Defaults to the user's preferred currency, but can be overridden
        in a subclass.
        """
        return self.request.session.get(
            middleware.PREFERRED_CURRENCY_SESSION_KEY, models.DEFAULT_CURRENCY
        )

    def dispatch(self, request, *args, **kwargs):
        # Get the prerequisites for handling any request in this view.
        self.plan = get_object_or_404(models.Plan, pk=kwargs['plan_id'])
        self.plan_variation = get_object_or_404(
            self.plan.variations.active(), pk=kwargs['plan_variation_id']
        )
        self.user = self.request.user
        if self.user.is_authenticated:
            self.customer = self.user.customer
        else:
            self.customer = models.Customer()
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs) -> dict:
        ctx = {
            **super().get_context_data(**kwargs),
            'plan': self.plan,
            'plan_variation': self.plan_variation,
        }
        return ctx

    def get_form_kwargs(self) -> dict:
        return {
            **super().get_form_kwargs(),
            'instance': self.customer.billing_address,
        }

    def get_form(self, form_class=None):
        form = super().get_form(form_class)

        if self.request.method == 'GET':
            # Do NOT call this here on a POST. At this point in the code,
            # the form hasn't been updated with the POSTed data yet,
            # and thus will nag about recent subscriptions even when the user has confirmed.
            recent_subs = self._check_recently_created_subscription(form)
            if recent_subs is not None:
                self._update_form_for_recent_subscription(form, recent_subs)

        return form

    def get_initial(self) -> dict:
        return {
            'gateway': 'stripe',
            'price': self.plan_variation.price.decimals_string,
            'email': self.customer.billing_address.email,
            'full_name': self.customer.billing_address.full_name,
        }

    def form_valid(self, form: forms.CheckoutStripeForm) -> HttpResponse:
        assert self.request.method == 'POST'

        response = self._check_customer_ip_address(form)
        if response:
            return response

        # Update billing address if it changed.
        if form.has_changed():
            form.instance.customer.save()
            form.save()

        recent_subs = self._check_recently_created_subscription(form)
        if recent_subs is not None:
            self._update_form_for_recent_subscription(form, recent_subs)
            return self.form_invalid(form)

        gateway = form.cleaned_data['gateway']
        subscription = self._get_existing_subscription(form)
        if not subscription:
            subscription = self._create_subscription(gateway)
        order = self._fetch_or_create_order(form, subscription)

        if not gateway.provider.supports_transactions:
            log.info(
                'Not creating transaction for order pk=%r because gateway %r does '
                'not support it',
                order.pk,
                gateway.name,
            )
            url = reverse(
                'looper:transactionless_checkout_done',
                kwargs={'pk': order.pk, 'gateway_name': gateway.name}
            )
            if self.user.is_anonymous:
                url += '?' + urllib.parse.urlencode({'token': order.customer.token.token})
            return redirect(url)

        success_url = self.request.build_absolute_uri(
            reverse(
                'looper:stripe_success',
                kwargs={
                    'pk': order.id,
                    'stripe_session_id': 'CHECKOUT_SESSION_ID',
                }
            )
        )
        # we have to do it to avoid uri-encoding of curly braces,
        # otherwise stripe doesn't do the template substitution
        success_url = success_url.replace('CHECKOUT_SESSION_ID', '{CHECKOUT_SESSION_ID}', 1)
        if self.user.is_anonymous:
            success_url += '?' + urllib.parse.urlencode({'token': order.customer.token.token})
        cancel_url = self.request.build_absolute_uri()

        session = stripe_utils.create_stripe_checkout_session_for_order(
            order,
            success_url,
            cancel_url,
            payment_intent_metadata={'order_id': order.pk},
        )
        return redirect(session.url)

    def _get_existing_subscription(
        self, form: forms.CheckoutStripeForm
    ) -> Optional[models.Subscription]:
        subs_pk = form.cleaned_data.get('subscription_pk')
        if not subs_pk:
            return None

        # The following subscription lookup is only possible for a logged-in user
        if self.user.is_anonymous:
            return None

        subscription: models.Subscription = get_object_or_404(
            self.user.customer.subscription_set, pk=subs_pk
        )
        log.debug('Reusing subscription pk=%r for checkout', subscription.pk)
        return subscription

    def _create_subscription(self, gateway: models.Gateway) -> models.Subscription:
        collection_method = self.plan_variation.collection_method
        if collection_method not in gateway.provider.supported_collection_methods:
            # TODO(Sybren): when automatic is not supported, we need to switch to manual.
            # However, we may want to notify the user about this beforehand.
            # We also may want to choose a collection method more explicitly than just
            # picking a supported one randomly. In practice this is not so random, though,
            # as we only have two supported collection methods (automatic and manual),
            # so if one is not supported this always picks the other one.
            supported = gateway.provider.supported_collection_methods
            collection_method = supported.pop()

        if self.request.user.is_anonymous:
            token = models.LinkCustomerToken(customer=self.customer)
            token.save()
            log.info(
                'Created new link token pk=%r for account-less customer pk=%r',
                token.pk,
                self.customer.pk
            )
        if not gateway.provider.supports_transactions:
            payment_method = self.customer.paymentmethod_set.filter(gateway=gateway).first()
            if not payment_method:
                self.customer.gateway_customer_id_get_or_create(gateway)
                pm_info = gateway.provider.payment_method_create(None, None)
                payment_method = self.customer.paymentmethod_set.create(
                    gateway=gateway,
                    method_type=pm_info.type_for_database(),
                    recognisable_name=pm_info.recognisable_name(),
                )
        else:
            # for stripe payment method will be stored when processing success_url
            payment_method = None
        subscription = models.Subscription.objects.create(
            plan=self.plan,
            customer=self.customer,
            payment_method=payment_method,
            price=self.plan_variation.price,
            currency=self.plan_variation.currency,
            interval_unit=self.plan_variation.interval_unit,
            interval_length=self.plan_variation.interval_length,
            collection_method=collection_method,
        )
        log.debug('Created new subscription pk=%r for checkout', subscription.pk)
        return subscription

    def _fetch_or_create_order(
        self,
        form: forms.CheckoutStripeForm,
        subscription: models.Subscription,
    ) -> models.Order:
        """Get an existing order of the subscription, or create a new one."""

        # Either get the existing order, or create a new one. Once the order is
        # created we perform a Charge on a new transaction.
        order_pk = form.cleaned_data.get('order_pk')
        if order_pk:
            order: models.Order = get_object_or_404(subscription.order_set, pk=order_pk)
            log.debug('Reusing order pk=%r for checkout', order.pk)
            return order

        latest_order = subscription.latest_order()
        if latest_order and latest_order.status == 'created':
            # There already was some order created on this subscription, so try
            # and pay for that.
            return latest_order

        # This is the expected situation: a new subscription won't have any order
        # on it yet, so we have to create it.
        order = subscription.generate_order()
        log.debug('Using auto-created order pk=%r for checkout', order.pk)
        return order

    def _check_recently_created_subscription(
        self, form: forms.CheckoutStripeForm
    ) -> Optional[models.Subscription]:
        """Check recently created subscriptions.

        If a subscription was created recently, AND the user did not indicate
        that creating a new subscription is okay, return that subscription.
        """

        # The 'cleaned_data' attribute is only available after the form has been posted, but this
        # function is called also on creation of the form.
        if hasattr(form, 'cleaned_data'):
            if form.cleaned_data.get('subscription_pk'):
                # Checking out an existing subscription, so a new subscription won't be  created.
                return None

            if form.cleaned_data.get('approve_new_subscription'):
                # User approved the creation of a new subscription.
                return None

        return self._recently_created_subscription(form)

    def _update_form_for_recent_subscription(
        self, form: forms.CheckoutStripeForm, recent_subs: models.Subscription
    ) -> None:
        """Updates the form with a confirmation checkbox if the user recently obtained a
        subscription.

        :return: The recent subscription (if there is one).
        """

        help_text = (
            f"You recently obtained a {recent_subs.plan.name} Membership already. "
            f"Please check this checkbox if it is your intention to obtain another Membership."
        )
        form.show_approve_new_subscription(help_text)

    def _recently_created_subscription(
        self, form: forms.CheckoutStripeForm
    ) -> Optional[models.Subscription]:
        """Check for recently created subscriptions, preventing users from accidentally creating
        multiple.

        :return: None if all is OK and creation can continue, or an existing subscription otherwise.
        """

        now = timezone.now()
        nag_threshold = now - settings.LOOPER_SUBSCRIPTION_CREATION_WARNING_THRESHOLD

        if self.customer.pk is None:
            return None

        try:
            subscription = self.customer.subscription_set.filter(
                created_at__gte=nag_threshold
            ).latest('created_at')
        except models.Subscription.DoesNotExist:
            return None

        if subscription.status != 'active' and not subscription.can_be_activated:
            # The subscription cannot be resurrected, so no need to ask for confirmation.
            return None

        return subscription
