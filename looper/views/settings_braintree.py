import logging

from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404

from .. import exceptions, forms_braintree, models
import looper.views.checkout

log = logging.getLogger(__name__)


class PaymentMethodChangeView(looper.views.checkout_braintree.AbstractPaymentView):
    """Use the Braintree drop-in UI to switch a subscription's payment method."""

    template_name = 'looper/settings/payment_method_change_braintree.html'
    form_class = forms_braintree.ChangePaymentMethodForm
    success_url = '/settings/'

    log = log.getChild('PaymentMethodChangeView')

    subscription: models.Subscription

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            self.subscription = get_object_or_404(
                request.user.customer.subscription_set, pk=kwargs['subscription_id']
            )
        return super().dispatch(request, *args, **kwargs)

    def get_currency(self) -> str:
        return self.subscription.currency

    def get_context_data(self, **kwargs) -> dict:
        ctx = {
            **super().get_context_data(**kwargs),
            'subscription': self.subscription,
            'current_payment_method': self.subscription.payment_method,
        }
        return ctx

    def get_initial(self) -> dict:
        return {
            **super().get_initial(),
            'price': self.subscription.price.decimals_string,
            'email': self.customer.billing_address.email,
            'full_name': self.customer.billing_address.full_name,
            'next_url_after_done': self.request.META.get('HTTP_REFERER', '/'),
        }

    def get_form_kwargs(self) -> dict:
        return {
            **super().get_form_kwargs(),
            'instance': self.customer.billing_address,
        }

    def form_valid(self, form: forms_braintree.ChangePaymentMethodForm) -> HttpResponse:
        gateway = self.gateway_from_form(form)

        nonce = form.cleaned_data['payment_method_nonce']
        try:
            payment_method = self.customer.payment_method_add(nonce, gateway)
        except exceptions.GatewayError as e:
            self.log.info('Error adding payment method: %s', e.errors)
            form.add_error('', e.with_errors())
            return self.form_invalid(form)

        if self.subscription.payment_method_id == payment_method.id:
            self.log.debug('User tried to replace a payment method with itself')
            form.add_error(
                '',
                f'Please select a different payment method with which to replace '
                f'{self.subscription.payment_method.recognisable_name}.',
            )
            return self.form_invalid(form)

        # Switch to this new payment method.
        subs = self.subscription
        if subs.payment_method is None or subs.payment_method.pk != payment_method.pk:
            subs.attach_log_entry(
                f'User {self.user} changed their payment method to '
                f'{payment_method.recognisable_name}'
            )
            subs.switch_payment_method(payment_method)

        order = subs.latest_order()
        if order is None:
            # If there is no order, we don't have to switch it either.
            self.log.warning('Switching payment method on subscription %r without order', subs.pk)
        elif order.status in {'soft-failed', 'created'} and (
            order.payment_method is None or order.payment_method.pk != payment_method.pk
        ):
            # Also switch the order to the new payment method.
            order.attach_log_entry(
                f'User {self.user} changed their payment method to '
                f'{payment_method.recognisable_name}'
            )
            order.switch_payment_method(payment_method)

        self.erase_client_token()
        return HttpResponseRedirect(form.cleaned_data['next_url_after_done'] or self.success_url)
