import logging

from django.http import Http404
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.views.generic import DetailView, ListView, RedirectView

from .. import middleware, models

log = logging.getLogger(__name__)


class ChooseDefaultPlanVariationView(RedirectView):
    """Chose the appropriate variation for this plan, then forward the user to checkout."""

    permanent = False
    query_string = True

    def get_redirect_url(self, *args, **kwargs):
        plan_id = kwargs['plan_id']
        plan = get_object_or_404(models.Plan, pk=plan_id)

        # Determine the proper plan for the user's currency.
        prefcur: str = self.request.session.get(
            middleware.PREFERRED_CURRENCY_SESSION_KEY, models.DEFAULT_CURRENCY
        )

        default_variation = plan.variation_for_currency(prefcur)
        if default_variation is None and prefcur != models.DEFAULT_CURRENCY:
            log.warning('Default variation in %s for Plan %i not found' % (prefcur, plan_id))
            # Check if the a variation is available in the default currency
            default_variation = plan.variation_for_currency(models.DEFAULT_CURRENCY)
            if default_variation is None:
                log.error('No variation for for Plan %i found' % plan_id)
                raise Http404('This Plan does not have variations at the moment.')

        url_kwargs = {'plan_id': plan_id, 'plan_variation_id': default_variation.id}
        return reverse(self.pattern_name, kwargs=url_kwargs)


class ChoosePlanVariationView(ListView):
    template_name = 'checkout/choose_plan_variation.html'

    def get(self, request, *args, **kwargs):
        self.plan = get_object_or_404(models.Plan, pk=kwargs['plan_id'])
        self.current_pv = get_object_or_404(
            self.plan.variations.active(), pk=kwargs['current_plan_variation_id']
        )
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        # Determine the current currency.
        currency: str = self.request.session.get(
            middleware.PREFERRED_CURRENCY_SESSION_KEY, models.DEFAULT_CURRENCY
        )
        # Fetch only active variations in the current currency.
        return self.plan.variations.active().filter(currency=currency)

    def get_context_data(self, *, object_list=None, **kwargs) -> dict:
        return {
            **super().get_context_data(object_list=object_list, **kwargs),
            'plan': self.plan,
            'current_plan_variation': self.current_pv,
        }


class DoneView(DetailView):
    template_name = 'checkout/checkout_done.html'

    def get_object(self, queryset=None) -> models.Order:
        transaction_id: int = self.kwargs['transaction_id']
        if self.request.user.is_authenticated:
            transactions_q = models.Transaction.objects.filter(customer=self.request.user.customer)
        else:
            token = self.request.GET.get('token')
            if not token:
                raise Http404()
            transactions_q = models.Transaction.objects.filter(customer__token__token=token)
        trans: models.Transaction = get_object_or_404(transactions_q, pk=transaction_id)
        return trans.order


class TransactionlessCheckoutDoneView(DetailView):
    template_name = 'checkout/checkout_done_transactionless.html'

    def get_object(self, queryset=None) -> models.Order:
        order_id: int = self.kwargs['pk']
        if self.request.user.is_authenticated:
            order_q = models.Order.objects.filter(customer=self.request.user.customer)
        else:
            token = self.request.GET.get('token')
            if not token:
                raise Http404()
            order_q = models.Order.objects.filter(customer__token__token=token)
        return get_object_or_404(order_q, pk=order_id)

    def get_context_data(self, **kwargs) -> dict:
        gateway = get_object_or_404(models.Gateway, name=self.kwargs['gateway_name'])
        return {
            **super().get_context_data(**kwargs),
            'gateway': gateway,
        }
