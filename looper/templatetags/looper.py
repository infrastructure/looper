from typing import Mapping, Any, Union
from collections import namedtuple

from django import template

from looper.models import PlanVariation, Subscription
import looper.money
import looper.taxes

register = template.Library()


@register.filter
def pricing(pricable) -> str:
    if pricable is None:
        return '-'

    price: str = pricable.price.with_currency_symbol_nonocents()

    if pricable.interval_length == 1:
        return f'{price} / {pricable.interval_unit}'
    return f'{price} / {pricable.interval_length}\u00A0{pricable.interval_unit}s'


@register.filter
def get_plan_variation(plan_variation_id: int) -> PlanVariation:
    """Return a PlanVariation with a matching ID, if it exists."""
    return PlanVariation.objects.get(pk=plan_variation_id)


@register.simple_tag(takes_context=True)
def get_taxable(
    context: Mapping[Any, Any], price: looper.money.Money, product_type: str
) -> looper.taxes.Taxable:
    request = context['request']
    return looper.taxes.Taxable.from_request(request, price=price, product_type=product_type)


@register.filter
def renewal_period(variation: Union[PlanVariation, Subscription, namedtuple]) -> str:
    """Format plan variation's renewal period in a human-readable manner.

    Also works for Subscriptions, because they share the same set of fields.
    """
    if variation.interval_unit == 'month':
        if variation.interval_length == 1:
            return 'monthly'
        if variation.interval_length == 3:
            return 'quarterly'
        if variation.interval_length == 6:
            return 'half-yearly'
    elif variation.interval_unit == 'year':
        if variation.interval_length == 1:
            return 'yearly'
    return 'every\u00A0{variation.interval_length}\u00A0{variation.interval_unit}s'
