"""Tax calculations.

Looper supports VAT calculations for generic electronically supplied services.
See https://ec.europa.eu/taxation_customs/individuals/buying-goods-services-online-personal-use/buying-services/electronically-supplied-services_en  # noqa: E501
"""
from decimal import Decimal
from enum import Enum, unique
from typing import Tuple, Iterable, Callable, List, Optional, Dict
import datetime
import logging

from django.http.request import HttpRequest
from django.utils import timezone
from stdnum.eu import vat
import attr

from looper.middleware import COUNTRY_CODE_SESSION_KEY
from looper.money import Money
from looper.tax_utils import Party, VatCharge
from looper.utils import ensure_decimal, format_percentage

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
seller = Party(country_code='NL', is_business=True)
JANUARY_1_2015 = datetime.date(2015, 1, 1)


@unique
class TaxType(Enum):
    """Known tax types."""

    # Blank string was a default for `SharedFields.tax_type` before other values were introduced.
    # To avoid overwriting existing data in Development Fund DB, blank string is used for NO_CHARGE
    NO_CHARGE = ''
    VAT_REVERSE_CHARGE = 'VATRC'
    VAT_CHARGE = 'VATC'

    @classmethod
    def as_choices(cls) -> Iterable[Tuple[str, str]]:
        """Return choices for use in CharField model field."""
        return (
            (cls.NO_CHARGE.value, 'No tax charged'),
            (cls.VAT_CHARGE.value, 'VAT charged'),
            (
                cls.VAT_REVERSE_CHARGE.value,
                'No VAT charged but customer is required to account via reverse-charge',
            ),  # noqa: E501
        )

    @property
    def display_name(self) -> str:
        """How tax is displayed to a customer."""
        if self == TaxType.VAT_CHARGE:
            return 'VAT'
        return ''


@unique
class ProductType(Enum):
    """Currently supported types of goods or services, used in tax calculations.

    We support (and currently need) European VAT rates applicable to sales of
    generic electronic services from a Dutch seller to European buyers.
    In other scenarios, such as donations and sales to non-EU countries, taxes are not charged.
    """

    ELECTRONIC_SERVICE = 'GES'
    DONATION = 'DNT'

    @classmethod
    def as_choices(cls) -> Iterable[Tuple[str, str]]:
        """Return choices for use in CharField model field."""
        return (
            (cls.ELECTRONIC_SERVICE.value, 'Generic electronic service'),
            (cls.DONATION.value, 'Donation'),
        )

    def get_tax(
        self,
        buyer_country_code: str,
        is_business: bool,
    ) -> Tuple['TaxType', Decimal]:
        """Return tax charge type and rate based on given buyer country and business status."""
        now = timezone.now().date()
        buyer = Party(country_code=buyer_country_code, is_business=is_business)
        vat_charge = TaxRulesRegistry.get_sale_vat_charge(
            now, product_type=self, buyer=buyer, seller=seller
        )
        if not vat_charge:
            return TaxType.NO_CHARGE, Decimal(0)

        return vat_charge.action, vat_charge.rate


@attr.s(auto_attribs=True)
class Taxable:
    """Calculate and display tax for given price, tax rate and type."""

    price_with_tax: Money
    """Price amount including the tax"""
    tax_type: TaxType
    tax_rate: Decimal
    """Tax rate percentage (%), e.g. value of Decimal(21) is interpreted as 21%"""

    def __attrs_post_init__(self) -> None:
        """Enforce the types after __init__ is done, because attr does not do that."""
        assert isinstance(self.tax_type, TaxType), self.tax_type
        assert isinstance(self.tax_rate, Decimal), self.tax_rate
        assert isinstance(self.price_with_tax, Money), self.price_with_tax
        self.tax_rate = ensure_decimal(self.tax_rate)

    @property
    def tax(self) -> Money:
        """Calculate tax amount, if charged or must be accounted for via reverse-charge."""
        if self.tax_type in (TaxType.VAT_CHARGE, TaxType.VAT_REVERSE_CHARGE):
            tax_cents = (self.price_with_tax._cents * self.tax_rate) / (100 + self.tax_rate)
            return Money(self.currency, round(tax_cents))
        return Money(self.currency, 0)

    @property
    def price_without_tax(self) -> Money:
        """
        Calculate amount without tax.

        Also used in invoices/receipts that must be more tax office compliant.
        """
        return self.price_with_tax - self.tax

    @property
    def price(self) -> Money:
        """Calculate final amount the customer is being charged.

        Subtracts the tax amount from the tax-inclusive price when VAT reverse-charge applies.
        """
        if self.tax_type == TaxType.VAT_REVERSE_CHARGE:
            return self.price_without_tax

        return self.price_with_tax

    @property
    def currency(self) -> str:
        return self.price_with_tax.currency

    @property
    def tax_is_charged(self) -> bool:
        return self.tax_type == TaxType.VAT_CHARGE

    @property
    def charged_tax(self) -> Money:
        """Return tax amount if it must be displayed to the customer and charged."""
        if not self.tax_is_charged or not self.tax:
            return Money(self.currency, 0)
        return self.tax

    def format_tax_amount(self) -> str:
        """Return a formatted tax to be displayed to the customer."""
        if not self.tax:
            return ''
        if self.tax_is_charged:
            formatted_tax = self.tax.with_currency_symbol()
            formatted_percentage = format_percentage(self.tax_rate)
            return f'Inc. {formatted_percentage} {self.tax_type.display_name} ({formatted_tax})'
        return ''

    def __eq__(self, other: object) -> bool:
        """Compare two Taxables."""
        if not isinstance(other, Taxable):
            return NotImplemented
        return bool(
            self.tax == other.tax
            and self.tax_rate == other.tax_rate
            and self.tax_type == other.tax_type
        )

    @classmethod
    def from_request(cls, request: HttpRequest, price: Money, product_type: str) -> 'Taxable':
        """Return a Taxable based on request's country and given product's price and type."""
        user = request.user
        # Fallback to geo country when there's no Customer
        country_code = request.session.get(COUNTRY_CODE_SESSION_KEY, 'US')
        is_business = False
        if user and user.is_authenticated:
            customer = user.customer
            billing_address = customer.billing_address
            country_code = billing_address.country or country_code
            vat_number = billing_address.vat_number
            is_business = bool(vat_number)
        tax_type, tax_rate = ProductType(product_type).get_tax(
            buyer_country_code=country_code, is_business=is_business
        )
        return cls(price, tax_type=tax_type, tax_rate=tax_rate)

    def __str__(self):
        if self.tax_type == TaxType.VAT_CHARGE:
            _tax = self.format_tax_amount()
        elif self.tax_type == TaxType.VAT_REVERSE_CHARGE:
            _amount = self.tax.with_currency_symbol()
            _percentage = format_percentage(self.tax_rate)
            _tax = f'Excl. {_percentage} {self.tax_type.display_name} ({_amount})'

        maybe_formatted_tax = '' if self.tax_type == TaxType.NO_CHARGE else f' {_tax}'
        return (
            f'<Taxable {self.price_with_tax.with_currency_symbol()}: {self.tax_type}'
            f' {self.price.with_currency_symbol()}{maybe_formatted_tax}>'
        )


class VatRules(object):
    """Base VAT rules for a country."""
    taxed_country_codes: List[str]
    rules: List['looper.models.TaxRule']

    def __init__(
        self, country: str, taxed_country_codes: List[str], rules: List['looper.models.TaxRule']
    ):
        """Initialise VAT rules."""
        self.country = country
        self.taxed_country_codes = taxed_country_codes
        self.rules = rules

    def __str__(self) -> str:
        return f'VATRules for {self.country}: {self.rules}'

    def get_rate(self, product_type: str) -> Decimal:
        """Get VAT rate for a given product type."""
        for rule in self.rules:
            if rule.product_type == product_type.value:
                return rule.rate
        return Decimal(0)

    def get_sale_to_country_vat_charge(self, date, product_type, buyer, seller):
        # We only support business sellers at this time.
        if not seller.is_business:
            raise NotImplementedError('non-business sellers are currently not supported')

        # No tax charged on donations
        if product_type == ProductType.DONATION:
            return VatCharge(TaxType.NO_CHARGE, buyer.country_code, 0)

        # If the seller resides in the same country as the buyer, we charge
        # VAT regardless of whether the buyer is a business or not. Similarly,
        # if the buyer is a consumer, we must charge VAT in the buyer's country
        # of residence.
        if seller.country_code == buyer.country_code or (
            not buyer.is_business and date >= JANUARY_1_2015
        ):
            return VatCharge(TaxType.VAT_CHARGE, buyer.country_code, self.get_rate(product_type))

        # EU consumers are charged VAT in the seller's country prior to January
        # 1st, 2015.
        if not buyer.is_business:
            # Fall back to the seller's VAT rules for this one.
            raise NotImplementedError()

        # EU businesses will never be charged VAT but must account for the VAT
        # by the reverse-charge mechanism.
        return VatCharge(
            TaxType.VAT_REVERSE_CHARGE,
            buyer.country_code,
            # We still want to record the tax rate that applies, in order to be able
            # to subtract the tax while calculating the final charged amount.
            self.get_rate(product_type),
        )

    def get_sale_from_country_vat_charge(self, date, product_type, buyer, seller):
        # We only support business sellers at this time.
        if not seller.is_business:
            raise NotImplementedError('non-business sellers are currently not supported')

        # If the buyer resides outside the EU, we do not have to charge VAT.
        if buyer.country_code not in self.taxed_country_codes:
            return VatCharge(TaxType.NO_CHARGE, buyer.country_code, 0)

        # Both businesses and consumers are charged VAT in the seller's
        # country if both seller and buyer reside in the same country.
        if buyer.country_code == seller.country_code:
            return VatCharge(TaxType.VAT_CHARGE, seller.country_code, self.get_rate(product_type))

        # Businesses in other EU countries are not charged VAT but are
        # responsible for accounting for the tax through the reverse-charge
        # mechanism.
        if buyer.is_business:
            return VatCharge(
                TaxType.VAT_REVERSE_CHARGE,
                buyer.country_code,
                # We still want to record the tax rate that applies, in order to be able
                # to subtract the tax while calculating the final charged amount.
                self.get_rate(product_type),
            )

        # Consumers in other EU countries are charged VAT in their country of
        # residence after January 1st, 2015. Before this date, you charge VAT
        # in the country where the company is located.
        if date >= datetime.date(2015, 1, 1):
            buyer_rules = TaxRulesRegistry.rules()[buyer.country_code]

            return VatCharge(
                TaxType.VAT_CHARGE, buyer.country_code, buyer_rules.get_rate(product_type)
            )
        else:
            return VatCharge(TaxType.VAT_CHARGE, seller.country_code, self.get_rate(product_type))


class TaxRulesRegistry:
    """Handles loading tax rules from the database and using them according to VAT logic.

    Currently only VAT rules for generic electronic services are supported.
    """

    @classmethod
    def rules(cls) -> Iterable[str]:
        """Load all tax rules from the database and instantiate them as VatRules."""
        import looper.models

        rules_per_country = {}
        # Collect tax rules for each country
        for rule in looper.models.TaxRule.objects.all():
            if rule.country not in rules_per_country:
                rules_per_country[rule.country] = []
            rules_per_country[rule.country].append(rule)

        def _instantiate_rules(country: str, rules_for_country: List['TaxRule']) -> Callable:
            return VatRules(
                country=country,
                taxed_country_codes=rules_per_country.keys(),
                rules=rules_for_country,
            )

        # Map country to a VatRules object based on rules found earlier
        return {
            country: _instantiate_rules(country, rules)
            for country, rules in rules_per_country.items()
        }

    @classmethod
    def get_sale_vat_charge(
        cls, date, product_type: 'ProductType', buyer: 'Party', seller: 'Party'
    ):
        """Get the VAT charge for performing the sale of an item.

        Currently only supports determination of the VAT charge for
        electronic services in the EU.
        :param date: Sale date.
        :type date: datetime.date
        :param product_type: Type of the product or service being sold.
        :type product_type: ProductType
        :param buyer: Buyer.
        :type buyer: Party
        :param seller: Seller.
        :type seller: Party
        :rtype: VatCharge
        """
        all_vat_rules = cls.rules()
        # Determine the rules for the countries in which the buyer and seller reside.
        buyer_vat_rules = all_vat_rules.get(buyer.country_code, None)
        seller_vat_rules = all_vat_rules.get(seller.country_code, None)

        # Test if the country to which the item is being sold enforces specific
        # VAT rules for selling to the given country.
        if buyer_vat_rules:
            try:
                return buyer_vat_rules.get_sale_to_country_vat_charge(
                    date, product_type, buyer, seller
                )
            except NotImplementedError:
                pass

        # Fall back to applying VAT rules for selling from the seller's country.
        if seller_vat_rules:
            try:
                return seller_vat_rules.get_sale_from_country_vat_charge(
                    date, product_type, buyer, seller
                )
            except NotImplementedError:
                pass

        # Nothing we can do from here.
        raise NotImplementedError(
            'cannot determine VAT charge for a sale of item %r between %r and %r'
            % (product_type, seller, buyer)
        )


def vies_validate_number(vat_number: str) -> Tuple[Optional[bool], Dict[str, str]]:
    """Attempt to validate the number against VIES.

    Returns True or False if VIES responded, and None if VIES didn't respond anything conclusive.
    """
    try:
        vies_response = vat.check_vies(vat_number)
        logger.warning('Validating VAT number "%s", VIES response: %s', vat_number, vies_response)
        return vies_response.valid, vies_response.__values__
    except Exception:
        logger.exception('Unable to verify the VAT identification number')
    return None, {}
