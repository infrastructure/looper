import io
import logging
import re
import typing

from django.template.response import TemplateResponse
from django.contrib.staticfiles import finders

import xhtml2pdf.pisa as pisa

log = logging.getLogger(__name__)


class PDFResponse(TemplateResponse):
    md5_re = re.compile(r'\.[0-9a-f]{12}(\.[^.]+)$')

    def __init__(
        self,
        request,
        template,
        context=None,
        content_type: typing.Optional[str] = None,
        status=None,
        charset=None,
        using=None,
    ) -> None:
        if content_type is None:
            content_type = 'application/pdf'
        super().__init__(request, template, context, content_type, status, charset, using)

    @classmethod
    def link_callback(cls, uri: str, rel) -> str:
        """Return the path corresponding to a URI of a static file."""
        return finders.find(uri)

    @property
    def rendered_content(self) -> bytes:
        """Return the freshly rendered content for the template and context
        described by the TemplateResponse.

        This *does not* set the final content of the response. To set the
        response content, you must either call render(), or set the
        content explicitly using the value of this property.
        """
        template = self.resolve_template(self.template_name)
        context = self.resolve_context(self.context_data)
        content = template.render(context, self._request)

        response = io.BytesIO()
        pdf = pisa.pisaDocument(
            content, response, link_callback=self.link_callback
        )
        if pdf.err:
            log.error('Error rendering %r with context %r to PDF: %s', template, context, pdf.err)
            return b'Error Rendering PDF'

        return response.getvalue()
