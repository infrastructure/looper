import django.db.utils
import django.test

from ..models import Product, Plan, PlanVariation
from ..money import Money


class PlanVariationModelTestCase(django.test.TestCase):
    fixtures = ['gateways', 'devfund']

    def setUp(self):
        product = Product.objects.create(name='Ежедневный Пророк')
        self.plan1 = Plan.objects.create(product=product, name='стандарт')
        self.plan2 = Plan.objects.create(product=product, name='дорогая')

    def test_plan_variation_create_default(self):
        pv = self.plan1.variations.create(plan=self.plan1, price=1000)
        self.assertEqual(pv.currency, 'EUR')
        self.assertEqual(pv.interval_unit, 'month')
        self.assertEqual(pv.interval_length, 1)
        self.assertEqual(pv.collection_method, 'automatic')
        self.assertEqual(pv.is_active, True)

    def test_plan_variation_create_duplicate(self):
        self.plan1.variations.create(plan=self.plan1, price=1000)
        # Creating a variation with the same parameters is not allowed
        with self.assertRaises(django.db.utils.IntegrityError):
            self.plan1.variations.create(plan=self.plan1, price=1000)


class PlanVariationTest(django.test.TestCase):
    fixtures = ['gateways', 'devfund']

    def test_variation_for_currency(self):
        plan = Plan.objects.get(pk=1)
        self.assertEqual(1, plan.variation_for_currency('EUR').pk)
        self.assertEqual(2, plan.variation_for_currency('USD').pk)
        self.assertIsNone(plan.variation_for_currency('HRK'))

    def test_price_per_month(self):
        pvar = PlanVariation.objects.get(pk=1)  # € 5.00 / month
        self.assertEqual(Money('EUR', 500), pvar.price_per_month)

        pvar.interval_length = 3  # €5.00 / 3 months
        self.assertEqual(Money('EUR', 166), pvar.price_per_month)

        pvar = PlanVariation.objects.get(pk=22)  # $ 350.00 / year
        self.assertEqual(Money('USD', 2916), pvar.price_per_month)
