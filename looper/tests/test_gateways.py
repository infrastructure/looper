from unittest import mock

from django.contrib.auth import get_user_model

from .. import exceptions, gateways, models, money
from . import AbstractBaseTestCase

User = get_user_model()


class GatewayModelTest(AbstractBaseTestCase):
    def test_is_default(self):
        gw1 = models.Gateway.objects.create(name='gw1', is_default=True)
        gw2 = models.Gateway.objects.create(name='gw2', is_default=False)

        gw2.is_default = True
        gw2.save()

        gw1.refresh_from_db()
        self.assertFalse(gw1.is_default)


class BraintreeGatewayTest(AbstractBaseTestCase):
    def setUp(self):
        self.gateway = gateways.BraintreeGateway()

    @mock.patch('braintree.payment_method_gateway.PaymentMethodGateway.delete')
    def test_payment_method_delete_happy(self, mock_delete):
        from braintree import SuccessfulResult

        mock_delete.return_value = SuccessfulResult()
        self.gateway.payment_method_delete('some-token')

    @mock.patch('braintree.payment_method_gateway.PaymentMethodGateway.delete')
    def test_payment_method_delete_unknown_token(self, mock_delete):
        from braintree.exceptions.not_found_error import NotFoundError

        mock_delete.side_effect = [NotFoundError('no such token')]
        self.gateway.payment_method_delete('some-token')

    @mock.patch('braintree.payment_method_gateway.PaymentMethodGateway.delete')
    def test_payment_method_delete_other_error(self, mock_delete):
        from braintree.exceptions.braintree_error import BraintreeError

        mock_delete.side_effect = [BraintreeError('meuh?')]
        with self.assertRaises(exceptions.GatewayError):
            self.gateway.payment_method_delete('some-token')

    @mock.patch('braintree.transaction_gateway.TransactionGateway.refund')
    def test_refund_happy(self, mock_refund):
        from braintree import SuccessfulResult

        mock_refund.return_value = SuccessfulResult()
        self.gateway.refund('some-token', money.Money('EUR', 1499))
        mock_refund.assert_called_once_with('some-token', '14.99')

    @mock.patch('braintree.transaction_gateway.TransactionGateway.refund')
    def test_refund_fail(self, mock_refund):
        from braintree.exceptions.braintree_error import BraintreeError

        mock_refund.side_effect = [BraintreeError('meuh?')]
        with self.assertRaises(exceptions.GatewayError):
            self.gateway.refund('some-token', money.Money('EUR', 1499))
        mock_refund.assert_called_once_with('some-token', '14.99')


class BraintreeErrorTest(AbstractBaseTestCase):
    def setUp(self):
        self.gateway = gateways.BraintreeGateway()

    def test_repeating_message(self):
        from braintree import ErrorResult

        mock_error = ErrorResult(
            self.gateway.braintree,
            {
                'message': 'Payment method token is invalid',
                'errors': {
                    'transaction': {
                        'errors': [
                            {
                                'message': 'Payment method token is invalid',
                                'code': 91518,
                            }
                        ]
                    },
                },
            },
        )
        err = gateways.BraintreeError(mock_error)
        self.assertEqual('Payment method token is invalid', err.message)
        self.assertEqual('code 91518', err.errors[0])

    def test_nonrepeating_message(self):
        from braintree import ErrorResult

        mock_error = ErrorResult(
            self.gateway.braintree,
            {
                'message': 'Payment method token is invalid',
                'errors': {
                    'transaction': {
                        'errors': [
                            {
                                'message': 'Be Creative',
                                'code': 47,
                            }
                        ]
                    },
                },
            },
        )
        err = gateways.BraintreeError(mock_error)
        self.assertEqual('Payment method token is invalid', err.message)
        self.assertEqual('Be Creative (code 47)', err.errors[0])


class BankGatewayTest(AbstractBaseTestCase):
    fixtures = ['testuser', 'gateways', 'devfund']

    def setUp(self):
        super().setUp()
        self.gateway = models.Gateway.objects.get(name='bank')

    def test_multiple_via_db(self):
        user1: User = User.objects.get(email='harry@blender.org')
        user2 = User.objects.create_user(username='another', email='another@example.com')

        pmeth1 = user1.customer.paymentmethod_set.create(
            gateway=self.gateway, token='bank', method_type='ba', recognisable_name='Bank Transfer'
        )

        pmeth2 = user2.customer.paymentmethod_set.create(
            gateway=self.gateway, token='bank', method_type='ba', recognisable_name='Bank Transfer'
        )

        self.assertNotEqual(pmeth1.id, pmeth2.id)

    def test_multiple_via_customer(self):
        user1: User = User.objects.get(email='harry@blender.org')
        user2 = User.objects.create_user(username='another', email='another@example.com')

        cust1 = user1.customer
        cust2 = user2.customer

        pmeth1 = cust1.payment_method_add('useless-nonce', self.gateway)
        pmeth2 = cust2.payment_method_add('useless-nonce', self.gateway)

        self.assertNotEqual(pmeth1.id, pmeth2.id)
