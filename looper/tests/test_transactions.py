from unittest import mock

from django.contrib.admin.models import LogEntry
import requests.exceptions

from . import AbstractLooperTestCase
from .. import admin_log
from looper.money import Money
from looper.taxes import ProductType
import looper.exceptions


@mock.patch(
    'looper.gateways.BraintreeGateway.refund',
    new=mock.Mock(return_value='mocked-refund-transaction-id')
)
@mock.patch(
    'looper.gateways.BraintreeGateway.transact_sale',
    return_value={'transaction_id': 'mocked-transaction-id'}
)
class TransactionsTestCase(AbstractLooperTestCase):
    gateway_name = 'braintree'

    def test_transaction_create_from_subscription(self, _) -> None:
        subscription = self.create_subscription()

        # We used to generate the initial order automatically, but no longer.
        order = subscription.latest_order()
        self.assertIsNone(order)

        order = subscription.generate_order()
        self.assertIsNone(order.latest_transaction())

    def test_charge(self, mock_transact_sale) -> None:
        subscription = self.create_subscription()
        order = subscription.generate_order()
        trans = order.generate_transaction(ip_address='fe80::5ad5:4eaf:feb0:4747')
        mock_transact_sale.assert_not_called()

        self.assertTrue(trans.charge())

        trans.refresh_from_db()
        self.assertEqual('mocked-transaction-id', trans.transaction_id)
        self.assertEqual(trans.ip_address, 'fe80::5ad5:4eaf:feb0:4747')

        # The order should be marked as 'paid'.
        order.refresh_from_db()
        self.assertEqual('paid', order.status)

        # The subscription should have been activated.
        subscription.refresh_from_db()
        self.assertEqual('active', subscription.status)

        # A LogEntry should have been created for this success.
        entries_q = admin_log.entries_for(trans)
        self.assertEqual(1, len(entries_q))
        entry: LogEntry = entries_q.first()
        self.assertIn('charge', entry.change_message.lower())
        self.assertIn('success', entry.change_message.lower())

    def test_charge_gateway_error(self, mock_transact_sale) -> None:
        subscription = self.create_subscription()
        order = subscription.generate_order()
        trans = order.generate_transaction(ip_address='fe80::5ad5:4eaf:feb0:4747')
        mock_transact_sale.assert_not_called()

        mock_transact_sale.side_effect = looper.exceptions.GatewayError(
            'mocked error', errors=['äuẅ']
        )
        self.assertFalse(trans.charge())

        # The transaction should be marked as failed.
        trans.refresh_from_db()
        self.assertEqual('failed', trans.status)
        self.assertEqual('', trans.transaction_id)
        self.assertEqual(trans.ip_address, 'fe80::5ad5:4eaf:feb0:4747')

        # The order and subscription shouldn't be influenced.
        order.refresh_from_db()
        subscription.refresh_from_db()
        self.assertEqual('created', order.status)
        self.assertEqual('on-hold', subscription.status)

        # A LogEntry should have been created for this success.
        entries_q = admin_log.entries_for(trans)
        self.assertEqual(1, len(entries_q))
        entry: LogEntry = entries_q.first()
        self.assertIn('charge', entry.change_message.lower())
        self.assertIn('failed', entry.change_message.lower())

        self.assertEqual('mocked error: äuẅ', trans.failure_message)
        self.assertFalse(trans.paid)

    def test_charge_io_error(self, mock_transact_sale) -> None:
        subscription = self.create_subscription()
        order = subscription.generate_order()
        trans = order.generate_transaction(ip_address='fe80::5ad5:4eaf:feb0:4747')
        mock_transact_sale.assert_not_called()

        mock_transact_sale.side_effect = requests.exceptions.ConnectTimeout('mocked äuẅ')
        self.assertFalse(trans.charge())

        # The transaction should be marked as failed.
        trans.refresh_from_db()
        self.assertEqual('failed', trans.status)
        self.assertEqual('', trans.transaction_id)
        self.assertEqual(trans.ip_address, 'fe80::5ad5:4eaf:feb0:4747')

        # The order and subscription shouldn't be influenced.
        order.refresh_from_db()
        subscription.refresh_from_db()
        self.assertEqual('created', order.status)
        self.assertEqual('on-hold', subscription.status)

        # A LogEntry should have been created for this success.
        entries_q = admin_log.entries_for(trans)
        self.assertEqual(1, len(entries_q))
        entry: LogEntry = entries_q.first()
        self.assertIn('charge', entry.change_message.lower())
        self.assertIn('failed', entry.change_message.lower())

        self.assertEqual('mocked äuẅ', trans.failure_message)
        self.assertFalse(trans.paid)

    def test_charge_very_long_error(self, mock_transact_sale) -> None:
        subscription = self.create_subscription()
        order = subscription.generate_order()
        trans = order.generate_transaction(ip_address='fe80::5ad5:4eaf:feb0:4747')
        mock_transact_sale.assert_not_called()

        # We have had this error when the server rack was suffering a DDoS attack,
        # which was the reason to change the field type from VARCHAR(n) to TEXT.
        long_message = (
            "HTTPSConnectionPool(host='api.braintreegateway.com', port=443): Max "
            "retries exceeded with url: /merchants/abcabcdeadbeefaa/transactions ("
            "Caused by ConnectTimeoutError("
            "<urllib3.connection.VerifiedHTTPSConnection object at 0x7f4ed327a908>, "
            "'Connection to api.braintreegateway.com timed out. (connect "
            "timeout=60)'))"
        )
        mock_transact_sale.side_effect = requests.exceptions.ConnectTimeout(long_message)
        self.assertFalse(trans.charge())

        # The transaction should be marked as failed.
        trans.refresh_from_db()
        self.assertEqual('failed', trans.status)
        self.assertEqual('', trans.transaction_id)
        self.assertEqual(trans.ip_address, 'fe80::5ad5:4eaf:feb0:4747')

        # The order and subscription shouldn't be influenced.
        order.refresh_from_db()
        subscription.refresh_from_db()
        self.assertEqual('created', order.status)
        self.assertEqual('on-hold', subscription.status)

        # A LogEntry should have been created for this success.
        entries_q = admin_log.entries_for(trans)
        self.assertEqual(1, len(entries_q))
        entry: LogEntry = entries_q.first()
        self.assertIn('charge', entry.change_message.lower())
        self.assertIn('failed', entry.change_message.lower())

        # The failure message should be stored completely.
        self.assertEqual(long_message, trans.failure_message)
        self.assertFalse(trans.paid)

    def test_charge_bad_status(self, mock_transact_sale) -> None:
        subscription = self.create_subscription()
        order = subscription.generate_order()
        trans = order.generate_transaction(ip_address='fe80::5ad5:4eaf:feb0:4747')
        trans.status = 'failed'
        trans.save()

        with self.assertRaises(looper.exceptions.IncorrectStatusError):
            trans.charge()

        mock_transact_sale.assert_not_called()

        # This is a programming error, so it shouldn't create a log event.
        entries_q = admin_log.entries_for(trans)
        self.assertEqual(0, len(entries_q))

        self.assertEqual('', trans.failure_message)
        self.assertFalse(trans.paid)

    def test_refund_full(self, mock_transact_sale) -> None:
        subscription = self.create_subscription()
        order = subscription.generate_order()
        trans = order.generate_transaction(ip_address='fe80::5ad5:4eaf:feb0:4747')
        mock_transact_sale.assert_not_called()

        self.assertTrue(trans.charge())

        # No refund yet
        self.assertIsNone(trans.refunded_at)
        self.assertIsNone(order.refunded_at)
        self.assertEqual(trans.amount_refunded, Money('EUR', 0))
        self.assertEqual(trans.order.refunded, Money('EUR', 0))

        # Issue a refund of order's total
        trans.refund(Money('EUR', 2500))
        trans.refresh_from_db()
        trans.order.refresh_from_db()

        # Check that refund was successful and that it was recorded
        self.assertIsNotNone(trans.refunded_at)
        self.assertEqual(trans.refunded_at, trans.order.refunded_at)
        self.assertEqual(trans.amount_refunded, Money('EUR', 2500))
        self.assertEqual(trans.order.refunded, Money('EUR', 2500))
        self.assertEqual(trans.order.tax_refunded, Money('EUR', 0))

        # Check that order status has changed to 'refunded'
        self.assertEqual(order.status, 'refunded')

        # A LogEntry should have been created about the refund
        entries_q = admin_log.entries_for(trans)
        self.assertEqual(entries_q[0].change_message, 'Refund of EUR 25.00 was successful')
        entries_q = admin_log.entries_for(trans.order)
        self.assertEqual(
            entries_q[0].change_message,
            'Updated refunded amount to EUR 25.00 (refunded tax EUR 0.00)'
        )

    def test_refund_single(self, mock_transact_sale) -> None:
        subscription = self.create_subscription()
        order = subscription.generate_order()
        trans = order.generate_transaction(ip_address='fe80::5ad5:4eaf:feb0:4747')
        mock_transact_sale.assert_not_called()

        self.assertTrue(trans.charge())

        # No refund yet
        self.assertIsNone(trans.refunded_at)
        self.assertIsNone(order.refunded_at)
        self.assertEqual(trans.amount_refunded, Money('EUR', 0))
        self.assertEqual(trans.order.refunded, Money('EUR', 0))

        # Issue a refund
        trans.refund(Money('EUR', 100))
        trans.refresh_from_db()
        trans.order.refresh_from_db()

        # Check that refund was successful and that it was recorded
        self.assertIsNotNone(trans.refunded_at)
        self.assertEqual(trans.refunded_at, trans.order.refunded_at)
        self.assertEqual(trans.amount_refunded, Money('EUR', 100))
        self.assertEqual(trans.order.refunded, Money('EUR', 100))
        self.assertEqual(trans.order.tax_refunded, Money('EUR', 0))

        # A LogEntry should have been created about the refund
        entries_q = admin_log.entries_for(trans)
        self.assertEqual(entries_q[0].change_message, 'Refund of EUR 1.00 was successful')
        entries_q = admin_log.entries_for(trans.order)
        self.assertEqual(
            entries_q[0].change_message,
            'Updated refunded amount to EUR 1.00 (refunded tax EUR 0.00)'
        )

    def test_refund_single_with_tax(self, mock_transact_sale) -> None:
        # Prepare a subscription with Dutch VAT:
        subscription = self.create_subscription_with_product_and_billing_details(
            product_type=ProductType.ELECTRONIC_SERVICE.value,
            country='NL',
        )
        self.assertEqual(subscription.tax_country, 'NL')
        self.assertEqual(subscription.tax_rate, 21)
        self.assertEqual(subscription.tax_type, 'VATC')

        order = subscription.generate_order()
        trans = order.generate_transaction(ip_address='fe80::5ad5:4eaf:feb0:4747')
        mock_transact_sale.assert_not_called()

        mock_transact_sale.return_value = {'transaction_id': 'mocked-transaction-id'}
        self.assertTrue(trans.charge())

        # No refund yet
        self.assertIsNone(trans.refunded_at)
        self.assertIsNone(order.refunded_at)
        self.assertEqual(trans.amount_refunded, Money('EUR', 0))
        self.assertEqual(trans.order.refunded, Money('EUR', 0))

        # Issue a refund
        trans.refund(Money('EUR', 121))
        trans.refresh_from_db()
        trans.order.refresh_from_db()

        # Check that refund was successful and that it was recorded
        self.assertIsNotNone(trans.refunded_at)
        self.assertEqual(trans.amount_refunded, Money('EUR', 121))
        self.assertEqual(trans.order.refunded, Money('EUR', 121))
        self.assertEqual(trans.order.tax_refunded, Money('EUR', 21))

        # A LogEntry should have been created about the refund
        entries_q = admin_log.entries_for(trans)
        self.assertEqual(entries_q[0].change_message, 'Refund of EUR 1.21 was successful')
        entries_q = admin_log.entries_for(trans.order)
        self.assertEqual(
            entries_q[0].change_message,
            'Updated refunded amount to EUR 1.21 (refunded tax EUR 0.21)'
        )

    def test_refund_multiple(self, mock_transact_sale) -> None:
        subscription = self.create_subscription()
        order = subscription.generate_order()
        trans1 = order.generate_transaction(ip_address='fe80::5ad5:4eaf:feb0:4747')
        trans2 = order.generate_transaction(ip_address='fe80::5ad5:4eaf:feb0:4747')
        mock_transact_sale.assert_not_called()

        mock_transact_sale.return_value = {'transaction_id': 'mocked-transaction-id'}
        self.assertTrue(trans1.charge())
        self.assertTrue(trans2.charge())

        # No refund yet
        self.assertIsNone(order.refunded_at)
        self.assertIsNone(trans1.refunded_at)
        self.assertIsNone(trans2.refunded_at)
        self.assertEqual(trans1.amount_refunded, Money('EUR', 0))
        self.assertEqual(trans2.amount_refunded, Money('EUR', 0))
        self.assertEqual(trans1.order.refunded, Money('EUR', 0))
        self.assertEqual(trans2.order.refunded, Money('EUR', 0))

        # Issue a refund
        trans1.refund(Money('EUR', 100))
        trans1.refresh_from_db()
        trans1.order.refresh_from_db()

        # Check that refund was successful and that it was recorded
        self.assertIsNotNone(trans1.refunded_at)
        self.assertEqual(trans1.amount_refunded, Money('EUR', 100))
        self.assertEqual(trans1.order.refunded, Money('EUR', 100))
        self.assertEqual(trans1.order.tax_refunded, Money('EUR', 0))

        # A LogEntry should have been created about the refund
        entries_q = admin_log.entries_for(trans1)
        self.assertEqual(entries_q[0].change_message, 'Refund of EUR 1.00 was successful')
        entries_q = admin_log.entries_for(trans1.order)
        self.assertEqual(
            entries_q[0].change_message,
            'Updated refunded amount to EUR 1.00 (refunded tax EUR 0.00)'
        )

        # Issue another refund
        trans2.refund(Money('EUR', 250))
        trans2.refresh_from_db()
        trans2.order.refresh_from_db()

        self.assertIsNotNone(trans2.refunded_at)
        self.assertEqual(trans2.amount_refunded, Money('EUR', 250))
        self.assertEqual(trans2.order.refunded, Money('EUR', 350))
        self.assertEqual(trans2.order.tax_refunded, Money('EUR', 0))

        # A LogEntry should have been created about the refund
        entries_q = admin_log.entries_for(trans2)
        self.assertEqual(entries_q[0].change_message, 'Refund of EUR 2.50 was successful')
        entries_q = admin_log.entries_for(trans2.order)
        self.assertEqual(
            entries_q[0].change_message,
            'Updated refunded amount to EUR 3.50 (refunded tax EUR 0.00)'
        )

    def test_refund_multiple_with_tax(self, mock_transact_sale) -> None:
        # Prepare a subscription with Dutch VAT:
        subscription = self.create_subscription_with_product_and_billing_details(
            product_type=ProductType.ELECTRONIC_SERVICE.value,
            country='NL',
        )
        self.assertEqual(subscription.tax_country, 'NL')
        self.assertEqual(subscription.tax_rate, 21)
        self.assertEqual(subscription.tax_type, 'VATC')

        order = subscription.generate_order()
        trans1 = order.generate_transaction(ip_address='fe80::5ad5:4eaf:feb0:4747')
        trans2 = order.generate_transaction(ip_address='fe80::5ad5:4eaf:feb0:4747')
        mock_transact_sale.assert_not_called()

        mock_transact_sale.return_value = {'transaction_id': 'mocked-transaction-id'}
        self.assertTrue(trans1.charge())
        self.assertTrue(trans2.charge())

        # No refund yet
        self.assertIsNone(order.refunded_at)
        self.assertIsNone(trans1.refunded_at)
        self.assertIsNone(trans2.refunded_at)
        self.assertEqual(trans1.amount_refunded, Money('EUR', 0))
        self.assertEqual(trans2.amount_refunded, Money('EUR', 0))
        self.assertEqual(trans1.order.refunded, Money('EUR', 0))
        self.assertEqual(trans2.order.refunded, Money('EUR', 0))

        # Issue a refund
        trans1.refund(Money('EUR', 100))
        trans1.refresh_from_db()
        trans1.order.refresh_from_db()

        # Check that refund was successful and that it was recorded
        self.assertIsNotNone(trans1.refunded_at)
        self.assertEqual(trans1.amount_refunded, Money('EUR', 100))
        self.assertEqual(trans1.order.refunded, Money('EUR', 100))
        self.assertEqual(trans1.order.tax_refunded, Money('EUR', 17))

        # A LogEntry should have been created about the refund
        entries_q = admin_log.entries_for(trans1)
        self.assertEqual(entries_q[0].change_message, 'Refund of EUR 1.00 was successful')
        entries_q = admin_log.entries_for(trans1.order)
        self.assertEqual(
            entries_q[0].change_message,
            'Updated refunded amount to EUR 1.00 (refunded tax EUR 0.17)'
        )
        self.assertEqual(trans1.refunded_at, trans1.order.refunded_at)

        # Issue another refund
        trans2.refund(Money('EUR', 21))
        trans2.refresh_from_db()
        trans2.order.refresh_from_db()

        self.assertIsNotNone(trans2.refunded_at)
        self.assertEqual(trans2.amount_refunded, Money('EUR', 21))
        # Both refund total and tax refunded were summed and stored
        self.assertEqual(trans2.order.refunded, Money('EUR', 121))
        self.assertEqual(trans2.order.tax_refunded, Money('EUR', 21))

        # A LogEntry should have been created about the refund
        entries_q = admin_log.entries_for(trans2)
        self.assertEqual(entries_q[0].change_message, 'Refund of EUR 0.21 was successful')
        entries_q = admin_log.entries_for(trans2.order)
        self.assertEqual(
            entries_q[0].change_message,
            'Updated refunded amount to EUR 1.21 (refunded tax EUR 0.21)'
        )
        self.assertEqual(trans2.refunded_at, trans2.order.refunded_at)

    def test_refund_multiple_with_tax_full(self, mock_transact_sale) -> None:
        # Prepare a subscription with Dutch VAT:
        subscription = self.create_subscription_with_product_and_billing_details(
            product_type=ProductType.ELECTRONIC_SERVICE.value,
            country='NL',
        )
        self.assertEqual(subscription.tax_country, 'NL')
        self.assertEqual(subscription.tax_rate, 21)
        self.assertEqual(subscription.tax_type, 'VATC')

        order = subscription.generate_order()
        trans1 = order.generate_transaction(ip_address='fe80::5ad5:4eaf:feb0:4747')
        trans2 = order.generate_transaction(ip_address='fe80::5ad5:4eaf:feb0:4747')
        mock_transact_sale.assert_not_called()

        mock_transact_sale.return_value = {'transaction_id': 'mocked-transaction-id'}
        self.assertTrue(trans1.charge())
        self.assertTrue(trans2.charge())

        # No refund yet
        self.assertIsNone(order.refunded_at)
        self.assertIsNone(trans1.refunded_at)
        self.assertIsNone(trans2.refunded_at)
        self.assertEqual(trans1.amount_refunded, Money('EUR', 0))
        self.assertEqual(trans2.amount_refunded, Money('EUR', 0))
        self.assertEqual(trans1.order.refunded, Money('EUR', 0))
        self.assertEqual(trans2.order.refunded, Money('EUR', 0))

        # Issue a refund
        trans1.refund(Money('EUR', 100))
        trans1.refresh_from_db()
        trans1.order.refresh_from_db()

        # Check that refund was successful and that it was recorded
        self.assertIsNotNone(trans1.refunded_at)
        self.assertEqual(trans1.amount_refunded, Money('EUR', 100))
        self.assertEqual(trans1.order.refunded, Money('EUR', 100))
        self.assertEqual(trans1.order.tax_refunded, Money('EUR', 17))
        # Check that order status hasn't changed yet
        self.assertEqual(order.status, 'paid')

        # A LogEntry should have been created about the refund
        entries_q = admin_log.entries_for(trans1)
        self.assertEqual(entries_q[0].change_message, 'Refund of EUR 1.00 was successful')
        entries_q = admin_log.entries_for(trans1.order)
        self.assertEqual(
            entries_q[0].change_message,
            'Updated refunded amount to EUR 1.00 (refunded tax EUR 0.17)'
        )
        self.assertEqual(trans1.refunded_at, trans1.order.refunded_at)

        # Issue another refund, this time for the remained of order total
        trans2.refund(Money('EUR', 2400))
        trans2.refresh_from_db()
        trans2.order.refresh_from_db()

        self.assertIsNotNone(trans2.refunded_at)
        self.assertEqual(trans2.amount_refunded, Money('EUR', 2400))
        # Both refund total and tax refunded were summed and stored
        self.assertEqual(trans2.order.refunded, Money('EUR', 2500))
        self.assertEqual(trans2.order.tax_refunded, Money('EUR', 434))
        self.assertEqual(trans2.order.tax_refunded, order.tax)
        # Check that order status has changed to refunded
        self.assertEqual(order.status, 'refunded')

        # A LogEntry should have been created about the refund
        entries_q = admin_log.entries_for(trans2)
        self.assertEqual(entries_q[0].change_message, 'Refund of EUR 24.00 was successful')
        entries_q = admin_log.entries_for(trans2.order)
        self.assertEqual(
            entries_q[0].change_message,
            'Updated refunded amount to EUR 25.00 (refunded tax EUR 4.34)'
        )
        self.assertEqual(trans2.refunded_at, trans2.order.refunded_at)
