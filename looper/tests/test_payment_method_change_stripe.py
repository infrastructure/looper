from unittest import mock

from django.contrib.auth import get_user_model
from django.urls import reverse
# from responses import _recorder
import responses
import stripe

from .. import models, money, stripe_utils
from . import AbstractLooperTestCase, ResponsesMixin

from looper.tests.factories import SubscriptionFactory

User = get_user_model()


class _BaseTestCase(AbstractLooperTestCase):
    fixtures = ['gateways', 'devfund', 'testuser', 'systemuser']
    gateway_name = 'stripe'
    subscription = None

    def setUp(self):
        super().setUp()
        self.user = User.objects.get(email='harry@blender.org')
        self.subscription = SubscriptionFactory(
            customer=self.user.customer,
            price=money.Money('EUR', 100),
            status='active',
        )


@mock.patch('looper.gateways.stripe.webhook.WebhookSignature.verify_header', return_value=True)
class PaymentMethodChangeDone(ResponsesMixin, _BaseTestCase):
    responses_files = ['webhook_after_pm_change_done_url_does_nothing.yaml']

    # @_recorder.record(file_path=f'{ResponsesMixin.responses_file_path}/webhook_after_pm_change_done_url_does_nothing.yaml')
    @responses.activate
    def test_webhook_after_pm_change_done_url_does_nothing(self, _):
        # tweak the response because this test expects a different subscription_id in metadata
        for _ in responses.registered():
            if 'expand%5B0%5D=setup_intent' in _.url or '/setup_intents/' in _.url:
                assert '\"subscription_id\": \"1' in _.body
                _.body = _.body.replace(
                    '\"subscription_id\": \"1',
                    f'\"subscription_id\": \"{self.subscription.pk}'
                )
        url = reverse(
            'payment_method_change',
            kwargs={'subscription_id': self.subscription.pk}
        )
        old_pm = self.subscription.payment_method
        self.client.force_login(self.user)
        resp = self.client.post(url)

        # print('Continue to Stripe:', resp['Location'])
        # input('Press Enter when ready to continue')
        stripe_session_id = resp['Location'].split('#')[0].split('/')[-1]
        done_url = reverse(
            'payment_method_change_done',
            kwargs={
                'subscription_id': self.subscription.pk,
                'stripe_session_id': stripe_session_id,
            },
        )
        resp2 = self.client.get(done_url)
        self.assertEqual(resp2.status_code, 302)

        subscription = models.Subscription.objects.get(pk=self.subscription.pk)
        new_pm = subscription.payment_method
        self.assertNotEqual(old_pm, new_pm)

        # need to fetch the session to construct the webhook payload
        stripe_session = stripe.checkout.Session.retrieve(
            stripe_session_id,
            expand=['setup_intent']
        )
        webhook_payload = {
            'data': {
                'object': stripe_session.setup_intent,
            },
            'type': 'setup_intent.succeeded'
        }

        webhook_payload = {
            'data': {
                'object': {
                    "id": "seti_1OGTbpKh8wGqs1axRjEyum2P",
                    "metadata": {
                        "subscription_id": "1"
                    },
                    "payment_method": "pm_1OGTc3Kh8wGqs1axdxZKEv9i",
                    "status": "succeeded",
                }
            },
            'type': 'setup_intent.succeeded'
        }
        stripe_utils.upsert_subscription_payment_method_from_setup_intent(webhook_payload)
        subscription_copy = models.Subscription.objects.get(pk=self.subscription.pk)
        self.assertEqual(subscription_copy.payment_method, new_pm)

    @responses.activate
    def test_webhook_before_pm_change_done_url_does_nothing(self, _):
        # tweak the response because this test expects a different subscription_id in metadata
        for _ in responses.registered():
            if 'expand%5B0%5D=setup_intent' in _.url or '/setup_intents/' in _.url:
                assert '\"subscription_id\": \"1' in _.body
                _.body = _.body.replace(
                    '\"subscription_id\": \"1',
                    f'\"subscription_id\": \"{self.subscription.pk}'
                )
        url = reverse(
            'payment_method_change',
            kwargs={'subscription_id': self.subscription.pk}
        )
        # to reset the result of the first test case
        self.subscription.payment_method = None
        self.subscription.save()
        self.client.force_login(self.user)
        resp = self.client.post(url)
        stripe_session_id = resp['Location'].split('#')[0].split('/')[-1]

        webhook_payload = {
            'data': {
                'object': {
                    "id": "seti_1OGTbpKh8wGqs1axRjEyum2P",
                    "metadata": {
                        "subscription_id": "1"
                    },
                    "payment_method": "pm_1OGTc3Kh8wGqs1axdxZKEv9i",
                    "status": "succeeded",
                }
            },
            'type': 'setup_intent.succeeded'
        }
        stripe_utils.upsert_subscription_payment_method_from_setup_intent(webhook_payload)
        subscription = models.Subscription.objects.get(pk=self.subscription.pk)
        pm = subscription.payment_method
        self.assertIsNotNone(pm)

        done_url = reverse(
            'payment_method_change_done',
            kwargs={
                'subscription_id': self.subscription.pk,
                'stripe_session_id': stripe_session_id,
            },
        )
        resp2 = self.client.get(done_url)
        self.assertEqual(resp2.status_code, 302)
        subscription_copy = models.Subscription.objects.get(pk=self.subscription.pk)
        self.assertEqual(subscription_copy.payment_method, pm)
