from decimal import Decimal
from typing import Optional
from unittest import mock
import datetime
import logging

from django.utils import timezone
from django.test import override_settings
from dateutil.relativedelta import relativedelta
import responses

from . import AbstractLooperTestCase, _mock_vies_response

from ..clock import Clock
from .. import models, exceptions, signals, admin_log, taxes, money
from looper.tests.factories import SubscriptionFactory, create_customer_with_billing_address

utc = datetime.timezone.utc
log = logging.getLogger(__name__)

# Margin for clock ticks (the tick happens at the given time + this margin).
# Just to stabilise floating point comparisons.
tick_delay = datetime.timedelta(seconds=0.25)

# Retry charging failed orders after this delta.
retry_after = relativedelta(minutes=15)

# Expire on-hold subscriptions after this time of inactivity
expire_after = datetime.timedelta(weeks=4 * 6)


@override_settings(LOOPER_ORDER_RETRY_AFTER=retry_after)
class AbstractClockTest(AbstractLooperTestCase):
    fixtures = AbstractLooperTestCase.fixtures + ['mock-gateway']
    gateway_name = 'mock'

    # Start time of activated subscriptions.
    start_time = datetime.datetime(2015, 5, 4, 3, 2, 1, tzinfo=utc)

    def setUp(self):
        super().setUp()

        # The signal itself is passed to the connected function in the 'signal' kwarg,
        # so we can check all signals via one receiver.
        self.signals = mock.Mock()
        signals.automatic_payment_succesful.connect(self.signals)
        signals.automatic_payment_soft_failed.connect(self.signals)
        signals.automatic_payment_failed.connect(self.signals)
        signals.managed_subscription_notification.connect(self.signals)
        signals.automatic_payment_soft_failed_no_payment_method.connect(self.signals)
        signals.automatic_payment_failed_no_payment_method.connect(self.signals)
        signals.subscription_expired.connect(self.signals)

    def _activate(self, subs: models.Subscription) -> None:
        # This triggers the next_payment to be start_time + 1 month
        with mock.patch('django.utils.timezone.now') as mock_now:
            mock_now.return_value = self.start_time
            subs.status = 'active'
            subs.save()

    def _clock_tick(self, tick_time: datetime.datetime) -> None:
        """Perform the clock tick one hour after the expected next payment time."""
        log.debug('Ticking clock, tick_time=%s', tick_time)
        with mock.patch('django.utils.timezone.now') as mock_now:
            mock_now.return_value = tick_time + tick_delay
            clock = Clock()
            clock.tick()

    def create_active_subscription(self, **extra) -> models.Subscription:
        subs = self.create_subscription(**extra)
        self._activate(subs)
        subs.refresh_from_db()

        if subs.collection_method == 'managed':
            self.assertIsNone(subs.latest_order())
        else:
            order = subs.generate_order()
            self.assertEqual(0, order.collection_attempts)

        return subs


class RenewalTestCase(AbstractClockTest):
    def test_renew_no_subscriptions(self):
        # This shouldn't crash but also don't do anything.
        clock = Clock()
        clock.tick()

        self.signals.assert_not_called()

    def test_renew_manual_subscription(self):
        subs = self.create_active_subscription()
        subs.collection_method = 'manual'
        subs.save()
        last_order_pk = subs.latest_order().pk
        old_next_payment = subs.next_payment

        tick_time = self.start_time + subs.interval
        self._clock_tick(tick_time)

        # The subscription should not be renewed now.
        subs.refresh_from_db()
        self.assertEqual('on-hold', subs.status)
        self.assertEqual(1, subs.intervals_elapsed)
        self.assertAlmostEqualDateTime(old_next_payment, subs.next_payment)

        # Test the order
        new_order = subs.latest_order()
        self.assertEqual('created', new_order.status)
        self.assertNotEqual(last_order_pk, new_order.pk)

        # Test the transaction, it shouldn't exist.
        self.assertIsNone(new_order.latest_transaction())

        self.signals.assert_not_called()

    def _clock_tick_automatic_order_paid_subscription_renewed(
        self,
        order: models.Order,
        subs: models.Subscription,
        mock_transact_sale: mock.Mock,
    ):
        order.refresh_from_db()
        self.assertIsNone(order.retry_after)

        # Collect some properties before the clock ticks.
        last_order_pk = order.pk

        # Just a sanity check on the next payment date. It should be 1 interval after
        # the mocked start time.
        subs.refresh_from_db()
        self.assertAlmostEqualDateTime(self.start_time + subs.interval, subs.next_payment)

        mock_transact_sale.side_effect = [{'transaction_id': 'mocked-transaction-id'}]
        tick_time = self.start_time + subs.interval
        self._clock_tick(tick_time)

        # The subscription should be renewed now.
        subs.refresh_from_db()
        self.assertEqual('active', subs.status)
        self.assertEqual(1, subs.intervals_elapsed)
        self.assertAlmostEqualDateTime(tick_time + relativedelta(months=1), subs.next_payment)

        # Test the order
        new_order = subs.latest_order()
        self.assertEqual('paid', new_order.status)
        self.assertEqual(1, new_order.collection_attempts)
        self.assertIsNone(new_order.retry_after)
        self.assertNotEqual(last_order_pk, new_order.pk)

        # Test the transaction
        new_trans = new_order.latest_transaction()
        self.assertEqual('succeeded', new_trans.status)
        self.assertEqual(subs.price, new_trans.amount)
        entries_q = admin_log.entries_for(new_trans)
        self.assertEqual(1, len(entries_q))
        self.assertIn('success', entries_q.first().change_message)

        # Test that this change was logged
        entries_q = admin_log.entries_for(new_order.subscription)
        self.assertEqual(4, len(entries_q))
        self.assertIn('Automatic renewal successful. Updated next payment to', entries_q[0].change_message)
        self.assertIn('Charge of', entries_q[1].change_message)
        self.assertIn('Created order', entries_q[2].change_message)
        self.assertEqual(
            'Status changed from on-hold to active', entries_q[3].change_message
        )
        return new_order

    @mock.patch('looper.gateways.MockableGateway.transact_sale')
    def test_renew_active_subscription(self, mock_transact_sale):
        subs = self.create_active_subscription()

        # Collect some properties before the clock ticks.
        order = subs.latest_order()
        payment_token = subs.payment_method.token

        # Tick the clock and check that subscription was renews, order and transaction were created
        new_order = self._clock_tick_automatic_order_paid_subscription_renewed(
            order, subs, mock_transact_sale
        )

        # Test the calls on the gateway
        mock_transact_sale.assert_called_with(
            payment_token,
            subs.price,
            source='recurring_first',
            authentication_id=None,
            payment_intent_id=None,
            payment_intent_metadata={'order_id': new_order.pk},
        )

    @responses.activate
    @mock.patch('looper.gateways.MockableGateway.transact_sale')
    def test_renew_active_subscription_with_tax_number_vies_called_invalid(self, mock_transact_sale):
        subs_price = money.Money('EUR', 990)
        expected_tax = money.Money('EUR', 158)
        customer = create_customer_with_billing_address(
            vat_number='DE260543043',
            country='DE',
        )
        subs = SubscriptionFactory(
            customer=customer,
            tax_type=taxes.TaxType.VAT_REVERSE_CHARGE.value,
            tax_rate=Decimal(19),
            tax_country='DE',
            plan__product__type=taxes.ProductType.ELECTRONIC_SERVICE.value,
            price=subs_price,
        )
        self._activate(subs)
        self.assertEqual(subs.tax, money.Money('EUR', 0))
        self.assertEqual(subs.tax_type, taxes.TaxType.VAT_REVERSE_CHARGE.value)
        self.assertEqual(models.TaxNumberVerification.objects.count(), 0)

        # Just a sanity check on the next payment date. It should be 1 interval after
        # the mocked start time.
        subs.refresh_from_db()
        self.assertAlmostEqualDateTime(self.start_time + subs.interval, subs.next_payment)

        mock_transact_sale.side_effect = [{'transaction_id': 'mocked-transaction-id'}]
        # When clock ticks VIES will be called to validate the VAT number, and number will be invalid
        _mock_vies_response(is_valid=False)
        tick_time = self.start_time + subs.interval
        self._clock_tick(tick_time)

        # Check that subscription's tax fields has change and VIES response has been recorded
        subs.refresh_from_db()
        self.assertEqual(subs.price, subs_price)
        self.assertEqual(subs.tax, expected_tax)
        self.assertEqual(subs.tax_type, taxes.TaxType.VAT_CHARGE.value)
        self.assertEqual(models.TaxNumberVerification.objects.count(), 1)
        verification = models.TaxNumberVerification.get_latest(customer.billing_address.vat_number)
        self.assertFalse(verification.is_valid)
        self.assertEqual('vies', verification.source)
        self.assertEqual(
            '{"countryCode": "DE", "vatNumber": "260543043", "requestDate": "2021-07-05",'
            ' "valid": false, "name": "TESTBIZ L.T.D",'
            ' "address": "MOCKASTREET 123 00161       123-3234 TESTVILLE"}',
            verification.response,
        )

        # Test that the change of tax fields was logged
        entries_q = admin_log.entries_for(subs).order_by('action_time')
        self.assertEqual(8, entries_q.count())
        self.assertEqual(
            "Attempting to verify customer's VAT number DE260543043"
            ' because subscription has TaxType.VAT_REVERSE_CHARGE',
            entries_q[1].change_message,
        )
        self.assertEqual(
            "Customer's VAT number DE260543043 is not valid, according to VIES",
            entries_q[2].change_message,
        )
        self.assertEqual('Tax type changed from TaxType.VAT_REVERSE_CHARGE to TaxType.VAT_CHARGE', entries_q[3].change_message)
        self.assertEqual('Tax changed from EUR 0.00 to EUR 1.58 (VATC 19%, DE)', entries_q[4].change_message)
        # Test that other changes were logged as usual
        self.assertIn('Automatic renewal successful. Updated next payment to', entries_q[7].change_message)
        self.assertIn('Charge of', entries_q[6].change_message)
        self.assertIn('Created order', entries_q[5].change_message)

        # Check that subscription was renewed, order and transaction were created
        self.assertEqual('active', subs.status)
        self.assertEqual(1, subs.intervals_elapsed)
        self.assertAlmostEqualDateTime(tick_time + relativedelta(months=1), subs.next_payment)

        # Test the order
        new_order = subs.latest_order()
        self.assertEqual('paid', new_order.status)
        self.assertEqual(Decimal(19), new_order.tax_rate)
        self.assertEqual(taxes.TaxType.VAT_CHARGE.value, new_order.tax_type)
        self.assertEqual(subs_price, new_order.price)
        self.assertEqual(expected_tax, new_order.tax)
        self.assertEqual(1, new_order.collection_attempts)
        self.assertIsNone(new_order.retry_after)

        # Test the transaction
        new_trans = new_order.latest_transaction()
        self.assertEqual('succeeded', new_trans.status)
        self.assertEqual(subs.price, new_trans.amount)
        entries_q = admin_log.entries_for(new_trans)
        self.assertEqual(1, len(entries_q))
        self.assertIn('success', entries_q.first().change_message)

    @responses.activate
    @mock.patch('looper.gateways.MockableGateway.transact_sale')
    def test_renew_active_subscription_with_tax_number_vies_called_valid(self, mock_transact_sale):
        subs_price = money.Money('EUR', 990)
        expected_tax = money.Money('EUR', 0)
        order_price = money.Money('EUR', 832)
        customer = create_customer_with_billing_address(
            vat_number='DE260543043',
            country='DE',
        )
        subs = SubscriptionFactory(
            customer=customer,
            tax_type=taxes.TaxType.VAT_REVERSE_CHARGE.value,
            tax_rate=Decimal(19),
            tax_country='DE',
            plan__product__type=taxes.ProductType.ELECTRONIC_SERVICE.value,
            price=subs_price,
        )
        self._activate(subs)
        self.assertEqual(subs.tax, expected_tax)
        self.assertEqual(subs.tax_type, taxes.TaxType.VAT_REVERSE_CHARGE.value)
        self.assertEqual(models.TaxNumberVerification.objects.count(), 0)

        # Just a sanity check on the next payment date. It should be 1 interval after
        # the mocked start time.
        subs.refresh_from_db()
        self.assertAlmostEqualDateTime(self.start_time + subs.interval, subs.next_payment)

        mock_transact_sale.side_effect = [{'transaction_id': 'mocked-transaction-id'}]
        # When clock ticks VIES will be called to validate the VAT number, and number will be valid
        _mock_vies_response(is_valid=True)
        tick_time = self.start_time + subs.interval
        self._clock_tick(tick_time)

        # Check that subscription's tax fields didn't change and VIES response has been recorded
        subs.refresh_from_db()
        self.assertEqual(subs.price, subs_price)
        self.assertEqual(subs.tax, expected_tax)
        self.assertEqual(subs.tax_type, taxes.TaxType.VAT_REVERSE_CHARGE.value)
        self.assertEqual(models.TaxNumberVerification.objects.count(), 1)
        verification = models.TaxNumberVerification.get_latest(customer.billing_address.vat_number)
        self.assertTrue(verification.is_valid)
        self.assertEqual('vies', verification.source)
        self.assertEqual(
            '{"countryCode": "DE", "vatNumber": "260543043", "requestDate": "2021-07-05",'
            ' "valid": true, "name": "TESTBIZ L.T.D",'
            ' "address": "MOCKASTREET 123 00161       123-3234 TESTVILLE"}',
            verification.response,
        )

        # Check that subscription was renewed, order and transaction were created
        self.assertEqual('active', subs.status)
        self.assertEqual(1, subs.intervals_elapsed)
        self.assertAlmostEqualDateTime(tick_time + relativedelta(months=1), subs.next_payment)

        # Test the order
        new_order = subs.latest_order()
        self.assertEqual('paid', new_order.status)
        self.assertEqual(Decimal(19), new_order.tax_rate)
        self.assertEqual(taxes.TaxType.VAT_REVERSE_CHARGE.value, new_order.tax_type)
        self.assertEqual(order_price, new_order.price)
        self.assertEqual(expected_tax, new_order.tax)
        self.assertEqual(1, new_order.collection_attempts)
        self.assertIsNone(new_order.retry_after)

        # Test the transaction
        new_trans = new_order.latest_transaction()
        self.assertEqual('succeeded', new_trans.status)
        self.assertEqual(order_price, new_trans.amount)
        entries_q = admin_log.entries_for(new_trans)
        self.assertEqual(1, len(entries_q))
        self.assertIn('success', entries_q.first().change_message)

        # Test that changes were logged as usual
        entries_q = admin_log.entries_for(subs).order_by('action_time')
        self.assertEqual(5, entries_q.count())
        self.assertEqual(
            "Attempting to verify customer's VAT number DE260543043"
            ' because subscription has TaxType.VAT_REVERSE_CHARGE',
            entries_q[1].change_message,
        )
        self.assertIn('Automatic renewal successful. Updated next payment to', entries_q[4].change_message)
        self.assertIn('Charge of', entries_q[3].change_message)
        self.assertIn('Created order', entries_q[2].change_message)

    @responses.activate
    @mock.patch('looper.gateways.MockableGateway.transact_sale')
    def test_renew_active_subscription_with_tax_number_vies_called_valid_verification_already_exists(
        self, mock_transact_sale
    ):
        subs_price = money.Money('EUR', 990)
        expected_tax = money.Money('EUR', 0)
        order_price = money.Money('EUR', 832)
        expected_vies_response = (
            '{"countryCode": "DE", "vatNumber": "260543043", "requestDate": "2021-07-05",'
            ' "valid": true, "name": "TESTBIZ L.T.D",'
            ' "address": "MOCKASTREET 123 00161       123-3234 TESTVILLE"}'
        )
        customer = create_customer_with_billing_address(
            vat_number='DE260543043',
            country='DE',
        )
        existing_vefication = models.TaxNumberVerification(
            number=customer.billing_address.vat_number, is_valid=True, response=expected_vies_response
        )
        existing_vefication.save()
        subs = SubscriptionFactory(
            customer=customer,
            tax_type=taxes.TaxType.VAT_REVERSE_CHARGE.value,
            tax_rate=Decimal(19),
            tax_country='DE',
            plan__product__type=taxes.ProductType.ELECTRONIC_SERVICE.value,
            price=subs_price,
        )
        self._activate(subs)
        self.assertEqual(subs.tax, expected_tax)
        self.assertEqual(subs.tax_type, taxes.TaxType.VAT_REVERSE_CHARGE.value)
        self.assertEqual(models.TaxNumberVerification.objects.count(), 1)

        # Just a sanity check on the next payment date. It should be 1 interval after
        # the mocked start time.
        subs.refresh_from_db()
        self.assertAlmostEqualDateTime(self.start_time + subs.interval, subs.next_payment)

        mock_transact_sale.side_effect = [{'transaction_id': 'mocked-transaction-id'}]
        # When clock ticks VIES will be called to validate the VAT number, and number will be valid
        _mock_vies_response(is_valid=True)
        tick_time = self.start_time + subs.interval
        self._clock_tick(tick_time)

        # Check that subscription's tax fields didn't change and VIES response has been recorded
        subs.refresh_from_db()
        self.assertEqual(subs.price, subs_price)
        self.assertEqual(subs.tax, expected_tax)
        self.assertEqual(subs.tax_type, taxes.TaxType.VAT_REVERSE_CHARGE.value)
        self.assertEqual(models.TaxNumberVerification.objects.count(), 1)
        verification = models.TaxNumberVerification.get_latest(customer.billing_address.vat_number)
        self.assertTrue(verification.is_valid)
        self.assertEqual('vies', verification.source)
        # No new verification created, because the response is the same, only updated_at changed
        self.assertEqual(existing_vefication.pk, verification.pk)
        self.assertGreater(existing_vefication.updated_at, verification.updated_at)
        self.assertEqual(expected_vies_response, verification.response)

        # Check that subscription was renewed, order and transaction were created
        self.assertEqual('active', subs.status)
        self.assertEqual(1, subs.intervals_elapsed)
        self.assertAlmostEqualDateTime(tick_time + relativedelta(months=1), subs.next_payment)

        # Test the order
        new_order = subs.latest_order()
        self.assertEqual('paid', new_order.status)
        self.assertEqual(Decimal(19), new_order.tax_rate)
        self.assertEqual(taxes.TaxType.VAT_REVERSE_CHARGE.value, new_order.tax_type)
        self.assertEqual(order_price, new_order.price)
        self.assertEqual(expected_tax, new_order.tax)
        self.assertEqual(1, new_order.collection_attempts)
        self.assertIsNone(new_order.retry_after)

        # Test the transaction
        new_trans = new_order.latest_transaction()
        self.assertEqual('succeeded', new_trans.status)
        self.assertEqual(order_price, new_trans.amount)
        entries_q = admin_log.entries_for(new_trans)
        self.assertEqual(1, len(entries_q))
        self.assertIn('success', entries_q.first().change_message)

        # Test that changes were logged as usual
        entries_q = admin_log.entries_for(subs).order_by('action_time')
        self.assertEqual(5, entries_q.count())
        self.assertEqual(
            "Attempting to verify customer's VAT number DE260543043"
            ' because subscription has TaxType.VAT_REVERSE_CHARGE',
            entries_q[1].change_message,
        )
        self.assertIn('Automatic renewal successful. Updated next payment to', entries_q[4].change_message)
        self.assertIn('Charge of', entries_q[3].change_message)
        self.assertIn('Created order', entries_q[2].change_message)

    @responses.activate
    @mock.patch('looper.gateways.MockableGateway.transact_sale')
    def test_renew_active_subscription_with_tax_number_vies_called_broken(self, mock_transact_sale):
        subs_price = money.Money('EUR', 990)
        expected_tax = money.Money('EUR', 0)
        order_price = money.Money('EUR', 832)
        customer = create_customer_with_billing_address(
            vat_number='DE260543043',
            country='DE',
        )
        subs = SubscriptionFactory(
            customer=customer,
            tax_type=taxes.TaxType.VAT_REVERSE_CHARGE.value,
            tax_rate=Decimal(19),
            tax_country='DE',
            plan__product__type=taxes.ProductType.ELECTRONIC_SERVICE.value,
            price=subs_price,
        )
        self._activate(subs)
        self.assertEqual(subs.tax, expected_tax)
        self.assertEqual(subs.tax_type, taxes.TaxType.VAT_REVERSE_CHARGE.value)
        self.assertEqual(models.TaxNumberVerification.objects.count(), 0)

        # Just a sanity check on the next payment date. It should be 1 interval after
        # the mocked start time.
        subs.refresh_from_db()
        self.assertAlmostEqualDateTime(self.start_time + subs.interval, subs.next_payment)

        mock_transact_sale.side_effect = [{'transaction_id': 'mocked-transaction-id'}]
        # When clock ticks VIES will be called to validate the VAT number and "break" with an HTTP 500
        _mock_vies_response(is_broken=True)
        tick_time = self.start_time + subs.interval
        self._clock_tick(tick_time)

        # Check that subscription's tax fields didn't change and no verification record created
        subs.refresh_from_db()
        self.assertEqual(subs.price, subs_price)
        self.assertEqual(subs.tax, expected_tax)
        self.assertEqual(subs.tax_type, taxes.TaxType.VAT_REVERSE_CHARGE.value)
        self.assertEqual(models.TaxNumberVerification.objects.count(), 0)

        # Check that subscription was renewed, order and transaction were created
        self.assertEqual('active', subs.status)
        self.assertEqual(1, subs.intervals_elapsed)
        self.assertAlmostEqualDateTime(tick_time + relativedelta(months=1), subs.next_payment)

        # Test the order
        new_order = subs.latest_order()
        self.assertEqual('paid', new_order.status)
        self.assertEqual(Decimal(19), new_order.tax_rate)
        self.assertEqual(taxes.TaxType.VAT_REVERSE_CHARGE.value, new_order.tax_type)
        self.assertEqual(order_price, new_order.price)
        self.assertEqual(expected_tax, new_order.tax)
        self.assertEqual(1, new_order.collection_attempts)
        self.assertIsNone(new_order.retry_after)

        # Test the transaction
        new_trans = new_order.latest_transaction()
        self.assertEqual('succeeded', new_trans.status)
        self.assertEqual(order_price, new_trans.amount)
        entries_q = admin_log.entries_for(new_trans)
        self.assertEqual(1, len(entries_q))
        self.assertIn('success', entries_q.first().change_message)

        # Test that changes were logged as usual
        entries_q = admin_log.entries_for(subs).order_by('action_time')
        self.assertEqual(5, entries_q.count())
        self.assertEqual(
            "Attempting to verify customer's VAT number DE260543043"
            ' because subscription has TaxType.VAT_REVERSE_CHARGE',
            entries_q[1].change_message,
        )
        self.assertIn('Automatic renewal successful. Updated next payment to', entries_q[4].change_message)
        self.assertIn('Charge of', entries_q[3].change_message)
        self.assertIn('Created order', entries_q[2].change_message)

    @mock.patch('looper.gateways.MockableGateway.transact_sale')
    def test_renew_active_subscription_prev_order_exists(self, mock_transact_sale):
        subs = self.create_active_subscription()

        with mock.patch('django.utils.timezone.now') as mock_now:
            mock_now.return_value = self.start_time
            order = subs.latest_order()
            order.status = 'paid'
            order.save()

        order.refresh_from_db()
        self.assertIsNone(order.retry_after)

        # Collect some properties before the clock ticks.
        payment_token = subs.payment_method.token

        # Tick the clock and check that subscription was renews, order and transaction were created
        new_order = self._clock_tick_automatic_order_paid_subscription_renewed(
            order, subs, mock_transact_sale
        )

        # Test the calls on the gateway: transaction is not considered recurring
        # because no previous successful transaction was found for a matching payment method
        mock_transact_sale.assert_called_with(
            payment_token,
            subs.price,
            source='recurring_first',
            authentication_id=None,
            payment_intent_id=None,
            payment_intent_metadata={'order_id': new_order.pk},
        )

    @mock.patch('looper.gateways.MockableGateway.transact_sale')
    def test_renew_active_subscription_prev_successfull_transaction_exists(self, mock_transact_sale):
        subs = self.create_active_subscription()

        with mock.patch('django.utils.timezone.now') as mock_now:
            mock_now.return_value = self.start_time
            order = subs.latest_order()
            trans = order.generate_transaction()
            trans.status = 'succeeded'
            trans.save(update_fields={'status'})

        # Collect some properties before the clock ticks.
        payment_token = subs.payment_method.token

        # Tick the clock and check that subscription was renews, order and transaction were created
        new_order = self._clock_tick_automatic_order_paid_subscription_renewed(
            order, subs, mock_transact_sale
        )

        # Test the calls on the gateway: transaction is considered recurring
        mock_transact_sale.assert_called_with(
            payment_token,
            subs.price,
            source='recurring',
            authentication_id=None,
            payment_intent_id=None,
            payment_intent_metadata={'order_id': new_order.pk},
        )

    @mock.patch('looper.gateways.MockableGateway.transact_sale')
    def test_renew_active_subscription_prev_successfull_transaction_exists_sca(self, mock_transact_sale):
        subs = self.create_active_subscription()

        with mock.patch('django.utils.timezone.now') as mock_now:
            mock_now.return_value = self.start_time
            order = subs.latest_order()
            trans = order.generate_transaction()
            trans.status = 'succeeded'
            trans.save(update_fields={'status'})
            auth = subs.payment_method.paymentmethodauthentication_set.create(
                gateway=order.payment_method.gateway,
                payment_method=order.payment_method,
                authentication_id='mock-authentication-id',
            )
        # Payment method authentication hasn't been used yet
        self.assertIsNone(auth.consumed_at)

        # Collect some properties before the clock ticks.
        payment_token = subs.payment_method.token

        # Tick the clock and check that subscription was renews, order and transaction were created
        new_order = self._clock_tick_automatic_order_paid_subscription_renewed(
            order, subs, mock_transact_sale
        )

        # Even though there's a previous successful transaction for this payment method,
        # transaction is considered recurring-first because usable authentication exists.
        mock_transact_sale.assert_called_with(
            payment_token,
            subs.price,
            source='recurring_first',
            authentication_id='mock-authentication-id',
            payment_intent_id=None,
            payment_intent_metadata={'order_id': new_order.pk},
        )
        # Check that authentication was stored at the transaction and was marked as used
        trans_auth = subs.latest_order().latest_transaction().authentication
        self.assertIsNotNone(trans_auth)
        self.assertEqual(auth.pk, trans_auth.pk)
        self.assertIsNotNone(trans_auth.consumed_at)
        self.assertGreater(trans_auth.consumed_at, self.start_time)

    @mock.patch('looper.gateways.MockableGateway.transact_sale')
    def test_renew_active_subscription_prev_successfull_transaction_exists_sca_already_used(self, mock_transact_sale):
        subs = self.create_active_subscription()

        with mock.patch('django.utils.timezone.now') as mock_now:
            mock_now.return_value = self.start_time
            order = subs.latest_order()
            trans = order.generate_transaction()
            trans.status = 'succeeded'
            trans.save(update_fields={'status'})
            auth = subs.payment_method.paymentmethodauthentication_set.create(
                gateway=order.payment_method.gateway,
                payment_method=order.payment_method,
                authentication_id='mock-authentication-id',
                consumed_at=self.start_time,
            )

        # Collect some properties before the clock ticks.
        payment_token = subs.payment_method.token

        # Tick the clock and check that subscription was renews, order and transaction were created
        new_order = self._clock_tick_automatic_order_paid_subscription_renewed(
            order, subs, mock_transact_sale
        )

        # Previous successful transaction for this payment method exists,
        # but no usable authentication, so this transaction is considered recurring.
        mock_transact_sale.assert_called_with(
            payment_token,
            subs.price,
            source='recurring',
            authentication_id=None,
            payment_intent_id=None,
            payment_intent_metadata={'order_id': new_order.pk},
        )
        # Check that no authentication was stored with the transaction
        trans_auth = subs.latest_order().latest_transaction().authentication
        self.assertIsNone(trans_auth)
        # Sanity check: payment method authentication consumed_at didn't change
        self.assertEqual(auth.consumed_at, self.start_time)

    @mock.patch('looper.gateways.MockableGateway.transact_sale')
    def test_renew_active_subscription_prev_successfull_transaction_exists_sca_too_old(self, mock_transact_sale):
        subs = self.create_active_subscription()

        with mock.patch('django.utils.timezone.now') as mock_now:
            mock_now.return_value = self.start_time
            order = subs.latest_order()
            trans = order.generate_transaction()
            trans.status = 'succeeded'
            trans.save(update_fields={'status'})
            auth = subs.payment_method.paymentmethodauthentication_set.create(
                gateway=order.payment_method.gateway,
                payment_method=order.payment_method,
                authentication_id='mock-authentication-id',
            )
            # Authentication created too long ago
            auth.created_at = self.start_time - (models.AUTHORIZATION_EXPIRY + datetime.timedelta(days=1))
            auth.save(update_fields={'created_at'})

        # Collect some properties before the clock ticks.
        payment_token = subs.payment_method.token

        # Tick the clock and check that subscription was renews, order and transaction were created
        new_order = self._clock_tick_automatic_order_paid_subscription_renewed(
            order, subs, mock_transact_sale
        )

        # Previous successful transaction for this payment method exists,
        # but no usable authentication, so this transaction is considered recurring.
        mock_transact_sale.assert_called_with(
            payment_token,
            subs.price,
            source='recurring',
            authentication_id=None,
            payment_intent_id=None,
            payment_intent_metadata={'order_id': new_order.pk},
        )
        # Check that no authentication was stored with the transaction
        trans_auth = subs.latest_order().latest_transaction().authentication
        self.assertIsNone(trans_auth)
        # Sanity check: payment method authentication consumed_at is still not set
        self.assertIsNone(auth.consumed_at)

    @mock.patch('looper.gateways.MockableGateway.transact_sale')
    def test_transact_sale_gatewayerror(self, mock_transact_sale):
        subs = self.create_active_subscription()

        # Collect some properties before the clock ticks.
        last_order_pk = subs.latest_order().pk
        payment_token = subs.payment_method.token
        old_next_payment = subs.next_payment

        mock_transact_sale.side_effect = [exceptions.GatewayError('mock gateway error')]
        tick_time = self.start_time + subs.interval
        with override_settings(LOOPER_CLOCK_MAX_AUTO_ATTEMPTS=1):
            self._clock_tick(tick_time)
        mock_transact_sale.assert_called_with(
            payment_token,
            subs.price,
            source='recurring_first',
            authentication_id=None,
            payment_intent_id=None,
            payment_intent_metadata={'order_id': subs.latest_order().pk},
        )

        # The subscription should not have been renewed.
        subs.refresh_from_db()
        self.assertEqual('on-hold', subs.status)
        self.assertEqual(1, subs.intervals_elapsed)
        self.assertAlmostEqualDateTime(old_next_payment, subs.next_payment)

        # Test the order
        new_order = subs.latest_order()
        self.assertEqual('failed', new_order.status)
        self.assertEqual(1, new_order.collection_attempts)
        self.assertNotEqual(last_order_pk, new_order.pk)
        self.assertIsNone(new_order.retry_after, 'Failed orders should not be retried')

        # Test the transaction
        self.assertEqual(1, new_order.transaction_set.count())
        new_trans = new_order.latest_transaction()

        assert new_trans is not None  # make mypy happy
        self.assertEqual('failed', new_trans.status)
        self.assertEqual(subs.price, new_trans.amount)

        entries_q = admin_log.entries_for(new_trans)
        self.assertEqual(1, len(entries_q))
        self.assertIn('failed', entries_q.first().change_message)

        # Test the sent signals
        self.signals.assert_called_once_with(
            signal=signals.automatic_payment_failed, sender=new_order, transaction=new_trans
        )

    @mock.patch('looper.gateways.MockableGateway.transact_sale')
    def test_retry_failed(self, mock_transact_sale):
        subs = self.create_active_subscription()
        pre_renewal_next_payment = subs.next_payment
        pre_renewal_order_pk = subs.latest_order().pk

        # First call should fail, second should succeed.
        mock_transact_sale.side_effect = [
            exceptions.GatewayError('mock gateway error'),
            {'transaction_id': 'mocked-transaction-id'},
        ]

        max_collection_attempts = 2
        first_tick_time = self.start_time + subs.interval
        with override_settings(LOOPER_CLOCK_MAX_AUTO_ATTEMPTS=max_collection_attempts):
            self._clock_tick(first_tick_time)

        # The subscription should still be active after one little failure.
        subs.refresh_from_db()
        self.assertEqual('active', subs.status)
        self.assertEqual(1, subs.intervals_elapsed)
        self.assertAlmostEqualDateTime(pre_renewal_next_payment, subs.next_payment)

        # Test the order
        renewal_order = subs.latest_order()
        first_transaction = renewal_order.transaction_set.earliest()
        failed_trans_pk = first_transaction.pk
        self.assertEqual('soft-failed', renewal_order.status)
        self.assertEqual(1, renewal_order.collection_attempts)
        self.assertNotEqual(
            pre_renewal_order_pk,
            renewal_order.pk,
            'A new order should have been created for the renewal',
        )
        self.assertAlmostEqualDateTime(first_tick_time + retry_after, renewal_order.retry_after)

        payment_token = subs.payment_method.token

        self.signals.assert_called_once_with(
            signal=signals.automatic_payment_soft_failed,
            sender=renewal_order,
            transaction=first_transaction,
        )
        self.signals.reset_mock()

        # Do another clock tick an hour later. This should pick up on the
        # failed attempt and renew the subscription.
        second_tick_time = first_tick_time + relativedelta(hours=1)
        with override_settings(LOOPER_CLOCK_MAX_AUTO_ATTEMPTS=max_collection_attempts):
            self._clock_tick(second_tick_time)

        # The subscription should be renewed now.
        subs.refresh_from_db()
        self.assertEqual('active', subs.status)
        self.assertAlmostEqualDateTime(
            second_tick_time + relativedelta(months=1), subs.next_payment
        )
        self.assertEqual(1, subs.intervals_elapsed)

        # Test the order
        latest_order = subs.latest_order()
        self.assertEqual('paid', latest_order.status)
        self.assertEqual(2, latest_order.collection_attempts)
        self.assertNotEqual(pre_renewal_order_pk, latest_order.pk)
        self.assertEqual(
            renewal_order.pk, latest_order.pk, 'After failing, the renewal order should be reused.'
        )
        self.assertAlmostEqualDateTime(first_tick_time + retry_after, renewal_order.retry_after)

        # Test the transaction
        new_trans = renewal_order.latest_transaction()
        self.assertEqual(2, renewal_order.transaction_set.count())
        self.assertEqual(
            {failed_trans_pk, new_trans.pk}, {t.pk for t in renewal_order.transaction_set.all()}
        )
        self.assertEqual('succeeded', new_trans.status)
        self.assertEqual(subs.price, new_trans.amount)

        entries_q = admin_log.entries_for(new_trans)
        self.assertEqual(1, len(entries_q))
        self.assertIn('success', entries_q.first().change_message)

        self.assertGreater(
            new_trans.pk, failed_trans_pk, 'A new transaction should have been made for the retry.'
        )

        # Test the calls on the gateway
        mock_transact_sale.assert_has_calls(
            [
                mock.call(
                    payment_token,
                    subs.price,
                    source='recurring_first',
                    authentication_id=None,
                    payment_intent_id=None,
                    payment_intent_metadata={'order_id': latest_order.pk},
                ),
                mock.call(
                    payment_token,
                    subs.price,
                    source='recurring_first',
                    authentication_id=None,
                    payment_intent_id=None,
                    payment_intent_metadata={'order_id': latest_order.pk},
                ),
            ]
        )

        # Test the sent signals
        self.signals.assert_called_once_with(
            signal=signals.automatic_payment_succesful, sender=renewal_order, transaction=new_trans
        )

    @mock.patch('looper.gateways.MockableGateway.transact_sale')
    def test_double_failure(self, mock_transact_sale):
        subs = self.create_active_subscription()
        old_next_payment = subs.next_payment

        # Both calls should fail.
        mock_transact_sale.side_effect = [
            exceptions.GatewayError('mock gateway error'),
            exceptions.GatewayError('mock gateway error'),
        ]

        max_collection_attempts = 2
        first_tick_time = self.start_time + subs.interval
        with override_settings(LOOPER_CLOCK_MAX_AUTO_ATTEMPTS=max_collection_attempts):
            self._clock_tick(first_tick_time)

        self.signals.assert_called_once_with(
            signal=signals.automatic_payment_soft_failed, sender=mock.ANY, transaction=mock.ANY
        )
        self.signals.reset_mock()

        first_failure_order_pk = subs.latest_order().pk
        payment_token = subs.payment_method.token

        # Do another clock tick an hour later. This should pick up on the
        # failed attempt and fail again.
        second_tick_time = first_tick_time + relativedelta(hours=1)
        with override_settings(LOOPER_CLOCK_MAX_AUTO_ATTEMPTS=max_collection_attempts):
            self._clock_tick(second_tick_time)

        # The subscription should not have been renewed.
        subs.refresh_from_db()
        self.assertEqual('on-hold', subs.status)
        self.assertEqual(1, subs.intervals_elapsed)
        self.assertAlmostEqualDateTime(old_next_payment, subs.next_payment)

        # Test the order
        new_order = subs.latest_order()
        self.assertEqual('failed', new_order.status)
        self.assertEqual(2, new_order.collection_attempts)
        self.assertEqual(first_failure_order_pk, new_order.pk)
        self.assertAlmostEqualDateTime(first_tick_time + retry_after, new_order.retry_after)

        # Test the transaction
        self.assertEqual(2, new_order.transaction_set.count())
        new_trans = new_order.latest_transaction()
        self.assertEqual('failed', new_trans.status)
        self.assertEqual(subs.price, new_trans.amount)
        entries_q = admin_log.entries_for(new_trans)
        self.assertEqual(1, len(entries_q))
        self.assertIn('failed', entries_q.first().change_message)

        # Test the calls on the gateway
        mock_transact_sale.assert_has_calls(
            [
                mock.call(
                    payment_token,
                    subs.price,
                    source='recurring_first',
                    authentication_id=None,
                    payment_intent_id=None,
                    payment_intent_metadata={'order_id': first_failure_order_pk},
                ),
                mock.call(
                    payment_token,
                    subs.price,
                    source='recurring_first',
                    authentication_id=None,
                    payment_intent_id=None,
                    payment_intent_metadata={'order_id': new_order.pk},
                ),
            ]
        )

        # Test the sent signals
        self.signals.assert_called_once_with(
            signal=signals.automatic_payment_failed, sender=new_order, transaction=new_trans
        )

    @mock.patch('looper.gateways.MockableGateway.transact_sale')
    def test_high_frequency_clock(self, mock_transact_sale):
        subs = self.create_active_subscription()
        pre_renewal_next_payment = subs.next_payment

        # Only one call should be made, because the clock is running faster
        # than the LOOPER_ORDER_RETRY_AFTER setting.
        mock_transact_sale.side_effect = [
            exceptions.GatewayError('mock gateway error'),
        ]

        max_collection_attempts = 2
        first_tick_time = self.start_time + subs.interval
        with override_settings(LOOPER_CLOCK_MAX_AUTO_ATTEMPTS=max_collection_attempts):
            self._clock_tick(first_tick_time)

        # The subscription should still be active after one little failure.
        subs.refresh_from_db()
        self.assertEqual('active', subs.status)
        self.assertAlmostEqualDateTime(pre_renewal_next_payment, subs.next_payment)

        # Test the order
        renewal_order = subs.latest_order()
        self.assertAlmostEqualDateTime(first_tick_time + retry_after, renewal_order.retry_after)

        payment_token = subs.payment_method.token

        # Do another clock tick a minute later. This should skip the retry
        # because it's faster than LOOPER_ORDER_RETRY_AFTER.
        second_tick_time = first_tick_time + relativedelta(minutes=1)
        with override_settings(LOOPER_CLOCK_MAX_AUTO_ATTEMPTS=max_collection_attempts):
            self._clock_tick(second_tick_time)

        # The order should not be touched
        renewal_order.refresh_from_db()
        self.assertEqual('soft-failed', renewal_order.status)
        self.assertEqual(1, renewal_order.collection_attempts)
        self.assertAlmostEqualDateTime(first_tick_time + retry_after, renewal_order.retry_after)

        # Test the calls on the gateway
        mock_transact_sale.assert_has_calls(
            [
                mock.call(
                    payment_token,
                    subs.price,
                    source='recurring_first',
                    authentication_id=None,
                    payment_intent_id=None,
                    payment_intent_metadata={'order_id': renewal_order.pk},
                ),
            ]
        )

        # Test the sent signals
        self.signals.assert_called_once_with(
            signal=signals.automatic_payment_soft_failed,
            sender=renewal_order,
            transaction=renewal_order.latest_transaction(),
        )

    @mock.patch('looper.gateways.MockableGateway.transact_sale')
    def test_renewal_no_payment_method_sends_signal(self, mock_transact_sale):
        subs = self.create_active_subscription()
        subs.payment_method = None
        subs.save(update_fields={'payment_method'})
        subs.latest_order().delete()

        first_tick_time = self.start_time + subs.interval
        with override_settings(LOOPER_CLOCK_MAX_AUTO_ATTEMPTS=2):
            self._clock_tick(first_tick_time)

        # The subscription should still be active after one little failure.
        subs.refresh_from_db()
        self.assertEqual('active', subs.status)
        self.assertEqual(1, subs.intervals_elapsed)
        self.assertEqual(1, subs.order_set.count())
        renewal_order = subs.latest_order()

        self.signals.assert_called_once_with(
            signal=signals.automatic_payment_soft_failed_no_payment_method,
            sender=renewal_order,
        )
        self.signals.reset_mock()

        # Do another clock tick an hour later. This should pick up on the
        # failed attempt and send the fail signal.
        second_tick_time = first_tick_time + relativedelta(hours=1)
        with override_settings(LOOPER_CLOCK_MAX_AUTO_ATTEMPTS=2):
            self._clock_tick(second_tick_time)

        self.signals.assert_called_once_with(
            signal=signals.automatic_payment_failed_no_payment_method,
            sender=renewal_order,
        )
        self.signals.reset_mock()

        # Subscription should have been put on hold now
        subs.refresh_from_db()
        self.assertEqual('on-hold', subs.status)
        self.assertAlmostEqualDateTime(first_tick_time, subs.next_payment)

        # Test the order
        latest_order = subs.latest_order()
        self.assertEqual('failed', latest_order.status)
        self.assertEqual(2, latest_order.collection_attempts)
        self.assertAlmostEqualDateTime(first_tick_time + retry_after, renewal_order.retry_after)

        entries_q = admin_log.entries_for(renewal_order)
        self.assertEqual(2, len(entries_q))
        self.assertEqual(
            entries_q[0].change_message,
            f'Unable to collect automatic renewal order {renewal_order.pk}'
            f' for subscription {renewal_order.subscription_id}, attempt 2 of 2: no payment method.'
            '\nDeactivating subscription and failing order: no payment method',
        )
        self.assertEqual(
            entries_q[1].change_message,
            f'Unable to collect automatic renewal order {renewal_order.pk}'
            f' for subscription {renewal_order.subscription_id}, attempt 1 of 2: no payment method.'
            "\nKeeping subscription status 'active' and retrying later: no payment method"
        )

        # No transactions were attempted
        mock_transact_sale.assert_not_called()


class PendingCancellationTestCase(AbstractClockTest):
    def test_pending_cancellation(self):
        subs = self.create_active_subscription()
        last_order_pk = subs.latest_order().pk
        next_payment = subs.next_payment

        # Cancel four days into the subscription.
        with mock.patch('django.utils.timezone.now') as mock_now:
            mock_now.return_value = self.start_time + relativedelta(days=4)
            subs.cancel_subscription()

        subs.refresh_from_db()
        self.assertEqual('pending-cancellation', subs.status)

        # Perform the clock tick one hour after the next payment time.
        tick_time = self.start_time + subs.interval
        self._clock_tick(tick_time)

        # The subscription should be cancelled now.
        subs.refresh_from_db()

        # Test the subscription
        self.assertEqual('cancelled', subs.status)
        self.assertAlmostEqualDateTime(next_payment, subs.next_payment)

        # Test the order
        last_order = subs.latest_order()
        self.assertEqual(last_order_pk, last_order.pk, 'Cancelling should not create an order')


class ManagedSubscriptionTest(AbstractClockTest):
    def _test_renew_managed(self, last_notification: Optional[datetime.datetime]):
        subs = self.create_active_subscription(collection_method='managed')
        subs.next_payment = self.start_time
        subs.last_notification = last_notification
        subs.save()

        self.assertIsNone(subs.latest_order())
        old_next_payment = subs.next_payment

        # Tick after the next_payment has passed.
        tick_time = self.start_time + relativedelta(minutes=10)
        self._clock_tick(tick_time)

        # The subscription should still be active and unchanged.
        subs.refresh_from_db()
        self.assertEqual('active', subs.status)
        self.assertAlmostEqualDateTime(old_next_payment, subs.next_payment)
        self.assertAlmostEqualDateTime(tick_time, subs.last_notification)

        # Test the order, shouldn't be created all of a sudden.
        self.assertIsNone(subs.latest_order())

        self.signals.assert_called_once_with(
            signal=signals.managed_subscription_notification, sender=subs
        )

    def test_renew_managed_subscription(self):
        self._test_renew_managed(self.start_time - relativedelta(minutes=1))

    def test_renew_managed_subscription_no_last_notif(self):
        self._test_renew_managed(None)

    def test_renew_managed_subscription_already_notified(self):
        subs = self.create_active_subscription()
        subs.collection_method = 'managed'

        # Next payment has passed, but notification has already happened:
        subs.next_payment = self.start_time
        subs.last_notification = subs.next_payment + relativedelta(seconds=1)
        subs.save()

        last_order_pk = subs.latest_order().pk
        old_next_payment = subs.next_payment
        old_last_notif = subs.last_notification

        # Tick after the next_payment has passed.
        tick_time = self.start_time + relativedelta(minutes=10)
        self._clock_tick(tick_time)

        # The subscription should still be active and unchanged.
        subs.refresh_from_db()
        self.assertEqual('active', subs.status)
        self.assertAlmostEqualDateTime(old_next_payment, subs.next_payment)
        self.assertAlmostEqualDateTime(old_last_notif, subs.last_notification)

        self.assertEqual(last_order_pk, subs.latest_order().pk)

        self.signals.assert_not_called()


@override_settings(LOOPER_SUBSCRIPTION_EXPIRE_AFTER=expire_after)
class TestClockExpiredSubscription(AbstractClockTest):
    def test_subscription_on_hold_not_long_enough(self):
        now = timezone.now()
        customer = create_customer_with_billing_address(country='NL', full_name='Jane Doe')
        self.subscription = SubscriptionFactory(
            customer=customer,
            status='on-hold',
            # payment date has passed, but not long enough ago
            next_payment=now - expire_after + datetime.timedelta(days=1),
        )

        Clock().tick()

        # Check that nothing has happened
        self.subscription.refresh_from_db()
        self.assertEqual('on-hold', self.subscription.status)
        self.assertFalse(self.subscription.is_cancelled)
        self.assertIsNone(self.subscription.cancelled_at)

        # No signals sent
        self.signals.assert_not_called()

    @responses.activate
    def test_on_hold_too_long_status_changed_to_expired(self):
        now = timezone.now()
        long_ago = now - expire_after - datetime.timedelta(days=1)
        customer = create_customer_with_billing_address(country='NL', full_name='Jane Doe')
        subscription = SubscriptionFactory(
            customer=customer,
            status='on-hold',
            # payment date has passed a long long time ago
            next_payment=long_ago,
        )

        Clock().tick()

        # Check that subscription is expired now
        subscription.refresh_from_db()
        self.assertEqual('expired', subscription.status)
        self.assertTrue(subscription.is_cancelled)
        self.assertAlmostEqual(now, subscription.cancelled_at, delta=datetime.timedelta(seconds=1))

        # "Subscription expired" signal sent
        self.signals.assert_called_once_with(
            signal=signals.subscription_expired,
            sender=subscription,
            old_status='on-hold',
        )

    @responses.activate
    def test_on_hold_too_long_no_next_payment_has_recent_order_skipped(self):
        now = timezone.now()
        long_ago = now - expire_after - datetime.timedelta(days=1)
        subscription = SubscriptionFactory(
            customer=create_customer_with_billing_address(),
            status='on-hold',
            # no next payment date set
            next_payment=None,
        )
        # Both created and updated are a long time ago
        models.Subscription.objects.filter(pk=subscription.pk).update(
            updated_at=long_ago,
        )
        # One order is old
        subscription.generate_order(save=True)
        models.Order.objects.filter(pk=subscription.latest_order().pk).update(
            created_at=long_ago
        )
        # However, there's another order that's too recent
        subscription.generate_order(save=True)

        Clock().tick()

        # Check that subscription did not expire
        subscription.refresh_from_db()
        self.assertEqual('on-hold', subscription.status)
        self.assertFalse(subscription.is_cancelled)
        self.assertIsNone(subscription.cancelled_at)

        # No signals sent
        self.signals.assert_not_called()

    @responses.activate
    def test_on_hold_too_long_no_next_payment_updated_recently_skipped(self):
        now = timezone.now()
        long_ago = now - expire_after - datetime.timedelta(days=1)
        subscription = SubscriptionFactory(
            customer=create_customer_with_billing_address(),
            status='on-hold',
            # no next payment date set
            next_payment=None,
        )
        # Created long time ago, but updated recently
        models.Subscription.objects.filter(pk=subscription.pk).update(
            created_at=long_ago,
        )

        Clock().tick()

        # Check that subscription did not expire
        subscription.refresh_from_db()
        self.assertEqual('on-hold', subscription.status)
        self.assertFalse(subscription.is_cancelled)
        self.assertIsNone(subscription.cancelled_at)

        # No signals sent
        self.signals.assert_not_called()

    @responses.activate
    def test_on_hold_too_long_no_next_payment_or_orders_status_changed_to_expired(self):
        now = timezone.now()
        long_ago = now - expire_after - datetime.timedelta(days=1)
        subscription = SubscriptionFactory(
            customer=create_customer_with_billing_address(),
            status='on-hold',
            # no next payment date set
            next_payment=None,
        )
        models.Subscription.objects.filter(pk=subscription.pk).update(
            updated_at=long_ago,
        )
        # The only order is too long ago
        subscription.generate_order(save=True)
        models.Order.objects.filter(pk=subscription.latest_order().pk).update(created_at=long_ago)

        Clock().tick()

        # Check that subscription is expired now
        subscription.refresh_from_db()
        self.assertEqual('expired', subscription.status)
        self.assertTrue(subscription.is_cancelled)
        self.assertAlmostEqual(now, subscription.cancelled_at, delta=datetime.timedelta(seconds=1))

        # "Subscription expired" signal sent
        self.signals.assert_called_once_with(
            signal=signals.subscription_expired,
            sender=subscription,
            old_status='on-hold',
        )
