import datetime
from unittest import mock
import responses
# from responses import _recorder

from django.test import TestCase
import django.utils.timezone

from ..clock import Clock
from .. import models, money, stripe_utils
from . import ResponsesMixin

from looper.tests.factories import (
    PaymentMethodFactory,
    SubscriptionFactory,
    create_customer_with_billing_address,
)


@mock.patch('looper.gateways.stripe.webhook.WebhookSignature.verify_header', return_value=True)
class ClockForStripe(ResponsesMixin, TestCase):
    fixtures = ['gateways', 'devfund', 'systemuser']
    responses_files = ['clock_stripe.yaml']

    # @_recorder.record(file_path=f'{ResponsesMixin.responses_file_path}/clock_stripe.yaml')
    @responses.activate
    def test_webhook_does_nothing(self, _):
        customer = create_customer_with_billing_address(
            country='DE',
            email='test@example.com',
            full_name='A B',
        )
        gateway = models.Gateway.objects.get(name='stripe')
        payment_method = PaymentMethodFactory(
            customer=customer,
            gateway_id=gateway.pk,
            token='pm_card_visa',
        )
        subscription = SubscriptionFactory(
            customer=customer,
            payment_method=payment_method,
            price=money.Money('EUR', 100),
            status='active',
        )
        subscription.next_payment = django.utils.timezone.now() + datetime.timedelta(days=-1)
        subscription.save()
        Clock().tick()
        order = subscription.latest_order()
        # tweak the response because this second test expects a different order_id in metadata
        for _ in responses.registered():
            if 'expand%5B0%5D=payment_intent' in _.url or '/payment_intents/' in _.url:
                assert '\"order_id\": \"1' in _.body
                _.body = _.body.replace('\"order_id\": \"1', f'\"order_id\": \"{order.pk}')
        transaction = order.latest_transaction()
        self.assertEqual(order.status, 'paid')
        self.assertEqual(transaction.paid, True)
        # calling webhook handler directly, since we don't have an url set up in looper
        webhook_payload = {
            "data": {
                "object": {
                  "id": order.pi_id,
                  "object": "payment_intent",
                  "latest_charge": transaction.transaction_id,
                  "payment_method": transaction.payment_method.token,
                  "status": "succeeded",
                }
            },
            "type": "payment_intent.succeeded"
        }

        stripe_utils.upsert_order_from_payment_intent_and_product(webhook_payload)
        # no new objects created
        self.assertEqual(subscription.latest_order(), order)
        self.assertEqual(order.latest_transaction(), transaction)
