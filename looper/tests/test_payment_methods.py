from typing import Type, Optional
from unittest import mock

from django.urls import reverse

from ..middleware import PREFERRED_CURRENCY_SESSION_KEY
from .. import forms_braintree, gateways
from . import AbstractLooperTestCase
from looper.tests.factories import UserFactory, CustomerFactory
from looper import admin_log

address_data = {
    'email': 'erik@example.com',
    'full_name': 'Erik von Namenstein',
    'company': 'Nöming Thingéys',
    'street_address': 'Scotland Pl',
    'locality': 'Amsterdongel',
    'postal_code': '1025 ET',
    'region': 'Worbelmoster',
    'country': 'NL',
}


class PaymentMethodChangeBraintreeMockTest(AbstractLooperTestCase):
    fixtures = AbstractLooperTestCase.fixtures + ['mock-gateway']
    gateway_name = 'mock'

    def setUp(self):
        super().setUp()
        self.client.force_login(self.user)

    def _happy_change(
        self,
        subs,
        to_gateway: Type[gateways.AbstractPaymentGateway] = gateways.MockableGateway,
        expect_new_payment_method=True,
    ):
        orig_pm = subs.payment_method
        paymeth_count_before = self.user.customer.paymentmethod_set.count()

        url = reverse('looper-braintree:payment_method_change', kwargs={'subscription_id': subs.id})

        with mock.patch('looper.gateways.MockableGateway.generate_client_token') as mock_gct:
            mock_gct.return_value = 'mock-client-auth-token'
            resp = self.client.get(url)
        self.assertEqual(200, resp.status_code, resp.content.decode())

        form = forms_braintree.ChangePaymentMethodForm(
            {
                **address_data,
                'payment_method_nonce': 'new-pm-nonce',
                'gateway': to_gateway.gateway_name,
                'next_url_after_done': '/je-moeder',
            }
        )
        form.full_clean()
        gw_name = f'{to_gateway.__module__}.{to_gateway.__name__}'
        with mock.patch(f'{gw_name}.customer_create') as mock_cc, mock.patch(
            f'{gw_name}.payment_method_create'
        ) as mock_pmc:
            mock_cc.return_value = 'mock-customer-id'
            mock_paymeth = mock.Mock(spec=gateways.PaymentMethodInfo)
            mock_paymeth.token = 'new-mock-payment-token'
            mock_paymeth.recognisable_name.return_value = 'new-mock-recognisable-name'
            mock_paymeth.type_for_database.return_value = 'pa'
            mock_paymeth.authentication = None
            mock_pmc.return_value = mock_paymeth

            resp = self.client.post(url, data=form.cleaned_data)

        self.assertEqual(302, resp.status_code, resp.content.decode())
        self.assertEqual('/je-moeder', resp['Location'])

        # A new payment method could have been added.
        self.assertEqual(
            paymeth_count_before + expect_new_payment_method,
            self.user.customer.paymentmethod_set.count()
        )

        new_pm = self.user.customer.paymentmethod_set.exclude(
            pk=orig_pm and orig_pm.pk or None
        ).first()
        return new_pm, orig_pm

    def test_happy_change_subs_order_paid(self):
        subs = self.create_active_subscription()
        order = subs.latest_order()
        self.assertEqual('paid', order.status)

        new_pm, orig_pm = self._happy_change(subs)
        self.assertIsNotNone(orig_pm)

        subs.refresh_from_db()
        order.refresh_from_db()
        self.assertEqual(orig_pm.pk, subs.latest_order().payment_method.pk)
        self.assertEqual(new_pm.pk, subs.payment_method.pk)

    def test_happy_change_subs_and_order(self):
        subs = self.create_on_hold_subscription()
        order = subs.latest_order()

        self.assertEqual('created', order.status)
        new_pm, orig_pm = self._happy_change(subs)
        self.assertIsNotNone(orig_pm)

        subs.refresh_from_db()
        order.refresh_from_db()
        self.assertEqual(new_pm.pk, order.payment_method.pk)
        self.assertEqual(new_pm.pk, subs.payment_method.pk)
        self.assertEqual('created', order.status)

        entries_q = admin_log.entries_for(subs)
        self.assertEqual(
            entries_q[0].change_message,
            'Switching payment method from Test payment method to new-mock-recognisable-name'
            f' for <a href="/admin/looper/order/{order.pk}/change/">order #{order.pk}</a>',
        )
        entries_q = admin_log.entries_for(order)
        self.assertEqual(
            entries_q[0].change_message,
            'Switching payment method from Test payment method to new-mock-recognisable-name'
        )

    def test_mock_to_bank_to_mock(self):
        subs = self.create_on_hold_subscription()
        order = subs.latest_order()

        self.assertEqual('created', order.status)
        new_pm, orig_pm = self._happy_change(subs, gateways.BankGateway)
        self.assertIsNotNone(orig_pm)

        subs.refresh_from_db()
        order.refresh_from_db()
        self.assertEqual(new_pm.pk, order.payment_method.pk)
        self.assertEqual(new_pm.pk, subs.payment_method.pk)
        self.assertEqual('created', order.status)

        # Should have switched to manual, because automatic isn't supported for banks.
        self.assertEqual('manual', subs.collection_method)
        self.assertEqual('manual', order.collection_method)

        # Moving back should also move to automatic
        self._happy_change(subs, gateways.MockableGateway)
        subs.refresh_from_db()
        order.refresh_from_db()
        self.assertEqual('automatic', subs.collection_method)
        self.assertEqual('automatic', order.collection_method)

    def test_mock_to_bank_to_bank(self):
        subs = self.create_on_hold_subscription()
        order = subs.latest_order()

        self.assertEqual('created', order.status)
        new_pm1, orig_pm1 = self._happy_change(subs, gateways.BankGateway)
        self.assertIsNotNone(orig_pm1)
        self._happy_change(subs, gateways.MockableGateway)
        new_pm2, orig_pm2 = self._happy_change(
            subs, gateways.BankGateway, expect_new_payment_method=False
        )
        self.assertIsNotNone(orig_pm2)

        self.assertEqual(
            new_pm1, new_pm2, 'Switching from bank to bank should keep the payment method'
        )

    def test_different_currency(self):
        self.client.get('/')  # Get any URL to start a session.
        self.assertEqual('USD', self.client.session[PREFERRED_CURRENCY_SESSION_KEY])

        subs = self.create_active_subscription()
        self.assertEqual('EUR', subs.currency)

        url = reverse('looper-braintree:payment_method_change', kwargs={'subscription_id': subs.id})

        passed_currency = ''

        def generate_client_token(
            self, for_currency: str, gateway_customer_id: Optional[str] = None
        ) -> str:
            nonlocal passed_currency
            passed_currency = for_currency
            return 'mock-client-auth-token'

        with mock.patch(
            'looper.gateways.MockableGateway.generate_client_token', new=generate_client_token
        ):
            resp = self.client.get(url)
        self.assertEqual(200, resp.status_code, resp.content.decode())
        self.assertEqual('EUR', passed_currency, 'Expected the subscription currency to be used')

    def test_none_to_bank_to_mock(self):
        subs = self.create_on_hold_subscription()
        # Create a subscription without a valid payment method.
        subs.payment_method = None
        subs.save(update_fields={'payment_method'})
        order = subs.latest_order()
        order.payment_method = None
        order.save(update_fields={'payment_method'})
        self.assertEqual('created', order.status)

        # Changing payment method must work even if "original" payment method is not set
        new_pm, orig_pm = self._happy_change(subs, gateways.BankGateway)
        self.assertIsNone(orig_pm)

        entries_q = admin_log.entries_for(subs)
        self.assertEqual(
            entries_q[0].change_message,
            'Switching collection method from automatic to manual because'
            ' the new payment gateway only supports {&#x27;manual&#x27;}'
            f' for <a href="/admin/looper/order/{order.pk}/change/">order #{order.pk}</a>',
        )
        self.assertEqual(
            entries_q[1].change_message,
            'Switching payment method from none to new-mock-recognisable-name'
            f' for <a href="/admin/looper/order/{order.pk}/change/">order #{order.pk}</a>',
        )
        entries_q = admin_log.entries_for(order)
        self.assertEqual(
            entries_q[0].change_message,
            'Switching collection method from automatic to manual because'
            " the new payment gateway only supports {'manual'}",
        )
        self.assertEqual(
            entries_q[1].change_message,
            'Switching payment method from none to new-mock-recognisable-name'
        )


class PaymentMethodChangeBraintreeTest(AbstractLooperTestCase):
    """Test changing a payment method using Braintree's fake nonce.

    **N.B.**: This test actually calls Braintree API.
    See documentation for more details:
    https://developers.braintreepayments.com/reference/general/testing/python
    """
    fixtures = AbstractLooperTestCase.fixtures + ['devfund']
    gateway_name = 'braintree'

    def setUp(self):
        super().setUp()
        self.client.force_login(self.user)

    def test_change_successful_sca_stored_for_later_use(self):
        to_gateway = gateways.BraintreeGateway
        subs = self.create_on_hold_subscription()
        order = subs.latest_order()
        self.assertEqual('created', order.status)
        orig_pm = subs.payment_method
        self.assertIsNotNone(orig_pm)
        paymeth_count_before = self.user.customer.paymentmethod_set.count()

        url = reverse('looper-braintree:payment_method_change', kwargs={'subscription_id': subs.id})

        resp = self.client.get(url)
        self.assertEqual(200, resp.status_code, resp.content.decode())

        form = forms_braintree.ChangePaymentMethodForm(
            {
                **address_data,
                # Use a special fake enriched nonce that should have a SCA attached to it
                'payment_method_nonce': 'fake-three-d-secure-visa-full-authentication-nonce',
                'gateway': to_gateway.gateway_name,
                'next_url_after_done': '/',
            }
        )
        form.full_clean()

        resp = self.client.post(url, data=form.cleaned_data)
        self.assertEqual(302, resp.status_code, resp.content.decode())

        # A new payment method was added
        self.assertEqual(
            paymeth_count_before + 1, self.user.customer.paymentmethod_set.count()
        )

        new_pm = self.user.customer.paymentmethod_set.exclude(pk=orig_pm.pk).first()

        subs.refresh_from_db()
        order.refresh_from_db()
        self.assertEqual(new_pm.pk, order.payment_method.pk)
        self.assertEqual(new_pm.pk, subs.payment_method.pk)
        self.assertEqual('created', order.status)

        # SCA info was stored for use in a transaction that will happen later
        auth = order.payment_method.paymentmethodauthentication_set.first()
        self.assertIsNotNone(auth)
        self.assertEqual(18, len(auth.authentication_id))
        self.assertEqual('tds', auth.authentication_type)
        self.assertEqual('1.0.2', auth.version)
        self.assertEqual('authenticate_successful', auth.status)
        self.assertIsNone(auth.consumed_at)
        self.assertEqual(
            '{"acs_transaction_id": null, "liability_shift_possible": true,'
            ' "liability_shifted": true, "pares_status": "Y"}',
            auth.details,
        )

    def test_change_failed_sca_stored(self):
        # TODO(anna): test with other fake nonces
        pass


class PaymentMethodDeleteTest(AbstractLooperTestCase):
    gateway_name = 'braintree'

    def test_delete_payment_method_anonymously_redirects_to_login(self):
        subs = self.create_active_subscription()
        pm = subs.payment_method
        self.assertIsNotNone(pm)

        url = reverse('looper:payment_method_delete', kwargs={'pk': pm.pk})
        resp = self.client.delete(url)

        self.assertEqual(302, resp.status_code)
        self.assertEqual(f'/accounts/login/?next={url}', resp['Location'])
        pm.refresh_from_db()
        self.assertFalse(pm.is_deleted)

    def test_delete_payment_method_someone_elses_denied(self):
        subs = self.create_active_subscription()
        pm = subs.payment_method
        self.assertIsNotNone(pm)

        another_user = UserFactory()
        self.client.force_login(another_user)
        url = reverse('looper:payment_method_delete', kwargs={'pk': pm.pk})
        resp = self.client.delete(url)

        self.assertEqual(403, resp.status_code)
        pm.refresh_from_db()
        self.assertFalse(pm.is_deleted)

    def test_delete_payment_method(self):
        subs = self.create_active_subscription()
        pm = subs.payment_method
        self.assertIsNotNone(pm)
        self.assertIsNotNone(subs.customer.user)

        self.client.force_login(subs.customer.user)
        url = reverse('looper:payment_method_delete', kwargs={'pk': pm.pk})
        resp = self.client.delete(url)

        self.assertEqual(302, resp.status_code)
        self.assertEqual(reverse('looper:payment_methods'), resp['Location'])
        pm.refresh_from_db()
        self.assertTrue(pm.is_deleted)

        entries_q = admin_log.entries_for(pm)
        self.assertEqual(2, entries_q.count())
        self.assertEqual(
            'Soft-deleting payment method and deleting it at the payment provider',
            entries_q[0].change_message,
        )
        self.assertEqual(
            f'User pk=14 is deleting payment method pk={pm.pk}',
            entries_q[1].change_message,
        )
