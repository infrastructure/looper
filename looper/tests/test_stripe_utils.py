from unittest import mock

from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse
# from responses import _recorder

from looper.models import (
    Customer,
    Order,
    PlanVariation,
    Transaction,
)
import looper.stripe_utils
from looper.stripe_utils import upsert_order_from_payment_intent_and_product
from . import ResponsesMixin

User = get_user_model()
event_payload_cc = {
    # Most of the event payload is not used by our webhook handler:
    # it retrieves PaymentIntent from the API instead because event
    # doesn't carry enough meaningful data.
    'type': 'payment_intent.succeeded',
    'data': {
        'object': {
            'id': 'pi_3O82BkGxwuSUrRMt1am7Cvmv',
            'object': 'payment_intent',
            'latest_charge': 'ch_3O82BkGxwuSUrRMt1QbB3BoM',
            'payment_method': 'pm_1O82BjGxwuSUrRMtjF2TKhzE',
            'payment_method_types': ['card'],
        }
    }
}


@mock.patch('looper.gateways.stripe.webhook.WebhookSignature.verify_header', return_value=True)
class UpsertOrderFromPaymentIntentAndProduct(ResponsesMixin, TestCase):
    fixtures = ['gateways', 'devfund', 'systemuser']
    responses_files = [
        'payment_intent__card.yaml',
        'payment_intent__ideal.yaml',
        'payment_intent__eps.yaml',
        'payment_intent__giropay.yaml',
        'payment_intent__sofort.yaml',
        'payment_intent__bancontact.yaml',
        'payment_intent__alipay.yaml',
        'payment_intent__sepa_debit.yaml',
        'payment_intent__link.yaml',
        'payment_intent__klarna.yaml',
        'payment_intent__paypal.yaml',
    ]

    def _test_expected_transaction_properties(self, transaction, payment_intent_id):
        self.assertEqual(transaction.status, 'succeeded')
        self.assertEqual(transaction.paid, True)
        self.assertEqual(transaction.amount.currency, 'EUR')
        self.assertEqual(transaction.get_source_display(), 'Unscheduled')
        order = transaction.order
        self.assertIsNotNone(order.pi_id)
        self.assertEqual(order.pi_id, payment_intent_id)
        self.assertEqual(order.status, 'paid')
        self.assertEqual(order.collection_method, 'manual')
        self.assertEqual(order.name, 'Blender Development Fund Donation')
        self.assertTrue(hasattr(transaction.customer, 'token'))

    # Uncomment these if a new response YAML needs to be recorded.
    # @_recorder.record(file_path=f'{ResponsesMixin.responses_file_path}/payment_intent__card.yaml')
    def test_payment_intent_succeeded__credit_card(self, _):
        total_orders = Order.objects.count()
        total_transactions = Transaction.objects.count()
        total_customers = Customer.objects.count()
        payload = event_payload_cc.copy()

        old_status, order = upsert_order_from_payment_intent_and_product(payload, 2)

        self.assertIsNone(old_status)
        self.assertEqual(order.status, 'paid')

        self.assertEqual(total_orders + 1, Order.objects.count())
        self.assertEqual(total_transactions + 1, Transaction.objects.count())
        self.assertEqual(total_customers + 1, Customer.objects.count())

        charge_id = payload['data']['object']['latest_charge']
        payment_intent_id = payload['data']['object']['id']
        transaction = Transaction.objects.get(transaction_id=charge_id)
        self._test_expected_transaction_properties(transaction, payment_intent_id)
        self.assertEqual(transaction.amount.cents, 200)
        self.assertEqual(transaction.customer.billing_address.email, 'test@example.com')
        self.assertEqual(transaction.customer.billing_address.full_name, 'Jane Doe')
        payment_method = transaction.payment_method
        self.assertEqual(payment_method.method_type, 'cc')
        self.assertEqual(payment_method.recognisable_name, 'visa credit card ending in 4242')

    # @_recorder.record(file_path=f'{ResponsesMixin.responses_file_path}/payment_intent__ideal.yaml')
    def test_payment_intent_succeeded__ideal(self, _):
        payload = {
            'type': 'payment_intent.succeeded',
            'data': {
                'object': {
                    'id': 'pi_3O9QDEGxwuSUrRMt0UsPcCBQ',
                    'object': 'payment_intent',
                    'latest_charge': 'py_3O9QDEGxwuSUrRMt0RSh3jXQ',
                    'payment_method': 'pm_1O9QDEGxwuSUrRMtsVZHf6yr',
                    'payment_method_types': ['ideal'],
                }
            }
        }

        old_status, order = upsert_order_from_payment_intent_and_product(payload, 2)

        self.assertIsNone(old_status)
        self.assertEqual(order.status, 'paid')

        charge_id = payload['data']['object']['latest_charge']
        payment_intent_id = payload['data']['object']['id']
        transaction = Transaction.objects.get(transaction_id=charge_id)
        self._test_expected_transaction_properties(transaction, payment_intent_id)
        self.assertEqual(transaction.amount.cents, 400)
        self.assertEqual(order.email, 'ideal@example.com')
        self.assertEqual(order.billing_address, 'Jane Doe')
        self.assertTrue(hasattr(transaction.customer, 'token'))
        self.assertEqual(transaction.customer.billing_address.email, 'ideal@example.com')
        self.assertEqual(transaction.customer.billing_address.full_name, 'Jane Doe')
        payment_method = transaction.payment_method
        self.assertEqual(payment_method.method_type, 'ideal')
        self.assertEqual(
            payment_method.recognisable_name,
            'RABONL2U**********5264 via iDEAL',
        )

    # @_recorder.record(file_path=f'{ResponsesMixin.responses_file_path}/payment_intent__eps.yaml')
    def test_payment_intent_succeeded__eps(self, _):
        payload = {
            'type': 'payment_intent.succeeded',
            'data': {
                'object': {
                    'id': 'pi_3O9RCOGxwuSUrRMt0Ktjl8cw',
                    'object': 'payment_intent',
                    'latest_charge': 'py_3O9RCOGxwuSUrRMt0ATSDR99',
                    'payment_method': 'pm_1O9RF5GxwuSUrRMtSSVMJLOM',
                    'payment_method_configuration_details': None,
                    'payment_method_types': ['eps'],
                }
            }
        }

        old_status, order = upsert_order_from_payment_intent_and_product(payload, 2)

        self.assertIsNone(old_status)
        self.assertEqual(order.status, 'paid')

        charge_id = payload['data']['object']['latest_charge']
        payment_intent_id = payload['data']['object']['id']
        transaction = Transaction.objects.get(transaction_id=charge_id)
        self._test_expected_transaction_properties(transaction, payment_intent_id)
        self.assertEqual(transaction.amount.cents, 400)
        order = transaction.order
        self.assertEqual(order.email, 'test@example.com')
        self.assertEqual(order.billing_address, 'Jane Doe')
        self.assertEqual(transaction.customer.billing_address.email, 'test@example.com')
        self.assertEqual(transaction.customer.billing_address.full_name, 'Jane Doe')
        payment_method = transaction.payment_method
        self.assertEqual(payment_method.method_type, 'eps')
        self.assertEqual(payment_method.recognisable_name, 'EPS')

    # @_recorder.record(file_path=f'{ResponsesMixin.responses_file_path}/payment_intent__giropay.yaml')
    def test_payment_intent_succeeded__giropay(self, _):
        payload = {
            'type': 'payment_intent.succeeded',
            'data': {
                'object': {
                    'id': 'pi_3O9RvWGxwuSUrRMt0RHK1LWN',
                    'latest_charge': 'py_3O9RvWGxwuSUrRMt0QdwgF2t',
                    'object': 'payment_intent',
                    'payment_method': 'pm_1O9RvVGxwuSUrRMtiTzcXMQs',
                    'payment_method_types': ['giropay'],
                },
            }
        }

        old_status, order = upsert_order_from_payment_intent_and_product(payload, 2)

        self.assertIsNone(old_status)
        self.assertEqual(order.status, 'paid')

        charge_id = payload['data']['object']['latest_charge']
        payment_intent_id = payload['data']['object']['id']
        transaction = Transaction.objects.get(transaction_id=charge_id)
        self._test_expected_transaction_properties(transaction, payment_intent_id)
        self.assertEqual(transaction.amount.cents, 400)
        order = transaction.order
        self.assertEqual(order.email, 'test@example.com')
        self.assertEqual(order.billing_address, 'Jane Doe')
        self.assertEqual(transaction.customer.billing_address.email, 'test@example.com')
        self.assertEqual(transaction.customer.billing_address.full_name, 'Jane Doe')
        payment_method = transaction.payment_method
        self.assertEqual(payment_method.method_type, 'giropay')
        self.assertEqual(
            payment_method.recognisable_name,
            'DEUTDE2H via GIROPAY',
        )

    # @_recorder.record(file_path=f'{ResponsesMixin.responses_file_path}/payment_intent__sofort.yaml')
    def test_payment_intent_succeeded__sofort(self, _):
        payload = {
            'type': 'payment_intent.succeeded',
            'data': {
                'object': {
                    'id': 'pi_3O9RzTGxwuSUrRMt1ZyGPPab',
                    'object': 'payment_intent',
                    'latest_charge': 'py_3O9RzTGxwuSUrRMt1OBd7JRH',
                    'payment_method': 'pm_1O9RzTGxwuSUrRMtbJVjegGL',
                    'payment_method_types': ['sofort'],
                },
            }
        }

        old_status, order = upsert_order_from_payment_intent_and_product(payload, 2)

        self.assertIsNone(old_status)
        self.assertEqual(order.status, 'paid')

        charge_id = payload['data']['object']['latest_charge']
        payment_intent_id = payload['data']['object']['id']
        transaction = Transaction.objects.get(transaction_id=charge_id)
        self._test_expected_transaction_properties(transaction, payment_intent_id)
        self.assertEqual(transaction.amount.cents, 400)
        order = transaction.order
        self.assertEqual(order.email, 'test@example.com')
        self.assertEqual(order.billing_address, 'Jane Doe\nNetherlands')
        self.assertEqual(transaction.customer.billing_address.email, 'test@example.com')
        self.assertEqual(transaction.customer.billing_address.full_name, 'Jane Doe')
        payment_method = transaction.payment_method
        self.assertEqual(payment_method.method_type, 'sofort')
        self.assertEqual(
            payment_method.recognisable_name,
            'DEUTDE2H**********3000 via SOFORT',
        )

    # @_recorder.record(file_path=f'{ResponsesMixin.responses_file_path}/payment_intent__bancontact.yaml')
    def test_payment_intent_succeeded__bancontact(self, _):
        payload = {
            'type': 'payment_intent.succeeded',
            'data': {
                'object': {
                    'id': 'pi_3O9SHFGxwuSUrRMt1UzN03YZ',
                    'latest_charge': 'py_3O9SHFGxwuSUrRMt1CH4oeA1',
                    'object': 'payment_intent',
                    'payment_method': 'pm_1O9SHFGxwuSUrRMtNhAERykI',
                    'payment_method_types': ['bancontact'],
                }
            }
        }

        old_status, order = upsert_order_from_payment_intent_and_product(payload, 2)

        self.assertIsNone(old_status)
        self.assertEqual(order.status, 'paid')

        charge_id = payload['data']['object']['latest_charge']
        payment_intent_id = payload['data']['object']['id']
        transaction = Transaction.objects.get(transaction_id=charge_id)
        self._test_expected_transaction_properties(transaction, payment_intent_id)
        order = transaction.order
        self.assertEqual(order.email, 'test@example.com')
        self.assertEqual(order.billing_address, 'Jane Doe')
        self.assertEqual(transaction.customer.billing_address.email, 'test@example.com')
        self.assertEqual(transaction.customer.billing_address.full_name, 'Jane Doe')
        payment_method = transaction.payment_method
        self.assertEqual(payment_method.method_type, 'bancontact')
        self.assertEqual(
            payment_method.recognisable_name,
            'VAPEBE22**********7061 via BANCONTACT',
        )

    # @_recorder.record(file_path=f'{ResponsesMixin.responses_file_path}/payment_intent__alipay.yaml')
    def test_payment_intent_succeeded__alipay(self, _):
        payload = {
            'type': 'payment_intent.succeeded',
            'data': {
                'object': {
                    'id': 'pi_3O9WHiGxwuSUrRMt0N5TSTYC',
                    'latest_charge': 'py_3O9WHiGxwuSUrRMt0cq7qInF',
                    'object': 'payment_intent',
                    'payment_method': 'pm_1O9WHiGxwuSUrRMtxtQ80JuL',
                    'payment_method_types': ['alipay'],
                }
            },
        }

        old_status, order = upsert_order_from_payment_intent_and_product(payload, 2)

        self.assertIsNone(old_status)
        self.assertEqual(order.status, 'paid')

        charge_id = payload['data']['object']['latest_charge']
        payment_intent_id = payload['data']['object']['id']
        transaction = Transaction.objects.get(transaction_id=charge_id)
        self._test_expected_transaction_properties(transaction, payment_intent_id)
        order = transaction.order
        self.assertEqual(order.email, 'test@example.com')
        self.assertEqual(order.billing_address, 'test@example.com')
        self.assertEqual(transaction.customer.billing_address.email, 'test@example.com')
        self.assertEqual(transaction.customer.billing_address.full_name, 'test@example.com')
        payment_method = transaction.payment_method
        self.assertEqual(payment_method.method_type, 'alipay')
        self.assertEqual(payment_method.recognisable_name, 'Alipay')

    # @_recorder.record(file_path=f'{ResponsesMixin.responses_file_path}/payment_intent__sepa_debit.yaml')
    def test_payment_intent_succeeded__sepa_debit(self, _):
        payload = {
            'type': 'payment_intent.succeeded',
            'data': {
                'object': {
                    'id': 'pi_3O9mOtGxwuSUrRMt0qksOo2p',
                    'latest_charge': 'py_3O9mOtGxwuSUrRMt0anuLpka',
                    'object': 'payment_intent',
                    'payment_method': 'pm_1O9mOtGxwuSUrRMtaPBBMsTb',
                    'payment_method_types': ['sepa_debit'],
                }
            },
        }

        old_status, order = upsert_order_from_payment_intent_and_product(payload, 2)

        self.assertIsNone(old_status)
        self.assertEqual(order.status, 'paid')

        charge_id = payload['data']['object']['latest_charge']
        payment_intent_id = payload['data']['object']['id']
        transaction = Transaction.objects.get(transaction_id=charge_id)
        self._test_expected_transaction_properties(transaction, payment_intent_id)
        self.assertEqual(transaction.amount.cents, 400)
        order = transaction.order
        self.assertEqual(order.email, 'test@example.com')
        self.assertEqual(
            order.billing_address,
            'Jane Doe\nTeststreet 12\nTestville\n1234AB\nNetherlands',
        )
        self.assertEqual(transaction.customer.billing_address.email, 'test@example.com')
        self.assertEqual(transaction.customer.billing_address.full_name, 'Jane Doe')
        payment_method = transaction.payment_method
        self.assertEqual(payment_method.method_type, 'sepa_debit')
        self.assertEqual(payment_method.recognisable_name, '**********3000 via SEPA Direct Debit')

    # @_recorder.record(file_path=f'{ResponsesMixin.responses_file_path}/payment_intent__link.yaml')
    def test_payment_intent_succeeded__link(self, _):
        payload = {
            'type': 'payment_intent.succeeded',
            'data': {
                'object': {
                    'id': 'pi_3OAXUbGxwuSUrRMt0Ao4zzpC',
                    'latest_charge': 'py_3OAXUbGxwuSUrRMt0skm9sxF',
                    'object': 'payment_intent',
                    'payment_method': 'pm_1OAXUbGxwuSUrRMtTi9elCeu',
                    'payment_method_types': ['link'],
                }
            },
        }

        old_status, order = upsert_order_from_payment_intent_and_product(payload, 2)

        self.assertIsNone(old_status)
        self.assertEqual(order.status, 'paid')

        charge_id = payload['data']['object']['latest_charge']
        payment_intent_id = payload['data']['object']['id']
        transaction = Transaction.objects.get(transaction_id=charge_id)
        self._test_expected_transaction_properties(transaction, payment_intent_id)
        self.assertEqual(transaction.amount.cents, 400)
        order = transaction.order
        self.assertEqual(order.email, 'test@example.com')
        self.assertEqual(order.billing_address, 'Jane Doe\nNetherlands')
        self.assertEqual(transaction.customer.billing_address.email, 'test@example.com')
        self.assertEqual(transaction.customer.billing_address.full_name, 'Jane Doe')
        payment_method = transaction.payment_method
        self.assertEqual(payment_method.method_type, 'link')
        self.assertEqual(payment_method.recognisable_name, 'Link')

    # @_recorder.record(file_path=f'{ResponsesMixin.responses_file_path}/payment_intent__klarna.yaml')
    def test_payment_intent_succeeded__klarna(self, _):
        payload = {
            'type': 'payment_intent.succeeded',
            'data': {
                'object': {
                    'id': 'pi_3OAXHKGxwuSUrRMt0kxdglWC',
                    'latest_charge': 'py_3OAXHKGxwuSUrRMt0YLlGGHB',
                    'object': 'payment_intent',
                    'payment_method': 'pm_1OAXNVGxwuSUrRMteZ1HmomF',
                    'payment_method_types': ['klarna'],
                }
            },
        }

        old_status, order = upsert_order_from_payment_intent_and_product(payload, 2)

        self.assertIsNone(old_status)
        self.assertEqual(order.status, 'paid')

        charge_id = payload['data']['object']['latest_charge']
        payment_intent_id = payload['data']['object']['id']
        transaction = Transaction.objects.get(transaction_id=charge_id)
        self._test_expected_transaction_properties(transaction, payment_intent_id)
        self.assertEqual(transaction.amount.cents, 400)
        order = transaction.order
        self.assertEqual(order.email, 'test@example.com')
        self.assertEqual(order.billing_address, 'Jane Doe\nNetherlands')
        self.assertEqual(transaction.customer.billing_address.email, 'test@example.com')
        self.assertEqual(transaction.customer.billing_address.full_name, 'Jane Doe')
        payment_method = transaction.payment_method
        self.assertEqual(payment_method.method_type, 'klarna')
        self.assertEqual(payment_method.recognisable_name, 'Klarna')

    # @_recorder.record(file_path=f'{ResponsesMixin.responses_file_path}/payment_intent__paypal.yaml')
    def test_payment_intent_succeeded__paypal(self, _):
        payload = {
            'type': 'payment_intent.succeeded',
            'data': {
                'object': {
                    'id': 'pi_3O9WfTGxwuSUrRMt1DO8XtnR',
                    'object': 'payment_intent',
                    'latest_charge': 'py_3O9WfTGxwuSUrRMt1NpTvxr2',
                    'payment_method': 'pm_1O9WmfGxwuSUrRMtDGNVPrxO',
                    'payment_method_types': ['paypal'],
                }
            }
        }

        old_status, order = upsert_order_from_payment_intent_and_product(payload, 2)

        self.assertIsNone(old_status)
        self.assertEqual(order.status, 'paid')

        charge_id = payload['data']['object']['latest_charge']
        payment_intent_id = payload['data']['object']['id']
        transaction = Transaction.objects.get(transaction_id=charge_id)
        self._test_expected_transaction_properties(transaction, payment_intent_id)
        self.assertEqual(transaction.amount.cents, 400)
        order = transaction.order
        self.assertEqual(order.email, 'sb-j1a84328066132@personal.example.com')
        self.assertEqual(order.billing_address, 'John Doe\nUnited States of America')
        self.assertEqual(
            transaction.customer.billing_address.email, 'sb-j1a84328066132@personal.example.com'
        )
        self.assertEqual(transaction.customer.billing_address.full_name, 'John Doe')
        payment_method = transaction.payment_method
        self.assertEqual(payment_method.method_type, 'pa')
        self.assertEqual(
            payment_method.recognisable_name,
            'PayPal account sb-j1a84328066132@personal.example.com',
        )


class TestCheckoutPlanVariationAnonymous(ResponsesMixin, TestCase):
    fixtures = ['gateways', 'devfund', 'systemuser']
    responses_files = [
        'checkout_plan_variation_anonymous.yaml',
        'checkout_plan_variation_webhook.yaml',
    ]

    # @_recorder.record(file_path=f'{ResponsesMixin.responses_file_path}/checkout_plan_variation_anonymous.yaml')
    def test_anonymous(self):
        plan_variation_id = 1
        plan_variation = PlanVariation.objects.get(pk=plan_variation_id)
        session = looper.stripe_utils.create_stripe_checkout_session_for_plan_variation(
            plan_variation,
            None,
            '', '',
        )
        stripe_session_id = session.id
        success_url = reverse(
            'stripe_success_create_subscription',
            kwargs={
                'stripe_session_id': stripe_session_id,
            },
        )

        # Uncomment the next 2 lines if responses have to be re-recorded
        # print('Continue to Stripe:', resp['Location'])
        # input('Press Enter when ready to continue')

        resp2 = self.client.get(success_url)
        self.assertEqual(resp2.status_code, 302)
        self.assertIn('?token=', resp2['Location'])
        order = Order.objects.all().order_by('-id').first()
        self.assertIsNotNone(order.latest_transaction())
        self.assertNotEqual(order.customer.billing_address.email, '')
        self.assertNotEqual(order.email, '')
        self.assertIsNotNone(order.customer.token)
        self.assertEqual(order.status, 'paid')
        self.assertEqual(order.subscription.status, 'active')

        # repeated visit of success url does nothing
        resp3 = self.client.get(success_url)
        self.assertEqual(resp2.url, resp3.url)
        order2 = Order.objects.all().order_by('-id').first()
        self.assertEqual(order, order2)

    # @_recorder.record(file_path=f'{ResponsesMixin.responses_file_path}/checkout_plan_variation_webhook.yaml')
    def test_create_active_subscription(self):
        payload = {
            'type': 'payment_intent.succeeded',
            'data': {
                'object': {
                    "id": "pi_3OOg58Kh8wGqs1ax1RoXFsDP",
                    "object": "payment_intent",
                    "amount": 500,
                    "customer": {
                        "id": "cus_PD6HWIpPk7OnVn",
                        "email": "test@test.com",
                    },
                    "latest_charge": {
                        "id": "ch_3OOg58Kh8wGqs1ax1EViz7bQ",
                    },
                    "metadata": {
                        "plan_variation_id": "1"
                    },
                    "payment_method": {
                        "id": "pm_1OOg58Kh8wGqs1axLHnW7hMw",
                        "object": "payment_method",
                    },
                    "status": "succeeded",
                }
            }
        }
        looper.stripe_utils.create_active_subscription_from_payment_intent(
            payload, 1,
        )
        order = Order.objects.filter(
            gateway_order_ids__gateway_id=3,
            gateway_order_ids__gateway_order_id='pi_3OOg58Kh8wGqs1ax1RoXFsDP',
        ).get()
        self.assertIsNotNone(order.subscription)
        self.assertEqual(order.subscription.status, 'active')


class TestCheckoutPlanVariationLoggedIn(ResponsesMixin, TestCase):
    fixtures = ['gateways', 'devfund', 'systemuser']
    responses_files = [
        'checkout_plan_variation_logged_in.yaml',
    ]

    # @_recorder.record(file_path=f'{responses_file_path}/checkout_plan_variation_logged_in.yaml')
    def test_logged_in(self):
        some_user = User.objects.create(email='joe@example.com')
        self.client.force_login(some_user)

        plan_variation_id = 2
        plan_variation = PlanVariation.objects.get(pk=plan_variation_id)
        session = looper.stripe_utils.create_stripe_checkout_session_for_plan_variation(
            plan_variation,
            some_user.customer,
            '', '',
        )
        stripe_session_id = session.id
        success_url = reverse(
            'stripe_success_create_subscription',
            kwargs={
                'stripe_session_id': stripe_session_id,
            },
        )

        # Uncomment the next 2 lines if responses have to be re-recorded
        # print('Continue to Stripe:', resp['Location'])
        # input('Press Enter when ready to continue')

        resp2 = self.client.get(success_url)
        self.assertEqual(resp2.status_code, 302)
        order = Order.objects.all().order_by('-id').first()
        self.assertEqual(order.customer, some_user.customer)
        self.assertFalse(hasattr(order.customer, 'token'))
        self.assertIsNotNone(order.latest_transaction())
        self.assertEqual(order.customer.billing_address.email, 'joe@example.com')
        self.assertEqual(order.email, 'joe@example.com')
        self.assertEqual(order.status, 'paid')
        self.assertEqual(order.subscription.status, 'active')
        self.assertNotIn('?token=', resp2['Location'])

        # repeated visit of success url does nothing
        resp3 = self.client.get(success_url)
        self.assertEqual(resp2.url, resp3.url)
        order2 = Order.objects.all().order_by('-id').first()
        self.assertEqual(order, order2)
