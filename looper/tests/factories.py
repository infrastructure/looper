from django.contrib.auth import get_user_model
from django.db.models import signals

from factory.django import DjangoModelFactory
import factory

import looper.models
import looper.taxes

User = get_user_model()


class UserFactory(DjangoModelFactory):
    class Meta:
        model = User

    id = factory.Sequence(lambda n: 1000 + n)

    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')
    username = factory.LazyAttribute(lambda o: f'{o.first_name}_{o.last_name}')
    email = factory.LazyAttribute(
        lambda a: '{}.{}@example.com'.format(a.first_name, a.last_name).lower()
    )
    password = 'pass'


@factory.django.mute_signals(signals.pre_save, signals.post_save)
class CustomerFactory(DjangoModelFactory):
    class Meta:
        model = looper.models.Customer


class PaymentMethodFactory(DjangoModelFactory):
    class Meta:
        model = looper.models.PaymentMethod

    gateway_id = factory.LazyAttribute(lambda _: looper.models.Gateway.objects.first().pk)

    customer = factory.SubFactory(CustomerFactory)


class ProductFactory(DjangoModelFactory):
    class Meta:
        model = looper.models.Product

    type = looper.taxes.ProductType.ELECTRONIC_SERVICE.value


class PlanFactory(DjangoModelFactory):
    class Meta:
        model = looper.models.Plan

    product = factory.SubFactory(ProductFactory)


class SubscriptionFactory(DjangoModelFactory):
    class Meta:
        model = looper.models.Subscription

    customer = factory.SubFactory(CustomerFactory)
    plan = factory.SubFactory(PlanFactory)
    payment_method = factory.SubFactory(PaymentMethodFactory)


class OrderFactory(DjangoModelFactory):
    class Meta:
        model = looper.models.Order

    customer = factory.SubFactory(CustomerFactory)
    subscription = factory.SubFactory(SubscriptionFactory)
    payment_method = factory.SubFactory(PaymentMethodFactory)


class TransactionFactory(DjangoModelFactory):
    class Meta:
        model = looper.models.Transaction

    customer = factory.SubFactory(CustomerFactory)
    order = factory.SubFactory(OrderFactory)
    payment_method = factory.SubFactory(PaymentMethodFactory)


@factory.django.mute_signals(signals.pre_save, signals.post_save)
class CustomerFactory(DjangoModelFactory):
    class Meta:
        model = looper.models.Customer


@factory.django.mute_signals(signals.pre_save, signals.post_save)
class AddressFactory(DjangoModelFactory):
    class Meta:
        model = looper.models.Address

    email = factory.LazyAttribute(lambda o: '%s.billing@example.com' % o.customer.user.username)


@factory.django.mute_signals(signals.pre_save, signals.post_save)
def create_customer_with_billing_address(**data):
    """Use factories to create a User with a Customer and Address records."""
    customer_field_names = {f.name for f in looper.models.Customer._meta.get_fields()}
    address_field_names = {f.name for f in looper.models.Address._meta.get_fields()}
    user_field_names = {f.name for f in User._meta.get_fields()}
    gwc_id_field_names = {f.name for f in looper.models.GatewayCustomerId._meta.get_fields()}

    address_kwargs = {k: v for k, v in data.items() if k in address_field_names}
    customer_kwargs = {k: v for k, v in data.items() if k in customer_field_names}
    user_kwargs = {k: v for k, v in data.items() if k in user_field_names}
    gwc_id_kwargs = {k: v for k, v in data.items() if k in gwc_id_field_names}

    user = UserFactory(**user_kwargs)
    customer = CustomerFactory(user=user, **customer_kwargs)
    AddressFactory(customer=customer, **address_kwargs)
    if gwc_id_kwargs:
        looper.models.GatewayCustomerId.objects.get_or_create(
            customer=customer,
            gateway=looper.models.Gateway.objects.get(is_default=True),
            **gwc_id_kwargs,
        )
    return customer
