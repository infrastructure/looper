from ..templatetags.looper import pricing
from .. import models
from . import AbstractBaseTestCase


class PricingTest(AbstractBaseTestCase):
    def test_plan_variation(self):
        pv = models.PlanVariation(
            plan_id=1,
            currency='GBP',
            price=401,
            interval_length=3,
            interval_unit='day',
        )
        self.assertEqual('£\u00A04.01 / 3\u00A0days', pricing(pv))

        pv.interval_length = 1
        self.assertEqual('£\u00A04.01 / day', pricing(pv))
