from unittest.mock import patch

from django.contrib.auth import get_user_model
from django.urls import reverse
from django_countries.fields import Country

from looper import admin_log
from looper.exceptions import GatewayError
from looper.gateways import PaymentMethodInfo
from looper.models import (
    Address,
    Customer,
    Gateway,
    GatewayCustomerId,
    PaymentMethod,
    TaxNumberVerification,
)
from looper.taxes import ProductType, TaxType
from looper.tests import AbstractBaseTestCase

User = get_user_model()


class AbstractSingleUserTestCase(AbstractBaseTestCase):
    fixtures = ['testuser']

    def setUp(self):
        self.user: User = User.objects.get(email='harry@blender.org')
        self.customer: Customer = self.user.customer


class BillingAddressAsTextTest(AbstractSingleUserTestCase):
    def test_address_as_text(self):
        """Country is a special field, not just a CharField"""

        a = self.customer.billing_address
        a.street_address = 'Buikslotermeerplein 161'
        a.country = 'NL'
        a.save()

        self.assertIsInstance(a.country, Country)
        self.assertEqual(
            'Harry de B\u00f8ker\n\u1002\u102e\u1010 for music\nBuikslotermeerplein 161\nNetherlands',
            self.customer.billing_address.as_text(),
        )


class CustomerModelTestCase(AbstractSingleUserTestCase):
    fixtures = AbstractSingleUserTestCase.fixtures + ['systemuser']

    def setUp(self):
        super().setUp()

        # Add default Gateway
        self.gateway = Gateway.objects.create(name='braintree', is_default=True)
        # Isolate Braintree and

    def test_billing_address_property_returns_address_instance_even_if_none_stored(self):
        # Ensure the customer does not have an Address associated
        Address.objects.filter(customer=self.user.customer).delete()
        with self.assertRaises(Address.DoesNotExist):
            Address.objects.get(customer=self.customer)
        # Getting the customer address should always return an Address object
        self.assertIsInstance(self.customer.billing_address, Address)

    def test_get_tax_no_billing_address(self):
        # Ensure the customer does not have an Address associated
        Address.objects.filter(customer=self.user.customer).delete()
        with self.assertRaises(Address.DoesNotExist):
            Address.objects.get(customer=self.customer)

        tax_type, tax_rate = self.customer.get_tax(ProductType.ELECTRONIC_SERVICE.value)

        self.assertEqual(tax_type, TaxType.NO_CHARGE)
        self.assertEqual(tax_rate, 0)

    def test_get_tax_no_vat_number_nl(self):
        billing_address = self.customer.billing_address
        billing_address.vat_number = ''
        billing_address.country = 'NL'
        billing_address.save()

        tax_type, tax_rate = self.customer.get_tax(ProductType.ELECTRONIC_SERVICE.value)

        self.assertEqual(tax_type, TaxType.VAT_CHARGE)
        self.assertEqual(tax_rate, 21)

    def test_get_tax_vat_number_nl(self):
        billing_address = self.customer.billing_address
        billing_address.vat_number = 'NL818152011B01'
        billing_address.country = 'NL'
        billing_address.save()

        tax_type, tax_rate = self.customer.get_tax(ProductType.ELECTRONIC_SERVICE.value)

        self.assertEqual(tax_type, TaxType.VAT_CHARGE)
        self.assertEqual(tax_rate, 21)

    def test_get_tax_no_vat_number_de(self):
        billing_address = self.customer.billing_address
        billing_address.vat_number = ''
        billing_address.country = 'DE'
        billing_address.save()

        tax_type, tax_rate = self.customer.get_tax(ProductType.ELECTRONIC_SERVICE.value)

        self.assertEqual(tax_type, TaxType.VAT_CHARGE)
        self.assertEqual(tax_rate, 19)

    def test_get_tax_vat_number_de(self):
        billing_address = self.customer.billing_address
        billing_address.vat_number = 'DE260543043'
        billing_address.country = 'DE'
        billing_address.save()

        tax_type, tax_rate = self.customer.get_tax(ProductType.ELECTRONIC_SERVICE.value)

        self.assertEqual(tax_type, TaxType.VAT_REVERSE_CHARGE)
        self.assertEqual(tax_rate, 19)

    def test_get_tax_vat_number_de_tax_number_verification_exists_valid(self):
        billing_address = self.customer.billing_address
        billing_address.vat_number = 'DE260543043'
        billing_address.country = 'DE'
        billing_address.save()
        TaxNumberVerification(is_valid=True, number=billing_address.vat_number).save()

        tax_type, tax_rate = self.customer.get_tax(ProductType.ELECTRONIC_SERVICE.value)

        self.assertEqual(tax_type, TaxType.VAT_REVERSE_CHARGE)
        self.assertEqual(tax_rate, 19)

    def test_get_tax_vat_number_de_tax_number_verification_exists_invalid(self):
        billing_address = self.customer.billing_address
        billing_address.vat_number = 'DE260543043'
        billing_address.country = 'DE'
        billing_address.save()
        TaxNumberVerification(is_valid=False, number=billing_address.vat_number).save()

        tax_type, tax_rate = self.customer.get_tax(ProductType.ELECTRONIC_SERVICE.value)

        self.assertEqual(tax_type, TaxType.VAT_CHARGE)
        self.assertEqual(tax_rate, 19)

    def test_get_tax_vat_number_de_tax_number_verifications_exist_latest_valid(self):
        billing_address = self.customer.billing_address
        billing_address.vat_number = 'DE260543043'
        billing_address.country = 'DE'
        billing_address.save()
        TaxNumberVerification(is_valid=False, number=billing_address.vat_number).save()
        TaxNumberVerification(is_valid=False, number=billing_address.vat_number).save()
        # The latest stored verification indicates that the VAT number is valid
        TaxNumberVerification(is_valid=True, number=billing_address.vat_number).save()

        tax_type, tax_rate = self.customer.get_tax(ProductType.ELECTRONIC_SERVICE.value)

        self.assertEqual(tax_type, TaxType.VAT_REVERSE_CHARGE)
        self.assertEqual(tax_rate, 19)

    def test_get_tax_vat_number_de_tax_number_verifications_exist_latest_invalid(self):
        billing_address = self.customer.billing_address
        billing_address.vat_number = 'DE260543043'
        billing_address.country = 'DE'
        billing_address.save()
        TaxNumberVerification(is_valid=True, number=billing_address.vat_number).save()
        TaxNumberVerification(is_valid=True, number=billing_address.vat_number).save()
        # The latest stored verification indicates that the VAT number is invalid
        TaxNumberVerification(is_valid=False, number=billing_address.vat_number).save()

        tax_type, tax_rate = self.customer.get_tax(ProductType.ELECTRONIC_SERVICE.value)

        self.assertEqual(tax_type, TaxType.VAT_CHARGE)
        self.assertEqual(tax_rate, 19)

    def test_get_tax_no_vat_number_se(self):
        billing_address = self.customer.billing_address
        billing_address.vat_number = ''
        billing_address.country = 'SE'
        billing_address.save()

        tax_type, tax_rate = self.customer.get_tax(ProductType.ELECTRONIC_SERVICE.value)

        self.assertEqual(tax_type, TaxType.VAT_CHARGE)
        self.assertEqual(tax_rate, 25)

    def test_get_tax_vat_number_se(self):
        billing_address = self.customer.billing_address
        billing_address.vat_number = 'SE 556070171501'
        billing_address.country = 'SE'
        billing_address.save()

        tax_type, tax_rate = self.customer.get_tax(ProductType.ELECTRONIC_SERVICE.value)

        self.assertEqual(tax_type, TaxType.VAT_REVERSE_CHARGE)
        self.assertEqual(tax_rate, 25)

    def test_get_tax_no_vat_number_us(self):
        billing_address = self.customer.billing_address
        billing_address.vat_number = ''
        billing_address.country = 'US'
        billing_address.save()

        tax_type, tax_rate = self.customer.get_tax(ProductType.ELECTRONIC_SERVICE.value)

        self.assertEqual(tax_type, TaxType.NO_CHARGE)
        self.assertEqual(tax_rate, 0)

    def test_get_tax_vat_number_us(self):
        billing_address = self.customer.billing_address
        billing_address.vat_number = 'BOGUSVATNUMBER'
        billing_address.country = 'US'
        billing_address.save()

        tax_type, tax_rate = self.customer.get_tax(ProductType.ELECTRONIC_SERVICE.value)

        self.assertEqual(tax_type, TaxType.NO_CHARGE)
        self.assertEqual(tax_rate, 0)

    def test_payment_method_default_no_method(self):
        # Ensure no payment method is associated to the Customer
        with self.assertRaises(PaymentMethod.DoesNotExist):
            PaymentMethod.objects.get(customer=self.customer)
        # A customer without payment method should return payment method None
        self.assertIsNone(self.customer.payment_method_default)

    def test_payment_method_default_one_methods(self):
        # Add one default payment method
        first_payment_method = PaymentMethod.objects.create(
            customer=self.customer, token='abc', gateway=self.gateway
        )
        # Check that customer.payment_method_default returns that payment method
        self.assertEqual(self.customer.payment_method_default.id, first_payment_method.id)

    def test_payment_method_default_two_methods(self):
        # Add one default payment method
        first_payment_method = PaymentMethod.objects.create(
            customer=self.customer, token='abc', gateway=self.gateway
        )
        # Add another payment method
        PaymentMethod.objects.create(customer=self.customer, token='def', gateway=self.gateway)

        # Getting payment_method_default should still return the first payment method
        self.assertEqual(self.customer.payment_method_default.id, first_payment_method.id)

        # Add another customer
        other_user = User.objects.create_user('ron', 'ron@blender.org')
        other_customer = other_user.customer
        # Add a non default payment method for this customer
        other_payment_method = PaymentMethod.objects.create(
            customer=other_customer, token='123', gateway=self.gateway
        )
        # Check that we get the proper payment method
        self.assertEqual(other_customer.payment_method_default.id, other_payment_method.id)

    def test_payment_method_default_deleted_method(self):
        # Create a deleted payment method
        PaymentMethod.objects.create(
            customer=self.customer, token='hgr', gateway=self.gateway, is_deleted=True
        )
        self.assertIsNone(self.customer.payment_method_default)

    @patch('looper.gateways.BraintreeGateway.payment_method_create')
    def test_double_payment_method(self, mock_payment_method_create):
        # Braintree always returns the same payment method token when the
        # user selected the same payment method, even when the nonces are
        # different, and even when the function called is actually
        # `payment_method.create(...)`.
        mock_payment_method_create.return_value = PaymentMethodInfo('the-same-token-as-before')

        pm1 = self.customer.payment_method_add('nonce-one', self.gateway)
        pm2 = self.customer.payment_method_add('nonce-two', self.gateway)
        self.assertEqual(pm1.pk, pm2.pk)
        self.assertEqual(1, PaymentMethod.objects.count())

    @patch('looper.gateways.BraintreeGateway.customer_create')
    def test_gateway_customer_id_get_or_create(self, mock_customer_create):
        expected_customer_id = '214745181'
        # Mock the response from Braintree when creating a customer
        mock_customer_create.return_value = expected_customer_id

        # No customer exists on the the remote gateway
        with self.assertRaises(GatewayCustomerId.DoesNotExist):
            GatewayCustomerId.objects.get(customer=self.customer)

        # Create a new gateway customer
        gateway_customer_id = self.customer.gateway_customer_id_get_or_create(self.gateway)
        self.assertEqual(expected_customer_id, gateway_customer_id)
        mock_customer_create.assert_called_with({'email': 'harry+billing@example.com', 'full_name': 'Harry de Bøker'})

        # Check if it exists in the database
        self.assertEqual(
            expected_customer_id, GatewayCustomerId.objects.get(customer=self.customer).gateway_customer_id
        )

    @patch('braintree.CustomerGateway.create')
    def test_gateway_customer_id_get_or_create_email_first_last_name_passed_to_bt(self, mock_bt_customer_create):
        expected_customer_id = '214745181'
        billing_address = self.customer.billing_address
        billing_address.full_name = 'Гарри Potter'
        billing_address.save()
        # Mock the response from Braintree when creating a customer
        mock_bt_customer_create.return_value.customer.id = expected_customer_id

        # No customer exists on the the remote gateway
        with self.assertRaises(GatewayCustomerId.DoesNotExist):
            GatewayCustomerId.objects.get(customer=self.customer)

        # Create a new gateway customer
        gateway_customer_id = self.customer.gateway_customer_id_get_or_create(self.gateway)

        # Check that Braintree Customer create was called with email, first and last names
        self.assertEqual(expected_customer_id, gateway_customer_id)
        mock_bt_customer_create.assert_called_with(
            {
                'email': 'harry+billing@example.com',
                'first_name': 'Гарри',
                'last_name': 'Potter',
            },
        )

        # Check if it exists in the database
        self.assertEqual(
            expected_customer_id, GatewayCustomerId.objects.get(customer=self.customer).gateway_customer_id
        )

    @patch('looper.gateways.BraintreeGateway.customer_create')
    def test_gateway_customer_id_get_or_create_error(self, mock_customer_create):
        # Mock the response as a failure
        mock_customer_create.side_effect = GatewayError(message='mock', errors=['je moeder'])
        with self.assertRaises(GatewayError):
            self.customer.gateway_customer_id_get_or_create(self.gateway)

    @patch('looper.gateways.BraintreeGateway.payment_method_create')
    @patch('looper.gateways.BraintreeGateway.customer_create')
    def test_payment_method_add(self, mock_customer_create, mock_payment_method_create):
        expected_customer_id = '214745181'
        token = 'act'
        mock_customer_create.return_value = expected_customer_id

        pm_info = PaymentMethodInfo(token, PaymentMethodInfo.Type.PAYPAL_ACCOUNT)
        mock_payment_method_create.return_value = pm_info

        pm = self.customer.payment_method_add('fake-valid-nonce', self.gateway)
        self.assertEqual(token, pm.token)
        self.assertEqual('pa', pm.method_type)

    @patch('looper.gateways.BraintreeGateway.payment_method_delete')
    def test_payment_method_delete(self, mock_payment_method_delete):
        mock_payment_method_delete.return_value = None

        pm = PaymentMethod.objects.create(customer=self.customer, token='hgr', gateway=self.gateway)
        pm.delete()

        pm.refresh_from_db()
        self.assertTrue(pm.is_deleted, 'Payment method should be soft-deleted')
        mock_payment_method_delete.assert_called_with('hgr')

        entries_q = admin_log.entries_for(pm)
        self.assertEqual(1, entries_q.count())
        self.assertEqual(
            'Soft-deleting payment method and deleting it at the payment provider',
            entries_q[0].change_message,
        )

    @patch('looper.gateways.BraintreeGateway.payment_method_delete')
    @patch('looper.gateways.BraintreeGateway.payment_method_create')
    @patch('looper.gateways.BraintreeGateway.customer_create')
    def test_payment_method_add_deleted(
        self, mock_customer_create, mock_payment_method_create, mock_payment_method_delete
    ):
        expected_customer_id = '214745181'
        token = 'act'
        mock_customer_create.return_value = expected_customer_id
        mock_payment_method_create.return_value = PaymentMethodInfo(token)
        mock_payment_method_delete.return_value = None

        pm = self.customer.payment_method_add('fake-valid-nonce', self.gateway)
        original_pm_pk = pm.pk
        self.assertEqual(token, pm.token)

        pm.delete()
        pm.refresh_from_db()
        self.assertTrue(pm.is_deleted, 'Payment method should be soft-deleted')
        mock_payment_method_delete.assert_called_with('act')

        new_pm = self.customer.payment_method_add('another-fake-valid-nonce', self.gateway)
        self.assertFalse(new_pm.is_deleted, 'Payment method should not be deleted')
        self.assertEqual(original_pm_pk, new_pm.pk, 'Payment method should have been undeleted')

        entries_q = admin_log.entries_for(pm)
        self.assertEqual(2, entries_q.count())
        self.assertEqual(
            'Soft-deleting payment method and deleting it at the payment provider',
            entries_q[1].change_message,
        )
        self.assertEqual('Undeleting payment method', entries_q[0].change_message)


class BillingAddressEditTestCase(AbstractSingleUserTestCase):
    def setUp(self):
        super().setUp()

        ba: Address = self.customer.billing_address
        ba.country = 'NL'
        ba.street_address = 'Beukslotermeerplein 161'
        ba.save()

    def test_billing_address_redirects_when_not_logged_in(self):
        # Try with non authenticated user
        r = self.client.get(reverse('looper:settings_billing_info'))
        self.assertEqual(r.status_code, 302)
        # Try with authenticated user
        self.client.force_login(self.user)
        r = self.client.get(reverse('looper:settings_billing_info'))
        self.assertEqual(r.status_code, 200)

    def test_billing_address_form_missing_fields(self):
        pre_changes: str = self.customer.billing_address.as_text()

        self.client.force_login(self.user)
        self.client.post(reverse('looper:settings_billing_info'), data={})

        self.customer.refresh_from_db()
        self.customer.billing_address.refresh_from_db()
        self.assertEqual(pre_changes, self.customer.billing_address.as_text())

    def test_billing_address_form_success(self):
        payload = {
            'email': 'harry@example.com',
            'full_name': 'Гарри Поттер',
            'company': 'Министерство Магии',
            'street_address': 'Scotland Pl',
            'locality': 'London',
            'postal_code': 'SW1A 2BD',
            'region': 'Westminster',
            'country': 'GB',
        }
        ba: Address = self.customer.billing_address

        # Check that all original address fields are different from the payload we will submit
        for name, not_expect in payload.items():
            self.assertNotEqual(not_expect, getattr(ba, name))

        self.client.force_login(self.user)
        r = self.client.post(reverse('looper:settings_billing_info'), data=payload)
        self.assertEqual(302, r.status_code, 'After POST the page should redirect')
        self.assertEqual(r['Location'], reverse('looper:settings_billing_info'))

        # Check that all address fields are now matching the payload submitted
        ba.refresh_from_db()
        for name, expect in payload.items():
            self.assertEqual(expect, getattr(ba, name), r.content.decode())
