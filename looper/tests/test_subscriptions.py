from decimal import Decimal
from unittest import mock
import datetime
import logging

from django.dispatch import receiver
import django.test
import django.utils.timezone

from . import AbstractLooperTestCase
from ..exceptions import IncorrectStatusError
from ..models import Subscription
from ..money import Money
from looper import admin_log
from looper.taxes import TaxType, ProductType

utc = datetime.timezone.utc
log = logging.getLogger(__name__)


class SubscriptionsTest(AbstractLooperTestCase):
    gateway_name = 'braintree'

    def test_subscription_activation_signal(self):
        from ..signals import subscription_activated

        signal_old_status = ''
        signal_new_status = ''
        signal_count = 0

        @receiver(subscription_activated)
        def activated(sender: Subscription, old_status: str, **kwargs):
            nonlocal signal_old_status, signal_new_status, signal_count
            signal_old_status = old_status
            signal_new_status = sender.status
            signal_count += 1

        subs = self.create_subscription()
        self.assertFalse(subs.is_active)

        now = django.utils.timezone.now()
        with mock.patch('django.utils.timezone.now') as mock_now:
            mock_now.return_value = now
            subs.status = 'active'
            subs.save(update_fields={'status'})

        subs.refresh_from_db()
        self.assertTrue(subs.is_active)
        self.assertAlmostEqualDateTime(now, subs.started_at)
        self.assertEqual(1, signal_count)
        self.assertEqual('on-hold', signal_old_status)
        self.assertEqual('active', signal_new_status)

        # Saving with status='active' again shouldn't trigger another signal.
        subs.save(force_update=True)
        self.assertEqual(1, signal_count)

    def test_subscription_deactivation_signal(self):
        from ..signals import subscription_deactivated

        signal_old_status = ''
        signal_new_status = ''
        signal_count = 0

        @receiver(subscription_deactivated)
        def deactivated(sender: Subscription, old_status: str, **kwargs):
            nonlocal signal_old_status, signal_new_status, signal_count
            signal_old_status = old_status
            signal_new_status = sender.status
            signal_count += 1

        subs = self.create_subscription()
        subs.status = 'active'
        subs.save(update_fields={'status'})
        subs.status = 'cancelled'
        subs.save(update_fields={'status'})

        subs.refresh_from_db()
        self.assertFalse(subs.is_active)
        self.assertEqual(1, signal_count)
        self.assertEqual('active', signal_old_status)
        self.assertEqual('cancelled', signal_new_status)

    def test_subscription_pending_cancellation(self):
        from ..signals import subscription_deactivated

        signal_count = 0

        @receiver(subscription_deactivated)
        def deactivated(*args, **kwargs):
            nonlocal signal_count
            signal_count += 1

        subs = self.create_subscription()
        subs.status = 'active'
        subs.save(update_fields={'status'})
        subs.status = 'pending-cancellation'
        subs.save(update_fields={'status'})

        subs.refresh_from_db()
        self.assertTrue(subs.is_active)

        # This should *not* send the deactivation signal, as the subscription
        # is still active.
        self.assertEqual(0, signal_count)

    def test_create_managed_subscription(self):
        subscription = Subscription.objects.create(
            plan=self.plan,
            customer=self.user.customer,
            price=self.planvar.price,
            currency=self.planvar.currency,
            payment_method=None,
            collection_method='managed',
            interval_unit=self.planvar.interval_unit,
            interval_length=self.planvar.interval_length,
        )
        self.assertEqual('on-hold', subscription.status)
        self.assertEqual(0, subscription.order_set.count())
        self.assertIsNone(subscription.latest_order())


class SubscriptionTaxTest(AbstractLooperTestCase):
    gateway_name = 'braintree'

    def test_update_tax_plan_has_default_donation_product_type_no_billing_address(self):
        subs = self.create_subscription()

        subs.update_tax()
        order = subs.generate_order()

        self.assertEqual(subs.price, Money('EUR', 2500))
        self.assertEqual(subs.tax, Money('EUR', 0))
        self.assertEqual(subs.tax_country, '')
        self.assertEqual(subs.tax_type, TaxType.NO_CHARGE.value)
        self.assertEqual(subs.tax_rate, 0)
        # No tax charged, hence no difference between subscription and order prices
        self.assertEqual(order.price, Money('EUR', 2500))
        self.assertEqual(order.tax, Money('EUR', 0))
        self.assertEqual(order.tax_country, '')
        self.assertEqual(order.tax_type, TaxType.NO_CHARGE.value)
        self.assertEqual(order.tax_rate, 0)

    def test_plan_has_detault_donation_product_type_no_vat_number_nl(self):
        subs = self.create_subscription_with_product_and_billing_details(
            vat_number='',
            country='NL'
        )

        order = subs.generate_order()

        self.assertEqual(subs.price, Money('EUR', 2500))
        self.assertEqual(subs.tax, Money('EUR', 0))
        self.assertEqual(subs.tax_country, 'NL')
        self.assertEqual(subs.tax_type, TaxType.NO_CHARGE.value)
        self.assertEqual(subs.tax_rate, 0)
        # No tax charged, hence no difference between subscription and order prices
        self.assertEqual(order.price, Money('EUR', 2500))
        self.assertEqual(order.tax, Money('EUR', 0))
        self.assertEqual(order.tax_country, 'NL')
        self.assertEqual(order.tax_type, TaxType.NO_CHARGE.value)
        self.assertEqual(order.tax_rate, 0)

    def test_plan_has_electronic_services_type_no_billing_address(self):
        subs = self.create_subscription_with_product_and_billing_details(
            product_type=ProductType.ELECTRONIC_SERVICE.value,
            vat_number='',
        )

        order = subs.generate_order()

        self.assertEqual(subs.price, Money('EUR', 2500))
        self.assertEqual(subs.tax, Money('EUR', 0))
        self.assertEqual(subs.tax_country, '')
        self.assertEqual(subs.tax_type, TaxType.NO_CHARGE.value)
        self.assertEqual(subs.tax_rate, 0)
        # No tax charged, hence no difference between subscription and order prices
        self.assertEqual(order.price, Money('EUR', 2500))
        self.assertEqual(order.tax, Money('EUR', 0))
        self.assertEqual(order.tax_country, '')
        self.assertEqual(order.tax_type, TaxType.NO_CHARGE.value)
        self.assertEqual(order.tax_rate, 0)

    def test_plan_has_electronic_services_type_no_vat_number_nl(self):
        subs = self.create_subscription_with_product_and_billing_details(
            product_type=ProductType.ELECTRONIC_SERVICE.value,
            vat_number='',
            country='NL',
        )

        order = subs.generate_order()

        self.assertEqual(subs.price, Money('EUR', 2500))
        self.assertEqual(subs.tax, Money('EUR', 434))
        self.assertEqual(subs.tax_country, 'NL')
        self.assertEqual(subs.tax_rate, 21)
        self.assertEqual(subs.tax_type, TaxType.VAT_CHARGE.value)
        # VAT is included into the price, hence no difference between subscription and order prices
        self.assertEqual(order.price, Money('EUR', 2500))
        self.assertEqual(order.tax, Money('EUR', 434))
        self.assertEqual(order.tax_country, 'NL')
        self.assertEqual(order.tax_rate, 21)
        self.assertEqual(order.tax_type, TaxType.VAT_CHARGE.value)

    def test_plan_has_electronic_services_type_no_vat_number_fr(self):
        subs = self.create_subscription_with_product_and_billing_details(
            product_type=ProductType.ELECTRONIC_SERVICE.value,
            vat_number='',
            country='FR',
        )

        order = subs.generate_order()

        self.assertEqual(subs.price, Money('EUR', 2500))
        self.assertEqual(subs.tax, Money('EUR', 417))
        self.assertEqual(subs.tax_country, 'FR')
        self.assertEqual(subs.tax_rate, 20)
        self.assertEqual(subs.tax_type, TaxType.VAT_CHARGE.value)
        # VAT is included into the price, hence no difference between subscription and order prices
        self.assertEqual(order.price, Money('EUR', 2500))
        self.assertEqual(order.tax, Money('EUR', 417))
        self.assertEqual(order.tax_country, 'FR')
        self.assertEqual(order.tax_rate, 20)
        self.assertEqual(order.tax_type, TaxType.VAT_CHARGE.value)
        self.assertEqual(order.taxable.format_tax_amount(), 'Inc. 20% VAT (€ 4.17)')

    def test_plan_has_electronic_services_type_vat_number_nl(self):
        subs = self.create_subscription_with_product_and_billing_details(
            product_type=ProductType.ELECTRONIC_SERVICE.value,
            vat_number='NL818152011B01',
            country='NL',
        )

        order = subs.generate_order()

        self.assertEqual(subs.price, Money('EUR', 2500))
        self.assertEqual(subs.tax, Money('EUR', 434))
        self.assertEqual(subs.tax_country, 'NL')
        self.assertEqual(subs.tax_rate, 21)
        # No reverse-charge for NL customers
        self.assertEqual(subs.tax_type, TaxType.VAT_CHARGE.value)
        # VAT is included into the price, hence no difference between subscription and order prices
        self.assertEqual(order.price, Money('EUR', 2500))
        self.assertEqual(order.tax, Money('EUR', 434))
        self.assertEqual(order.tax_country, 'NL')
        self.assertEqual(order.tax_rate, 21)
        self.assertEqual(order.tax_type, TaxType.VAT_CHARGE.value)

    def test_on_save_tax_country_type_changes(self):
        subs = self.create_subscription_with_product_and_billing_details(
            product_type=ProductType.ELECTRONIC_SERVICE.value,
            country='NL',
        )
        self.assertEqual(subs.price, Money('EUR', 2500))
        self.assertEqual(subs.tax, Money('EUR', 434))
        self.assertEqual(subs.tax_country, 'NL')
        self.assertEqual(subs.tax_rate, 21)
        self.assertEqual(subs.tax_type, TaxType.VAT_CHARGE.value)

        # Change the tax type and country before saving
        subs.tax_type = TaxType.NO_CHARGE.value
        subs.tax_country = 'US'
        subs.save()

        self.assertEqual(subs.price, Money('EUR', 2500))
        self.assertEqual(subs.tax, Money('EUR', 0))
        self.assertEqual(subs.tax_country, 'US')
        # self.assertEqual(subs.tax_rate, 0)  TODO tax rate should also be updated based on TaxRules
        self.assertEqual(subs.tax_type, TaxType.NO_CHARGE.value)

        # check that all of the tax-related changes have been logged
        entries_q = list(admin_log.entries_for(subs).order_by('-action_time'))
        self.assertEqual(
            entries_q[0].change_message, 'Tax changed from EUR 4.34 to EUR 0.00 ( 21%, US)'
        )
        self.assertEqual(
            entries_q[1].change_message,
            'Tax type changed from TaxType.VAT_CHARGE to TaxType.NO_CHARGE',
        )
        self.assertEqual(
            entries_q[2].change_message, 'Tax country changed from NL to US'
        )

    def test_on_save_tax_changes(self):
        subs = self.create_subscription_with_product_and_billing_details(
            product_type=ProductType.ELECTRONIC_SERVICE.value,
            country='NL',
        )
        self.assertEqual(subs.price, Money('EUR', 2500))
        self.assertEqual(subs.tax, Money('EUR', 434))
        self.assertEqual(subs.tax_country, 'NL')
        self.assertEqual(subs.tax_rate, 21)
        self.assertEqual(subs.tax_type, TaxType.VAT_CHARGE.value)

        # Change tax rate and country before saving
        subs.tax_country = 'IE'
        subs.tax_rate = Decimal(23)
        subs.save()

        self.assertEqual(subs.price, Money('EUR', 2500))
        # check that tax was recalculated in accordance with the tax rate change
        self.assertEqual(subs.tax, Money('EUR', 467))
        self.assertEqual(subs.tax_country, 'IE')
        self.assertEqual(subs.tax_rate, 23)
        self.assertEqual(subs.tax_type, TaxType.VAT_CHARGE.value)

        # check that all of the tax-related changes have been logged
        entries_q = list(admin_log.entries_for(subs).order_by('-action_time'))
        self.assertEqual(
            entries_q[0].change_message, 'Tax changed from EUR 4.34 to EUR 4.67 (VATC 23%, IE)'
        )
        self.assertEqual(
            entries_q[1].change_message, 'Tax rate changed from 21.00% to 23%'
        )
        self.assertEqual(
            entries_q[2].change_message, 'Tax country changed from NL to IE'
        )

    def test_plan_has_electronic_services_type_no_vat_number_de(self):
        subs = self.create_subscription_with_product_and_billing_details(
            product_type=ProductType.ELECTRONIC_SERVICE.value,
            vat_number='',
            country='DE',
        )

        order = subs.generate_order()

        self.assertEqual(subs.price, Money('EUR', 2500))
        self.assertEqual(subs.tax, Money('EUR', 399))
        self.assertEqual(subs.tax_country, 'DE')
        self.assertEqual(subs.tax_rate, 19)
        self.assertEqual(subs.tax_type, TaxType.VAT_CHARGE.value)
        # VAT is included into the price, hence no difference between subscription and order prices
        self.assertEqual(order.price, Money('EUR', 2500))
        self.assertEqual(order.tax, Money('EUR', 399))
        self.assertEqual(order.tax_country, 'DE')
        self.assertEqual(order.tax_rate, 19)
        self.assertEqual(order.tax_type, TaxType.VAT_CHARGE.value)

    def test_plan_has_electronic_services_type_vat_number_de(self):
        subs = self.create_subscription_with_product_and_billing_details(
            product_type=ProductType.ELECTRONIC_SERVICE.value,
            vat_number='DE260543043',
            country='DE',
        )

        order = subs.generate_order()

        self.assertEqual(subs.price, Money('EUR', 2500))
        self.assertEqual(subs.tax, Money('EUR', 0))
        self.assertEqual(subs.tax_country, 'DE')
        self.assertEqual(subs.tax_rate, 19)
        self.assertEqual(subs.tax_type, TaxType.VAT_REVERSE_CHARGE.value)
        # Reverse-charged VAT is subtracted from the subscription price
        self.assertEqual(order.price, Money('EUR', 2101))
        self.assertEqual(order.tax, Money('EUR', 0))
        self.assertEqual(order.tax_country, 'DE')
        self.assertEqual(order.tax_rate, 19)
        self.assertEqual(order.tax_type, TaxType.VAT_REVERSE_CHARGE.value)

    def test_plan_has_electronic_services_type_no_vat_number_se(self):
        subs = self.create_subscription_with_product_and_billing_details(
            product_type=ProductType.ELECTRONIC_SERVICE.value,
            vat_number='',
            country='SE',
        )

        order = subs.generate_order()

        self.assertEqual(subs.price, Money('EUR', 2500))
        self.assertEqual(subs.tax, Money('EUR', 500))
        self.assertEqual(subs.tax_country, 'SE')
        self.assertEqual(subs.tax_rate, 25)
        self.assertEqual(subs.tax_type, TaxType.VAT_CHARGE.value)
        # VAT is included into the price, hence no difference between subscription and order prices
        self.assertEqual(order.price, Money('EUR', 2500))
        self.assertEqual(order.tax, Money('EUR', 500))
        self.assertEqual(order.tax_country, 'SE')
        self.assertEqual(order.tax_rate, 25)
        self.assertEqual(order.tax_type, TaxType.VAT_CHARGE.value)

    def test_plan_has_electronic_services_type_vat_number_se(self):
        subs = self.create_subscription_with_product_and_billing_details(
            product_type=ProductType.ELECTRONIC_SERVICE.value,
            vat_number='SE 556070171501',
            country='SE',
        )

        order = subs.generate_order()

        self.assertEqual(subs.price, Money('EUR', 2500))
        self.assertEqual(subs.tax, Money('EUR', 0))
        self.assertEqual(subs.tax_country, 'SE')
        self.assertEqual(subs.tax_rate, 25)
        self.assertEqual(subs.tax_type, TaxType.VAT_REVERSE_CHARGE.value)
        # Reverse-charged VAT is subtracted from the subscription price
        self.assertEqual(order.price, Money('EUR', 2000))
        self.assertEqual(order.tax, Money('EUR', 0))
        self.assertEqual(order.tax_country, 'SE')
        self.assertEqual(order.tax_rate, 25)
        self.assertEqual(order.tax_type, TaxType.VAT_REVERSE_CHARGE.value)

    def test_plan_has_electronic_services_type_no_vat_number_us(self):
        subs = self.create_subscription_with_product_and_billing_details(
            product_type=ProductType.ELECTRONIC_SERVICE.value,
            vat_number='',
            country='US',
        )

        order = subs.generate_order()

        self.assertEqual(subs.price, Money('EUR', 2500))
        self.assertEqual(subs.tax, Money('EUR', 0))
        self.assertEqual(subs.tax_country, 'US')
        self.assertEqual(subs.tax_type, TaxType.NO_CHARGE.value)
        self.assertEqual(subs.tax_rate, 0)
        # No tax charged, hence no difference between subscription and order prices
        self.assertEqual(order.price, Money('EUR', 2500))
        self.assertEqual(order.tax, Money('EUR', 0))
        self.assertEqual(order.tax_country, 'US')
        self.assertEqual(order.tax_type, TaxType.NO_CHARGE.value)
        self.assertEqual(order.tax_rate, 0)

    def test_plan_has_electronic_services_type_bogus_vat_number_us(self):
        subs = self.create_subscription_with_product_and_billing_details(
            product_type=ProductType.ELECTRONIC_SERVICE.value,
            vat_number='BOGUSVATNUMBER',
            country='US',
        )

        order = subs.generate_order()

        self.assertEqual(subs.price, Money('EUR', 2500))
        self.assertEqual(subs.tax, Money('EUR', 0))
        self.assertEqual(subs.tax_country, 'US')
        self.assertEqual(subs.tax_type, TaxType.NO_CHARGE.value)
        self.assertEqual(subs.tax_rate, 0)
        # No tax charged, hence no difference between subscription and order prices
        self.assertEqual(order.price, Money('EUR', 2500))
        self.assertEqual(order.tax, Money('EUR', 0))
        self.assertEqual(order.tax_country, 'US')
        self.assertEqual(order.tax_type, TaxType.NO_CHARGE.value)
        self.assertEqual(order.tax_rate, 0)


class SubscriptionCancellationTest(AbstractLooperTestCase):
    gateway_name = 'braintree'

    log = log.getChild('SubscriptionCancellationTest')

    def cancel(self, subscription: Subscription) -> datetime.datetime:
        log.debug('Cancelling subscription pk=%d', subscription.pk)
        now = django.utils.timezone.now()
        with mock.patch('django.utils.timezone.now') as mock_now:
            mock_now.side_effect = [now]
            subscription.cancel_subscription()

        return now

    def test_cancel_while_on_hold(self):
        subs = self.create_subscription()
        order = subs.generate_order()
        order.status = 'failed'
        order.save()
        self.assertEqual('on-hold', subs.status)

        cancelled_at = self.cancel(subs)

        subs.refresh_from_db()
        order = subs.latest_order()
        self.assertEqual('cancelled', subs.status)
        self.assertEqual(cancelled_at, subs.cancelled_at)
        self.assertEqual('cancelled', order.status)

    def test_cancel_while_active(self):
        subs = self.create_active_subscription()

        pending_cancellation_since = self.cancel(subs)

        subs.refresh_from_db()
        order = subs.latest_order()
        self.assertEqual('pending-cancellation', subs.status)
        self.assertEqual(subs.pending_cancellation_since, pending_cancellation_since)
        self.assertIsNone(subs.cancelled_at)
        self.assertEqual('paid', order.status)

    def test_cancel_while_pending_cancellation(self):
        subs = self.create_active_subscription()
        self.cancel(subs)

        subs.refresh_from_db()
        self.assertEqual('pending-cancellation', subs.status)

        with self.assertRaises(IncorrectStatusError):
            self.cancel(subs)

        subs.refresh_from_db()
        self.assertEqual('pending-cancellation', subs.status)
        self.assertIsNone(subs.cancelled_at)

    def test_cancel_while_cancelled(self):
        subs = self.create_subscription()
        subs.status = 'cancelled'
        subs.save()

        with self.assertRaises(IncorrectStatusError):
            self.cancel(subs)

        subs.refresh_from_db()
        self.assertEqual('cancelled', subs.status)

    def test_undo_pending_cancellation(self):
        subs = self.create_active_subscription()
        self.cancel(subs)

        subs.refresh_from_db()
        self.assertEqual('pending-cancellation', subs.status)

        subs.status = 'active'
        subs.save()

        subs.refresh_from_db()
        self.assertEqual('active', subs.status)
        self.assertIsNone(subs.cancelled_at)
        self.assertEqual('paid', subs.latest_order().status)


@mock.patch('looper.signals.subscription_expired.send')
class SubscriptionExpirationTest(AbstractLooperTestCase):
    gateway_name = 'braintree'

    log = log.getChild('SubscriptionExpirationTest')

    def expire(self, subscription: Subscription) -> datetime.datetime:
        log.debug('Expiring subscription pk=%d', subscription.pk)
        now = django.utils.timezone.now()
        with mock.patch('django.utils.timezone.now') as mock_now:
            mock_now.side_effect = [now]
            subscription.expire_subscription()

        return now

    def test_expire_while_on_hold(self, mock_subscription_expired_signal):
        subs = self.create_subscription()
        order = subs.generate_order()
        order.status = 'failed'
        order.save()
        self.assertEqual('on-hold', subs.status)

        expired_at = self.expire(subs)

        subs.refresh_from_db()
        order = subs.latest_order()
        self.assertEqual('expired', subs.status)
        self.assertEqual(expired_at, subs.cancelled_at)
        self.assertEqual('cancelled', order.status)

        entries_q = list(admin_log.entries_for(subs).order_by('-action_time'))
        self.assertEqual(entries_q[0].change_message, 'Status changed from on-hold to expired')

        mock_subscription_expired_signal.assert_called_once_with(
            sender=subs, old_status='on-hold'
        )

    def test_expire_while_active(self, mock_subscription_expired_signal):
        subs = self.create_active_subscription()

        with self.assertRaises(IncorrectStatusError):
            self.expire(subs)

        subs.refresh_from_db()
        self.assertEqual('active', subs.status)
        self.assertIsNone(subs.cancelled_at)

        mock_subscription_expired_signal.assert_not_called()

    def test_expire_while_pending_cancellation(self, mock_subscription_expired_signal):
        subs = self.create_active_subscription()
        subs.status = 'pending-cancellation'
        subs.save()

        with self.assertRaises(IncorrectStatusError):
            self.expire(subs)

        subs.refresh_from_db()
        self.assertEqual('pending-cancellation', subs.status)
        self.assertIsNone(subs.cancelled_at)

        mock_subscription_expired_signal.assert_not_called()

    def test_expire_while_cancelled(self, mock_subscription_expired_signal):
        subs = self.create_subscription()
        subs.status = 'cancelled'
        subs.save()

        with self.assertRaises(IncorrectStatusError):
            self.expire(subs)

        subs.refresh_from_db()
        self.assertEqual('cancelled', subs.status)

        mock_subscription_expired_signal.assert_not_called()


class NextPaymentTest(AbstractLooperTestCase):
    gateway_name = 'braintree'

    def test_after_creation(self):
        subs = self.create_subscription()
        self.assertFalse(subs.is_active)
        self.assertIsNone(subs.next_payment)

    def test_mid_month(self):
        subs = self.create_subscription()

        now = datetime.datetime(2018, 9, 12, 11, 59, 3, tzinfo=utc)
        with mock.patch('django.utils.timezone.now') as mock_now:
            mock_now.return_value = now
            subs.status = 'active'
            subs.save(update_fields={'status'})

        expect_renewal = datetime.datetime(2018, 10, 12, 11, 59, 3, tzinfo=utc)
        self.assertAlmostEqualDateTime(expect_renewal, subs.next_payment)

    def test_end_of_long_month_before_leapyear(self):
        subs = self.create_subscription()

        now = datetime.datetime(2016, 1, 31, 11, 59, 3, tzinfo=utc)
        with mock.patch('django.utils.timezone.now') as mock_now:
            mock_now.return_value = now
            subs.status = 'active'
            subs.save(update_fields={'status'})

        expect_renewal = datetime.datetime(2016, 2, 29, 11, 59, 3, tzinfo=utc)
        self.assertAlmostEqualDateTime(expect_renewal, subs.next_payment)


class MonthlyPriceTest(django.test.TestCase):
    def price(self, **kwargs) -> Money:
        subscription = Subscription(currency='EUR', price=1000, **kwargs)
        return subscription.monthly_rounded_price

    def test_daily(self):
        self.assertEqual(Money('EUR', 6000), self.price(interval_unit='day', interval_length=5))
        self.assertEqual(Money('EUR', 2308), self.price(interval_unit='day', interval_length=13))
        self.assertEqual(Money('EUR', 750), self.price(interval_unit='day', interval_length=40))

    def test_weekly(self):
        self.assertEqual(Money('EUR', 4286), self.price(interval_unit='week', interval_length=1))
        self.assertEqual(Money('EUR', 330), self.price(interval_unit='week', interval_length=13))

    def test_monthly(self):
        self.assertEqual(Money('EUR', 1000), self.price(interval_unit='month', interval_length=1))
        self.assertEqual(Money('EUR', 250), self.price(interval_unit='month', interval_length=4))

    def test_yearly(self):
        self.assertEqual(Money('EUR', 83), self.price(interval_unit='year', interval_length=1))
        self.assertEqual(Money('EUR', 5), self.price(interval_unit='year', interval_length=16))
