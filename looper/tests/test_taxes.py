from decimal import Decimal
from unittest import TestCase

import responses
import django.test

from looper import admin_log
from looper.money import Money
from looper.taxes import Taxable, TaxType, ProductType, vies_validate_number
from looper.tests import _mock_vies_response
import looper.models


class TaxableTest(TestCase):
    def test_no_charge(self):
        price = Money('EUR', 10000)
        taxable = Taxable(price, tax_type=TaxType.NO_CHARGE, tax_rate=Decimal(0))

        self.assertEqual(taxable.price_without_tax, price)
        self.assertEqual(taxable.price_with_tax, price)
        self.assertEqual(taxable.price, price)
        self.assertEqual(taxable.tax_type, TaxType.NO_CHARGE)
        self.assertEqual(taxable.tax_rate, 0)

        self.assertEqual(taxable.tax, Money('EUR', 0))
        self.assertFalse(taxable.tax_is_charged)
        self.assertEqual(taxable.format_tax_amount(), '')

    def test_vat_charge_20(self):
        price = Money('EUR', 990)
        taxable = Taxable(price, tax_type=TaxType.VAT_CHARGE, tax_rate=Decimal(20))

        self.assertEqual(taxable.price_without_tax, Money('EUR', 825))
        self.assertEqual(taxable.price_with_tax, price)
        self.assertEqual(taxable.price, price)
        self.assertEqual(taxable.tax_type, TaxType.VAT_CHARGE)
        self.assertEqual(taxable.tax_rate, 20)

        self.assertEqual(taxable.tax, Money('EUR', 165))
        self.assertTrue(taxable.tax_is_charged)
        self.assertEqual(taxable.format_tax_amount(), 'Inc. 20% VAT (€\xa01.65)')

    def test_vat_charge_21(self):
        price = Money('EUR', 990)
        taxable = Taxable(price, tax_type=TaxType.VAT_CHARGE, tax_rate=Decimal(21.0))

        self.assertEqual(taxable.price_without_tax, Money('EUR', 818))
        self.assertEqual(taxable.price_with_tax, price)
        self.assertEqual(taxable.price, price)
        self.assertEqual(taxable.tax_type, TaxType.VAT_CHARGE)
        self.assertEqual(taxable.tax_rate, 21)

        self.assertEqual(taxable.tax, Money('EUR', 172))
        self.assertTrue(taxable.tax_is_charged)
        self.assertEqual(taxable.format_tax_amount(), 'Inc. 21% VAT (€\xa01.72)')

    def test_vat_charge_19(self):
        price = Money('EUR', 990)
        taxable = Taxable(price, tax_type=TaxType.VAT_CHARGE, tax_rate=Decimal(19))

        self.assertEqual(taxable.price_without_tax, Money('EUR', 832))
        self.assertEqual(taxable.price_with_tax, price)
        self.assertEqual(taxable.price, price)
        self.assertEqual(taxable.tax_type, TaxType.VAT_CHARGE)
        self.assertEqual(taxable.tax_rate, 19)

        self.assertEqual(taxable.tax, Money('EUR', 158))
        self.assertTrue(taxable.tax_is_charged)
        self.assertEqual(taxable.format_tax_amount(), 'Inc. 19% VAT (€\xa01.58)')

    def test_vat_charge(self):
        price = Money('EUR', 10000)
        taxable = Taxable(price, tax_type=TaxType.VAT_CHARGE, tax_rate=Decimal(19.0))

        self.assertEqual(taxable.price_without_tax, Money('EUR', price._cents - 1597))
        self.assertEqual(taxable.price_with_tax, price)
        self.assertEqual(taxable.price, price)
        self.assertEqual(taxable.tax_type, TaxType.VAT_CHARGE)
        self.assertEqual(taxable.tax_rate, 19)

        self.assertEqual(taxable.tax, Money('EUR', 1597))
        self.assertTrue(taxable.tax_is_charged)
        self.assertEqual(taxable.format_tax_amount(), 'Inc. 19% VAT (€\xa015.97)')

    def test_vat_charge_round(self):
        price = Money('EUR', 12100)
        taxable = Taxable(price, tax_type=TaxType.VAT_CHARGE, tax_rate=Decimal(21))

        self.assertEqual(taxable.price_without_tax, Money('EUR', 10000))
        self.assertEqual(taxable.price_with_tax, price)
        self.assertEqual(taxable.price, price)
        self.assertEqual(taxable.tax_type, TaxType.VAT_CHARGE)
        self.assertEqual(taxable.tax_rate, 21)

        self.assertEqual(taxable.tax, Money('EUR', 2100))
        self.assertTrue(taxable.tax_is_charged)
        self.assertEqual(taxable.format_tax_amount(), 'Inc. 21% VAT (€\xa021.00)')

    def test_vat_reverse_charge_21(self):
        price = Money('EUR', 990)
        taxable = Taxable(price, tax_type=TaxType.VAT_REVERSE_CHARGE, tax_rate=Decimal(21))

        self.assertEqual(taxable.price_without_tax, taxable.price)
        self.assertEqual(taxable.price_with_tax, price)
        self.assertEqual(taxable.price, Money('EUR', 818))
        self.assertEqual(taxable.tax_type, TaxType.VAT_REVERSE_CHARGE)
        self.assertEqual(taxable.tax_rate, 21)

        self.assertEqual(taxable.tax, Money('EUR', 172))
        self.assertFalse(taxable.tax_is_charged)
        self.assertEqual(taxable.format_tax_amount(), '')

    def test_vat_reverse_charge_19(self):
        price = Money('EUR', 990)
        taxable = Taxable(price, tax_type=TaxType.VAT_REVERSE_CHARGE, tax_rate=Decimal(19))

        self.assertEqual(taxable.price_without_tax, taxable.price)
        self.assertEqual(taxable.price_with_tax, price)
        self.assertEqual(taxable.price, Money('EUR', 832))
        self.assertEqual(taxable.tax_type, TaxType.VAT_REVERSE_CHARGE)
        self.assertEqual(taxable.tax_rate, 19)

        self.assertEqual(taxable.tax, Money('EUR', 158))
        self.assertFalse(taxable.tax_is_charged)
        self.assertEqual(taxable.format_tax_amount(), '')

    def test_vat_reverse_charge_round(self):
        price = Money('EUR', 12100)
        taxable = Taxable(price, tax_type=TaxType.VAT_REVERSE_CHARGE, tax_rate=Decimal(21))

        self.assertEqual(taxable.price_without_tax, taxable.price)
        self.assertEqual(taxable.price_with_tax, price)
        self.assertEqual(taxable.price, Money('EUR', 10000))
        self.assertEqual(taxable.tax_type, TaxType.VAT_REVERSE_CHARGE)
        self.assertEqual(taxable.tax_rate, 21)

        self.assertEqual(taxable.tax, Money('EUR', 2100))
        self.assertFalse(taxable.tax_is_charged)
        self.assertEqual(taxable.format_tax_amount(), '')

    def test_types_not_equal(self):
        price = Money('EUR', 10000)
        self.assertNotEqual(
            Taxable(price, tax_type=TaxType.VAT_REVERSE_CHARGE, tax_rate=Decimal(21.0)),
            Taxable(price, tax_type=TaxType.VAT_CHARGE, tax_rate=Decimal(21.0)),
        )

    def test_rates_not_equal(self):
        price = Money('EUR', 10000)
        self.assertNotEqual(
            Taxable(price, tax_type=TaxType.VAT_CHARGE, tax_rate=Decimal(19.0)),
            Taxable(price, tax_type=TaxType.VAT_CHARGE, tax_rate=Decimal(21.0)),
        )

    def test_price_not_equal(self):
        price_a = Money('EUR', 10000)
        price_b = Money('EUR', 9900)
        self.assertNotEqual(
            Taxable(price_a, tax_type=TaxType.VAT_CHARGE, tax_rate=Decimal(21.0)),
            Taxable(price_b, tax_type=TaxType.VAT_CHARGE, tax_rate=Decimal(21.0)),
        )

    def test_equal(self):
        price = Money('EUR', 9900)
        self.assertEqual(
            Taxable(price, tax_type=TaxType.VAT_CHARGE, tax_rate=Decimal(21.0)),
            Taxable(price, tax_type=TaxType.VAT_CHARGE, tax_rate=Decimal(21.0)),
        )

    def test_rate_with_fractions(self):
        taxable = Taxable(Money('EUR', 990), tax_type=TaxType.VAT_CHARGE, tax_rate=Decimal(21.6))

        self.assertEqual(taxable.price_with_tax, Money('EUR', 990))
        self.assertEqual(taxable.price, Money('EUR', 990))
        self.assertEqual(taxable.tax_type, TaxType.VAT_CHARGE)
        self.assertEqual(taxable.tax_rate, Decimal('21.6'))

        self.assertEqual(taxable.tax, Money('EUR', 176))
        self.assertTrue(taxable.tax_is_charged)
        self.assertEqual(taxable.format_tax_amount(), 'Inc. 21.6% VAT (€ 1.76)')


class TaxTypeTest(TestCase):
    def test_get_for_electronic_service_mc_non_business(self):
        tax_type, tax_rate = ProductType('GES').get_tax('MC', is_business=False)

        self.assertEqual(tax_type, TaxType.VAT_CHARGE)
        self.assertEqual(tax_rate, 20)

    def test_get_for_electronic_service_nl_non_business(self):
        tax_type, tax_rate = ProductType('GES').get_tax('IE', is_business=False)

        self.assertEqual(tax_type, TaxType.VAT_CHARGE)
        self.assertEqual(tax_rate, 23)

    def test_get_for_electronic_service_nl_business(self):
        tax_type, tax_rate = ProductType('GES').get_tax('NL', is_business=True)

        self.assertEqual(tax_type, TaxType.VAT_CHARGE)
        self.assertEqual(tax_rate, 21)

    def test_get_for_electronic_service_de_non_business(self):
        tax_type, tax_rate = ProductType('GES').get_tax('DE', is_business=False)

        self.assertEqual(tax_type, TaxType.VAT_CHARGE)
        self.assertEqual(tax_rate, 19)

    def test_get_for_electronic_service_ie_non_business(self):
        tax_type, tax_rate = ProductType('GES').get_tax('IE', is_business=False)

        self.assertEqual(tax_type, TaxType.VAT_CHARGE)
        self.assertEqual(tax_rate, 23)

    def test_get_for_electronic_service_de_business(self):
        tax_type, tax_rate = ProductType('GES').get_tax('DE', is_business=True)

        self.assertEqual(tax_type, TaxType.VAT_REVERSE_CHARGE)
        self.assertEqual(tax_rate, 19)

    def test_get_for_electronic_service_it_non_business(self):
        tax_type, tax_rate = ProductType('GES').get_tax('IT', is_business=False)

        self.assertEqual(tax_type, TaxType.VAT_CHARGE)
        self.assertEqual(tax_rate, 22)

    def test_get_for_electronic_service_it_business(self):
        tax_type, tax_rate = ProductType('GES').get_tax('IT', is_business=True)

        self.assertEqual(tax_type, TaxType.VAT_REVERSE_CHARGE)
        self.assertEqual(tax_rate, 22)

    def test_get_for_electronic_service_us_non_business(self):
        tax_type, tax_rate = ProductType('GES').get_tax('US', is_business=False)

        self.assertEqual(tax_type, TaxType.NO_CHARGE)
        self.assertEqual(tax_rate, 0)

    def test_get_for_electronic_service_us_business(self):
        tax_type, tax_rate = ProductType('GES').get_tax('US', is_business=True)

        self.assertEqual(tax_type, TaxType.NO_CHARGE)
        self.assertEqual(tax_rate, 0)


class ChangeTaxTest(django.test.TestCase):
    fixtures = ['systemuser']

    def test_get_for_electronic_service_can_change_tax_rule_rate(self):
        tax_type, tax_rate = ProductType('GES').get_tax('GB', is_business=False)

        self.assertEqual(tax_type, TaxType.NO_CHARGE)
        self.assertEqual(tax_rate, 0)

        # Add a tax rule for GB (with default product type for electronic services)
        tax_rule = looper.models.TaxRule(country='GB', rate=20)
        tax_rule.save()
        tax_type, tax_rate = ProductType('GES').get_tax('GB', is_business=False)

        # Now a VAT applies with 20% rate
        self.assertEqual(tax_type, TaxType.VAT_CHARGE)
        self.assertEqual(tax_rate, 20)

        # Modify the rate
        tax_rule.rate = 21
        tax_rule.save(update_fields={'rate'})
        entries_q = admin_log.entries_for(tax_rule)
        self.assertEqual(entries_q.count(), 1)
        self.assertEqual(entries_q.first().change_message, 'Rate changed from 20% to 21%')
        tax_type, tax_rate = ProductType('GES').get_tax('GB', is_business=False)

        # Now a VAT applies with 21% rate
        self.assertEqual(tax_type, TaxType.VAT_CHARGE)
        self.assertEqual(tax_rate, 21)

    def test_get_for_electronic_service_can_change_tax_rule_rate_to_fractions(self):
        tax_rule = looper.models.TaxRule.objects.get(country='NL')

        # Modify the rate to something with fractions
        tax_rule.rate = 21.6
        tax_rule.save(update_fields={'rate'})
        entries_q = admin_log.entries_for(tax_rule)
        self.assertEqual(entries_q.count(), 1)
        self.assertEqual(entries_q.first().change_message, 'Rate changed from 21% to 21.6%')
        tax_type, tax_rate = ProductType('GES').get_tax('NL', is_business=False)

        # Now a VAT applies with 21.6% rate
        self.assertEqual(tax_type, TaxType.VAT_CHARGE)
        self.assertEqual(tax_rate, Decimal('21.6'))


class ValidateVATNumberVIESTest(TestCase):
    def test_invalid_(self):
        is_valid, response = vies_validate_number('1234123')

        self.assertFalse(is_valid)
        self.assertEqual(response, {})

    def test_valid(self):
        is_valid, response = vies_validate_number('NL818152011B01')

        self.assertTrue(is_valid)
        self.assertEqual(response['valid'], True)
        self.assertEqual(response['countryCode'], 'NL')
        self.assertEqual(response['vatNumber'], '818152011B01')

    def test_valid_has_space_separators(self):
        is_valid, response = vies_validate_number('NL  818 152011\tB01')

        self.assertTrue(is_valid)
        self.assertEqual(response['valid'], True)
        self.assertEqual(response['countryCode'], 'NL')
        self.assertEqual(response['vatNumber'], '818152011B01')


class ValidateVATNumberVIESMockedTest(TestCase):
    @responses.activate
    def test_invalid(self):
        _mock_vies_response(is_valid=False)

        is_valid, response = vies_validate_number('1234123')

        self.assertFalse(is_valid)
        self.assertEqual(response, {})

    @responses.activate
    def test_valid(self):
        _mock_vies_response(is_valid=True)

        is_valid, response = vies_validate_number('NL818152011B01')

        self.assertTrue(is_valid)
        self.assertEqual(response['valid'], True)
        self.assertEqual(response['countryCode'], 'NL')
        self.assertEqual(response['vatNumber'], '818152011B01')

    @responses.activate
    def test_valid_has_space_separators(self):
        _mock_vies_response(is_valid=True)

        is_valid, response = vies_validate_number('NL  818 152011\tB01')

        self.assertTrue(is_valid)
        self.assertEqual(response['valid'], True)
        self.assertEqual(response['countryCode'], 'NL')
        self.assertEqual(response['vatNumber'], '818152011B01')

    @responses.activate
    def test_broken(self):
        _mock_vies_response(is_broken=True)

        is_valid, response = vies_validate_number('NL 818152011B01')

        self.assertIsNone(is_valid)
        self.assertEqual(response, {})
