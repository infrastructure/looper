import datetime
from typing import Optional, Tuple
from unittest import mock

from django.contrib.auth import get_user_model
from django.dispatch import receiver
from django.test import override_settings
from django.urls import reverse, reverse_lazy
from django.utils import timezone
from django.http import HttpResponse
import responses

from looper import admin_log
from looper.exceptions import GatewayError
from .. import models, signals, gateways
from . import AbstractLooperTestCase

User = get_user_model()
CHECKOUT = 'looper-braintree:checkout'


# Prevent communication with Google's reCAPTCHA API.
@override_settings(GOOGLE_RECAPTCHA_SECRET_KEY='')
class AbstractCheckoutTestCase(AbstractLooperTestCase):
    fixtures = ['gateways', 'devfund', 'testuser', 'systemuser']
    checkout_url = reverse_lazy(CHECKOUT, kwargs={'plan_id': 2, 'plan_variation_id': 5})

    def setUp(self):
        super().setUp()
        self.user = User.objects.get(email='harry@blender.org')


# The signed-cookies session engine doesn't play nice with the test client.
@override_settings(SESSION_ENGINE='django.contrib.sessions.backends.file')
class BraintreeClientTokenTestCase(AbstractCheckoutTestCase):
    gateway_name = 'braintree'

    def set_session(self, token: str, expire: str):
        session = self.client.session
        if token:
            session['PAYMENT_GATEWAY_CLIENT_TOKEN_1_USD'] = token
        if expire:
            session['PAYMENT_GATEWAY_CLIENT_TOKEN_1_USD_expire'] = expire
        session.save()
        return session

    @mock.patch('braintree.client_token_gateway.ClientTokenGateway.generate')
    def test_client_token_happy(self, mock_generate):
        mock_generate.side_effect = ['mock-client-token']

        self.client.force_login(self.user)
        r = self.client.get(self.checkout_url)
        self.assertEqual(r.status_code, 200)

        mock_generate.assert_called_once()

    @mock.patch('braintree.client_token_gateway.ClientTokenGateway.generate')
    def test_token_in_session__no_expiry_in_session(self, mock_generate):
        mock_generate.side_effect = ['mock-client-token']

        self.client.force_login(self.user)
        self.set_session('token-in-session', '')
        r = self.client.get(self.checkout_url)
        self.assertEqual(r.status_code, 200)

        # Without expiry in the session, the cached token should not be reused.
        mock_generate.assert_called_once()

    @mock.patch('braintree.client_token_gateway.ClientTokenGateway.generate')
    def test_token_in_session__expired_in_session(self, mock_generate):
        mock_generate.side_effect = ['mock-client-token']

        self.client.force_login(self.user)
        self.set_session('token-in-session', '2020-04-07 12:31:14')
        r = self.client.get(self.checkout_url)
        self.assertEqual(r.status_code, 200)

        # Expired in the session, the cached token should not be reused.
        mock_generate.assert_called_once()

    @mock.patch('braintree.client_token_gateway.ClientTokenGateway.generate')
    def test_token_in_session__expiry_just_right(self, mock_generate):
        mock_generate.side_effect = ['mock-client-token']

        expiry = datetime.datetime.now() + datetime.timedelta(minutes=5)
        self.client.force_login(self.user)

        self.set_session('token-in-session', expiry.isoformat())
        r = self.client.get(self.checkout_url)
        self.assertEqual(r.status_code, 200)

        # Not trustworthy in the session, the cached token should not be reused.
        mock_generate.assert_not_called()


class CheckoutTestCase(AbstractCheckoutTestCase):
    gateway_name = 'braintree'

    def setUp(self):
        super().setUp()

        # Do a GET first to mimick the normal request flow.
        self.client.force_login(self.user)
        r = self.client.get(self.checkout_url)
        self.assertEqual(r.status_code, 200)

    def test_checkout_create_missing_required_fields(self):
        self.client.force_login(self.user)

        payload = self.valid_payload_braintree.copy()
        del payload['full_name']
        r = self.client.post(self.checkout_url, data=payload)

        # Django doesn't use a different status code for failed form submissions.
        self.assertEqual(200, r.status_code)
        self.assertNothingHappened()

    def test_checkout_invalid_customer_ip_address(self):
        self.client.force_login(self.user)

        r = self.client.post(
            self.checkout_url,
            data=self.valid_payload_braintree,
            REMOTE_ADDR='bogus',
            HTTP_X_FORWARDED_FOR='bogus',
        )

        self.assertEqual(400, r.status_code, r)
        self.assertEqual(r.reason_phrase, 'Unable to proceed with the payment')
        self.assertNothingHappened()

    def assertNothingHappened(self):
        self.assertEqual(0, models.Subscription.objects.count())
        self.assertEqual(0, models.Order.objects.count())
        self.assertEqual(0, models.Transaction.objects.count())

    def test_checkout_create_invalid_plan(self):
        self.client.force_login(self.user)
        url = reverse_lazy(CHECKOUT, kwargs={'plan_id': 200, 'plan_variation_id': 5})
        r = self.client.post(url, data=self.valid_payload_braintree)
        self.assertEqual(r.status_code, 404)

    def test_checkout_create_consumed_nonce(self):
        self.client.force_login(self.user)
        payload = {
            **self.valid_payload_braintree,
            'payment_method_nonce': 'fake-consumed-nonce',
        }
        r = self.client.post(self.checkout_url, data=payload)
        self.assertEqual(200, r.status_code)
        self.assertNothingHappened()

    def test_checkout_failed_sca(self):
        self.client.force_login(self.user)
        url = reverse(CHECKOUT, kwargs={'plan_id': 2, 'plan_variation_id': 6})

        # Use a special enriched nonce that should have a failed SCA attached to it
        payload = {
            **self.valid_payload_braintree,
            'payment_method_nonce': 'fake-three-d-secure-visa-failed-authentication-nonce',
        }
        r = self.client.post(url, data=payload)

        # A validation error is displayed, no subscription activated
        self.assertEqual(200, r.status_code)
        self.assertIn('Gateway Rejected: three_d_secure', str(r.content))
        self.assertEqual('on-hold', models.Subscription.objects.first().status)

        transaction = models.Transaction.objects.first()
        # Transaction failed, SCA info was stored
        self.assertEqual('failed', transaction.status)
        self.assertEqual('recurring_first', transaction.source)
        self.assertIsNotNone(transaction.authentication)
        self.assertEqual('tds', transaction.authentication.authentication_type)
        self.assertEqual('authenticate_failed', transaction.authentication.status)
        # SCA was not marked as used
        self.assertIsNone(transaction.authentication.consumed_at)

    def test_checkout_failed_failure_message_and_code_stored_do_not_honor(self):
        self.client.force_login(self.user)
        pv = models.PlanVariation.objects.get(pk=6)
        # Change plan variation price to a value that will trigger a Declined processor response:
        pv.price = 200000
        pv.save(update_fields={'price'})
        url = reverse(CHECKOUT, kwargs={'plan_id': 2, 'plan_variation_id': pv.pk})

        payload = {
            **self.valid_payload_braintree,
            'payment_method_nonce': 'fake-valid-nonce',
        }
        r = self.client.post(url, data=payload)

        # A validation error is displayed, no subscription activated
        self.assertEqual(200, r.status_code)
        self.assertIn('Do Not Honor', str(r.content))
        self.assertEqual('on-hold', models.Subscription.objects.first().status)

        transaction = models.Transaction.objects.first()
        self.assertEqual('failed', transaction.status)
        self.assertEqual('recurring_first', transaction.source)
        # Processor response code has been stored
        self.assertEqual('2000', transaction.failure_code)
        self.assertEqual('Do Not Honor', transaction.failure_message)

    def test_checkout_create_valid_subscription(self):
        created_subs: Optional[models.Subscription] = None

        @receiver(signals.subscription_activated)
        def subs_activated(sender: models.Subscription, **kwargs):
            nonlocal created_subs
            created_subs = sender

        self.client.force_login(self.user)
        url = reverse(CHECKOUT, kwargs={'plan_id': 2, 'plan_variation_id': 6})
        r = self.client.post(url, data=self.valid_payload_braintree)
        self.assertEqual(302, r.status_code, r.content.decode()[0:100])

        self.assertIsNotNone(created_subs)
        self.assertEqual('active', created_subs.status)

        for key in self.client.session.keys():
            self.assertFalse(
                key.startswith('PAYMENT_GATEWAY_'), f'Did not expect {key!r} in the session'
            )

        # We should now be able to get the 'done' view for this order.
        order = created_subs.latest_order()
        transaction = order.latest_transaction()
        self.assertEqual(transaction.source, 'recurring_first')
        success_url = reverse(
            'looper:checkout_done',
            kwargs={
                'transaction_id': transaction.pk,
            },
        )
        self.assertEqual(success_url, r['Location'])
        resp = self.client.get(success_url)
        self.assertEqual(200, resp.status_code)

    def test_checkout_create_valid_subscription_successful_sca(self):
        created_subs: Optional[models.Subscription] = None

        @receiver(signals.subscription_activated)
        def subs_activated(sender: models.Subscription, **kwargs):
            nonlocal created_subs
            created_subs = sender

        self.client.force_login(self.user)
        url = reverse(CHECKOUT, kwargs={'plan_id': 2, 'plan_variation_id': 6})
        # Use a special enriched nonce that should have a successful SCA attached to it
        payload = {
            **self.valid_payload_braintree,
            'payment_method_nonce': 'fake-three-d-secure-visa-full-authentication-nonce',
        }
        r = self.client.post(url, data=payload)
        self.assertEqual(302, r.status_code, r.content.decode()[0:100])

        self.assertIsNotNone(created_subs)
        self.assertEqual('active', created_subs.status)

        order = created_subs.latest_order()
        transaction = order.latest_transaction()
        self.assertEqual('recurring_first', transaction.source)
        self.assertIsNotNone(transaction.authentication)
        self.assertEqual('tds', transaction.authentication.authentication_type)
        self.assertEqual('authenticate_successful', transaction.authentication.status)
        self.assertIsNotNone(transaction.authentication.consumed_at)

        # We should now be able to get the 'done' view for this order.
        success_url = reverse(
            'looper:checkout_done',
            kwargs={
                'transaction_id': transaction.pk,
            },
        )
        self.assertEqual(success_url, r['Location'])
        resp = self.client.get(success_url)
        self.assertEqual(200, resp.status_code)

    def test_bank(self):
        sig_receiver = mock.Mock()
        signals.subscription_activated.connect(sig_receiver)
        self.assertEqual(0, models.Subscription.objects.count())

        self.client.force_login(self.user)
        assert self.user.customer.billing_address.email, 'missing email'
        url = reverse(CHECKOUT, kwargs={'plan_id': 2, 'plan_variation_id': 6})
        resp = self.client.post(
            url,
            data={
                **self.valid_payload_braintree,
                'gateway': 'bank',
            },
        )
        self.assertEqual(302, resp.status_code, resp.content.decode())

        sig_receiver.assert_not_called()
        created_subs = models.Subscription.objects.first()
        self.assertIsNotNone(created_subs)
        self.assertEqual('on-hold', created_subs.status)

        order = created_subs.latest_order()
        assert order is not None
        self.assertEqual('created', order.status)
        self.assertIsNone(created_subs.latest_order().latest_transaction())

        for key in self.client.session.keys():
            self.assertFalse(
                key.startswith('PAYMENT_GATEWAY_'), f'Did not expect {key!r} in the session'
            )

        # We should now be able to get the 'done' view for this order.
        success_url = reverse(
            'looper:transactionless_checkout_done',
            kwargs={
                'gateway_name': 'bank',
                'pk': order.pk,
            },
        )
        self.assertEqual(success_url, resp['Location'])
        resp = self.client.get(success_url)
        self.assertEqual(200, resp.status_code)

    def _create_existing_subscription(self) -> Tuple[dict, models.Subscription]:
        # Do a bank payment; otherwise BrainTree detects too-fast, too-similar
        # transactions and flags them as duplicates.
        payload = {
            **self.valid_payload_braintree,
            'gateway': 'bank',
        }

        # Create a subscription first, so that a recent subscription exists.
        self.client.force_login(self.user)
        url = reverse(CHECKOUT, kwargs={'plan_id': 2, 'plan_variation_id': 6})
        r = self.client.post(url, data=payload)
        self.assertEqual(302, r.status_code, r.content.decode()[0:100])

        self.assertEqual(1, len(self.user.customer.subscription_set.all()))
        subs = self.user.customer.subscription_set.first()

        return payload, subs

    def test_checkout_recently_created_subscription(self):
        payload, existing_subscription = self._create_existing_subscription()

        # Try to create another subscription.
        url = reverse(CHECKOUT, kwargs={'plan_id': 1, 'plan_variation_id': 1})
        r = self.client.get(url)
        self.assertIn('You recently obtained ', r.content.decode())

        r = self.client.post(url, data=payload)
        self.assertEqual(200, r.status_code, r.content.decode())

        # There should still only be one subscription.
        self.assertEqual(1, len(self.user.customer.subscription_set.all()))

        # Approve creation of a new subscription.
        payload['approve_new_subscription'] = 'approve_new_subscription'
        r = self.client.post(url, data=payload)
        self.assertEqual(302, r.status_code, r.content.decode()[0:100])
        self.assertEqual(2, len(self.user.customer.subscription_set.all()))

    def test_checkout_old_existing_subscription(self):
        payload, existing_subscription = self._create_existing_subscription()
        existing_subscription.created_at = timezone.now() - timezone.timedelta(days=2)
        existing_subscription.save()

        # Try to create another subscription.
        url = reverse(CHECKOUT, kwargs={'plan_id': 1, 'plan_variation_id': 1})
        r = self.client.get(url)
        self.assertNotIn('You recently obtained ', r.content.decode())

        r = self.client.post(url, data=payload)
        self.assertEqual(302, r.status_code, r.content.decode()[0:100])
        self.assertEqual(2, len(self.user.customer.subscription_set.all()))

    def test_checkout_existing_cancelled_subscription(self):
        payload, existing_subscription = self._create_existing_subscription()
        existing_subscription.status = 'cancelled'
        existing_subscription.save()

        # Try to create another subscription.
        url = reverse(CHECKOUT, kwargs={'plan_id': 1, 'plan_variation_id': 1})
        r = self.client.get(url)
        self.assertNotIn('You recently obtained ', r.content.decode())

        r = self.client.post(url, data=payload)
        self.assertEqual(302, r.status_code, r.content.decode()[0:100])
        self.assertEqual(2, len(self.user.customer.subscription_set.all()))


class AccountlessCheckoutTestCase(AbstractCheckoutTestCase):
    gateway_name = 'braintree'

    def setUp(self):
        super().setUp()
        r = self.client.get(self.checkout_url)
        self.assertEqual(r.status_code, 200)
        self.assertEqual(models.Customer.objects.count(), 2)

    def assertNothingHappened(self):
        self.assertEqual(0, models.Subscription.objects.count())
        self.assertEqual(0, models.Order.objects.count())
        self.assertEqual(0, models.Transaction.objects.count())

    def test_checkout_create_missing_required_fields(self):
        payload = self.valid_payload_braintree.copy()
        del payload['full_name']
        r = self.client.post(self.checkout_url, data=payload)

        # Django doesn't use a different status code for failed form submissions.
        self.assertEqual(200, r.status_code)
        self.assertNothingHappened()

    def test_checkout_invalid_customer_ip_address(self):
        r = self.client.post(
            self.checkout_url,
            data=self.valid_payload_braintree,
            REMOTE_ADDR='bogus',
            HTTP_X_FORWARDED_FOR='bogus',
        )

        self.assertEqual(400, r.status_code, r)
        self.assertEqual(r.reason_phrase, 'Unable to proceed with the payment')
        self.assertNothingHappened()

    def test_checkout_create_invalid_plan(self):
        url = reverse_lazy(CHECKOUT, kwargs={'plan_id': 200, 'plan_variation_id': 5})
        r = self.client.post(url, data=self.valid_payload_braintree)
        self.assertEqual(r.status_code, 404)

    def test_checkout_create_consumed_nonce(self):
        payload = {
            **self.valid_payload_braintree,
            'payment_method_nonce': 'fake-consumed-nonce',
        }
        r = self.client.post(self.checkout_url, data=payload)
        self.assertEqual(200, r.status_code)
        self.assertNothingHappened()

    def test_checkout_failed_sca(self):
        url = reverse(CHECKOUT, kwargs={'plan_id': 2, 'plan_variation_id': 6})

        # Use a special enriched nonce that should have a failed SCA attached to it
        payload = {
            **self.valid_payload_braintree,
            'payment_method_nonce': 'fake-three-d-secure-visa-failed-authentication-nonce',
        }
        r = self.client.post(url, data=payload)

        # A validation error is displayed, no subscription activated
        self.assertEqual(200, r.status_code)
        self.assertIn('Gateway Rejected: three_d_secure', str(r.content))
        self.assertEqual('on-hold', models.Subscription.objects.first().status)

        transaction = models.Transaction.objects.first()
        # Transaction failed, SCA info was stored
        self.assertEqual('failed', transaction.status)
        self.assertEqual('recurring_first', transaction.source)
        self.assertIsNotNone(transaction.authentication)
        self.assertEqual('tds', transaction.authentication.authentication_type)
        self.assertEqual('authenticate_failed', transaction.authentication.status)
        # SCA was not marked as used
        self.assertIsNone(transaction.authentication.consumed_at)

    def test_checkout_failed_failure_message_and_code_stored_do_not_honor(self):
        pv = models.PlanVariation.objects.get(pk=6)
        # Change plan variation price to a value that will trigger a Declined processor response:
        pv.price = 200000
        pv.save(update_fields={'price'})
        url = reverse(CHECKOUT, kwargs={'plan_id': 2, 'plan_variation_id': pv.pk})

        payload = {
            **self.valid_payload_braintree,
            'payment_method_nonce': 'fake-valid-nonce',
        }
        r = self.client.post(url, data=payload)

        # A validation error is displayed, no subscription activated
        self.assertEqual(200, r.status_code)
        self.assertIn('Do Not Honor', str(r.content))
        self.assertEqual('on-hold', models.Subscription.objects.first().status)

        transaction = models.Transaction.objects.first()
        self.assertEqual('failed', transaction.status)
        self.assertEqual('recurring_first', transaction.source)
        # Processor response code has been stored
        self.assertEqual('2000', transaction.failure_code)
        self.assertEqual('Do Not Honor', transaction.failure_message)

    def test_checkout_create_valid_subscription(self):
        created_subs: Optional[models.Subscription] = None

        @receiver(signals.subscription_activated)
        def subs_activated(sender: models.Subscription, **kwargs):
            nonlocal created_subs
            created_subs = sender

        url = reverse(CHECKOUT, kwargs={'plan_id': 2, 'plan_variation_id': 6})
        r = self.client.post(url, data=self.valid_payload_braintree)
        self.assertEqual(302, r.status_code, r.content.decode()[0:100])

        self.assertIsNotNone(created_subs)
        self.assertEqual('active', created_subs.status)

        for key in self.client.session.keys():
            self.assertFalse(
                key.startswith('PAYMENT_GATEWAY_'), f'Did not expect {key!r} in the session'
            )

        # We should now be able to get the 'done' view for this order.
        order = created_subs.latest_order()
        transaction = order.latest_transaction()
        self.assertEqual(transaction.source, 'recurring_first')
        success_url = reverse(
            'looper:checkout_done',
            kwargs={
                'transaction_id': transaction.pk,
            },
        )
        token = models.LinkCustomerToken.objects.first().token
        self.assertEqual(success_url + f'?token={token}', r['Location'])
        resp = self.client.get(success_url)
        self.assertEqual(404, resp.status_code)
        resp = self.client.get(success_url + f'?token={token}')
        self.assertEqual(200, resp.status_code)

    def test_checkout_create_valid_subscription_successful_sca(self):
        created_subs: Optional[models.Subscription] = None

        @receiver(signals.subscription_activated)
        def subs_activated(sender: models.Subscription, **kwargs):
            nonlocal created_subs
            created_subs = sender

        url = reverse(CHECKOUT, kwargs={'plan_id': 2, 'plan_variation_id': 6})
        # Use a special enriched nonce that should have a successful SCA attached to it
        payload = {
            **self.valid_payload_braintree,
            'payment_method_nonce': 'fake-three-d-secure-visa-full-authentication-nonce',
        }
        r = self.client.post(url, data=payload)
        self.assertEqual(302, r.status_code, r.content.decode()[0:100])

        self.assertIsNotNone(created_subs)
        self.assertEqual('active', created_subs.status)

        order = created_subs.latest_order()
        transaction = order.latest_transaction()
        self.assertEqual('recurring_first', transaction.source)
        self.assertIsNotNone(transaction.authentication)
        self.assertEqual('tds', transaction.authentication.authentication_type)
        self.assertEqual('authenticate_successful', transaction.authentication.status)
        self.assertIsNotNone(transaction.authentication.consumed_at)

        # We should now be able to get the 'done' view for this order.
        success_url = reverse(
            'looper:checkout_done',
            kwargs={
                'transaction_id': transaction.pk,
            },
        )
        token = models.LinkCustomerToken.objects.first().token
        self.assertEqual(success_url + f'?token={token}', r['Location'])
        # The success page is not accessible without the token
        resp = self.client.get(success_url)
        self.assertEqual(404, resp.status_code)
        resp = self.client.get(success_url + f'?token={token}')
        self.assertEqual(200, resp.status_code)

    def test_bank(self):
        sig_receiver = mock.Mock()
        signals.subscription_activated.connect(sig_receiver)
        self.assertEqual(0, models.Subscription.objects.count())

        assert self.user.customer.billing_address.email, 'missing email'
        url = reverse(CHECKOUT, kwargs={'plan_id': 2, 'plan_variation_id': 6})
        resp = self.client.post(
            url,
            data={
                **self.valid_payload_braintree,
                'gateway': 'bank',
            },
        )
        self.assertEqual(302, resp.status_code, resp.content.decode())

        sig_receiver.assert_not_called()
        created_subs = models.Subscription.objects.first()
        self.assertIsNotNone(created_subs)
        self.assertEqual('on-hold', created_subs.status)

        order = created_subs.latest_order()
        assert order is not None
        self.assertEqual('created', order.status)
        self.assertIsNone(created_subs.latest_order().latest_transaction())

        for key in self.client.session.keys():
            self.assertFalse(
                key.startswith('PAYMENT_GATEWAY_'), f'Did not expect {key!r} in the session'
            )

        # We should now be able to get the 'done' view for this order.
        success_url = reverse(
            'looper:transactionless_checkout_done',
            kwargs={
                'gateway_name': 'bank',
                'pk': order.pk,
            },
        )
        token = models.LinkCustomerToken.objects.first().token
        self.assertEqual(success_url + f'?token={token}', resp['Location'])
        resp = self.client.get(success_url)
        self.assertEqual(404, resp.status_code)
        resp = self.client.get(success_url + f'?token={token}')
        self.assertEqual(200, resp.status_code)


@override_settings(
    GOOGLE_RECAPTCHA_SECRET_KEY='-secret-key-',
    GOOGLE_RECAPTCHA_SITE_KEY='-site-key-',
    ALLOWED_HOSTS=['testserver'],
)
class RecaptchaTest(AbstractCheckoutTestCase):
    gateway_name = 'braintree'

    httpmock = responses.RequestsMock()

    def setUp(self):
        super().setUp()

        # Do a GET first to mimick the normal request flow.
        self.client.force_login(self.user)
        r = self.client.get(self.checkout_url)
        self.assertEqual(r.status_code, 200)

    def _do_recaptcha_protected_request(self, client_code='the-captcha-code') -> HttpResponse:
        self.client.force_login(self.user)
        url = reverse(CHECKOUT, kwargs={'plan_id': 2, 'plan_variation_id': 6})
        payload = {
            **self.valid_payload_braintree,
            'gateway': 'bank',  # to avoid having to mock BrainTree traffic too.
            'g-recaptcha-response': client_code,
        }
        return self.client.post(url, data=payload)

    def _mock_recaptcha(self, response: dict):
        self.httpmock.add('POST', 'https://www.google.com/recaptcha/api/siteverify', json=response)

    @httpmock.activate
    def _test_recaptcha_unreachable(self):
        # Nothing mocked, so every HTTP request gives a connection error.
        r = self._do_recaptcha_protected_request()
        self.assertEqual(400, r.status_code, r.content.decode())
        self.assertIn('CAPTCHA', r.reason_phrase)
        self.assertEqual(1, len(self.httpmock.calls))

    @httpmock.activate
    def _test_recaptcha_invalid(self):
        self._mock_recaptcha(
            {
                'success': False,
                'challenge_ts': '2020-02-06T15:02:22+01:00',
                'hostname': 'testserver',
                'error-codes': ['invalid-input-response'],
            }
        )
        r = self._do_recaptcha_protected_request()
        self.assertEqual(400, r.status_code, r.content.decode())
        self.assertIn('CAPTCHA', r.reason_phrase)
        self.assertEqual(1, len(self.httpmock.calls))

    @httpmock.activate
    def _test_recaptcha_missing(self):
        r = self._do_recaptcha_protected_request(client_code='')
        self.assertEqual(400, r.status_code, r.content.decode())
        self.assertIn('CAPTCHA', r.reason_phrase)
        self.assertEqual(0, len(self.httpmock.calls))

    @httpmock.activate
    def _test_recaptcha_success_for_wrong_host(self):
        self._mock_recaptcha(
            {
                'success': True,
                'challenge_ts': '2020-02-06T15:02:22+01:00',
                'hostname': 'funny.blender.org',
            }
        )
        r = self._do_recaptcha_protected_request()
        self.assertEqual(400, r.status_code, r.content.decode())
        self.assertIn('CAPTCHA', r.reason_phrase)
        self.assertEqual(1, len(self.httpmock.calls))

    @httpmock.activate
    def _test_recaptcha_happy(self):
        self._mock_recaptcha(
            {
                'success': True,
                'challenge_ts': '2020-02-06T15:02:22+01:00',
                'hostname': 'testserver',
            }
        )
        r = self._do_recaptcha_protected_request()
        self.assertEqual(302, r.status_code, r.content.decode()[0:100])
        self.assertEqual(1, len(self.httpmock.calls))

    @override_settings(GOOGLE_RECAPTCHA_SECRET_KEY='')
    @httpmock.activate
    def _test_recaptcha_deactivated(self):
        r = self._do_recaptcha_protected_request()
        self.assertEqual(302, r.status_code, r.content.decode()[0:100])
        self.assertEqual(0, len(self.httpmock.calls))


class MockedCheckoutTest(AbstractCheckoutTestCase):
    fixtures = AbstractCheckoutTestCase.fixtures + ['mock-gateway']
    gateway_name = 'mock'

    valid_payload = {
        **AbstractCheckoutTestCase.valid_payload_braintree,
        'gateway': 'mock',
    }

    def setUp(self):
        super().setUp()

        # Do a GET first to mimick the normal request flow.
        self.client.force_login(self.user)

        with mock.patch('looper.gateways.MockableGateway.generate_client_token') as mock_gct:
            mock_gct.return_value = 'mock-client-auth-token'
            r = self.client.get(self.checkout_url)
        self.assertEqual(r.status_code, 200)

    def test_processor_declined(self):
        # The rest of the test assumes there aren't any order/subscription/transaction objects yet.
        self.assertEqual(0, models.Subscription.objects.count())
        self.assertEqual(0, models.Order.objects.count())
        self.assertEqual(0, models.Transaction.objects.count())

        with mock.patch('looper.gateways.MockableGateway.customer_create') as mock_cc, mock.patch(
            'looper.gateways.MockableGateway.payment_method_create'
        ) as mock_pmc, mock.patch(
            'looper.gateways.MockableGateway.transact_sale') as mock_ts, mock.patch(
                'looper.gateways.MockableGateway.generate_client_token') as mock_gct:
            # Client token is erased on failed charge, so generate_client_token is called
            mock_gct.return_value = 'mock-client-auth-token'
            mock_cc.return_value = 'mock-customer-id'

            mock_paymeth = mock.Mock(spec=gateways.PaymentMethodInfo)
            mock_paymeth.token = 'mock-payment-token'
            mock_paymeth.recognisable_name.return_value = 'mock-recognisable-name'
            mock_paymeth.type_for_database.return_value = 'cc'
            mock_paymeth.authentication = None
            mock_pmc.return_value = mock_paymeth

            mock_ts.side_effect = [GatewayError('Processor Declined')]

            r = self.client.post(self.checkout_url, data=self.valid_payload)

        # This should render the same page, with an error message.
        content = r.content.decode()
        self.assertEqual(200, r.status_code, content[0:100])
        self.assertIn('Processor Declined', content)

        # The transaction must be created, and thus also its order and subscription.
        self.assertEqual(1, models.Subscription.objects.count())
        self.assertEqual(1, models.Order.objects.count())
        self.assertEqual(1, models.Transaction.objects.count())

        subs_pk = models.Subscription.objects.first().pk
        order_pk = models.Order.objects.first().pk
        self.assertIn(f'<input type="hidden" name="subscription_pk" value="{subs_pk}"', content)
        self.assertIn(f'<input type="hidden" name="order_pk" value="{order_pk}"', content)

        # Paying correctly should not create a new subscription + order, but just pay for
        # the one that was already created.
        with mock.patch(
            'looper.gateways.MockableGateway.payment_method_create'
        ) as mock_pmc, mock.patch('looper.gateways.MockableGateway.transact_sale') as mock_ts:
            mock_cc.return_value = 'mock-customer-id'

            mock_paymeth = mock.Mock(spec=gateways.PaymentMethodInfo)
            mock_paymeth.token = 'mock-another-payment-token'
            mock_paymeth.recognisable_name.return_value = 'mock-another-recognisable-name'
            mock_paymeth.type_for_database.return_value = 'pa'
            mock_paymeth.authentication = None
            mock_pmc.return_value = mock_paymeth

            mock_ts.return_value = {'transaction_id': 'mock-transaction-id'}
            r = self.client.post(
                self.checkout_url,
                data={
                    **self.valid_payload,
                    'subscription_pk': subs_pk,
                    'order_pk': order_pk,
                },
            )

        content = r.content.decode()
        self.assertEqual(302, r.status_code, content[0:100])

        self.assertEqual(1, models.Subscription.objects.count())
        self.assertEqual(1, models.Order.objects.count())
        self.assertEqual(2, models.Transaction.objects.count())


class PayExistingOrder(AbstractCheckoutTestCase):
    gateway_name = 'braintree'

    def test_cannot_view_unless_logged_in_and_owns_order(self):
        subs = self.create_on_hold_subscription()
        order = subs.generate_order()
        url = reverse('looper-braintree:checkout_existing_order', kwargs={'order_id': order.pk})

        # Try visiting PayExistingOrder page anonymously
        r = self.client.get(url)
        self.assertEqual(403, r.status_code)

        # Try visiting PayExistingOrder page while logged in as someone else
        other_user = User.objects.create_user(username='another', email='another@example.com')
        self.client.force_login(other_user)
        r = self.client.get(url)
        self.assertEqual(403, r.status_code)

    def test_pay_for_failed_order(self):
        subs = self.create_on_hold_subscription()
        order = subs.generate_order()
        billing_address = subs.customer.billing_address
        billing_address.full_name = 'Гарри Поттер'
        billing_address.save()

        activated_subs: Optional[models.Subscription] = None

        @receiver(signals.subscription_activated)
        def subs_activated(sender: models.Subscription, **kwargs):
            nonlocal activated_subs
            activated_subs = sender

        self.client.force_login(self.user)
        url = reverse('looper-braintree:checkout_existing_order', kwargs={'order_id': order.pk})
        r = self.client.post(url, data=self.valid_payload_braintree)
        self.assertEqual(302, r.status_code, r.content.decode()[0:100])

        self.assertIsNotNone(activated_subs)
        self.assertEqual('active', activated_subs.status)

        for key in self.client.session.keys():
            self.assertFalse(
                key.startswith('PAYMENT_GATEWAY_'), f'Did not expect {key!r} in the session'
            )

    def test_pay_by_bank(self):
        subs = self.create_on_hold_subscription()
        order = subs.generate_order()
        self.assertEqual('automatic', subs.collection_method)
        self.assertEqual('automatic', order.collection_method)

        activated_subs: Optional[models.Subscription] = None

        @receiver(signals.subscription_activated)
        def subs_activated(sender: models.Subscription, **kwargs):
            nonlocal activated_subs
            activated_subs = sender

        self.client.force_login(self.user)
        url = reverse('looper-braintree:checkout_existing_order', kwargs={'order_id': order.pk})
        payload = {
            **self.valid_payload_braintree,
            'gateway': 'bank',
        }
        r = self.client.post(url, data=payload)
        self.assertEqual(302, r.status_code, r.content.decode()[0:100])

        self.assertIsNone(activated_subs)
        subs.refresh_from_db()
        self.assertEqual('on-hold', subs.status)
        # Subscription collection method should have changed to manual
        self.assertEqual('manual', subs.collection_method)
        # Order collection method should have changed to manual
        order.refresh_from_db()
        self.assertEqual('manual', order.collection_method)
        # Test that this change to was logged
        entries_q = admin_log.entries_for(subs)
        self.assertEqual(
            'Switching collection method from automatic to manual because'
            ' the new payment gateway only supports {&#x27;manual&#x27;}'
            f' for <a href="/admin/looper/order/{order.pk}/change/">order #{order.pk}</a>',
            entries_q[0].change_message,
        )
        self.assertEqual(
            'Switching payment method from Test payment method to Bank Transfer'
            f' for <a href="/admin/looper/order/{order.pk}/change/">order #{order.pk}</a>',
            entries_q[1].change_message,
        )
        entries_q = admin_log.entries_for(order)
        self.assertEqual(
            'Switching collection method from automatic to manual because'
            " the new payment gateway only supports {'manual'}",
            entries_q[0].change_message,
        )
        self.assertEqual(
            'Switching payment method from Test payment method to Bank Transfer',
            entries_q[1].change_message,
        )

        for key in self.client.session.keys():
            self.assertFalse(
                key.startswith('PAYMENT_GATEWAY_'), f'Did not expect {key!r} in the session'
            )

    def test_pay_invalid_customer_ip_address(self):
        subs = self.create_on_hold_subscription()
        order = subs.generate_order()

        self.client.force_login(self.user)
        url = reverse('looper-braintree:checkout_existing_order', kwargs={'order_id': order.pk})
        r = self.client.post(
            url,
            data=self.valid_payload_braintree,
            REMOTE_ADDR='bogus',
            HTTP_X_FORWARDED_FOR='bogus',
        )

        self.assertEqual(400, r.status_code, r)
        self.assertEqual(r.reason_phrase, 'Unable to proceed with the payment')


class DefaultPlanVariationTest(AbstractCheckoutTestCase):
    gateway_name = 'braintree'

    url = reverse_lazy('looper-braintree:checkout_new', kwargs={'plan_id': 2})

    def test_euro_ipv6(self):
        from .test_preferred_currency import EURO_IPV6

        self.client.force_login(self.user)
        resp = self.client.get(self.url, REMOTE_ADDR=EURO_IPV6)
        self.assertEqual(302, resp.status_code)

        # The redirect should point to the preferred EUR/monthly variation.
        checkout_url = reverse(CHECKOUT, kwargs={'plan_id': 2, 'plan_variation_id': 5})
        self.assertEqual(checkout_url, resp['Location'])

    def test_usa_ipv6_behind_proxy(self):
        from .test_preferred_currency import USA_IPV6

        self.client.force_login(self.user)
        resp = self.client.get(self.url, REMOTE_ADDR='::1', HTTP_X_FORWARDED_FOR=USA_IPV6)
        self.assertEqual(302, resp.status_code)

        # The redirect should point to the preferred USD/monthly variation.
        checkout_url = reverse(CHECKOUT, kwargs={'plan_id': 2, 'plan_variation_id': 6})
        self.assertEqual(checkout_url, resp['Location'])
