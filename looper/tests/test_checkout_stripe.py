from collections import namedtuple
from typing import Tuple
from unittest import mock

from django.contrib.auth import get_user_model
from django.urls import reverse, reverse_lazy
from django.utils import timezone
# from responses import _recorder
import responses

from .. import models, signals, stripe_utils
from . import AbstractLooperTestCase, ResponsesMixin

User = get_user_model()


class _BaseTestCase(AbstractLooperTestCase):
    fixtures = ['gateways', 'devfund', 'testuser', 'systemuser']
    checkout_url = reverse_lazy('looper:checkout', kwargs={'plan_id': 2, 'plan_variation_id': 5})
    gateway_name = 'stripe'

    def setUp(self):
        super().setUp()
        self.user = User.objects.get(email='harry@blender.org')


class CheckoutTestCase(_BaseTestCase):
    def setUp(self):
        super().setUp()

        # Do a GET first to mimick the normal request flow.
        self.client.force_login(self.user)
        r = self.client.get(self.checkout_url)
        self.assertEqual(r.status_code, 200)

    def test_checkout_create_missing_required_fields(self):
        self.client.force_login(self.user)

        payload = self.valid_payload.copy()
        del payload['full_name']
        r = self.client.post(self.checkout_url, data=payload)

        # Django doesn't use a different status code for failed form submissions.
        self.assertEqual(200, r.status_code)
        self.assertNothingHappened()

    def test_checkout_invalid_customer_ip_address(self):
        self.client.force_login(self.user)

        r = self.client.post(
            self.checkout_url,
            data=self.valid_payload,
            REMOTE_ADDR='bogus',
            HTTP_X_FORWARDED_FOR='bogus',
        )

        self.assertEqual(400, r.status_code, r)
        self.assertEqual(r.reason_phrase, 'Unable to proceed with the payment')
        self.assertNothingHappened()

    def assertNothingHappened(self):
        self.assertEqual(0, models.Subscription.objects.count())
        self.assertEqual(0, models.Order.objects.count())
        self.assertEqual(0, models.Transaction.objects.count())

    def test_checkout_create_invalid_plan(self):
        self.client.force_login(self.user)
        url = reverse_lazy('looper:checkout', kwargs={'plan_id': 200, 'plan_variation_id': 5})
        r = self.client.post(url, data=self.valid_payload)
        self.assertEqual(r.status_code, 404)

    def test_checkout_create_valid_subscription(self):
        self.client.force_login(self.user)
        session_url = 'https://example.com/session'
        with mock.patch('stripe.checkout.Session.create') as mock_stripe:
            Session = namedtuple('Session', ['url'])
            mock_stripe.return_value = Session(session_url)
            url = reverse('looper:checkout', kwargs={'plan_id': 2, 'plan_variation_id': 6})
            r = self.client.post(url, data=self.valid_payload)
            self.assertEqual(302, r.status_code, r.content.decode()[0:100])
            self.assertEqual(session_url, r['Location'])

        # TODO check order and subscription

    def test_bank(self):
        sig_receiver = mock.Mock()
        signals.subscription_activated.connect(sig_receiver)
        self.assertEqual(0, models.Subscription.objects.count())

        self.client.force_login(self.user)
        assert self.user.customer.billing_address.email, 'missing email'
        url = reverse('looper:checkout', kwargs={'plan_id': 2, 'plan_variation_id': 6})
        resp = self.client.post(
            url,
            data={
                **self.valid_payload,
                'gateway': 'bank',
            },
        )
        self.assertEqual(302, resp.status_code, resp.content.decode())

        sig_receiver.assert_not_called()
        created_subs = models.Subscription.objects.first()
        self.assertIsNotNone(created_subs)
        self.assertEqual('on-hold', created_subs.status)

        order = created_subs.latest_order()
        assert order is not None
        self.assertEqual('created', order.status)
        self.assertIsNone(created_subs.latest_order().latest_transaction())

        for key in self.client.session.keys():
            self.assertFalse(
                key.startswith('PAYMENT_GATEWAY_'), f'Did not expect {key!r} in the session'
            )

        # We should now be able to get the 'done' view for this order.
        success_url = reverse(
            'looper:transactionless_checkout_done',
            kwargs={
                'gateway_name': 'bank',
                'pk': order.pk,
            },
        )
        self.assertEqual(success_url, resp['Location'])
        resp = self.client.get(success_url)
        self.assertEqual(200, resp.status_code)

    def _create_existing_subscription(self) -> Tuple[dict, models.Subscription]:
        # Do a bank payment; otherwise BrainTree detects too-fast, too-similar
        # transactions and flags them as duplicates.
        payload = {
            **self.valid_payload,
            'gateway': 'bank',
        }

        # Create a subscription first, so that a recent subscription exists.
        self.client.force_login(self.user)
        url = reverse('looper:checkout', kwargs={'plan_id': 2, 'plan_variation_id': 6})
        r = self.client.post(url, data=payload)
        self.assertEqual(302, r.status_code, r.content.decode()[0:100])

        self.assertEqual(1, len(self.user.customer.subscription_set.all()))
        subs = self.user.customer.subscription_set.first()

        return payload, subs

    def test_checkout_recently_created_subscription(self):
        payload, existing_subscription = self._create_existing_subscription()

        # Try to create another subscription.
        url = reverse('looper:checkout', kwargs={'plan_id': 1, 'plan_variation_id': 1})
        r = self.client.get(url)
        self.assertIn('You recently obtained ', r.content.decode())

        r = self.client.post(url, data=payload)
        self.assertEqual(200, r.status_code, r.content.decode())

        # There should still only be one subscription.
        self.assertEqual(1, len(self.user.customer.subscription_set.all()))

        # Approve creation of a new subscription.
        payload['approve_new_subscription'] = 'approve_new_subscription'
        r = self.client.post(url, data=payload)
        self.assertEqual(302, r.status_code, r.content.decode()[0:100])
        self.assertEqual(2, len(self.user.customer.subscription_set.all()))

    def test_checkout_old_existing_subscription(self):
        payload, existing_subscription = self._create_existing_subscription()
        existing_subscription.created_at = timezone.now() - timezone.timedelta(days=2)
        existing_subscription.save()

        # Try to create another subscription.
        url = reverse('looper:checkout', kwargs={'plan_id': 1, 'plan_variation_id': 1})
        r = self.client.get(url)
        self.assertNotIn('You recently obtained ', r.content.decode())

        r = self.client.post(url, data=payload)
        self.assertEqual(302, r.status_code, r.content.decode()[0:100])
        self.assertEqual(2, len(self.user.customer.subscription_set.all()))

    def test_checkout_existing_cancelled_subscription(self):
        payload, existing_subscription = self._create_existing_subscription()
        existing_subscription.status = 'cancelled'
        existing_subscription.save()

        # Try to create another subscription.
        url = reverse('looper:checkout', kwargs={'plan_id': 1, 'plan_variation_id': 1})
        r = self.client.get(url)
        self.assertNotIn('You recently obtained ', r.content.decode())

        r = self.client.post(url, data=payload)
        self.assertEqual(302, r.status_code, r.content.decode()[0:100])
        self.assertEqual(2, len(self.user.customer.subscription_set.all()))


class AccountlessCheckoutTestCase(_BaseTestCase):
    def setUp(self):
        super().setUp()
        r = self.client.get(self.checkout_url)
        self.assertEqual(r.status_code, 200)
        self.assertEqual(models.Customer.objects.count(), 2)

    def assertNothingHappened(self):
        self.assertEqual(0, models.Subscription.objects.count())
        self.assertEqual(0, models.Order.objects.count())
        self.assertEqual(0, models.Transaction.objects.count())

    def test_checkout_create_missing_required_fields(self):
        payload = self.valid_payload.copy()
        del payload['full_name']
        r = self.client.post(self.checkout_url, data=payload)

        # Django doesn't use a different status code for failed form submissions.
        self.assertEqual(200, r.status_code)
        self.assertNothingHappened()

    def test_checkout_invalid_customer_ip_address(self):
        r = self.client.post(
            self.checkout_url,
            data=self.valid_payload,
            REMOTE_ADDR='bogus',
            HTTP_X_FORWARDED_FOR='bogus',
        )

        self.assertEqual(400, r.status_code, r)
        self.assertEqual(r.reason_phrase, 'Unable to proceed with the payment')
        self.assertNothingHappened()

    def test_checkout_create_invalid_plan(self):
        url = reverse_lazy('looper:checkout', kwargs={'plan_id': 200, 'plan_variation_id': 5})
        r = self.client.post(url, data=self.valid_payload)
        self.assertEqual(r.status_code, 404)

    def test_checkout_create_valid_subscription(self):
        session_url = 'https://example.com/session'
        with mock.patch('stripe.checkout.Session.create') as mock_stripe:
            Session = namedtuple('Session', ['url'])
            mock_stripe.return_value = Session(session_url)
            url = reverse('looper:checkout', kwargs={'plan_id': 2, 'plan_variation_id': 6})
            r = self.client.post(url, data=self.valid_payload)
            self.assertEqual(302, r.status_code, r.content.decode()[0:100])
            self.assertEqual(session_url, r['Location'])

        # TODO check order and subscription

    def test_bank(self):
        sig_receiver = mock.Mock()
        signals.subscription_activated.connect(sig_receiver)
        self.assertEqual(0, models.Subscription.objects.count())

        assert self.user.customer.billing_address.email, 'missing email'
        url = reverse('looper:checkout', kwargs={'plan_id': 2, 'plan_variation_id': 6})
        resp = self.client.post(
            url,
            data={
                **self.valid_payload,
                'gateway': 'bank',
            },
        )
        self.assertEqual(302, resp.status_code, resp.content.decode())

        sig_receiver.assert_not_called()
        created_subs = models.Subscription.objects.first()
        self.assertIsNotNone(created_subs)
        self.assertEqual('on-hold', created_subs.status)

        order = created_subs.latest_order()
        assert order is not None
        self.assertEqual('created', order.status)
        self.assertIsNone(created_subs.latest_order().latest_transaction())

        for key in self.client.session.keys():
            self.assertFalse(
                key.startswith('PAYMENT_GATEWAY_'), f'Did not expect {key!r} in the session'
            )

        # We should now be able to get the 'done' view for this order.
        success_url = reverse(
            'looper:transactionless_checkout_done',
            kwargs={
                'gateway_name': 'bank',
                'pk': order.pk,
            },
        )
        token = models.LinkCustomerToken.objects.first().token
        self.assertEqual(success_url + f'?token={token}', resp['Location'])
        resp = self.client.get(success_url)
        self.assertEqual(404, resp.status_code)
        resp = self.client.get(success_url + f'?token={token}')
        self.assertEqual(200, resp.status_code)


class PayExistingOrder(_BaseTestCase):
    def test_cannot_view_unless_logged_in_and_owns_order(self):
        subs = self.create_on_hold_subscription()
        order = subs.generate_order()
        url = reverse('looper:checkout_existing_order', kwargs={'order_id': order.pk})

        # Try visiting PayExistingOrder page anonymously
        r = self.client.get(url)
        self.assertEqual(302, r.status_code)

        # Try visiting PayExistingOrder page while logged in as someone else
        other_user = User.objects.create_user(username='another', email='another@example.com')
        self.client.force_login(other_user)
        r = self.client.get(url)
        self.assertEqual(404, r.status_code)

    def test_pay_for_failed_order(self):
        subs = self.create_on_hold_subscription()
        order = subs.generate_order()
        order.status = 'failed'
        order.save()
        billing_address = subs.customer.billing_address
        billing_address.full_name = 'Гарри Поттер'
        billing_address.save()

        self.client.force_login(self.user)
        url = reverse('looper:checkout_existing_order', kwargs={'order_id': order.pk})
        session_url = 'https://example.com/session'
        with mock.patch('stripe.checkout.Session.create') as mock_stripe:
            Session = namedtuple('Session', ['url'])
            mock_stripe.return_value = Session(session_url)
            r = self.client.get(url)
        self.assertEqual(302, r.status_code, r.content.decode()[0:100])
        self.assertEqual(session_url, r['Location'])

        # TODO success url handling to check active subscription

    def test_pay_invalid_customer_ip_address(self):
        subs = self.create_on_hold_subscription()
        order = subs.generate_order()

        self.client.force_login(self.user)
        url = reverse('looper:checkout_existing_order', kwargs={'order_id': order.pk})
        r = self.client.get(
            url,
            REMOTE_ADDR='bogus',
            HTTP_X_FORWARDED_FOR='bogus',
        )

        self.assertEqual(400, r.status_code, r)
        self.assertEqual(r.reason_phrase, 'Unable to proceed with the payment')


class DefaultPlanVariationTest(_BaseTestCase):
    url = reverse_lazy('looper:checkout_new', kwargs={'plan_id': 2})

    def test_euro_ipv6(self):
        from .test_preferred_currency import EURO_IPV6

        self.client.force_login(self.user)
        resp = self.client.get(self.url, REMOTE_ADDR=EURO_IPV6)
        self.assertEqual(302, resp.status_code)

        # The redirect should point to the preferred EUR/monthly variation.
        checkout_url = reverse('looper:checkout', kwargs={'plan_id': 2, 'plan_variation_id': 5})
        self.assertEqual(checkout_url, resp['Location'])

    def test_usa_ipv6_behind_proxy(self):
        from .test_preferred_currency import USA_IPV6

        self.client.force_login(self.user)
        resp = self.client.get(self.url, REMOTE_ADDR='::1', HTTP_X_FORWARDED_FOR=USA_IPV6)
        self.assertEqual(302, resp.status_code)

        # The redirect should point to the preferred USD/monthly variation.
        checkout_url = reverse('looper:checkout', kwargs={'plan_id': 2, 'plan_variation_id': 6})
        self.assertEqual(checkout_url, resp['Location'])


@mock.patch('looper.gateways.stripe.webhook.WebhookSignature.verify_header', return_value=True)
class SuccessUrl(ResponsesMixin, _BaseTestCase):
    responses_files = ['webhook_after_success_url_does_nothing.yaml']

    # @_recorder.record(file_path=f'{ResponsesMixin.responses_file_path}/webhook_after_success_url_does_nothing.yaml')
    def test_webhook_after_success_url_does_nothing(self, _):
        url = reverse('looper:checkout', kwargs={'plan_id': 2, 'plan_variation_id': 6})
        resp = self.client.post(url, data=self.valid_payload)
        order = models.Order.objects.all().order_by('-id').first()
        # tweak the response because this second test expects a different order_id in metadata
        for _ in responses.registered():
            if 'expand%5B0%5D=payment_intent' in _.url or '/payment_intents/' in _.url:
                assert '\"order_id\": \"1' in _.body
                _.body = _.body.replace('\"order_id\": \"1', f'\"order_id\": \"{order.pk}')
        self.assertIsNone(order.latest_transaction())
        # print('Continue to Stripe:', resp['Location'])
        # input('Press Enter when ready to continue')
        # Parse the Stripe's session ID from the redirect URL we got.
        stripe_session_id = resp['Location'].split('#')[0].split('/')[-1]
        success_url = reverse(
            'looper:stripe_success',
            kwargs={
                'pk': order.pk,
                'stripe_session_id': stripe_session_id,
            },
        )
        self.client.get(success_url)
        # assert new transaction, no new orders
        order.refresh_from_db()
        transaction = order.latest_transaction()
        self.assertIsNotNone(transaction)
        # Check that subscription was activated and order marked as paid
        subscription = order.subscription
        self.assertEqual('succeeded', transaction.status)
        self.assertEqual('paid', subscription.latest_order().status)
        self.assertEqual('active', subscription.status)

        webhook_payload = {
            'data': {
                'object': {
                    'id': order.pi_id,
                    'object': 'payment_intent',
                    'latest_charge': transaction.transaction_id,
                    'payment_method': transaction.payment_method.token,
                    'status': 'succeeded',
                }
            },
            'type': 'payment_intent.succeeded'
        }
        stripe_utils.upsert_order_from_payment_intent_and_product(
            webhook_payload,
            123,  # second argument doesn't matter in case of a subscription order
        )
        # no new objects created
        latest_order = models.Order.objects.all().order_by('-id').first()
        self.assertEqual(latest_order, order)
        self.assertEqual(order.latest_transaction(), transaction)

    # @_recorder.record(file_path=f'{ResponsesMixin.responses_file_path}/webhook_after_success_url_does_nothing.yaml')
    def test_webhook_before_success_url_does_nothing(self, _):
        url = reverse('looper:checkout', kwargs={'plan_id': 2, 'plan_variation_id': 6})
        resp = self.client.post(url, data=self.valid_payload)
        order = models.Order.objects.all().order_by('-id').first()
        # tweak the response because this second test expects a different order_id in metadata
        for _ in responses.registered():
            if 'expand%5B0%5D=payment_intent' in _.url or '/payment_intents/' in _.url:
                assert '\"order_id\": \"1' in _.body
                _.body = _.body.replace('\"order_id\": \"1', f'\"order_id\": \"{order.pk}')
        self.assertIsNone(order.latest_transaction())
        # webhook comes before we hit success_url
        webhook_payload = {
            'data': {
                'object': {
                    'id': 'pi_3OG1S0Kh8wGqs1ax0H98AgKw',
                    'latest_charge': 'ch_3OG1S0Kh8wGqs1ax0UttLm3N',
                    'object': 'payment_intent',
                    'payment_method': 'pm_1OG1RzKh8wGqs1axLJSCfYXW',
                    'status': 'succeeded'
                }
            },
            'type': 'payment_intent.succeeded'
        }
        stripe_utils.upsert_order_from_payment_intent_and_product(
            webhook_payload,
            123,  # second argument doesn't matter in case of a subscription order
        )
        # assert new transaction, no new orders
        order.refresh_from_db()
        transaction = order.latest_transaction()
        self.assertIsNotNone(transaction)
        # Check that subscription was activated and order marked as paid
        subscription = order.subscription
        self.assertEqual('succeeded', transaction.status)
        self.assertEqual('paid', subscription.latest_order().status)
        self.assertEqual('active', subscription.status)

        # and now we trigger success_url, expect no changes
        stripe_session_id = resp['Location'].split('#')[0].split('/')[-1]
        success_url = reverse(
            'looper:stripe_success',
            kwargs={
                'pk': order.pk,
                'stripe_session_id': stripe_session_id,
            },
        )
        self.client.get(success_url)
        # no new objects created
        latest_order = models.Order.objects.all().order_by('-id').first()
        self.assertEqual(latest_order, order)
        self.assertEqual(order.latest_transaction(), transaction)

    # @_recorder.record(file_path=f'{ResponsesMixin.responses_file_path}/webhook_after_success_url_does_nothing.yaml')
    def test_paying_for_a_failed_order(self, _):
        url = reverse('looper:checkout', kwargs={'plan_id': 2, 'plan_variation_id': 6})
        resp = self.client.post(url, data=self.valid_payload)
        order = models.Order.objects.all().order_by('-id').first()

        order.status = 'failed'
        order.save()
        order.subscription.status = 'on-hold'
        order.subscription.save()

        # tweak the response because this second test expects a different order_id in metadata
        for _ in responses.registered():
            if 'expand%5B0%5D=payment_intent' in _.url or '/payment_intents/' in _.url:
                assert '\"order_id\": \"1' in _.body
                _.body = _.body.replace('\"order_id\": \"1', f'\"order_id\": \"{order.pk}')
        self.assertIsNone(order.latest_transaction())
        # print('Continue to Stripe:', resp['Location'])
        # input('Press Enter when ready to continue')
        # Parse the Stripe's session ID from the redirect URL we got.
        stripe_session_id = resp['Location'].split('#')[0].split('/')[-1]
        success_url = reverse(
            'looper:stripe_success',
            kwargs={
                'pk': order.pk,
                'stripe_session_id': stripe_session_id,
            },
        )
        self.client.get(success_url)
        # assert new transaction, no new orders
        order.refresh_from_db()
        transaction = order.latest_transaction()
        self.assertIsNotNone(transaction)
        # Check that subscription was activated and order marked as paid
        subscription = order.subscription
        subscription.refresh_from_db()
        self.assertEqual('succeeded', transaction.status)
        self.assertEqual('paid', subscription.latest_order().status)
        self.assertEqual('active', subscription.status)

        webhook_payload = {
            'data': {
                'object': {
                    'id': order.pi_id,
                    'object': 'payment_intent',
                    'latest_charge': transaction.transaction_id,
                    'payment_method': transaction.payment_method.token,
                    'status': 'succeeded',
                }
            },
            'type': 'payment_intent.succeeded'
        }
        stripe_utils.upsert_order_from_payment_intent_and_product(
            webhook_payload,
            123,  # second argument doesn't matter in case of a subscription order
        )
        # no new objects created
        latest_order = models.Order.objects.all().order_by('-id').first()
        self.assertEqual(latest_order, order)
        self.assertEqual(order.latest_transaction(), transaction)

    # @_recorder.record(file_path=f'{ResponsesMixin.responses_file_path}/webhook_after_success_url_does_nothing.yaml')
    def test_cant_pay_for_a_cancelled_order(self, _):
        url = reverse('looper:checkout', kwargs={'plan_id': 2, 'plan_variation_id': 6})
        resp = self.client.post(url, data=self.valid_payload)
        order = models.Order.objects.all().order_by('-id').first()

        order.status = 'cancelled'
        order.save()
        order.subscription.status = 'on-hold'
        order.subscription.save()

        # tweak the response because this second test expects a different order_id in metadata
        for _ in responses.registered():
            if 'expand%5B0%5D=payment_intent' in _.url or '/payment_intents/' in _.url:
                assert '\"order_id\": \"1' in _.body
                _.body = _.body.replace('\"order_id\": \"1', f'\"order_id\": \"{order.pk}')
        self.assertIsNone(order.latest_transaction())
        # print('Continue to Stripe:', resp['Location'])
        # input('Press Enter when ready to continue')
        # Parse the Stripe's session ID from the redirect URL we got.
        stripe_session_id = resp['Location'].split('#')[0].split('/')[-1]
        success_url = reverse(
            'looper:stripe_success',
            kwargs={
                'pk': order.pk,
                'stripe_session_id': stripe_session_id,
            },
        )
        resp2 = self.client.get(success_url)
        self.assertEqual(resp2.status_code, 400)
