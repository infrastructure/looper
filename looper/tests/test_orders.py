from decimal import Decimal
from unittest import mock
import datetime

import django.utils.timezone

from . import AbstractLooperTestCase
from ..exceptions import IncorrectStatusError
from looper.money import Money
from looper.taxes import TaxType


class OrdersTest(AbstractLooperTestCase):
    def test_create_from_subscription(self):
        subscription = self.create_subscription()
        order = subscription.generate_order()
        self.assertEqual('created', order.status)
        self.assertEqual('Blender Development Fund Membership / Gold', order.name)

    def test_subscription_activation(self):
        subscription = self.create_subscription()
        order = subscription.generate_order()

        # Even without a transaction in the correct state,
        # setting an order to 'paid' should activate the
        # connected subscription.
        order.status = 'paid'

        now = django.utils.timezone.now()
        with mock.patch('django.utils.timezone.now') as mock_now:
            mock_now.return_value = now
            order.save(update_fields={'status'})

        subscription.refresh_from_db()
        order.refresh_from_db()
        self.assertEqual('paid', order.status)
        self.assertEqual(now, order.paid_at)
        self.assertEqual('active', subscription.status)
        self.assertEqual('active', subscription.status)

    def test_subscription_already_active(self):
        subscription = self.create_subscription()
        subscription.status = 'active'
        subscription.save()

        order = subscription.generate_order()
        order.status = 'paid'
        order.save()

        subscription.refresh_from_db()
        order.refresh_from_db()
        self.assertEqual('paid', order.status)
        self.assertEqual('active', subscription.status)

    def test_subscription_already_active_bump_next_payment(self):
        start_time = django.utils.timezone.now()
        old_next_payment = start_time - datetime.timedelta(hours=1)

        # Create a subscription that's active, but with a next_payment in the past.
        # This shouldn't happen, but might happen when things hickup.
        subscription = self.create_subscription()
        subscription.status = 'active'
        subscription.save()

        # Don't do this in the same database change as setting the status to active,
        # as it will otherwise auto-bump the next payment date.
        subscription.next_payment = old_next_payment
        subscription.save()

        subscription.refresh_from_db()
        self.assertEqual(
            subscription.next_payment, old_next_payment, 'Next payment date should be in the past'
        )

        # The order is paid while the subscription is active, which should bump
        # the next payment date.
        order = subscription.generate_order()
        order.status = 'paid'
        order.save()

        subscription.refresh_from_db()
        order.refresh_from_db()
        self.assertEqual('paid', order.status)
        self.assertEqual('active', subscription.status)
        self.assertGreaterEqual(
            subscription.next_payment,
            start_time,
            'Paying for an active subscription should bump the next payment date '
            'if that date is in the past.',
        )
        self.assertEqual(0, subscription.intervals_elapsed)

    def test_subscription_active_order_cancelled(self):
        subscription = self.create_subscription()
        subscription.status = 'active'
        subscription.save()

        order = subscription.generate_order()
        order.status = 'cancelled'
        order.save()

        subscription.refresh_from_db()
        order.refresh_from_db()
        self.assertEqual('cancelled', order.status)
        self.assertEqual(
            'active',
            subscription.status,
            'Cancelling an order should not influence an already-active subscription',
        )

    def test_subscription_direct_active_order_fulfilled(self):
        # This way the subscription is marked as 'active' while
        # the order is still in 'created' state. The order skipping
        # the 'paid' state shouldn't matter to the subscription.
        subscription = self.create_subscription()
        subscription.status = 'active'
        subscription.save()

        order = subscription.generate_order()
        order.status = 'fulfilled'
        order.save()

        subscription.refresh_from_db()
        order.refresh_from_db()
        self.assertEqual('fulfilled', order.status)
        self.assertEqual('active', subscription.status)

    def test_subscription_active_order_fulfilled(self):
        # Contrary to the test above, this test lets the order go through
        # the 'paid' status, and lets that activate the subscription.
        # Fulfilling the order still shouldn't change the Subscription status.
        subscription = self.create_subscription()
        order = subscription.generate_order()
        order.status = 'paid'
        order.save()

        subscription.refresh_from_db()
        self.assertEqual('active', subscription.status)

        order.status = 'fulfilled'
        order.save()

        subscription.refresh_from_db()
        order.refresh_from_db()
        self.assertEqual('fulfilled', order.status)
        self.assertEqual('active', subscription.status)

    def test_subscription_inactive_order_fulfilled(self):
        subscription = self.create_subscription()
        order = subscription.generate_order()
        order.status = 'paid'
        order.save()

        # Mimick a race condition between a subscription going to 'on-hold'
        # and after that the order going from 'paid' to 'fulfilled'.
        # This should not change the subscription status.
        subscription.status = 'on-hold'
        subscription.save()
        order.status = 'fulfilled'
        order.save()

        subscription.refresh_from_db()
        order.refresh_from_db()
        self.assertEqual('fulfilled', order.status)
        self.assertEqual('on-hold', subscription.status)

    def test_update_not_allowed_if_paid(self):
        subscription = self.create_subscription()
        order = subscription.generate_order()
        order.status = 'paid'
        order.save()

        with self.assertRaises(IncorrectStatusError):
            order.update()

    def test_update_not_allowed_if_cancelled(self):
        subscription = self.create_subscription()
        order = subscription.generate_order()
        order.status = 'cancelled'
        order.save()

        with self.assertRaises(IncorrectStatusError):
            order.update()

    def test_update_not_allowed_if_fulfilled(self):
        subscription = self.create_subscription()
        order = subscription.generate_order()
        order.status = 'fulfilled'
        order.save()

        with self.assertRaises(IncorrectStatusError):
            order.update()

    def test_update_updates_order_price_and_tax_fields_reverse_charge_subtracts_tax(self):
        subscription = self.create_subscription()
        order = subscription.generate_order()
        self.assertEqual(order.price, subscription.price)

        subscription.tax_type = TaxType.VAT_REVERSE_CHARGE.value
        subscription.tax_rate = Decimal(19)
        subscription.save()

        order.update()

        order.refresh_from_db()
        self.assertNotEqual(order.price, subscription.price)
        self.assertEqual(subscription.price, Money('EUR', 2500))
        self.assertEqual(order.price, Money('EUR', 2101))
        self.assertEqual(order.tax_type, TaxType.VAT_REVERSE_CHARGE.value)
        self.assertEqual(order.tax_rate, 19)
        # Make sure order is not copied
        self.assertEqual(subscription.order_set.count(), 1)
