from typing import Optional, Dict
from unittest import mock
import datetime
import logging
import os
import xml.etree.cElementTree as et

from django.contrib.auth import get_user_model
from django.test import TestCase, override_settings
import django.utils.timezone
import responses


from ..models import Plan, Subscription, Gateway, Customer, Transaction, LinkCustomerToken
from looper.taxes import ProductType

utc = datetime.timezone.utc

User = get_user_model()
log = logging.getLogger(__name__)


@override_settings(
    STATICFILES_STORAGE='django.contrib.staticfiles.storage.StaticFilesStorage',
    EMAIL_BACKEND='django.core.mail.backends.locmem.EmailBackend',
)
class AbstractBaseTestCase(TestCase):
    """Applies certain overrides to settings we always need."""


class AbstractLooperTestCase(AbstractBaseTestCase):
    log = log.getChild('AbstractLooperTestCase')
    fixtures = ['gateways', 'devfund', 'testuser', 'systemuser']
    valid_payload = {
        'gateway': 'stripe',
        'email': 'erik@example.com',
        'full_name': 'Erik von Namenstein',
        'company': 'Nöming Thingéys',
        'street_address': 'Scotland Pl',
        'locality': 'Amsterdongel',
        'postal_code': '1025 ET',
        'region': 'Worbelmoster',
        'country': 'NL',
    }
    valid_payload_braintree = {
        'gateway': 'braintree',
        'payment_method_nonce': 'fake-valid-nonce',
        'email': 'erik@example.com',
        'full_name': 'Erik von Namenstein',
        'company': 'Nöming Thingéys',
        'street_address': 'Scotland Pl',
        'locality': 'Amsterdongel',
        'postal_code': '1025 ET',
        'region': 'Worbelmoster',
        'country': 'NL',
    }
    gateway_name = None

    def setUp(self):
        super().setUp()

        self.create_default_user()
        self.get_default_plan()

    def _get_test_gateway(self) -> 'Gateway':
        if self.gateway_name:
            return Gateway.objects.get(name=self.gateway_name)
        return None

    def create_default_user(self):
        self.user = User.objects.get(email='harry@blender.org')
        with mock.patch('stripe.PaymentMethod.retrieve') as mock_stripe:
            mock_stripe.return_value = {
                'type': 'card',
                'card': {'brand': 'test', 'last4': '1234', 'exp_month': '12', 'exp_year': '2023'},
            }
            gw = self._get_test_gateway()
            if gw:
                self.user.customer.paymentmethod_set.create(
                    gateway=gw, token='123', recognisable_name='Test payment method'
                )
        self.pay_meth = self.user.customer.payment_method_default

    def create_accountless_customer(self):
        self.accountless_customer = Customer.objects.create(user=None)
        self.accountless_customer.billing_address.save()
        gw = self._get_test_gateway()
        if gw:
            self.accountless_customer.paymentmethod_set.create(
                gateway=gw, token='123', recognisable_name='Test payment method'
            )
        self.pay_meth = self.accountless_customer.payment_method_default
        self.link_customer_token = LinkCustomerToken.objects.create(
            customer=self.accountless_customer
        )
        self.assertIsNone(self.accountless_customer.user)

    def get_default_plan(self):
        self.plan = Plan.objects.get(name='Gold')
        self.planvar = self.plan.variations.active().get(
            currency='EUR', interval_unit='month', interval_length=1
        )

    def create_subscription(self, **extra) -> Subscription:
        subscription = Subscription.objects.create(
            plan=self.plan,
            customer=self.user.customer,
            price=self.planvar.price,
            currency=self.planvar.currency,
            payment_method=self.pay_meth,
            interval_unit=self.planvar.interval_unit,
            interval_length=self.planvar.interval_length,
            **extra,
        )
        self.assertEqual('on-hold', subscription.status)
        return subscription

    def create_accountless_subscription(self, **extra) -> Subscription:
        subscription = Subscription.objects.create(
            plan=self.plan,
            customer=self.accountless_customer,
            price=self.planvar.price,
            currency=self.planvar.currency,
            payment_method=self.pay_meth,
            interval_unit=self.planvar.interval_unit,
            interval_length=self.planvar.interval_length,
            **extra,
        )
        self.assertEqual('on-hold', subscription.status)
        return subscription

    def create_active_subscription(self) -> Subscription:
        subs = self.create_subscription()

        now = django.utils.timezone.now()
        subs.next_payment = now + datetime.timedelta(days=4)
        subs.save(update_fields={'next_payment'})

        self.log.debug('Activating subscription pk=%d', subs.pk)
        order = subs.generate_order()
        order.status = 'paid'
        order.save()

        subs.refresh_from_db()
        self.assertEqual('active', subs.status)

        return subs

    def create_active_accountless_subscription(self) -> Subscription:
        subs = self.create_accountless_subscription()

        now = django.utils.timezone.now()
        subs.next_payment = now + datetime.timedelta(days=4)
        subs.save(update_fields={'next_payment'})

        order = subs.generate_order()
        order.status = 'paid'
        order.save()

        transaction = Transaction.objects.create(
            order=order,
            customer=order.customer,
            status='succesful',
            payment_method=order.payment_method,
        )

        subs.refresh_from_db()
        self.assertEqual('active', subs.status)
        self.assertIsNone(subs.customer.user)
        self.assertIsNone(subs.payment_method.customer.user)
        self.assertIsNone(order.customer.user)
        self.assertIsNone(transaction.customer.user)
        return subs

    def create_on_hold_subscription(self) -> Subscription:
        subs = self.create_active_subscription()

        now = django.utils.timezone.now()

        self.log.debug('Mimicking overdue payment on subscription pk=%d', subs.pk)
        subs.next_payment = now - datetime.timedelta(days=1)
        subs.status = 'on-hold'
        subs.save()

        subs.generate_order()
        subs.refresh_from_db()
        return subs

    def create_subscription_with_product_and_billing_details(
        self,
        product_type=ProductType.DONATION.value,
        vat_number: str = '',
        **billing_address_data: Dict[str, str],
    ):
        self.plan.product.type = product_type
        self.plan.product.save(update_fields={'type'})

        billing_address = self.user.customer.billing_address
        billing_address.vat_number = vat_number
        billing_address.save(update_fields={'vat_number'})

        billing_address = self.user.customer.billing_address
        for k, v in billing_address_data.items():
            setattr(billing_address, k, v)
        billing_address.save()

        subscription = self.create_subscription()
        # Make sure that changes to billing details are taken into account
        subscription.update_tax()
        return subscription

    def assertAlmostEqualDateTime(
        self,
        expect: Optional[datetime.datetime],
        actual: Optional[datetime.datetime],
        margin_seconds: float = 1.0,
    ):
        self.assertIsNotNone(expect)
        self.assertIsNotNone(actual)

        assert isinstance(expect, datetime.datetime)
        assert isinstance(actual, datetime.datetime)

        expect_utc = expect.astimezone(utc)
        actual_utc = actual.astimezone(utc)
        difference = expect_utc.timestamp() - actual_utc.timestamp()

        if abs(difference) < margin_seconds:
            return

        # Make sure that the difference shown is never negative. timedelta
        # objects are a bit strange in that they will always show positive
        # time: '-1 hour' is shown as '-1 day +23 hours'.
        if difference < 0:
            diff = actual_utc - expect_utc
        else:
            diff = expect_utc - actual_utc

        self.fail(
            f'Expected and actual timestamps differ {diff}:\n'
            f'    Expected: {expect}\n'
            f'    Actual  : {actual}'
        )


def _mock_vies_response(
    is_valid=True,
    is_broken=False,
    country_code='NL',
    number='818152011B01',
    name='TESTBIZ L.T.D',
    date='2021-07-05+02:00',
    address=''
        'MOCKASTREET 123 00161'
        '       123-3234 TESTVILLE',
):
    path = os.path.abspath(__file__)
    dir_path = os.path.join(os.path.dirname(path), 'vies')

    vies_base_url = 'https://ec.europa.eu/taxation_customs/vies'
    wsdl_file = 'checkVatService.wsdl'
    with open(os.path.join(dir_path, wsdl_file), 'r') as f:
        responses.add(
            responses.GET,
            url=f'{vies_base_url}/checkVatService.wsdl',
            body=f.read(),
            content_type='text/xml',
        )

    def _xml_response_callback_response(request):
        with open(os.path.join(dir_path, 'checkVatService_POST.xml'), 'r') as f:
            root = et.fromstring(request.body)
            for child in root[0][0]:
                if child.tag.endswith('countryCode'):
                    country_code = child.text
                if child.tag.endswith('vatNumber'):
                    number = child.text
            body = f.read().format(
                is_valid='true' if is_valid else 'false',
                country_code=country_code,
                name=name,
                number=number,
                address=address,
                date=date,
            )
            status = 500 if is_broken else 200
            return (status, {}, body)
    responses.add_callback(
        responses.POST,
        url=f'{vies_base_url}/services/checkVatService',
        callback=_xml_response_callback_response,
        content_type='text/xml',
    )


class ResponsesMixin(object):
    responses_file_path = 'looper/tests/stripe/'
    responses_files = None

    def setUp(self):
        responses.start()
        # Load previously recorded Stripe responses from YAML files
        for file_name in os.listdir(self.responses_file_path):
            if self.responses_files:
                if file_name not in self.responses_files:
                    continue
            if not file_name.endswith('.yaml'):
                continue
            responses._add_from_file(file_path=f'{self.responses_file_path}{file_name}')
        super(ResponsesMixin, self).setUp()

    def tearDown(self):
        super(ResponsesMixin, self).tearDown()
        responses.stop()
        responses.reset()
