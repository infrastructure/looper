from looper.utils import ensure_decimal


class Party(object):
    """Trading party.

    Represents either a consumer or business in a given country acting as a
    party to a transaction.
    :ivar country_code:
        Party's legal or effective country of registration or residence as an
        ISO 3166-1 alpha-2 country code.
    :type country_code: str
    :ivar is_business: Whether the part is a legal business entity.
    :type is_business: bool
    """

    def __init__(self, country_code, is_business):
        """Initialize a trading party.

        :param country_code:
            Party's legal or effective country of registration or residence as
            an ISO 3166-1 alpha-2 country code.
        :type country_code: str
        :param is_business: Whether the part is a legal business entity.
        :type is_business: bool
        """

        self.country_code = country_code
        self.is_business = is_business

    def __repr__(self):
        return '<Party: country code = %s, is business = %r>' % (
            self.country_code,
            self.is_business,
        )


class VatCharge(object):
    """VAT charge.

    :ivar action: VAT charge action.
    :type action: TaxType
    :ivar country_code:
        Country in which the action applies as the ISO 3166-1 alpha-2 code of
        the country.
    :type country_code: str
    :ivar rate:
        VAT rate in percent. I.e. a value of 25 indicates a VAT charge of 25 %.
    :type rate: Decimal
    """

    def __init__(self, action, country_code, rate):
        self.action = action
        self.country_code = country_code
        self.rate = ensure_decimal(rate)

    def __repr__(self):
        return '<%s.%s: action = %r, country code = %r, rate = %s>' % (
            self.__class__.__module__,
            self.__class__.__name__,
            self.action,
            self.country_code,
            self.rate,
        )
