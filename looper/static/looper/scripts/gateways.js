// Our BrainTree interface stuff for payment.
// Using it requires the following Braintree scripts:
//   https://js.braintreegateway.com/web/3.85.3/js/data-collector.min.js
//   https://js.braintreegateway.com/web/dropin/1.33.2/js/dropin.min.js
//
// Optionally allows reCAPTCHA to be requested before a payment method
// can be sent to the payment gateway and our back-end. In this case,
// a verification token is included into the form payload,
// if reCAPTCHA challenge was completed.
//

/* Fires a custom event indicating that Google reCAPTCHA has loaded.
 * Must exist in the global scope to be accessible to reCAPTCHA's API.
 * Allows reCAPTCHA to be later configured with a callback
 * defined outside the global scope.
 */
function recaptchaOnLoad () {
    const event = new Event("recaptchaLoaded");
    document.dispatchEvent(event);
}

function looperInit() {
    // See below declarations for details about what HTML elements
    // are required for this script to work.
    const recaptcha = document.getElementById('recaptcha');
    const checkoutForm = document.querySelector('form[data-looper-payment-form]');
    const braintreeClientToken = checkoutForm.dataset.braintreeClientToken;
    const gatewayErrors = document.getElementById('gateway-errors');

    const submitButton = document.getElementById('submit-button');
    const submitButtonHTML = submitButton.innerHTML;

    // Form fields are based on Looper's model forms,
    // and are expected to be templated with default Django form magic.
    const paymentMethodNonce = document.getElementById('id_payment_method_nonce');
    const deviceData = document.getElementById('id_device_data');

    const gatewayRadio = document.querySelectorAll('[name=gateway]');
    const gatewayRadioBank = document.querySelector('[name=gateway][value=bank]');
    const gatewayRadioBraintree = document.querySelector('[name=gateway][value=braintree]');

    const braintreeContainer = document.querySelector("div[data-gateway-name='braintree']");
    // This container added to the form by looper/forms/widgets/gateway_radio_option.html
    const braintreeContainerId = 'bt-dropin';
    const braintreeDropinStyleOverridesEl = document.getElementById('bt-dropin-styles');
    let braintreeDropinStyleOverrides = {};
    if (braintreeDropinStyleOverridesEl) {
      braintreeDropinStyleOverrides = JSON.parse(braintreeDropinStyleOverridesEl.textContent);
    }
    let braintreeOptions = {
        authorization: braintreeClientToken,
        // This is why the container must be a single element with an ID:
        container: '#' + braintreeContainerId,
        paypal: {
            flow: 'vault',
            overrides: {styles: braintreeDropinStyleOverrides},
        },
        card: {
          overrides: {styles: braintreeDropinStyleOverrides},
        },
        preselectVaultedPaymentMethod: false,
        dataCollector: {
            kount: false,  // true only if Kount fraud data collection is enabled
        },
        threeDSecure: true,
    };

    // braintreeClient will be initialized later.
    let braintreeClient = null;
    let braintreeCreateCalled = false;

    function gatherErrorMsg(braintreeError) {
        // Initialise storage for all messages in all recursive calls made from here.
        var messageArray = [];

        function recurse(errorObj, depth) {
            if (depth > 10) return;  // infinity is too far away.
            if (typeof errorObj != 'object') return '';

            // If this object has a "message" property, use it.
            if (typeof errorObj.message != 'undefined') {
                messageArray.push(errorObj.message);
            }

            // Keep recursing into sub-properties until we find no more "messages" keys.
            for (propname in errorObj) {
                if (propname == 'message') continue;  // already have this one.
                if (!braintreeError.hasOwnProperty(propname)) continue;

                recurse(errorObj[propname], depth+1);
            }
        }

        recurse(braintreeError, 0);
        return messageArray.join(" ");
    }

    function showGatewayError(error) {
        console.error(error);
        gatewayErrors.classList.add('text-danger');
        gatewayErrors.innerText = error;
    }

    function clearGatewayError() {
        gatewayErrors.classList.remove('text-danger');
        gatewayErrors.innerText = '';
    }

    function getRecaptchaResponseEl() {
        return document.getElementById("g-recaptcha-response");
    }

    function getGatewayErrorDetail(err) {
      try {
        return err.details.originalError.details.originalError.error.message;
      } catch(err) {
        return null;
      }
    }

    /* Called by the reCAPTCHA v2 API after a successful verification. */
    function recaptchaSuccessful(token) {
        if (recaptcha) {
            let recaptchaEl = getRecaptchaResponseEl();
            recaptchaEl.textContent = token;
        }

        if (isGatewayBraintreeSelected()) {
            let challenge = {threeDSecure: gatherChallengeData()};
            //console.log('challenge: ', challenge);

            disableSubmitButton('Processing...');

            braintreeClient.requestPaymentMethod(challenge, function(err, payload) {
                // console.log('payload', payload);
                if (err) {
                    console.log('Error', err);
                    // After an error, the button should be re-enabled to allow a retry.
                    enableSubmitButton();
                    // TODO(fsiddi): Investigate the API to properly handle the case of a selected, but not clicked
                    // PayPal checkout button.
                    if (err.message === 'No payment method is available.') {
                        err.message += ' If you selected PayPal, click on the Yellow PayPal Checkout button.'
                    }
                    let errorMsg = getGatewayErrorDetail(err) || err.message;
                    showGatewayError(errorMsg);
                    braintreeClient.clearSelectedPaymentMethod();
                    return;
                }

                if (payload.liabilityShiftPossible && !payload.liabilityShifted) {
                    // This payment method was eligible for 3D Secure
                    // and the customer failed 3D Secure authentication
                    showGatewayError('Security challenge failed');
                    braintreeClient.clearSelectedPaymentMethod();
                    // After an error, the button should be re-enabled to allow a retry.
                    enableSubmitButton();
                    return;
                }

                // Either the payment method is not eligible, or the challenge succeeded
                //console.log('verification success or not eligible:', payload);
                // Add the nonce to the form and submit (input field is created by the CheckoutForm)
                paymentMethodNonce.value = payload.nonce;
                deviceData.value = payload.deviceData;
                validateAndSubmit();
            });
        } else if (isGatewayBankSelected()) {
            validateAndSubmit();
        }
    }

    function submitFormWithRecaptcha() {
        if (recaptcha != null) {
            // Make the user pass a recaptcha.

            /* reCAPTCHA won't execute twice. However, the user can still click the button twice.
             * This happens for example once before checking the "Another Membership" checkbox,
             * at which time the browser may block the form submission because the checkbox is
             *  marked required, and then another click after checking the checkbox. */
            let recaptchaEl = getRecaptchaResponseEl();
            if (recaptchaEl.textContent) {
                recaptchaSuccessful(recaptchaEl.textContent);
                return;
            }

            grecaptcha.execute();
            enableSubmitOnRecaptchaClose();
        } else {
            recaptchaSuccessful();
        }
    }

    function enableSubmitOnRecaptchaClose() {
        // Try to find out if reCAPTCHA has been closed to re-enable the submit button
        try {
            let iframe = document.querySelector(
                'iframe[src^="https://www.google.com/recaptcha"][src*="bframe"]'
            );
            let container = iframe.parentNode.parentNode;
            let observer = new MutationObserver(mutations => {
                if(container && container.style.visibility === 'hidden'){
                    enableSubmitButton();
                    observer.disconnect();
                }
            });
            observer.observe(container, {attributes: true, attributeFilter: ['style']})
        } catch (error) {
            // The try-catch was added here out of caution
            // (any uncaught error here would break the payment flow)
            // rather than expectations of any specific errors.
            console.error(error)
        }
    }

    function getFieldHelptext(el) {
        let helptext = el.nextElementSibling;
        if (!helptext || !helptext.classList.contains('helptext')) {
            return null;
        }
        return helptext;
    }

    function validateAndSubmit(form) {
        disableSubmitButton();

        // requestSubmit is not supported in Safari WebKit (and IE).
        // However, validation popovers are still displayed in case of invalid fields,
        // and we can use `checkValidity` which better support.
        if (checkoutForm.checkValidity()) {
            // Proceed to submit the form
            checkoutForm.submit();
        } else {
            enableSubmitButton();
        }
    }

    function enableSubmitButton() {
        submitButton.disabled = false;
        submitButton.setAttribute('aria-disabled', 'false');
        submitButton.classList.remove('disabled');
        submitButton.classList.add('btn-success');
        submitButton.innerHTML = submitButtonHTML;
    }

    function disableSubmitButton(text) {
        submitButton.disabled = true;
        submitButton.setAttribute('aria-disabled', 'true');
        submitButton.classList.add('disabled');
        submitButton.classList.remove('btn-success');
        if (!!text) {
            submitButton.innerText = text;
        }
    }

    function getFormInputs() {
        let fieldNames = [
            'full_name',
            'street_address',
            'extended_address',
            'locality',
            'postal_code',
            'region',
            'country',
            'email',
            'price',
        ];
        let fields = {}
        for (fieldName of fieldNames) {
            fields[fieldName] = {
                input: document.getElementById(`id_${fieldName}`),
            };
        }
        return fields;
    }

    function gatherChallengeData() {
        // See https://developers.braintreepayments.com/guides/3d-secure/client-side/javascript/v3#drop-in-ui
        let billingFields = getFormInputs();
        let fullName = billingFields['full_name'].input.value;
        let firstName = fullName.split(' ').slice(0, -1).join(' ');
        let lastName = fullName.split(' ').slice(-1).join(' ');
        return {
            amount: parseFloat(billingFields['price'].input.value),
            email: billingFields['email'].input.value,
            billingAddress: {
                // FIXME(anna): make sure these are ASCII
                givenName: firstName,
                surname: lastName,
                // TODO(anna): Adding the phone might reduce the chance of challenge to be presented
                // remove (), spaces, and - from phone number
                //phoneNumber: billingFields['phone'].input.value.replace(/[\(\)\s\-]/g, ''),
                streetAddress: billingFields['street_address'].input.value,
                extendedAddress: billingFields['extended_address'].input.value,
                locality: billingFields['locality'].input.value,
                region: billingFields['region'].input.value,
                postalCode: billingFields['postal_code'].input.value,
                countryCodeAlpha2: billingFields['country'].input.value
            }
        }
    }

    function getDropinEl() {
        return document.getElementById(braintreeContainerId);
    }

    function isGatewayBraintreeSelected() {
        return gatewayRadioBraintree && gatewayRadioBraintree.checked;
    }

    function isGatewayBankSelected() {
        return gatewayRadioBank && gatewayRadioBank.checked;
    }

    function initSelectedPaymentGateway() {
        clearGatewayError();
        if (isGatewayBraintreeSelected()) {
            getDropinEl().classList.add('show');
            // Braintree is selected, initialising Braintree DropIn UI
            if (braintreeCreateCalled || braintreeClient) {
                // Already initialized
                return;
            }
            // Initialise Braintree UI
            disableSubmitButton('Loading...');
            braintree.dropin.create(braintreeOptions, function (createErr, instance) {
                if (createErr != null) {
                    console.error(createErr);
                    var msg = gatherErrorMsg(createErr);

                    disableSubmitButton('Error loading payment interface')
                    return;
                }
                braintreeClient = instance;

                // If Braintree is **still** selected, disable the button
                // until a payment method is chosen.
                if (isGatewayBraintreeSelected()) {
                    // To avoid confusion, change the text on the button
                    // from "Loading..." to a more sensible one as well.
                    disableSubmitButton('Select a payment method');
                }
                braintreeClient.on('paymentMethodRequestable', enableSubmitButton);
                braintreeClient.on('noPaymentMethodRequestable', disableSubmitButton);
            });
            braintreeCreateCalled = true;
        } else if (isGatewayBankSelected()) {
            // Bank transfer is selected, can collapse Braintree DropIn,
            // it it's been initialized already
            let btDropin = getDropinEl();
            if (btDropin) {
                btDropin.classList.remove("show")
            }
            enableSubmitButton();
        }
    }

    // Event handlers for payment options and form submit button
    gatewayRadio.forEach(el => el.addEventListener('change', initSelectedPaymentGateway));
    checkoutForm.addEventListener('submit', event => {
        // Disable default form submission, since we submit the form
        // explicitly in the `recaptchaSuccessful` callback.
        event.preventDefault();
        event.stopPropagation();

        clearGatewayError();
        disableSubmitButton('Processing...');

        submitFormWithRecaptcha();
    });

    // Setup reCAPTCHA with a callback that can access Braintree client, if necessary
    document.addEventListener('recaptchaLoaded', event => {
        let recaptchaWidgetID = grecaptcha.render(
            recaptcha, {
                'sitekey' : recaptcha.dataset.sitekey,
                'callback': recaptchaSuccessful,
            }
        );
    });

    // Finally, initialise currently selected payment gateway
    initSelectedPaymentGateway();
}
