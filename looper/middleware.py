from typing import Callable, Optional, TypeVar
import functools
import logging

from django.conf import settings
from django.http import HttpRequest
from django.http.response import HttpResponseBase
from django.core.exceptions import ValidationError
from geoip2.errors import AddressNotFoundError
import geoip2.database
import geoip2.records

from looper import utils

PREFERRED_CURRENCY_SESSION_KEY = 'PREFERRED_CURRENCY'
COUNTRY_CODE_SESSION_KEY = 'GEOIP2_COUNTRY_CODE'

SKIP_GEO_LOOKUP = {'127.0.0.1', '::1', 'unknown', ''}
"""Addresses for which country lookups should be skipped."""

log = logging.getLogger(__name__)

GetResponseType = TypeVar('GetResponseType', bound=Callable[[HttpRequest], HttpResponseBase])


class PreferredCurrencyMiddleware:
    """Expose preferred currency in request.session[PREFERRED_CURRENCY_SESSION_KEY].

    Very simple selector; if the country is in the European Union, we assume
    the Euro is preferred, otherwise the US Dollar is chosen.

    Uses the session, so install after django.contrib.sessions.middleware.SessionMiddleware.
    Additionally exposes detected country in request.session[COUNTRY_CODE_SESSION_KEY].
    """

    log = log.getChild('PreferredCurrencyMiddleware')

    def __init__(self, get_response: GetResponseType) -> None:
        self.log.info('Opening GeoIP2 database %s', settings.GEOIP2_DB)
        try:
            self.geoip = geoip2.database.Reader(settings.GEOIP2_DB)
        except Exception:
            log.exception('Unable to open GeoIP2 database')
        self.get_response: GetResponseType = get_response

    def __call__(self, request: HttpRequest) -> HttpResponseBase:
        self.update_session(request)
        return self.get_response(request)

    @functools.lru_cache(maxsize=128)
    def lookup_country(self, remote_addr: str) -> Optional[geoip2.records.Country]:
        if remote_addr in SKIP_GEO_LOOKUP:
            # We already know that the below call will fail; don't bother
            # even trying to look up the address.
            return None

        if not getattr(self, 'geoip', None):
            log.error('GeoIP is not configured, check settings.GEOIP2_DB')
            return None

        try:
            country = self.geoip.country(remote_addr)
        except AddressNotFoundError:
            return None
        except OSError as ex:
            self.log.warning(
                'Unable to look up remote address %r in GeoIP database: %s', remote_addr, ex
            )
            return None
        return country.country

    def preferred_currency(
        self, request: HttpRequest, country: Optional[geoip2.records.Country]
    ) -> str:
        """Determine default currency for the given country.

        Unfortunately geoip2.models.Country isn't defined in a MyPy-compatible
        way, so we can't properly declare its type.

        :type country: geoip2.models.Country
        """
        if country is None:
            return 'USD'
        if country.is_in_european_union:
            return 'EUR'
        return 'USD'

    def update_session(self, request: HttpRequest) -> None:
        if not hasattr(request, 'session'):
            self.log.warning('Session middleware not available, unable to set preferred currency')
            return

        if (
            COUNTRY_CODE_SESSION_KEY in request.session
            and PREFERRED_CURRENCY_SESSION_KEY in request.session
        ):
            # We've already tried to detect country and set preferred currency.
            return

        country = None
        request.session[COUNTRY_CODE_SESSION_KEY] = None
        try:
            remote_addr = utils.clean_ip_address(request)
            country = self.lookup_country(remote_addr)
            if country:
                request.session[COUNTRY_CODE_SESSION_KEY] = country.iso_code
        except ValidationError:
            pass

        if PREFERRED_CURRENCY_SESSION_KEY in request.session:
            # Preferred currency is already set; don't overwrite it.
            return

        currency = self.preferred_currency(request, country)
        request.session[PREFERRED_CURRENCY_SESSION_KEY] = currency
