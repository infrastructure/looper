import logging
from typing import Optional

from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.models import AbstractUser
from django.core.exceptions import ValidationError
import django.contrib.auth.forms as auth_forms

from . import exceptions, models, model_mixins, money, form_fields, form_mixins


User: AbstractUser = get_user_model()
log = logging.getLogger(__name__)


class AddressForm(form_mixins.BlenderClassesFormMixin, forms.ModelForm):
    class Meta:
        model = models.Address
        exclude = ['category', 'customer', 'tax_exempt']


class BasePaymentForm(forms.Form):
    gateway = form_fields.GatewayChoiceField()
    device_data = forms.CharField(
        initial='set-in-javascript', widget=forms.HiddenInput(), required=False
    )

    # The following fields are used in the client-side 3D Secure flow
    # Price value is a decimal number in major units of selected currency.
    price = forms.CharField(widget=forms.HiddenInput(), required=False)
    email = forms.EmailField(required=True)


class BaseCheckoutForm(BasePaymentForm):
    # The CheckoutView will test whether the user has recently already created a subscription.
    # If that is the case, the user will get a warning about this, and will have to confirm
    # the creation of a new subscription.
    approve_new_subscription = forms.BooleanField(
        label='Another Membership',
        required=False,
        initial=False,
        widget=forms.HiddenInput(),
        help_text='',
    )

    # These are used when a payment fails, so that the next attempt to pay can reuse
    # the already-created subscription and order.
    subscription_pk = forms.CharField(widget=forms.HiddenInput(), required=False)
    order_pk = forms.CharField(widget=forms.HiddenInput(), required=False)

    def show_approve_new_subscription(self, help_text: str):
        """Show the 'approve_new_subscription' checkbox."""

        field = self.fields['approve_new_subscription']
        field.widget = form_fields.BootstrapCheckbox()
        field.help_text = help_text
        field.required = True


class CheckoutStripeForm(BaseCheckoutForm, AddressForm):
    class Meta(AddressForm.Meta):
        pass

    gateway = form_fields.GatewayChoiceField(
        queryset=models.Gateway.objects.filter(
            name__in={'stripe', 'bank', 'mock'}
        ).order_by('-is_default')
    )


class StateMachineMixin(forms.ModelForm):
    """Mix-in for forms that handle looper.model_mixins.StateMachineMixin objects."""

    def clean_status(self):
        assert isinstance(self.instance, model_mixins.StateMachineMixin)

        old_status = self.instance.status
        new_status = self.cleaned_data['status']
        if old_status == new_status:
            # Always accept the current status.
            return new_status

        if not self.instance.may_transition_to(new_status):
            raise ValidationError(
                f'Status transition {old_status.title()} → {new_status.title()} is not allowed.'
            )
        return new_status


class PaymentMethodLimitMixin(forms.ModelForm):
    def __init__(self, *args, instance: Optional[models.Subscription] = None, **kwargs) -> None:
        if not instance:
            # When there is no instance, we don't know whose payment methods to list,
            # so it's better to not list them at all.
            invalid_customer_id = -1
            queryset = models.PaymentMethod.objects.filter(customer_id=invalid_customer_id)
        else:
            queryset = instance.customer.paymentmethod_set
        self.base_fields['payment_method'].queryset = queryset
        super().__init__(*args, instance=instance, **kwargs)


class SubscriptionAdminForm(StateMachineMixin, PaymentMethodLimitMixin, forms.ModelForm):
    class Meta:
        model = models.Subscription
        exclude = ['id']

    def clean_collection_method(self) -> str:
        payment_method = self.cleaned_data['payment_method']
        collection_method = self.cleaned_data['collection_method']

        if self.instance.collection_method != collection_method and 'managed' in {
            self.instance.collection_method,
            collection_method,
        }:
            raise ValidationError(
                'Toggling between managed and non-managed is not possible at the moment.'
            )

        if collection_method != 'automatic':
            return collection_method
        if not payment_method:
            raise ValidationError('Automatic payment requires a payment method.')
        return collection_method


class OrderAdminForm(StateMachineMixin, PaymentMethodLimitMixin, forms.ModelForm):
    class Meta:
        model = models.Order
        exclude = ['id']


class AdminTransactionRefundForm(forms.ModelForm):
    class Meta:
        model = models.Order
        exclude = ['id']

    refund_amount = form_fields.MoneyFormField(required=False, help_text='Amount to refund.')

    log = log.getChild('AdminTransactionRefundForm')

    def clean_refund_amount(self):
        refund_in_cents = self.cleaned_data['refund_amount']
        refund = money.Money(self.instance.currency, refund_in_cents)

        if not refund:
            # No refund is fine.
            return

        if self.instance.status != 'succeeded':
            raise ValidationError('Cannot refund: transaction must have status "succeeded"')

        refundable = self.instance.refundable
        if refund > refundable:
            raise ValidationError(f'Cannot refund more than {refundable.with_currency_symbol()}')
        if refund.cents < 0:
            raise ValidationError('Cannot refund negative amounts')
        return refund

    def save(self, commit=True):
        """Save the model and process the refund if successful."""
        instance: models.Transaction = super().save(commit=commit)
        refund: money.Money = self.cleaned_data['refund_amount']
        if not refund:
            self.log.debug('Not processing empty refund for transaction pk=%r', self.instance.pk)
            return instance

        self.log.debug('Processing refund of %s for transaction pk=%r', refund, self.instance.pk)
        try:
            instance.refund(refund)
        except exceptions.GatewayError as ex:
            self.log.exception(
                'Error refunding %s for transaction pk=%r: %s', refund, self.instance.pk, ex
            )
            raise ValidationError(f'The refund could not be processed: {ex}')

        return instance


class EmailRequiredMixin:
    cleaned_data: dict
    instance: AbstractUser

    def clean_email(self) -> str:
        if self.cleaned_data['email']:
            return self.cleaned_data['email']

        try:
            if self.instance.customer and self.instance.customer.billing_address.email:
                # If the user has no email but it does have a billing email,
                # just use that instead.
                return self.instance.customer.billing_address.email
        except models.Customer.DoesNotExist:
            pass

        from django.core.exceptions import ValidationError

        raise ValidationError('Every user should have an email address')


class AdminUserChangeForm(EmailRequiredMixin, auth_forms.UserChangeForm):
    pass


class AdminUserCreationForm(EmailRequiredMixin, auth_forms.UserCreationForm):
    pass
