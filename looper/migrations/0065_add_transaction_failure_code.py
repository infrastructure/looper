# Generated by Django 3.0.4 on 2021-07-14 11:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('looper', '0064_fix_paypal_method_type'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='failure_code',
            field=models.CharField(blank=True, db_index=True, default='', max_length=50),
        ),
    ]
