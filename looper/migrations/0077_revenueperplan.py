# Generated by Django 3.0.4 on 2022-07-25 09:55

from django.db import migrations
import looper.admin.proxy_models


class Migration(migrations.Migration):

    dependencies = [
        ('looper', '0076_order_external_reference'),
    ]

    operations = [
        migrations.CreateModel(
            name='RevenuePerPlan',
            fields=[
            ],
            options={
                'verbose_name': 'Revenue',
                'verbose_name_plural': '[Report] Revenue per plan',
                'managed': False,
                'proxy': True,
            },
            bases=(looper.admin.proxy_models._PerPlanMixin, 'looper.revenue'),
        ),
    ]
