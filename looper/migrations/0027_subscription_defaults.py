# Generated by Django 2.1.1 on 2018-10-01 12:30

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('looper', '0026_remove_transaction_failure_code'),
    ]

    operations = [
        migrations.AlterField(
            model_name='subscription',
            name='interval_length',
            field=models.IntegerField(
                default=1, help_text='How many "interval units" is each billing cycle.'
            ),
        ),
        migrations.AlterField(
            model_name='subscription',
            name='interval_unit',
            field=models.CharField(
                choices=[('day', 'Day'), ('week', 'Week'), ('month', 'Month'), ('year', 'Year')],
                default='month',
                help_text='The units in which "interval length" is expressed',
                max_length=50,
            ),
        ),
    ]
