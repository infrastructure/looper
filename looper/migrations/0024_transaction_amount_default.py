# Generated by Django 2.1.1 on 2018-09-28 12:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('looper', '0023_transaction_amount_refunded_in_cents'),
    ]

    operations = [
        migrations.AlterField(
            model_name='transaction',
            name='amount_in_cents',
            field=models.IntegerField(default=0),
        ),
    ]
