# Generated by Django 3.2.16 on 2023-10-30 08:49

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('looper', '0081_linkcustomertoken'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='product',
            field=models.ForeignKey(editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, to='looper.product'),
        ),
        migrations.AlterField(
            model_name='order',
            name='subscription',
            field=models.ForeignKey(editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, to='looper.subscription'),
        ),
        migrations.AddConstraint(
            model_name='order',
            constraint=models.CheckConstraint(check=models.Q(('product__isnull', False), ('subscription__isnull', False), _connector='OR'), name='check_looper_order_subscription_or_product_not_null'),
        ),
    ]
