# Generated by Django 3.0.4 on 2021-08-04 10:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('looper', '0066_add_refunded_status'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='name',
            field=models.CharField(blank=True, help_text='What the order is for; shown to the customer', max_length=255),
        ),
    ]
