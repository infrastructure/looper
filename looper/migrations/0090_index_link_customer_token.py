# Generated by Django 3.2.16 on 2023-12-05 08:17

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('looper', '0089_order_looper_orde_paid_at_604778_idx'),
    ]

    operations = [
        migrations.AlterField(
            model_name='linkcustomertoken',
            name='linked_to_user',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='linked_tokens', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddIndex(
            model_name='linkcustomertoken',
            index=models.Index(fields=['token'], name='looper_link_token_5289bb_idx'),
        ),
    ]
