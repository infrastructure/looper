# Generated by Django 3.2.9 on 2023-10-17 13:05

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('looper', '0079_paymentmethod_token_nullable'),
    ]

    operations = [
        migrations.AddField(
            model_name='address',
            name='customer',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='looper.customer'),
        ),
        migrations.AddField(
            model_name='gateway',
            name='customers',
            field=models.ManyToManyField(null=True, through='looper.GatewayCustomerId', to='looper.Customer'),
        ),
        migrations.AddField(
            model_name='gatewaycustomerid',
            name='customer',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='looper.customer'),
        ),
        migrations.AddField(
            model_name='order',
            name='customer',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='looper.customer'),
        ),
        migrations.AddField(
            model_name='paymentmethod',
            name='customer',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='looper.customer'),
        ),
        migrations.AddField(
            model_name='subscription',
            name='customer',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='looper.customer'),
        ),
        migrations.AddField(
            model_name='transaction',
            name='customer',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='looper.customer'),
        ),
        migrations.RunSQL(
            "update looper_address as u set customer_id = c.id "
            "from looper_customer as c where u.user_id = c.user_id",
            "update looper_address as u set user_id = c.user_id "
            "from looper_customer as c where u.customer_id = c.id and c.user_id is not null",
        ),
        migrations.RunSQL(
            "update looper_gatewaycustomerid as u set customer_id = c.id "
            "from looper_customer as c where u.user_id = c.user_id",
            "update looper_gatewaycustomerid as u set user_id = c.user_id "
            "from looper_customer as c where u.customer_id = c.id and c.user_id is not null",
        ),
        migrations.RunSQL(
            "update looper_order as u set customer_id = c.id "
            "from looper_customer as c where u.user_id = c.user_id",
            "update looper_order as u set user_id = c.user_id "
            "from looper_customer as c where u.customer_id = c.id and c.user_id is not null",
        ),
        migrations.RunSQL(
            "update looper_subscription as u set customer_id = c.id "
            "from looper_customer as c where u.user_id = c.user_id",
            "update looper_subscription as u set user_id = c.user_id "
            "from looper_customer as c where u.customer_id = c.id and c.user_id is not null",
        ),
        migrations.RunSQL(
            "update looper_transaction as u set customer_id = c.id "
            "from looper_customer as c where u.user_id = c.user_id",
            "update looper_transaction as u set user_id = c.user_id "
            "from looper_customer as c where u.customer_id = c.id and c.user_id is not null",
        ),
        migrations.RunSQL(
            "update looper_paymentmethod as u set customer_id = c.id "
            "from looper_customer as c where u.user_id = c.user_id",
            "update looper_paymentmethod as u set user_id = c.user_id "
            "from looper_customer as c where u.customer_id = c.id and c.user_id is not null",
        ),
        migrations.AlterField(
            model_name='customer',
            name='user',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='customer', to=settings.AUTH_USER_MODEL, help_text="When this is empty, it means that payment was made or attempted without signing in with an account,<br>and this customer's billing, subscription and payment records are yet to be claimed by a user account.<br>Someone should sign in and use a customer token link to do so.", blank=True),
        ),
        migrations.AlterUniqueTogether(
            name='paymentmethod',
            unique_together={('gateway', 'token', 'customer')},
        ),
        migrations.AlterField(
            model_name='address',
            name='customer',
            field=models.ForeignKey(null=False, on_delete=django.db.models.deletion.CASCADE, to='looper.customer'),
        ),
        migrations.AlterField(
            model_name='gateway',
            name='customers',
            field=models.ManyToManyField(null=False, through='looper.GatewayCustomerId', to='looper.Customer'),
        ),
        migrations.AlterField(
            model_name='gatewaycustomerid',
            name='customer',
            field=models.ForeignKey(null=False, on_delete=django.db.models.deletion.CASCADE, to='looper.customer'),
        ),
        migrations.AlterField(
            model_name='order',
            name='customer',
            field=models.ForeignKey(null=False, on_delete=django.db.models.deletion.CASCADE, to='looper.customer'),
        ),
        migrations.AlterField(
            model_name='paymentmethod',
            name='customer',
            field=models.ForeignKey(null=False, on_delete=django.db.models.deletion.CASCADE, to='looper.customer'),
        ),
        migrations.AlterField(
            model_name='subscription',
            name='customer',
            field=models.ForeignKey(null=False, on_delete=django.db.models.deletion.CASCADE, to='looper.customer'),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='customer',
            field=models.ForeignKey(null=False, on_delete=django.db.models.deletion.CASCADE, to='looper.customer'),
        ),
        migrations.RemoveField(
            model_name='paymentmethod',
            name='user',
        ),
        migrations.RemoveField(
            model_name='address',
            name='user',
        ),
        migrations.RemoveField(
            model_name='gateway',
            name='users',
        ),
        migrations.RemoveField(
            model_name='gatewaycustomerid',
            name='user',
        ),
        migrations.RemoveField(
            model_name='order',
            name='user',
        ),
        migrations.RemoveField(
            model_name='subscription',
            name='user',
        ),
        migrations.RemoveField(
            model_name='transaction',
            name='user',
        ),
    ]
