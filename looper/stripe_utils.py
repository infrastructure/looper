import logging
from typing import Dict, Tuple, Optional

from django.conf import settings
from django.db import transaction
import django.utils.timezone
import stripe

from . import gateways, models

logger = logging.getLogger(__name__)


def create_stripe_checkout_session_for_order(
    order: models.Order,
    success_url: str,
    cancel_url: str,
    payment_intent_metadata: Dict,
) -> stripe.api_resources.checkout.Session:
    assert 'order_id' in payment_intent_metadata, 'missing payment_intent_metadata.order_id'
    gateway = models.Gateway.objects.get(name='stripe')
    gateway_customer_id = order.customer.gateway_customer_id_get_or_create(gateway)
    return stripe.checkout.Session.create(
        cancel_url=cancel_url,
        customer=gateway_customer_id,
        line_items=[
            {
                'price_data': {
                    'currency': order.currency.lower(),
                    'product_data': {'name': order.name},
                    'unit_amount': order.price.cents,
                },
                'quantity': 1,
            },
        ],
        mode='payment',
        payment_intent_data={
            'metadata': payment_intent_metadata,
            'setup_future_usage': 'off_session',
        },
        payment_method_types=settings.STRIPE_OFF_SESSION_PAYMENT_METHOD_TYPES,
        submit_type=getattr(settings, 'STRIPE_CHECKOUT_SUBMIT_TYPE', 'auto'),
        success_url=success_url,
        ui_mode='hosted',
    )


def create_stripe_checkout_session_for_plan_variation(
    plan_variation: models.PlanVariation,
    customer: models.Customer,
    success_url: str,
    cancel_url: str,
    unit_amount: int = None,
) -> stripe.api_resources.checkout.Session:
    gateway = models.Gateway.objects.get(name='stripe')
    customer_kwargs = {}
    if customer:
        gateway_customer_id = customer.gateway_customer_id_get_or_create(gateway)
        customer_kwargs['customer'] = gateway_customer_id
    else:
        customer_kwargs['customer_creation'] = 'always'

    return stripe.checkout.Session.create(
        cancel_url=cancel_url,
        **customer_kwargs,
        line_items=[
            {
                'price_data': {
                    'currency': plan_variation.currency.lower(),
                    'product_data': {'name': plan_variation.plan.product_plan_name()},
                    'unit_amount': unit_amount and unit_amount or plan_variation.price.cents,
                },
                'quantity': 1,
            },
        ],
        mode='payment',
        payment_intent_data={
            'metadata': {'plan_variation_id': plan_variation.pk},
            'setup_future_usage': 'off_session',
        },
        payment_method_types=settings.STRIPE_OFF_SESSION_PAYMENT_METHOD_TYPES,
        submit_type=getattr(settings, 'STRIPE_CHECKOUT_SUBMIT_TYPE', 'auto'),
        success_url=success_url,
        ui_mode='hosted',
    )


def setup_stripe_payment_method(
    currency: str,
    customer: models.Customer,
    success_url: str,
    cancel_url: str,
    setup_intent_metadata: Dict,
) -> stripe.api_resources.checkout.Session:
    assert 'subscription_id' in setup_intent_metadata, (
        'missing setup_intent_metadata.subscription_id'
    )
    gateway = models.Gateway.objects.get(name='stripe')
    gateway_customer_id = customer.gateway_customer_id_get_or_create(gateway)
    return stripe.checkout.Session.create(
        cancel_url=cancel_url,
        currency=currency,
        customer=gateway_customer_id,
        mode='setup',
        payment_method_types=settings.STRIPE_OFF_SESSION_PAYMENT_METHOD_TYPES,
        setup_intent_data={
            'metadata': setup_intent_metadata,
        },
        success_url=success_url,
        ui_mode='hosted',
    )


def upsert_order_from_payment_intent_and_product(
    payload: str, product_id: int = None
) -> Tuple[Optional[str], models.Order]:
    """Create or mark as paid an Order based on a successful PaymentIntent.

    We currently assume the following:

        Stripe's PaymentIntent roughly maps to an Order
        Stripe's Charge roughly maps to a Transaction
        Stripe's PaymentMethod has the same meaning as looper's PaymentMethod

    Account for cases when the webhook may be called repeatedly with the same payload.

    1. Check if we already have an Order matching the payment_intent: lookup via payment_intent
       metadata.order_id
    2. If we don't, we assume that this is a donation, so we backfill an Order and a Transaction,
       using product_id and the details from the payload.
       We also create a Customer and an Address, unless we had a corresponding Customer.
    3. If we have a matching Order:
        3.0. If the Order is marked as paid or fulfilled, do nothing.
             If we can't transition to paid, log an exception.
             Otherwise proceed.
        3.1. If the Order has a Subscription:
             If we don't have a GatewayOrderId matching the payment_intent, create it.
             If we don't have a PaymentMethod matching the payment_method, create it.
             Update Order with the PaymentMethod.
             Update Subscription with the PaymentMethod, unless payment_intent metadata has a flag
             `extend_subscription`.
             See if we have a Transaction matching the latest_charge, generate one if we don't.
             If the transaction is not yet marked as paid, mark it as paid.
        3.2. If there is no Subscription, then the webhook was called twice for the same donation,
             but the order is still unpaid, this should not be possible.
             Log an error and return.
    """
    gateway = models.Gateway.objects.get(name='stripe')
    event = gateway.provider.construct_event(payload)

    payment_intent = gateway.provider.retrieve_payment_intent(event.data.object.id)
    order_id = payment_intent.metadata.get('order_id', None)
    logger.info('payment_intent metadata references order pk=%s', order_id)
    order = models.Order.objects.filter(pk=order_id).first()

    if order:
        logger.info('Found an order pk=%s with status %s', order_id, order.status)
        if order.status in {'paid', 'fulfilled'}:
            logger.info('Nothing to do: order pk=%s already has status=%s', order_id, order.status)
            return (order.status, order)
        elif not order.may_transition_to('paid'):
            logger.error(
                'Can\'t transition order pk=%s to paid: has status=%s', order_id, order.status
            )
            return (order.status, order)

        if order.subscription:
            old_order_status = order.status
            logger.info('Processing as a subsription order pk=%s', order_id)
            trans = process_payment_intent_for_subscription_order(payment_intent, order)
            return (old_order_status, trans.order)
        elif order.product:
            logger.error(
                'Treating as an existing unpaid order pk=%s for product %s: this should not happen',
                order.pk,
                product_id,
            )
            return (order.status, order)
        else:
            logger.error(
                'Order pk=%s is missing both subscription and product: this should not happen',
                order.pk
            )
            return (None, None)

    else:
        assert product_id, 'Expected product_id to be passed if no existing order found'
        logger.info('Processing as a new order for product %s', product_id)
        trans = process_payment_intent_for_product(payment_intent, product_id)
        return (None, trans.order)


def create_active_subscription_from_payment_intent(payload, plan_variation_id):
    """Check if we already have an Order linked to a given payment_intent, create one if not."""
    gateway = models.Gateway.objects.get(name='stripe')
    event = gateway.provider.construct_event(payload)
    payment_intent = gateway.provider.retrieve_payment_intent(event.data.object.id)
    payment_intent_id = payment_intent.id

    plan_variation = models.PlanVariation.objects.get(pk=plan_variation_id)
    gateway_order_id = models.GatewayOrderId.objects.filter(
        gateway=gateway,
        gateway_order_id=payment_intent_id,
    ).first()
    if gateway_order_id:
        order = gateway_order_id.order
        logger.info(
            'Found existing Order pk=%s for payment_intent id=%s, plan_variation_id=%s, skipping',
            order.pk,
            payment_intent_id,
            plan_variation_id,
        )
        return
    payment_intent = gateway.provider.retrieve_payment_intent(payment_intent_id)
    process_payment_intent_for_plan_variation(
        payment_intent, plan_variation
    )


def process_payment_intent_for_plan_variation(
    payment_intent, plan_variation, customer_ip_address=None
) -> models.Transaction:
    """Backfills db records before calling process_payment_intent_for_subscription_order.

    Creates a Subscription, an Order, looks up a GatewayCustomerId record by
    payment_intent.customer, if no corresponding record is found, a new GatewayCustomerId,
    Customer, LinkCustomerToken records are created.
    """
    gateway = models.Gateway.objects.get(name='stripe')

    with transaction.atomic():
        gateway_customer_id = models.GatewayCustomerId.objects.filter(
            gateway=gateway,
            gateway_customer_id=payment_intent.customer.id,
        ).first()
        if gateway_customer_id:
            customer = gateway_customer_id.customer
        else:
            customer = models.Customer()
            customer.save()
            customer.billing_address.save()
            customer.gatewaycustomerid_set.create(
                gateway=gateway,
                gateway_customer_id=payment_intent.customer.id,
            )
            # A token is required to get access to this newly created Customer record.
            # We assume that whoever creates a Customer record makes sure that it is "reachable",
            # either via a User or via a LinkCustomerToken.
            models.LinkCustomerToken.objects.create(customer=customer)

        subscription = models.Subscription(
            plan=plan_variation.plan,
            customer=customer,
            price=plan_variation.price,
            currency=plan_variation.currency,
            interval_unit=plan_variation.interval_unit,
            interval_length=plan_variation.interval_length,
            collection_method=plan_variation.collection_method,
        )
        subscription.update_tax(save=False)
        subscription.save()
        order = subscription.generate_order(save=False)
        order.price = payment_intent.amount
        if subscription.price != order.price:
            logger_args = [subscription.price, order.price]
            # There might a legitimate reason for this: reverse-charge VAT
            logger.warning("Subscription price '%s' doesn't match its order '%s'", *logger_args)
        order.currency = payment_intent.currency.upper()
        # There's no reason for this to happen at all, however:
        assert subscription.currency == order.currency, (
            "Currency of subscription '{}' doesn't match currency of its order '{}'".format(
                subscription.currency, order.currency
            )
        )
        order.save()
        return process_payment_intent_for_subscription_order(
            payment_intent,
            order,
            customer_ip_address,
        )


def process_payment_intent_for_subscription_order(
    payment_intent, order, customer_ip_address=None
) -> models.Transaction:
    """Creates a paid Transaction for a given Order.

    Uses payment_intent.latest_charge and payment_intent.payment_method, both structures must be
    expanded, as returned by StripeGateway.retrieve_payment_intent or
    StripeGateway.retrieve_checkout_session_expanded.
    """
    gateway = models.Gateway.objects.get(name='stripe')

    subscription = order.subscription
    assert subscription is not None

    with transaction.atomic():
        billing_address = order.customer.billing_address
        if not billing_address.email:
            billing_address.email = payment_intent.customer.email
            billing_address.save(update_fields={'email'})
            order.billing_address = billing_address.as_text()
            order.email = payment_intent.customer.email
            order.save(update_fields={'billing_address', 'email'})
        models.GatewayOrderId.objects.get_or_create(
            order=order,
            gateway=gateway,
            gateway_order_id=payment_intent.id,
        )
        payment_method, is_new = models.PaymentMethod.objects.get_or_create(
            customer=order.customer,
            gateway=gateway,
            token=payment_intent.payment_method.id,
        )
        # TODO deduplicate
        if is_new:
            pm_info = gateway.provider.find_customer_payment_method(
                payment_intent.payment_method.id
            )
            payment_method.method_type = pm_info.type_for_database()
            payment_method.recognisable_name = (pm_info.recognisable_name()
                                                or payment_method.get_method_type_display())
            payment_method.save()
            logger.info('Created a new PaymentMethod pk=%s', payment_method.pk)
        else:
            logger.info('Using existing PaymentMethod pk=%s', payment_method.pk)
        order.payment_method = payment_method
        order.save(update_fields={'payment_method'})

        is_recurring = True
        if 'extend_subscription' in payment_intent.metadata:
            is_recurring = False
            months_bought = order.price / subscription.price_per_month()
            assert isinstance(months_bought, float)
            from_timestamp = subscription.next_payment or django.utils.timezone.now()
            subscription.extend_subscription(
                from_timestamp=from_timestamp,
                months=months_bought,
            )
        else:
            subscription.payment_method = payment_method
            subscription.save(update_fields={'payment_method'})

        trans = models.Transaction.objects.filter(
            order=order, transaction_id=payment_intent.latest_charge.id
        ).first()
        if trans is None:
            trans = order.generate_transaction(
                ip_address=customer_ip_address,
                is_recurring=is_recurring,
            )
            logger.info('Generated a new Transaction pk=%s', trans.pk)
        if not trans.paid:
            trans.mark_as_paid(payment_intent.latest_charge.id)
            logger.info('Marked Transaction pk=%s as paid', trans.pk)
    logger.info('Finished processing payment_intent id=%s order pk=%s', payment_intent.id, order.pk)
    return trans


def process_payment_intent_for_product(payment_intent, product_id):
    gateway = models.Gateway.objects.get(name='stripe')

    if payment_intent.customer:
        logger.error(
            'Payment intent %s for product %s should not have a customer, proceeding anyway',
            payment_intent.id, product_id
        )

    with transaction.atomic():
        customer = models.Customer.objects.create()
        billing_address = customer.billing_address
        charge = payment_intent.latest_charge
        _update_billing_address_from_charge(billing_address, charge)
        # The only way we can save full name which is needed in the later email, is to save it here.
        billing_address.save()
        # Issue a LinkCustomerToken for a newly created customer
        models.LinkCustomerToken.objects.create(customer=customer)

        # Most of payment method details are only returned with Charge.payment_method_details,
        # so we use Charge.payment_method_details in order to be able to have a recognisable_name.
        pm_info = gateways.PaymentMethodInfo.from_stripe(
            payment_intent.payment_method.id, charge.payment_method_details
        )
        pm, is_new = models.PaymentMethod.objects.get_or_create(
            token=pm_info.token,
            gateway=gateway,
            defaults={'customer': customer},
        )
        # TODO deduplicate
        if is_new:
            pm.method_type = pm_info.type_for_database()
            pm.recognisable_name = pm_info.recognisable_name() or pm.get_method_type_display()
            pm.save()
            logger.info('Created a new PaymentMethod pk=%s', pm.pk)

        currency = payment_intent.currency.upper()
        # Hardcoding ID of the one-time donation product because we don't currently
        # have another way of associating it with Stripe events.
        product = models.Product.objects.get(pk=product_id)
        order = models.Order(
            product=product,
            name=product.name,
            collection_method='manual',
            customer=customer,
            currency=currency,
            price=charge.amount,
            email=charge.billing_details.email,
        )

        order.payment_method = pm
        order.billing_address = billing_address.as_text()
        order.save()

        trans = order.generate_transaction()
        trans.mark_as_paid(payment_intent.latest_charge.id)
        logger.info('Generated a new Transaction pk=%s and marked it as paid', trans.pk)

        # Make sure to store Stripe's payment_intent.id
        models.GatewayOrderId.objects.create(
            gateway=gateway,
            gateway_order_id=payment_intent.id,
            order=order,
        )
    return trans


def _update_billing_address_from_charge(billing_address, charge):
    _from_to = (
        ('city', 'locality'),
        ('country', 'country'),
        ('state', 'region'),
        ('line1', 'street_address'),
        ('line2', 'extended_address'),
        ('postal_code', 'postal_code'),
    )
    billing_address.email = charge.billing_details.email
    # FIXME: Alipay doesn't appear to be retuning any names, at least during tests
    billing_address.full_name = charge.billing_details.name or billing_address.email
    for _from, _to in _from_to:
        new_value = getattr(charge.billing_details.address, _from, None)
        if new_value:
            setattr(billing_address, _to, new_value)


def upsert_subscription_payment_method_from_setup_intent(payload: str):
    gateway = models.Gateway.objects.get(name='stripe')
    event = gateway.provider.construct_event(payload)

    setup_intent = stripe.SetupIntent.retrieve(
        event.data.object.id, expand=['payment_method']
    )
    subscription = models.Subscription.objects.get(pk=setup_intent.metadata['subscription_id'])
    process_setup_intent_for_subscription(setup_intent, subscription)


def process_setup_intent_for_subscription(setup_intent, subscription):
    gateway = models.Gateway.objects.get(name='stripe')

    with transaction.atomic():
        payment_method, is_new = models.PaymentMethod.objects.get_or_create(
            customer=subscription.customer,
            gateway=gateway,
            token=setup_intent.payment_method.id,
        )
        # TODO deduplicate
        if is_new:
            pm_info = gateways.PaymentMethodInfo.from_stripe(
                setup_intent.payment_method.id,
                setup_intent.payment_method
            )
            payment_method.method_type = pm_info.type_for_database()
            payment_method.recognisable_name = (pm_info.recognisable_name()
                                                or payment_method.get_method_type_display())
            payment_method.save()
            logger.info('Created a new PaymentMethod pk=%s', payment_method.pk)
        if subscription.payment_method != payment_method:
            subscription.payment_method = payment_method
            subscription.save(update_fields={'payment_method'})
            logger.info(
                'Setting PaymentMethod pk=%s for Subscription pk=%s',
                payment_method.pk, subscription.pk
            )
        else:
            logger.info(
                'PaymentMethod pk=%s is already set for Subscription pk=%s, nothing to do',
                payment_method.pk, subscription.pk
            )
