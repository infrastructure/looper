from django.apps import AppConfig


class LooperConfig(AppConfig):
    name = 'looper'

    def ready(self) -> None:
        import looper.signals  # noqa
