from typing import Any, Callable, TypeVar, cast
import functools

import looper.exceptions

F = TypeVar('F', bound=Callable[..., Any])


def short_description(description: str) -> Callable[[F], F]:
    """Set wrapped.short_description to 'description'.

    This makes it possible to stick to PEP8 (two newlines before/after
    function) and still keep the short description next to the function
    itself.
    """

    def decorator(wrapped: F) -> F:
        setattr(wrapped, 'short_description', description)
        return wrapped

    return decorator


def requires_status(*required_statuses: str) -> Callable[[F], F]:
    """Function decorator to assert a model has a certain status.

    :raise looper.exceptions.IncorrectStatusError:
    """
    _required_statuses = set(required_statuses)

    def decorator(wrapped: F) -> F:
        @functools.wraps(wrapped)
        def checker(self: Any, *args: Any, **kwargs: Any) -> Any:
            if self.status not in _required_statuses:
                raise looper.exceptions.IncorrectStatusError(
                    f'To call {self.__class__}.{wrapped}, {self} needs to have '
                    f'status in {_required_statuses} but it has {self.status!r}',
                    self.status,
                    _required_statuses,
                )
            return wrapped(self, *args, **kwargs)

        return cast(F, checker)

    return decorator
