#!/usr/bin/env sh
set -eux

./manage.py test --parallel
PYTHONPATH=. mypy --show-error-codes .
black --check .
flake8 --select ISC001 looper looper_example_project
